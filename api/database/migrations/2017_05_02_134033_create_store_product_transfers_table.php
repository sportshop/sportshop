<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreProductTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_product_transfers', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('from_store_id')->unsigned()->nullable();
            $table->smallInteger('to_store_id')->unsigned();
            $table->integer('product_size_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();
            $table->smallInteger('quantity')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('from_store_id')->references('id')->on('stores')
                ->onUpdate('cascade')->onDelete('set null');
            $table->foreign('to_store_id')->references('id')->on('stores')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('product_size_id')->references('id')->on('product_sizes')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_product_transfers');
    }
}
