<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_groups', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('name');
        });

        Schema::create('attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('group_id')->unsigned()->nullable();
            $table->char('type', 16);
            $table->string('name');

            $table->index('type');
            $table->index('name');

            $table->foreign('group_id')->references('id')->on('attribute_groups')
                ->onUpdate('cascade')->onDelete('set null');
        });

        Schema::create('x_attribute_value', function (Blueprint $table) {
            $table->integer('attribute_id')->unsigned();
            $table->morphs('entity');
            $table->text('value');

            $table->foreign('attribute_id')->references('id')->on('attributes')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('x_attribute_value');
        Schema::dropIfExists('attributes');
        Schema::dropIfExists('attribute_groups');
    }
}
