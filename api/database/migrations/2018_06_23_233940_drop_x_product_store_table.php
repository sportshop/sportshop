<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropXProductStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @throws Throwable
     */
    public function up()
    {
        Schema::dropIfExists('x_product_store');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('x_product_store', function (Blueprint $table) {
            $table->integer('product_id')->unsigned();
            $table->smallInteger('store_id')->unsigned();
            $table->smallInteger('discount_id')->unsigned()->nullable();
            $table->smallInteger('currency_id')->unsigned()->nullable();
            $table->char('discount_type', 16);
            $table->decimal('discount_value', 5, 2);
            $table->decimal('price', 11, 2);

            $table->index('price');

            $table->foreign('product_id')->references('id')->on('products')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('store_id')->references('id')->on('stores')
                ->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('discount_id')->references('id')->on('discounts')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('currency_id')->references('id')->on('currencies')
                ->onUpdate('cascade')->onDelete('restrict');
        });
    }
}
