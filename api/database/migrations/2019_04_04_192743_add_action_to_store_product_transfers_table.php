<?php

use App\Models\StoreProductTransfer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActionToStoreProductTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store_product_transfers', function (Blueprint $table) {
            $table->char('action', 10)->after('user_id');
        });

        $this->updateExistsRecords();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_product_transfers', function (Blueprint $table) {
            $table->dropColumn('action');
        });
    }

    /**
     * Fill column action to existed records.
     */
    private function updateExistsRecords()
    {
        $table = DB::table('store_product_transfers');

        $table->whereNull('from_store_id')->update(['action' => StoreProductTransfer::ACTION_ADD]);
        $table->whereNotNull('from_store_id')->update(['action' => StoreProductTransfer::ACTION_MOVE]);
    }
}
