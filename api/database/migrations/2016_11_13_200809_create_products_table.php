<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('sku');
            $table->string('name');
            $table->text('description')->nullable();
            $table->char('status', 16);
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();

            $table->smallInteger('sort_order')->unsigned()->default(0);
            $table->boolean('is_active');
            $table->timestamps();
            $table->softDeletes();

            $table->index('slug');
            $table->index('sku');
            $table->index('status');
            $table->index('is_active');
            $table->index('deleted_at');
            $table->unique('sku');
            $table->unique('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
