<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsShowSizesToUserDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_data', function (Blueprint $table) {
            $table->boolean('is_show_sizes')->default(false);
        });

        $this->updateExistsRows();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_data', function (Blueprint $table) {
            $table->dropColumn('is_show_sizes');
        });
    }

    /**
     * Set "is_show_sizes" column to all users besides member.
     */
    private function updateExistsRows()
    {
        $rows = DB::table('user_data')
            ->join('users', 'user_data.user_id', '=', 'users.id')
            ->join('x_role_user', 'x_role_user.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'x_role_user.role_id')
            ->whereNotIn('name', ['member'])
            ->get();

        $rows->each(function ($item) {
            DB::table('user_data')
                ->where('user_id', $item->user_id)
                ->update(['is_show_sizes' => true]);
        });
    }
}
