<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('payment_method');

            $table->dropForeign(['user_id']);
            $table->renameColumn('user_id', 'manager_id');
            $table->foreign('manager_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->renameColumn('customer', 'customer_data');
            $table->renameColumn('comment', 'notes');
            $table->renameColumn('is_from_web', 'is_from_web_store');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('is_from_web_store', 'is_from_web');
            $table->renameColumn('notes', 'comment');
            $table->renameColumn('customer_data', 'customer');

            $table->dropForeign(['manager_id']);
            $table->renameColumn('manager_id', 'user_id');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->char('payment_method', 16);
        });
    }
}
