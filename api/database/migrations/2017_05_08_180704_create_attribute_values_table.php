<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attribute_id')->unsigned();
            $table->string('value');

            $table->index('value');

            $table->foreign('attribute_id')->references('id')->on('attributes')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('x_attribute_value_entity', function (Blueprint $table) {
            $table->integer('value_id')->unsigned();
            $table->morphs('entity');

            $table->primary(['value_id', 'entity_id', 'entity_type']);

            $table->foreign('value_id')->references('id')->on('attribute_values')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('x_attribute_value_entity');
        Schema::dropIfExists('attribute_values');
    }
}
