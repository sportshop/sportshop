<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductStoreDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_store_discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_store_id')->unsigned();
            $table->smallInteger('discount_id')->unsigned();
            $table->decimal('user', 5, 2);
            $table->decimal('sale', 5, 2)->nullable();

            $table->foreign('product_store_id')->references('id')->on('product_store_data')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('discount_id')->references('id')->on('discounts')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_store_discounts');
    }
}
