<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');

            NestedSet::columns($table);

            $table->string('slug');
            $table->string('name');
            $table->text('description');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();

            $table->integer('sort_order')->unsigned()->default(0);
            $table->boolean('is_active');
            $table->timestamps();
            $table->softDeletes();

            $table->index('slug');
            $table->index('is_active');
            $table->index('deleted_at');
            $table->unique('slug');

            $table->foreign('parent_id')->references('id')->on('categories')
                ->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
