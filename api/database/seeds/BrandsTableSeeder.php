<?php

use App\Services\MediaManager;
use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('brands')->truncate();

        Artisan::call('media:purge', ['disk' => 'public']);

        $brands = json_decode(File::get('storage/app/stubs/brands.json'));

        foreach ($brands as $brand) {
            $media = MediaManager::upload(storage_path('app/stubs/brands-logo/' . $brand->thumbnail), 'image');
            $thumbnail = MediaManager::thumbnail($media, 'image', 'fit', config('image.size.brand.medium'), true);
            $model = factory(App\Models\Brand::class)->create(['name' => $brand->name]);
            $model->attachMedia($thumbnail->id, \App\Models\Brand::MEDIA_THUMBNAIL);
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
