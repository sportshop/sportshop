<?php

use Illuminate\Database\Seeder;

class OrderProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('order_products')->truncate();

        factory(App\Models\OrderProduct::class, 3)->create();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
