<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('x_product_category')->truncate();
        DB::table('categories')->truncate();

        //Artisan::call('media:purge');

        $categories = json_decode(\Illuminate\Support\Facades\File::get('storage/app/stubs/categories.json'));

        // Insert the main categories
        foreach ($categories as $category) {
            $categoryModel = factory(App\Models\Category::class)->create([
                'name'      => $category->name,
                'icon'      => $category->icon,
                'is_active' => true
            ]);

            foreach ($category->children as $node) {
                $nodeModel = factory(App\Models\Category::class)->create([
                    'name'      => $node->name,
                    'icon'      => $node->icon,
                    'is_active' => true
                ]);
                $nodeModel->appendToNode($categoryModel)->save();
            }
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
