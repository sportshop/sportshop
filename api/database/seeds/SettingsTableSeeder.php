<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->truncate();

        $data = [
            [
                'name'         => 'system_email',
                'display_name' => 'System Email',
                'value'        => 'noreply@sportshop.com'
            ],
            [
                'name'         => 'support_email',
                'display_name' => 'Support Email',
                'value'        => 'support@sportshop.com'
            ],
            [
                'name'         => 'social_facebook',
                'display_name' => 'Facebook url',
                'value'        => 'http://facebook.com'
            ],
            [
                'name'         => 'social_instagram',
                'display_name' => 'Instagram url',
                'value'        => 'http://instagram.com'
            ],
            [
                'name'         => 'social_google',
                'display_name' => 'Google plus url',
                'value'        => 'http://google.com'
            ],
            [
                'name'         => 'phones',
                'display_name' => 'Phones',
                'value'        => '+38 (096) 666-66-66, +38 (097) 777-77-77'
            ],
            [
                'name'         => 'markup',
                'display_name' => 'Націнка',
                'value'        => 5
            ],
            [
                'name'         => 'discount_sale',
                'display_name' => 'Sale знижка',
                'value'        => 5
            ],
            [
                'name'         => 'discount_max_sale',
                'display_name' => 'Max sale знижка',
                'value'        => 5
            ],
            [
                'name'         => 'feature_categories',
                'display_name' => 'Категорії (головна ст.)',
                'value'        => ''
            ],
        ];

        foreach ($data as $item) {
            factory(App\Models\Settings::class)->create($item);
        }
    }
}
