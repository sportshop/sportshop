<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET foreign_key_checks=0;');

        DB::table('x_role_user')->delete();
        DB::table('x_permission_role')->delete();
        DB::table('permissions')->truncate();
        DB::table('roles')->truncate();
        DB::table('users')->truncate();

        DB::statement('SET foreign_key_checks=1;');

        // Seed the roles
        $roles = config('laravel-permission.roles');
        foreach ($roles as $role) {
            factory(App\Models\Role::class)->create($role);
        }

        // Seed the permissions
        $permissions = config('laravel-permission.permissions');
        foreach ($permissions as $perm) {
            factory(App\Models\Permission::class)->create(['name' => $perm]);
        }

        // Attach permissions to roles
        $relations = config('laravel-permission.roles_permissions');
        foreach ($relations as $role => $permissions) {
            $roleModel = App\Models\Role::findByName($role);
            foreach ($permissions as $perm) {
                $permModel = App\Models\Permission::where('name', $perm)->first();
                $roleModel->permissions()->attach($permModel->id);
            }
        }

        // Seed the admin users
        $user = factory(App\Models\User::class)->create([
            'email'              => 'admin@sportshop.com',
            'password'           => 'admin',
            'first_name'         => 'Admin',
            'last_name'          => 'Admin',
            'nickname'           => null,
            'phone'              => null,
            'address'            => null,
            'is_active'          => true,
            'remember_token'     => null,
            'confirmation_token' => null,
        ]);
        $user->assignRole('admin');
    }
}
