<?php

use App\Services\MediaManager;
use Illuminate\Database\Seeder;

class SizeTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('size_values')->truncate();
        DB::table('sizes')->truncate();
        DB::table('size_groups')->truncate();

        $types = json_decode(File::get('storage/app/stubs/sizes.json'));

        foreach ($types as $type => $groups) {
            foreach ($groups as $group) {

                // Find brand by name.
                $brand = \App\Models\Brand::where('name', $group->brand)->first();

                if (!$brand) {
                    continue;
                }

                // Create size group model and attach brand.
                $groupModel = factory(App\Models\SizeGroup::class)->create([
                    'brand_id' => $brand->id,
                    'type'     => $type,
                    'name'     => $group->name
                ]);

                // Create sizes.
                foreach ($group->sizes as $size) {
                    $sizeModel = $groupModel->sizes()->save(
                        factory(App\Models\Size::class)->create(['group_id' => $groupModel->id])
                    );

                    // Create size values.
                    foreach ($size as $value) {
                        $sizeModel->values()->save(
                            factory(App\Models\SizeValue::class)->create([
                                'size_id'    => $sizeModel->id,
                                'title'      => $value->title,
                                'value'      => $value->value,
                                'is_default' => ($value->title == 'US' || $value->title == 'Ріст') ? true : false
                            ])
                        );
                    }
                }
            }
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
