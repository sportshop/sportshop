<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class StoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('stores')->truncate();

        factory(App\Models\Store::class)->create([
            'name'         => 'Магазин Юри',
            'description'  => 'Магазин Юри - опис...',
            'address'      => 'Тест адреса 1',
            'website_name' => 'Магазин Юри',
            'url'          => 'http://sportshop.com',
            'is_active'    => true
        ]);

        factory(App\Models\Store::class)->create([
            'name'         => 'Магазин Андрія',
            'description'  => 'Магазин Андрія - опис...',
            'address'      => 'Тест адреса 2',
            'website_name' => 'Магазин Андрія',
            'url'          => 'http://sportshop.dev',
            'is_active'    => true
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
