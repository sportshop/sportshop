<?php

use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('currencies')->truncate();

        $currencies = [
            ['code' => 'uah', 'value' => 1,   'name' => 'Гривня', 'symbol' => '&#8372;', 'is_default' => true],
            ['code' => 'usd', 'value' => 25,  'name' => 'Доллар', 'symbol' => '$', 'is_default' => false],
            ['code' => 'eur', 'value' => 30,  'name' => 'Євро',   'symbol' => '&euro;', 'is_default' => false],
            ['code' => 'pln', 'value' => 6.5, 'name' => 'Злотий', 'symbol' => 'zł', 'is_default' => false],
        ];

        foreach ($currencies as $currency) {
            factory(App\Models\Currency::class)->create($currency);
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
