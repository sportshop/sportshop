<?php

use Illuminate\Database\Seeder;

class BannersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('banners')->truncate();

        // Insert the banners
        factory(App\Models\Banner::class, 5)->create();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
