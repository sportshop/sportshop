<?php

use Illuminate\Database\Seeder;

class DiscountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('discounts')->truncate();

        $discounts = [
            ['title' => 'A', 'value' => 5,  'sale_value' => 2, 'is_default' => false],
            ['title' => 'B', 'value' => 10, 'sale_value' => 4, 'is_default' => false],
            ['title' => 'C', 'value' => 12, 'sale_value' => 6, 'is_default' => false],
            ['title' => 'D', 'value' => 15, 'sale_value' => 8, 'is_default' => false],
            ['title' => 'E', 'value' => 20, 'sale_value' => 10, 'is_default' => true]
        ];

        foreach ($discounts as $discount) {
            factory(App\Models\Discount::class)->create($discount);
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
