<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->truncate();

        $data = [
            [
                'slug'      => 'about',
                'title'     => 'Про нас',
                'is_active' => true,
                'is_system' => true
            ],
            [
                'slug'      => 'payment-and-delivery',
                'title'     => 'Оплата і доставка',
                'is_active' => true,
                'is_system' => true
            ]
        ];

        foreach ($data as $item) {
            factory(App\Models\Page::class)->create($item);
        }
    }
}
