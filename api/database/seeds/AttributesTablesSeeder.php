<?php

use Illuminate\Database\Seeder;

class AttributesTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('attributes')->truncate();
        DB::table('attribute_groups')->truncate();

        factory(App\Models\AttributeGroup::class, 3)
            ->create()
            ->each(function ($group) {
                $group->attributes()->saveMany(
                    factory(App\Models\Attribute::class, 5)->create([
                        'group_id' => $group->id
                    ])
                );
            });

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
