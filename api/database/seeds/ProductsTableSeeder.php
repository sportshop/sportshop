<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('products')->truncate();

        // Insert the products
        factory(App\Models\Product::class, 16)->create();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
