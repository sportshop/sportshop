<?php

$factory->define(App\Models\Category::class, function (Faker\Generator $faker) {
    return [
        'slug'             => $faker->slug,
        'name'             => ucfirst($faker->word),
        'description'      => $faker->sentences(3, true),
        'meta_title'       => $faker->sentences(3, true),
        'meta_description' => $faker->sentences(3, true),
        'sort_order'       => $faker->numberBetween(0, 200),
        'is_active'        => $faker->boolean
    ];
});
