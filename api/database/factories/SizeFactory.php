<?php

$factory->define(App\Models\Size::class, function () {
    return [
        'group_id' => function () {
            return factory(App\Models\SizeGroup::class)->create()->id;
        }
    ];
});
