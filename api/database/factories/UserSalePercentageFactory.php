<?php

$factory->define(App\Models\UserSalePercentage::class, function (Faker\Generator $faker) {
    $user = App\Models\User::inRandomOrder()->first() ?? factory(App\Models\User::class)->create();

    return [
        'user_id'     => $user->id,
        'value'       => $faker->numberBetween(1, 15)
    ];
});

$factory->state(App\Models\UserSalePercentage::class, 'with_discount', function () {
    $discount = App\Models\Discount::inRandomOrder()->first() ?? factory(App\Models\Discount::class)->create();

    return [
        'discount_id' => $discount->id,
    ];
});
