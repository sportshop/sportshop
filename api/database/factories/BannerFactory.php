<?php

$factory->define(App\Models\Banner::class, function (Faker\Generator $faker) {
    return [
        'data'       => [
            'title' => ucfirst($faker->word)
        ],
        'sort_order' => $faker->numberBetween(0, 200),
        'is_active'  => $faker->boolean
    ];
});
