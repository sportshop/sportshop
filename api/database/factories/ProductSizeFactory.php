<?php

$factory->define(App\Models\ProductSize::class, function (Faker\Generator $faker) {
    return [
        'product_id' => function () {
            return factory(App\Models\Product::class)->create()->id;
        },
        'size_id'    => function () {
            return factory(App\Models\Size::class)->create()->id;
        },
        'store_id'   => function () {
            return factory(App\Models\Store::class)->create()->id;
        },
        'quantity'   => $faker->randomNumber()
    ];
});
