<?php

$factory->define(App\Models\Attribute::class, function (Faker\Generator $faker) {
    return [
        'group_id' => function () {
            return factory(App\Models\AttributeGroup::class)->create()->id;
        },
        'type'     => $faker->word,
        'name'     => $faker->word
    ];
});
