<?php

$factory->define(App\Models\ProductSupply::class, function (Faker\Generator $faker) {
    return [
        'product_size_id' => function () {
            return factory(App\Models\ProductSize::class)->create()->id;
        },
        'user_id'         => function () {
            if ($user = App\Models\User::inRandomOrder()->first()) {
                return $user->id;
            }
            return factory(App\Models\User::class)->create()->id;
        },
        'quantity'        => $faker->numberBetween(1, 100)
    ];
});
