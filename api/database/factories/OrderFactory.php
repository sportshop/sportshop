<?php

$factory->define(App\Models\Order::class, function (Faker\Generator $faker) {
    return [
        'store_id'       => function () {
            return factory(App\Models\Store::class)->create()->id;
        },
        'user_id'        => function () {
            return factory(App\Models\User::class)->create()->id;
        },
        'customer_id'    => function () {
            return factory(App\Models\User::class)->create()->id;
        },
        'customer'       => [
            'first_name' => $faker->firstName,
            'last_name'  => $faker->lastName,
            'email'      => $faker->email,
            'phone'      => $faker->phoneNumber,
            'address'    => $faker->address
        ],
        'status'         => $faker->randomElement(App\Models\Order::statuses()),
        'payment_method' => $faker->randomElement(App\Models\Order::paymentMethods()),
        'delivery'       => [
            'method'      => $faker->randomElement(App\Models\Order::deliveryMethods()),
            'address'     => $faker->address,
            'declaration' => $faker->randomNumber(11)
        ],
        'comment'        => $faker->sentence,
        'is_from_web'    => $faker->boolean
    ];
});
