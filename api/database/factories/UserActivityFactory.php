<?php

$factory->define(App\Models\UserActivity::class, function (Faker\Generator $faker) {
    $user = App\Models\User::inRandomOrder()->first() ?? factory(App\Models\User::class)->create();

    return [
        'user_id'     => $user->id,
        'created_at'  => $faker->dateTimeBetween('-19 hours', '-10 hours'),
        'finished_at' => $faker->dateTimeBetween('-10 hours', '-1 hours'),
    ];
});
