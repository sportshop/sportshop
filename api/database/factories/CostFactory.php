<?php

$factory->define(App\Models\Cost::class, function (Faker\Generator $faker) {
    return [
        'store_id'    => function () {
            if ($cost = App\Models\Store::inRandomOrder()->first()) {
                return $cost->id;
            }

            return factory(App\Models\Store::class)->create()->id;
        },
        'type'        => function () use ($faker) {
            return $faker->randomElement(App\Models\Cost::types());
        },
        'description' => $faker->sentences(3, true),
        'created_at'  => $faker->dateTimeBetween('-2 years')
    ];
});
