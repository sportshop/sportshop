<?php

$factory->define(App\Models\SizeGroup::class, function (Faker\Generator $faker) {
    return [
        'brand_id' => function () {
            return factory(App\Models\Brand::class)->create()->id;
        },
        'name'     => ucfirst($faker->word)
    ];
});
