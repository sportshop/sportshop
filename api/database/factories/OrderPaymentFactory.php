<?php

$factory->define(App\Models\OrderPayment::class, function (Faker\Generator $faker) {
    return [
        'order_id'     => function () {
            return factory(App\Models\Order::class)->create()->id;
        },
        'withdrawn_at' => $faker->dateTimeBetween('-10 days')
    ];
});
