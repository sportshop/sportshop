<?php

$factory->define(App\Models\Product::class, function (Faker\Generator $faker) {
    return [
        'slug'                 => $faker->slug,
        'sku'                  => $faker->randomNumber(),
        'warehouse_number'     => $faker->randomNumber(),
        'name'                 => ucfirst($faker->word),
        'description'          => $faker->sentences(3, true),
        'status'               => $faker->randomElement(App\Models\Product::statuses()),
        'meta_title'           => $faker->sentence,
        'meta_description'     => $faker->sentences(3, true),
        'uniform_builder_data' => [
            'type'       => $faker->randomElement(App\Models\Product::uniformBuilderTypes()),
            'position'   => $faker->randomElement(App\Models\Product::uniformBuilderPositions()),
            'model'      => $faker->word,
            'is_visible' => $faker->boolean
        ],
        'sort_order'           => $faker->numberBetween(0, 200),
        'is_active'            => $faker->boolean
    ];
});
