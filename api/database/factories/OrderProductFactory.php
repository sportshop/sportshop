<?php

$factory->define(App\Models\OrderProduct::class, function (Faker\Generator $faker) {
    return [
        'order_id' => function () {
            return factory(App\Models\Order::class)->create()->id;
        },
        'size_id'  => function () {
            return factory(App\Models\ProductSize::class)->create()->id;
        },
        'quantity' => $faker->numberBetween(1, 10),
        'discount' => $faker->randomFloat(2, 0, 5)
    ];
});
