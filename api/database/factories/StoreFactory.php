<?php

$factory->define(App\Models\Store::class, function (Faker\Generator $faker) {
    return [
        'name'         => ucfirst($faker->word),
        'description'  => $faker->sentences(3, true),
        'address'      => $faker->address,
        'website_name' => $faker->words(3, true),
        'url'          => 'http://' . $faker->domainName,
        'is_active'    => $faker->boolean
    ];
});
