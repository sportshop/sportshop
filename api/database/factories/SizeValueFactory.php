<?php

$factory->define(App\Models\SizeValue::class, function (Faker\Generator $faker) {
    return [
        'size_id'    => function () {
            return factory(App\Models\Size::class)->create()->id;
        },
        'title'      => $faker->word,
        'value'      => $faker->randomNumber(2),
        'is_default' => $faker->boolean
    ];
});
