<?php

$factory->define(App\Models\UserData::class, function (Faker\Generator $faker) {
    $user = App\Models\User::inRandomOrder()->first() ?? factory(App\Models\User::class)->create();

    return [
        'user_id' => $user->id
    ];
});

$factory->state(App\Models\UserData::class, 'member', function () {
    $discount = App\Models\Discount::inRandomOrder()->first() ?? factory(App\Models\Discount::class)->create();

    return [
        'discount_id' => $discount->id
    ];
});

$factory->state(App\Models\UserData::class, 'manager', function (Faker\Generator $faker) {
    $rateTypes = \App\Models\UserData::rateTypes();

    return [
        'company_name' => $faker->company,
        'store_number' => $faker->numberBetween(10, 500),
        'rate_type'    => $faker->randomElement($rateTypes)
    ];
});
