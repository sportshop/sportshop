<?php

$factory->define(App\Models\Discount::class, function (Faker\Generator $faker) {
    return [
        'title'       => $faker->word,
        'value'       => $faker->randomFloat(0, 5, 15),
        'sale_value'  => $faker->randomFloat(0, 2, 10),
        'is_increase' => $faker->numberBetween(0, 1)
    ];
});
