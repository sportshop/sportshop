<?php

$factory->define(App\Models\Currency::class, function (Faker\Generator $faker) {
    $code = $faker->currencyCode;

    return [
        'code'       => $code,
        'value'      => $faker->randomFloat(2, 10, 30),
        'name'       => $code,
        'symbol'     => $code,
        'is_default' => $faker->boolean
    ];
});
