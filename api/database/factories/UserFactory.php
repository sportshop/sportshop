<?php

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'email'              => $faker->safeEmail,
        'password'           => bcrypt(str_random(10)),
        'first_name'         => $faker->firstName,
        'last_name'          => $faker->lastName,
        'nickname'           => $faker->userName,
        'phone'              => $faker->phoneNumber,
        'address'            => $faker->address,
        'is_active'          => $faker->boolean,
        'remember_token'     => str_random(10),
        'confirmation_token' => str_random(10)
    ];
});
