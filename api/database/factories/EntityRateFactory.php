<?php

$factory->define(App\Models\EntityRate::class, function (Faker\Generator $faker) {
    $currency = App\Models\Currency::inRandomOrder()->first() ?? factory(App\Models\Currency::class)->create();

    return [
        'currency_id' => $currency->id,
        'value'       => $faker->randomFloat(2, 100, 1000),
        'tag'         => $faker->word
    ];
});
