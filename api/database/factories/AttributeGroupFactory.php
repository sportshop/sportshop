<?php

$factory->define(App\Models\AttributeGroup::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence
    ];
});
