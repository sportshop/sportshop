<?php

$factory->define(App\Models\UserTransaction::class, function (Faker\Generator $faker) {
    $types = App\Models\UserTransaction::types();
    $user = App\Models\User::inRandomOrder()->first() ?? factory(App\Models\User::class)->create();

    return [
        'user_id'  => $user->id,
        'type'     => $faker->randomElement($types),
        'comment'  => $faker->sentence,
        'payed_at' => $faker->dateTimeThisMonth
    ];
});
