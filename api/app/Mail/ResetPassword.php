<?php

namespace App\Mail;

use App\Models\User;
use App\Repositories\Contracts\SettingsRepositoryContract;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPassword extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var \App\Models\User
     */
    protected $user;

    /**
     * @var string
     */
    protected $url;

    /**
     * @param \App\Models\User $user
     * @param string $url
     */
    public function __construct(User $user, string $url)
    {
        $this->user = $user;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @param SettingsRepositoryContract $settingsRepository
     *
     * @return $this
     */
    public function build(SettingsRepositoryContract $settingsRepository)
    {
        $systemEmail = $settingsRepository->findByName('system_email');

        return $this
            ->from($systemEmail)
            ->to($this->user)
            ->subject(trans('emails.password_link.subject'))
            ->view('emails.password_reset')
            ->with([
                'user' => $this->user,
                'url'  => $this->url
            ]);
    }
}
