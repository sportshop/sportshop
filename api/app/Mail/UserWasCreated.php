<?php

namespace App\Mail;

use App\Models\User;
use App\Services\SettingsService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserWasCreated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $url;

    /**
     * @param User   $user
     * @param string $password
     * @param string $url
     */
    public function __construct(User $user, string $password, string $url)
    {
        $this->user = $user;
        $this->password = $password;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @param SettingsService $settingsService
     * @return $this
     */
    public function build(SettingsService $settingsService)
    {
        $systemEmail = $settingsService->get('system_email');

        return $this
            ->from($systemEmail)
            ->to($this->user->email)
            ->subject(trans('emails.user_was_created.subject'))
            ->view('emails.user_was_created')
            ->with([
                'user'     => $this->user,
                'password' => $this->password,
                'url'      => $this->url
            ]);
    }
}
