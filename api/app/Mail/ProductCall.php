<?php

namespace App\Mail;

use App\Models\Product;
use App\Services\SettingsService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductCall extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $form_data;

    /**
     * @var Product
     */
    protected $product;

    /**
     * Create a new message instance.
     *
     * @param array $form_data
     * @param Product $product
     * @param string $url
     */
    public function __construct(array $form_data, Product $product, string $url)
    {
        $this->form_data = $form_data;

        $main_category = $product->categories->where('is_default', true)->first();

        $product = $product->toArray();
        $product['link'] = $url;
        $product['link'] .= '/products/' . $main_category->slug . '/' . $product['slug'];

        $this->product = $product;
    }

    /**
     * Build the message.
     *
     * @param SettingsService $settings
     *
     * @return $this
     */
    public function build(SettingsService $settings)
    {
        return $this
            ->from($settings->get('system_email'))
            ->to($settings->get('system_email'))
            ->subject(trans('emails.product_call.subject'))
            ->view('emails.product_call')
            ->with([
                'form_data' => $this->form_data,
                'product'   => $this->product,
            ]);
    }
}
