<?php

namespace App\Mail;

use App\Services\SettingsService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeedbackSend extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $data;

    /**
     * Create a new message instance.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $data['text'] = $data['message'];
        unset($data['message']);

        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @param SettingsService $settings
     *
     * @return $this
     */
    public function build(SettingsService $settings)
    {
        return $this
            ->from($this->data['email'], array_get($this->data, 'name'))
            ->to($settings->get('system_email'))
            ->subject(trans('emails.feedback.subject'))
            ->view('emails.feedback')
            ->with($this->data);
    }
}
