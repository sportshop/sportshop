<?php

namespace App\Mail\Order;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderCreatedToManagerMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var array
     */
    private $order;

    /**
     * @param array $order
     * @return void
     */
    public function __construct(array $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject(trans('emails.order_created_to_manager.subject', ['id' => $this->order['id']]))
            ->view('emails.order_created_to_manager')
            ->with(['order' => $this->order]);
    }
}
