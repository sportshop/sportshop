<?php

namespace App\Mail;

use App\Models\Product;
use App\Services\SettingsService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductFeedback extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $form_data;

    /**
     * @var Product
     */
    protected $product;

    /**
     * Create a new message instance.
     *
     * @param array $form_data
     * @param Product $product
     * @param string $url
     */
    public function __construct(array $form_data, Product $product, string $url)
    {
        $form_data['text'] = $form_data['message'];
        unset($form_data['message']);

        $this->form_data = $form_data;

        $main_category = $product->categories->where('is_default', true)->first();

        $product = $product->toArray();
        $product['link'] = $url;
        $product['link'] .= '/products/' . $main_category->slug . '/' . $product['slug'];

        $this->product = $product;
    }

    /**
     * Build the message.
     *
     * @param SettingsService $settings
     *
     * @return $this
     */
    public function build(SettingsService $settings)
    {
        return $this
            ->from($this->form_data['email'], array_get($this->form_data, 'name'))
            ->to($settings->get('system_email'))
            ->subject(trans('emails.product_feedback.subject'))
            ->view('emails.product_feedback')
            ->with([
                'form_data' => $this->form_data,
                'product'   => $this->product,
            ]);
    }
}
