<?php

namespace App\Jobs;

use App\Models\EntityRate;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DropDBUnrelatedRecords implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $records = EntityRate::all();

        foreach ($records as $record) {
            $class_name = $record->entity_type;

            $modelExists = $class_name::whereKey($record->entity_id)->exists();

            if (!$modelExists) {
                $record->delete();
            }
        }
    }
}
