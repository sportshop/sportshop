<?php

namespace App\Jobs;

use App\Models\UserActivity;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class FinishWorkdays implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $items = UserActivity::query()
            ->whereDate('created_at', '<' , Carbon::now()->toDateString())
            ->whereNull('finished_at')
            ->get();

        foreach ($items as $item) {
            $time = '20:00:00';

            if ($item->created_at->hour >= 20) {
                $time = '23:59:59';
            }

            $item->finished_at = $item->created_at->setTimeFromTimeString($time);
            $item->save();
        }
    }
}
