<?php

namespace App\Listeners\Order;

use App\Events\Order\OrderWasCreated;
use App\Mail\Order\OrderCreatedToCustomerMail;
use App\Mail\Order\OrderCreatedToManagerMail;
use App\Repositories\Contracts\UserRepositoryContract;
use App\Services\SettingsService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailsWhenOrderCreatedListener
{
    use InteractsWithQueue;

    /**
     * @var UserRepositoryContract
     */
    private $userRepository;

    /**
     * @var SettingsService
     */
    private $settings;

    /**
     * Create the event listener.
     *
     * @param UserRepositoryContract $userRepository
     * @param SettingsService $settings
     * @return void
     */
    public function __construct(
        UserRepositoryContract $userRepository,
        SettingsService $settings
    ) {
        $this->userRepository = $userRepository;
        $this->settings = $settings;
    }

    /**
     * Handle the event.
     *
     * @param OrderWasCreated $event
     * @return void
     */
    public function handle(OrderWasCreated $event)
    {
        $this->sendEmailToCustomer($event->order);

        $this->sendEmailToManagers($event->order);
    }

    /**
     * Find managers.
     * Roles: admin, manager, limited_manager.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function findManagers()
    {
        $criteria = ['role' => 'admin,manager,limited_manager'];

        return $this->userRepository->findAll($criteria);
    }

    /**
     * Send email to managers.
     *
     * @param array $order
     */
    private function sendEmailToManagers(array $order)
    {
        // Get recipients.
        $users = $this->findManagers();
        $users = $users->map(function ($user) {
            return [
                'name'  => $user->full_name,
                'email' => $user->email
            ];
        });

        Mail::to($users)->send(new OrderCreatedToManagerMail($order));
    }

    /**
     * Send email to customer.
     *
     * @param array $order
     */
    private function sendEmailToCustomer(array $order)
    {
        $email = $order['customer']['email'];
        $name = $order['customer']['first_name'] . ' ' . $order['customer']['last_name'];

        if (!$email) {
            return;
        }

        Mail::to($email, $name)->send(new OrderCreatedToCustomerMail($order));
    }
}
