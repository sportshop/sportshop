<?php

namespace App\Exceptions;

use Exception;

class NoRequestRefererException extends Exception {}
