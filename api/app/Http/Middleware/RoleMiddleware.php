<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @param  mixed                    $roles
     *
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        if (Auth::guest()) {
            return response()->jsonError('Unauthorized', 401);
        }

        if (!$request->user()->hasAnyRole($roles)) {
            return response()->jsonForbidden();
        }

        return $next($request);
    }
}
