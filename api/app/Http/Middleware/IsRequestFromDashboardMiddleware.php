<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;

class IsRequestFromDashboardMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $store_url = $request->headers->get('store-url');

        if (!$store_url) {
            $store_url = $request->headers->get('origin');
        }

        $store_url = preg_replace('/https?:\/\/(.*)/', '${1}', $store_url);

        $dashboard_url = config('app.dashboard_url');
        $dashboard_url = preg_replace('/https?:\/\/(.*)/', '${1}', $dashboard_url);

        Config::set('app.is_dashboard', $store_url == $dashboard_url);

        return $next($request);
    }
}
