<?php

namespace App\Http\Middleware;

use App\Exceptions\NoRequestRefererException;
use App\Services\StoreService;
use Closure;

class StoreMiddleware
{
    /**
     * @var StoreService
     */
    protected $storeService;

    /**
     * StoreMiddleware constructor.
     *
     * @param StoreService $storeService
     */
    public function __construct(StoreService $storeService)
    {
        $this->storeService = $storeService;
    }

    /**
     * Handle an incoming request.
     *
     * @param $request
     * @param Closure $next
     *
     * @return mixed
     * @throws NoRequestRefererException
     */
    public function handle($request, Closure $next)
    {
        $store_url = $request->headers->get('store-url');

        if (!$store_url) {
            $store_url = $request->headers->get('origin');
        }

        if ($store_url) {
            if (!$this->storeService->isDashboard($store_url)) {
                $this->storeService->setCurrentStore($store_url);
            }

            return $next($request);
        }

        throw new NoRequestRefererException();
    }
}
