<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Support\Facades\Response;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @param  mixed                    $permissions
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $permissions)
    {
        if (Auth::guest()) {
            return Response::jsonError('Unauthorized', 401);
        }

        $permissions = explode('|', $permissions);

        $access = false;
        foreach ((array)$permissions as $perm) {
            if ($request->user()->hasPermissionTo($perm)) {
                $access = true;
            }
        }

        if (!$access) {
            return Response::jsonForbidden();
        }

        return $next($request);
    }
}
