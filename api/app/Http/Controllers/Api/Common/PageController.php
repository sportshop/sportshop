<?php

namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Common\PageCreateRequest;
use App\Http\Requests\Common\PageUpdateRequest;
use App\Repositories\Contracts\PageRepositoryContract;
use Exception;

class PageController extends ApiController
{
    /**
     * @var PageRepositoryContract
     */
    protected $pageRepository;

    /**
     * @param PageRepositoryContract $pageRepository
     */
    public function __construct(PageRepositoryContract $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * Get all pages.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $pages = $this->pageRepository->findAll();

        return response()->json($pages);
    }

    /**
     * Get the page by id.
     *
     * @param int $page_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPageById(int $page_id)
    {
        $page = $this->pageRepository->findById($page_id);

        if (!$page) {
            return response()->jsonNotFound();
        }

        return response()->json($page);
    }

    /**
     * Get the page by slug.
     *
     * @param string $slug
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPageBySlug(string $slug)
    {
        $page = $this->pageRepository->findBySlug($slug);

        if (!$page) {
            return response()->jsonNotFound();
        }

        return response()->json($page);
    }

    /**
     * Create a new page.
     *
     * @param PageCreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function store(PageCreateRequest $request)
    {
        $data = $request->all();

        $result = $this->pageRepository->create($data);

        return response()->json($result);
    }

    /**
     * Update a page by id.
     *
     * @param int $page_id
     * @param PageUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function update(int $page_id, PageUpdateRequest $request)
    {
        $data = $request->all();

        $result = $this->pageRepository->update($page_id, $data);

        return response()->json($result);
    }

    /**
     * Delete a page by id.
     *
     * @param int $page_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $page_id)
    {
        $page = $this->pageRepository->findById($page_id);

        if (!$page) {
            return response()->jsonNotFound();
        }

        if ($page->isSystem()) {
            return response()->jsonForbidden('This page cannot be deleted.');
        }

        if ($this->pageRepository->delete($page_id)) {
            return response()->jsonDeleted();
        }

        return response()->jsonError();
    }
}
