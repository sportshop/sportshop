<?php

namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\ApiController;
use App\Repositories\Contracts\SettingsRepositoryContract;
use App\Services\StoreService;
use Illuminate\Support\Facades\Response;

class ConfigController extends ApiController
{
    /**
     * @var SettingsRepositoryContract
     */
    protected $settingsRepository;

    /**
     * @var StoreService
     */
    protected $storeService;

    /**
     * @param SettingsRepositoryContract $settingsRepository
     * @param StoreService $storeService
     */
    public function __construct(
        SettingsRepositoryContract $settingsRepository,
        StoreService $storeService)
    {
        $this->settingsRepository = $settingsRepository;
        $this->storeService = $storeService;
    }

    /**
     * Get the settings list.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $criteria = [
            'name' => [
                'system_email',
                'support_email',
                'social_facebook',
                'social_instagram',
                'phones'
            ]
        ];

        $config = $this->settingsRepository->findAll($criteria);
        $config['store'] = $this->storeService->current();

        return Response::json($config);
    }
}
