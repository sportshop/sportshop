<?php

namespace App\Http\Controllers\Api\Common;

use App\Events\Currency\CurrenciesWasUpdated;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Common\CurrencySaveRequest;
use App\Repositories\Contracts\CurrencyRepositoryContract;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Response;

class CurrencyController extends ApiController
{
    /**
     * @var CurrencyRepositoryContract
     */
    protected $currencyRepository;

    /**
     * @param CurrencyRepositoryContract $currencyRepository
     */
    public function __construct(CurrencyRepositoryContract $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * Get all currencies.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $currencies = $this->currencyRepository->all();

        return Response::json($currencies);
    }

    /**
     * Save all currencies.
     *
     * @param CurrencySaveRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function save(CurrencySaveRequest $request)
    {
        $data = $request->input('currencies');

        $result = $this->currencyRepository->save($data);

        Event::dispatch(new CurrenciesWasUpdated($result));

        return Response::json($result);
    }
}
