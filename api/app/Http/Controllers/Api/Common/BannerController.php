<?php

namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Common\BannerCreateRequest;
use App\Http\Requests\Common\BannerMediaUploadRequest;
use App\Http\Requests\Common\BannerUpdateRequest;
use App\Repositories\Contracts\BannerRepositoryContract;
use App\Services\MediaManager;
use Auth;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class BannerController extends ApiController
{
    /**
     * @var BannerRepositoryContract
     */
    protected $bannerRepository;

    /**
     * @param BannerRepositoryContract $bannerRepository
     */
    public function __construct(BannerRepositoryContract $bannerRepository)
    {
        $this->bannerRepository = $bannerRepository;
    }

    /**
     * Get all banners.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function findAll(Request $request)
    {
        $criteria = [];

        if ($request->input('active')) {
            $criteria['active'] = $request->input('active') == 'true';
        }

        if (Auth::guest() || !Auth::user()->can('banners.view')) {
            $criteria['active'] = true;
        }

        $banners = $this->bannerRepository->findAll($criteria);

        return Response::json($banners);
    }

    /**
     * Get banner by id.
     *
     * @param int $banner_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function findById(int $banner_id)
    {
        $banner = $this->bannerRepository->findById($banner_id);

        if (!$banner) {
            return Response::jsonNotFound();
        }

        return Response::json($banner);
    }

    /**
     * Create banner.
     *
     * @param BannerCreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(BannerCreateRequest $request)
    {
        $data = $request->all();

        $result = $this->bannerRepository->create($data);

        return Response::jsonCreated($result);
    }

    /**
     * Update banner by id.
     *
     * @param int                 $banner_id
     * @param BannerUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $banner_id, BannerUpdateRequest $request)
    {
        $data = $request->all();

        try {
            $result = $this->bannerRepository->update($banner_id, $data);

            return Response::json($result);
        } catch (Exception $e) {
            return Response::jsonNotFound();
        }
    }

    /**
     * Delete banner by id.
     *
     * @param int $banner_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $banner_id)
    {
        if ($this->bannerRepository->delete($banner_id)) {
            return Response::jsonDeleted();
        }

        return Response::jsonNotFound();
    }

    /**
     * Upload a main image to banner.
     *
     * @param BannerMediaUploadRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(BannerMediaUploadRequest $request)
    {
        $file['data'] = $request->input('base64');
        preg_match('/data\:image\/([a-zA-Z]+);/', $request->input('base64'), $ext);

        $file['ext'] = $ext[1];

        $media = MediaManager::uploadFromBlob($file, 'image');

        return Response::json($media);
    }
}
