<?php
namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\ApiController;
use App\Repositories\Contracts\MediaRepositoryContract;

class MediaController extends ApiController
{
    /**
     * @var MediaRepositoryContract
     */
    protected $mediaRepository;

    /**
     * @param MediaRepositoryContract $mediaRepository
     */
    public function __construct(MediaRepositoryContract $mediaRepository)
    {
        $this->mediaRepository = $mediaRepository;
    }

    /**
     * Delete a main image of category.
     *
     * @param int $media_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $media_id)
    {
        if ($this->mediaRepository->delete($media_id)) {
            return response()->jsonSuccess(trans('events.deleted'));
        }

        return response()->jsonError();
    }
}
