<?php

namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Common\FeedbackSendRequest;
use App\Mail\FeedbackSend;
use Illuminate\Support\Facades\Response;
use Mail;

class FeedbackController extends ApiController
{
    /**
     * Send a feedback form.
     *
     * @param FeedbackSendRequest $request
     *
     * @return mixed
     */
    public function store(FeedbackSendRequest $request)
    {
        $data = $request->all();

        Mail::send(new FeedbackSend($data));

        return Response::json(null, 204);
    }
}
