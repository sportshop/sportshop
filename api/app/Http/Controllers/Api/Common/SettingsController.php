<?php

namespace App\Http\Controllers\Api\Common;

use App\Events\Settings\SettingsWasUpdated;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Common\SettingsUpdateAllRequest;
use App\Repositories\Contracts\SettingsRepositoryContract;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Response;

class SettingsController extends ApiController
{
    /**
     * @var SettingsRepositoryContract
     */
    protected $settingsRepository;

    /**
     * @param SettingsRepositoryContract $settingsRepository
     */
    public function __construct(SettingsRepositoryContract $settingsRepository)
    {
        $this->settingsRepository = $settingsRepository;
    }

    /**
     * Get all settings.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $settings = $this->settingsRepository->findAll();

        return Response::json($settings);
    }

    /**
     * Update all settings by key.
     *
     * @param SettingsUpdateAllRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(SettingsUpdateAllRequest $request)
    {
        $data = $request->all();

        $result = $this->settingsRepository->updateAll($data);

        Event::dispatch(new SettingsWasUpdated($result->toArray()));

        return Response::json($result);
    }
}
