<?php

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Store\AttributeGroupCreateRequest;
use App\Http\Requests\Store\AttributeGroupUpdateRequest;
use App\Repositories\Contracts\AttributeRepositoryContract;
use Exception;
use Illuminate\Support\Facades\Response;

class AttributeController extends ApiController
{
    /**
     * @var AttributeRepositoryContract
     */
    protected $attributeRepository;

    /**
     * @param AttributeRepositoryContract $attributeRepository
     */
    public function __construct(AttributeRepositoryContract $attributeRepository)
    {
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * Get attribute groups with attributes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function findAll()
    {
        $groups = $this->attributeRepository->findAll();

        return Response::json($groups);
    }

    /**
     * Get attribute group by id.
     *
     * @param int $group_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function findById(int $group_id)
    {
        $group = $this->attributeRepository->findById($group_id);

        if (!$group) {
            return Response::jsonNotFound();
        }

        return Response::json($group);
    }

    /**
     * Create an attribute group.
     *
     * @param AttributeGroupCreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(AttributeGroupCreateRequest $request)
    {
        $data = $request->all();

        $result = $this->attributeRepository->create($data);

        return Response::jsonCreated($result);
    }

    /**
     * Update attribute group by id.
     *
     * @param int $group_id
     * @param AttributeGroupUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(int $group_id, AttributeGroupUpdateRequest $request)
    {
        $data = $request->all();

        $result = $this->attributeRepository->update($group_id, $data);

        return Response::json($result);
    }

    /**
     * Delete attribute by id.
     *
     * @param int $group_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $group_id)
    {
        if ($this->attributeRepository->delete($group_id)) {
            return Response::jsonDeleted();
        }

        return Response::jsonNotFound();
    }
}
