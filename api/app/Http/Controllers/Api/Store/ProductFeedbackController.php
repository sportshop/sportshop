<?php

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Store\ProductFeedbackSendRequest;
use App\Mail\ProductFeedback;
use App\Repositories\Contracts\ProductRepositoryContract;
use App\Services\StoreService;
use Mail;
use Response;

class ProductFeedbackController extends ApiController
{
    /**
     * @var ProductRepositoryContract
     */
    protected $productRepository;

    /**
     * @param ProductRepositoryContract $productRepository
     */
    public function __construct(ProductRepositoryContract $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Create a product.
     *
     * @param int $product_id
     * @param ProductFeedbackSendRequest $request
     * @param StoreService $store
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(int $product_id, ProductFeedbackSendRequest $request, StoreService $store)
    {
        $form_data = $request->all();
        $product = $this->productRepository->findById($product_id);
        $url = $store->url();

        Mail::send(new ProductFeedback($form_data, $product, $url));

        return Response::json(null, 204);
    }
}
