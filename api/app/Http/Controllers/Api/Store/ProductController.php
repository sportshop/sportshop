<?php

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Common\MediaUploadRequest;
use App\Http\Requests\Store\ProductCreateRequest;
use App\Http\Requests\Store\ProductUpdateTypeRequest;
use App\Http\Requests\Store\ProductUpdateVisibilityStatusRequest;
use App\Http\Requests\Store\ProductUpdateRequest;
use App\Repositories\Contracts\ProductRepositoryContract;
use App\Services\MediaManager;
use App\Services\StoreService;
use Auth;
use Exception;
use Illuminate\Http\Request;
use Response;

class ProductController extends ApiController
{
    /**
     * @var ProductRepositoryContract
     */
    protected $productRepository;

    /**
     * @param ProductRepositoryContract $productRepository
     */
    public function __construct(ProductRepositoryContract $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Find all products.
     *
     * @param Request $request
     * @param StoreService $storeService
     * @return \Illuminate\Http\JsonResponse
     */
    public function findAll(Request $request, StoreService $storeService)
    {
        // Collect query criteria.
        $criteria = $request->only([
            'id',
            'name',
            'sku',
            'type',
            'status',
            'search',
            'attributes',
            'brands',
            'categories',
            'category_slug',
            'colors',
            'sizes',
            'price_min',
            'price_max',
            'currency_id',
            'uniform_type',
            'order_by',
            'excluded_slug'
        ]);

        // Collect options.
        $options = [
            'limit' => $request->input('limit'),
            'page'  => $request->input('page')
        ];

        // Retrieve discount id.
        if (!Auth::guest()) {
            if (Auth::user()->can('products.view')) {
                $criteria['discount_id'] = $request->input('discount_id');
            } elseif (Auth::user()->data) {
                $criteria['discount_id'] = Auth::user()->data->discount_id;
            }
        }

        // Collect criteria.
        if (!config('app.is_dashboard')) {
            $criteria['store_id'] = $storeService->id();
            $criteria['status'] = 'active';
            $criteria['is_visible'] = true;
            $criteria['exists'] = true;

            if (!array_get($criteria, 'search') && $options['limit'] > 24) {
                $options['limit'] = 24;
            }
        } else {
            $criteria['store_id'] = $request->input('store_id');
            $criteria['available_for_order'] = $request->input('available_for_order');
            $criteria['with_status'] = true;
            $criteria['with_cost_price'] = true;
        }

        // Retrieve products.
        $result['products'] = $this->productRepository->findAll($criteria, $options);

        // Retrieve total cost price and quantity.
        if (!array_get($criteria, 'available_for_order') && array_get($criteria, 'with_cost_price')) {
            unset($criteria['order_by']);
            $total = $this->productRepository->getTotalCostPriceAndQuantity($criteria);
            $result = array_merge($result, $total);
        }

        return Response::json($result);
    }

    /**
     * Find product by id.
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findById(int $id, Request $request)
    {
        $criteria = $request->only('store_id');

        // Retrieve product.
        $product = $this->productRepository->findById($id, $criteria);

        if (!$product) {
            return Response::jsonNotFound();
        }

        return Response::json($product);
    }

    /**
     * Find product by slug.
     *
     * @param string $category_slug
     * @param string $slug
     * @param Request $request
     * @param StoreService $storeService
     * @return \Illuminate\Http\JsonResponse
     */
    public function findBySlug(string $category_slug, string $slug, Request $request, StoreService $storeService)
    {
        $criteria = [
            'store_id'      => $storeService->id(),
            'category_slug' => $category_slug,
            'discount_id'   => $request->input('discount_id')
        ];

        // Retrieve user discount id.
        if (
            !Auth::guest()
            && !Auth::user()->hasAnyRole(['admin', 'manager', 'limited_manager'])
            && Auth::user()->data
        ) {
            $criteria['discount_id'] = Auth::user()->data->discount_id;
        }

        // Retrieve product.
        $product = $this->productRepository->findBySlug($slug, $criteria);

        if (!$product) {
            return Response::jsonNotFound();
        }

        return Response::json($product);
    }

    /**
     * Create product.
     *
     * @param ProductCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(ProductCreateRequest $request)
    {
        $data = $request->all();
        $data['manager_id'] = Auth::id();

        $result = $this->productRepository->create($data);

        return Response::json($result);
    }

    /**
     * Update product by id.
     *
     * @param int $product_id
     * @param ProductUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function update(int $product_id, ProductUpdateRequest $request)
    {
        $data = $request->all();
        $data['manager_id'] = Auth::id();
        $result = $this->productRepository->update($product_id, $data);

        return Response::json($result);
    }

    /**
     * Update product type.
     *
     * @param int $product_id
     * @param ProductUpdateTypeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateType(int $product_id, ProductUpdateTypeRequest $request)
    {
        $type = $request->input('type');

        try {
            $result = $this->productRepository->updateType($product_id, $type);

            return Response::json($result);
        } catch (Exception $e) {
            return Response::jsonNotFound();
        }
    }

    /**
     * Update visibility status.
     *
     * @param int $product_id
     * @param ProductUpdateVisibilityStatusRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateVisibilityStatus(int $product_id, ProductUpdateVisibilityStatusRequest $request)
    {
        $status = $request->input('is_visible');

        try {
            $result = $this->productRepository->updateVisibilityStatus($product_id, $status);

            return Response::json($result);
        } catch (Exception $e) {
            return Response::jsonNotFound();
        }
    }

    /**
     * Delete product by id.
     *
     * @param int $product_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $product_id)
    {
        $product = $this->productRepository->findById($product_id);

        if (!$product) {
            return Response::jsonNotFound();
        }

        if ($product->hasQuantity()) {
            return Response::jsonForbidden('The product has sizes', 'product_has_sizes');
        }

        $this->productRepository->delete($product_id);

        return Response::jsonDeleted();
    }

    /**
     * Upload images.
     *
     * @param MediaUploadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(MediaUploadRequest $request)
    {
        $file['data'] = $request->input('base64');
        preg_match('/data\:image\/([a-zA-Z]+);/', $request->input('base64'), $ext);

        $file['ext'] = $ext[1];

        $media = MediaManager::uploadFromBlob($file, 'image');

        return Response::json($media);
    }
}
