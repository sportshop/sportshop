<?php

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\ApiController;
use App\Repositories\Contracts\CostRepositoryContract;
use App\Repositories\Contracts\OrderRepositoryContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class StatisticController extends ApiController
{
    /**
     * @var OrderRepositoryContract
     */
    protected $orderRepository;

    /**
     * @var CostRepositoryContract
     */
    protected $costRepository;

    /**
     * @param OrderRepositoryContract $orderRepository
     * @param CostRepositoryContract  $costRepository
     */
    public function __construct(
        OrderRepositoryContract $orderRepository,
        CostRepositoryContract $costRepository
    ) {
        $this->orderRepository = $orderRepository;
        $this->costRepository = $costRepository;
    }

    /**
     * Get all stores.
     * TODO: MUST optimize code.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $criteria = $request->only([
            'store_id',
            'manager_id',
            'customer_id',
            'date_start',
            'date_end',
            'date'
        ]);

        // Result structure.
        $resultDataTemplate = [
            'receipts'   => null,
            'profit'     => null,
            'costs'      => null,
            'net_profit' => null
        ];
        $result = [
            'data'  => [],
            'total' => $resultDataTemplate
        ];

        // Retrieve orders and order products prices.
        $orders_and_products_prices = $this->orderRepository->getCompletedOrdersAndProductsPrices($criteria);

        // Retrieve costs.
        $groupedCosts = $this->costRepository->getRatesByDate($criteria);

        // Calculate receipts and profit.
        foreach ($orders_and_products_prices as $date => $prices) {
            if (!isset($result[$date])) {
                $result['data'][$date] = $resultDataTemplate;
            }

            $result['data'][$date]['receipts'] = $prices['orders'];

            $result['data'][$date]['profit'] = $result['data'][$date]['receipts'];
            foreach ($prices['products'] as $currencyId => $currencyPrice) {
                $result['data'][$date]['profit'][$currencyId]['value'] -= $currencyPrice['value'];
                $result['data'][$date]['profit'][$currencyId]['value'] = round($result['data'][$date]['profit'][$currencyId]['value']);
            }
        }

        // Calculate costs.
        foreach ($groupedCosts as $date => $costs) {
            if (!isset($result['data'][$date])) {
                $result['data'][$date] = $resultDataTemplate;
            }

            $result['data'][$date]['costs'] = $costs->toArray();
        }

        // Calculate net profit.
        foreach ($result['data'] as $date => $data) {
            $result['data'][$date]['net_profit'] = $data['profit'];

            if ($data['costs']) {
                if ($result['data'][$date]['net_profit']) {
                    foreach ($data['costs'] as $currencyId => $currencyRate) {
                        if (!isset($result['data'][$date]['net_profit'][$currencyId])) {
                            continue;
                        }

                        $result['data'][$date]['net_profit'][$currencyId]['value'] -= $data['costs'][$currencyId]['value'];
                        $result['data'][$date]['net_profit'][$currencyId]['value'] = round($result['data'][$date]['net_profit'][$currencyId]['value']);
                    }
                } else {
                    $result['data'][$date]['net_profit'] = $data['costs'];

                    foreach ($data['costs'] as $currencyId => $currencyRate) {
                        $result['data'][$date]['net_profit'][$currencyId]['value'] = -$data['costs'][$currencyId]['value'];
                        $result['data'][$date]['net_profit'][$currencyId]['value'] = round($result['data'][$date]['net_profit'][$currencyId]['value']);
                    }
                }
            }
        }

        // Calculate total result.
        foreach ($result['data'] as $date => $data) {
            $entities = array_keys($resultDataTemplate);

            foreach ($entities as $entity) {
                if ($data[$entity]) {
                    if (!$result['total'][$entity]) {
                        $result['total'][$entity] = $data[$entity];
                    } else {
                        foreach ($data[$entity] as $currencyId => $currencyRate) {
                            if (!isset($result['total'][$entity][$currencyId])) {
                                continue;
                            }

                            $result['total'][$entity][$currencyId]['value'] += $currencyRate['value'];
                        }
                    }
                }
            }
        }

        return Response::json($result);
    }
}
