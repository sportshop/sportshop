<?php

namespace App\Http\Controllers\Api\Store;

use App\Events\Order\OrderWasAccepted;
use App\Events\Order\OrderWasCanceled;
use App\Events\Order\OrderWasCompleted;
use App\Events\Order\OrderWasCreated;
use App\Events\Order\OrderWasRefunded;
use App\Events\Order\OrderWasSent;
use App\Events\Order\OrderWasUpdated;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Store\OrderCreateRequest;
use App\Http\Requests\Store\OrderRefundRequest;
use App\Http\Requests\Store\OrderSendRequest;
use App\Http\Requests\Store\OrderUpdateRequest;
use App\Repositories\Contracts\CurrencyRepositoryContract;
use App\Repositories\Contracts\OrderRepositoryContract;
use App\Services\StoreService;
use Auth;
use Exception;
use Illuminate\Http\Request;
use PDF;
use Response;

class OrderController extends ApiController
{
    /**
     * @var OrderRepositoryContract
     */
    protected $orderRepository;

    /**
     * @var CurrencyRepositoryContract
     */
    protected $currencyRepository;

    /**
     * @var StoreService
     */
    protected $storeService;

    /**
     * @param OrderRepositoryContract $orderRepository
     * @param CurrencyRepositoryContract $currencyRepository
     * @param StoreService $storeService
     */
    public function __construct(
        OrderRepositoryContract $orderRepository,
        CurrencyRepositoryContract $currencyRepository,
        StoreService $storeService
    ) {
        $this->orderRepository = $orderRepository;
        $this->currencyRepository = $currencyRepository;
        $this->storeService = $storeService;
    }

    /**
     * Find all orders.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findAll(Request $request)
    {
        // Collect query criteria.
        $criteria = $request->only([
            'status',
            'search',
            'date_start',
            'date_end',
            'created_at',
            'delivery_method',
            'payment_method',
            'payment_status',
            'product',
            'is_withdrawn'
        ]);

        // Collect options.
        $options = [
            'limit' => $request->input('limit', 15),
            'page'  => $request->input('page')
        ];

        // Collect criteria for admins, managers.
        if (Auth::user()->can('orders.view')) {
            $criteria = array_merge($criteria, $request->only([
                'id',
                'store_id',
                'manager_id',
                'customer'
            ]));
            $criteria['with_full_info'] = true;
            $criteria['with_total_residual'] = true;
        } // Collect criteria, options for members.
        else {
            $criteria['customer_id'] = Auth::id();
            $criteria['store_id'] = $this->storeService->id();

            if ($options['limit'] > 100) {
                $options['limit'] = 15;
            }
        }

        // Retrieve orders.
        $orders = $this->orderRepository->findAll($criteria, $options);

        // Retrieve non-withdrawal payments.
        $non_withdrawal_payments_sum = $this->orderRepository->getNonWithdrawalPaymentsSum($criteria);

        return Response::json(compact('orders', 'non_withdrawal_payments_sum'));
    }

    /**
     * Find order by id.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function findById(int $id)
    {
        $order = $this->orderRepository->findById($id);

        if (!$order) {
            return Response::jsonNotFound();
        }

        return Response::json($order);
    }

    /**
     * Create order.
     *
     * @param OrderCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function create(OrderCreateRequest $request)
    {
        $data = $request->only([
            'customer',
            'payment',
            'delivery',
            'notes',
            'products',
            'currency_id'
        ]);

        if (!config('app.is_dashboard')) {
            $data['store_id'] = $this->storeService->id();
        } else {
            $data['store_id'] = $request->input('store_id');
        }

        // Admin, managers
        if (!Auth::guest() && !Auth::user()->hasRole('member')) {
            $data = array_merge($data, $request->only([
                'store_id',
                'is_from_web_store',
                'payments',
                'is_withdrawn'
            ]));
        }

        // Guests, members
        if (Auth::guest() || Auth::user()->hasRole('member')) {
            $data['is_from_web_store'] = true;
            $data['customer']['id'] = Auth::id();
            if (!Auth::guest()) {
                $data['customer']['discount_id'] = Auth::user()->data->discount_id;
            }
        }

        // Admin
        if (!Auth::guest() && Auth::user()->hasRole('admin')) {
            $data['manager_id'] = $request->input('manager_id', Auth::id());
        }

        // Managers
        if (!Auth::guest() && (Auth::user()->hasRole('manager') || Auth::user()->hasRole('limited_manager'))) {
            $data['manager_id'] = Auth::id();
        }

        // Create order.
        $result = $this->orderRepository->create($data);

        // Trigger event.
        event(new OrderWasCreated($result->toArray()));

        return Response::json($result);
    }

    /**
     * Update order by id.
     *
     * @param int $order_id
     * @param OrderUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function update(int $order_id, OrderUpdateRequest $request)
    {
        $data = $request->only([
            'store_id',
            'payment',
            'delivery',
            'notes',
            'is_from_web_store',
            'payments',
            'currency_id',
            'is_withdrawn'
        ]);

        $order = $this->orderRepository->findById($order_id);

        if (!$order) {
            return Response::jsonNotFound();
        }

        // If order is canceled.
        if ($order->isCanceled()) {
            return Response::jsonForbidden('The order is canceled');
        }

        // If order is completed.
        if ($order->isCompleted()) {
            if ($request->input('is_withdrawn')) {
                $result = $this->orderRepository->withdrawnPayments($order_id);

                return Response::json($result);
            }

            return Response::jsonForbidden('The order is completed');
        }

        // Collect products if order is not sent and not completed.
        if (!$order->isSent() && !$order->isCompleted()) {
            $data['products'] = $request->input('products');
        }

        // Admin
        if (!Auth::guest() && Auth::user()->hasRole('admin')) {
            $data['manager_id'] = $request->input('manager_id');
        }

        $result = $this->orderRepository->update($order_id, $data);

        event(new OrderWasUpdated($result));

        return Response::json($result);
    }

    /**
     * Accept order by admins, managers.
     *
     * @param int $order_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function accept(int $order_id)
    {
        $order = $this->orderRepository->findById($order_id);

        if (!$order) {
            return Response::jsonNotFound();
        }

        if (!$order->isNew()) {
            return Response::jsonForbidden('The order is accepted.');
        }

        $result = $this->orderRepository->accept($order_id, Auth::id());

        event(new OrderWasAccepted($result));

        return Response::json($result);
    }

    /**
     * Update delivery declaration and set "sent" status.
     *
     * @param int $order_id
     * @param OrderSendRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function send(int $order_id, OrderSendRequest $request)
    {
        $order = $this->orderRepository->findById($order_id);

        if (!$order) {
            return Response::jsonNotFound();
        }

        if (!$order->isAccepted()) {
            return Response::jsonForbidden('The order is not accepted');
        }

        if ($order->isCanceled()) {
            return Response::jsonForbidden('The order is canceled');
        }

        $declaration = $request->input('declaration');

        $result = $this->orderRepository->send($order_id, $declaration);

        event(new OrderWasSent($result));

        return Response::json($result);
    }

    /**
     * Complete order. Set "completed", "paid" status.
     *
     * @param int $order_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function complete(int $order_id)
    {
        $order = $this->orderRepository->findById($order_id);

        if (!$order) {
            return Response::jsonNotFound();
        }

        if (!$order->hasManager()) {
            return Response::jsonForbidden('No manager');
        }

        if ($order->isCanceled()) {
            return Response::jsonForbidden('The order is canceled');
        }

        if (!$order->isPaid()) {
            return Response::jsonForbidden('The order is not fully paid.');
        }

        $result = $this->orderRepository->complete($order_id);

        event(new OrderWasCompleted($result));

        return Response::json($result);
    }

    /**
     * Refund order.
     *
     * @param int $order_id
     * @param OrderRefundRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException|\Exception
     */
    public function refund(int $order_id, OrderRefundRequest $request)
    {
        $order = $this->orderRepository->findById($order_id);

        if (!$order->isCompleted() && !$order->isPartiallyRefunded()) {
            return Response::jsonForbidden();
        }

        $products = $request->input('products');

        $result = $this->orderRepository->refund($order_id, $products);

        event(new OrderWasRefunded($result));

        return Response::json($result);
    }

    /**
     * Cancel order.
     *
     * @param int $order_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException|\Exception
     */
    public function cancel(int $order_id)
    {
        $order = $this->orderRepository->findById($order_id);

        if (!$order) {
            return Response::jsonNotFound();
        }

        if ($order->isCompleted() || $order->isRefunded() || $order->isPartiallyRefunded()) {
            return Response::jsonForbidden('The order is completed.');
        }

        if ($order->isCanceled()) {
            return Response::jsonForbidden('The order is canceled.');
        }

        if ($order->isPaid()) {
            return Response::jsonForbidden('The order is paid.');
        }

        $result = $this->orderRepository->cancel($order_id);

        event(new OrderWasCanceled($result));

        return Response::json(null, 204);
    }

    /**
     * Withdrawn payments of orders.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function withdrawnPayments(Request $request)
    {
        if ($id = $request->input('id')) {
            $order = $this->orderRepository->findById($id);

            if ($order->isCanceled()) {
                return Response::jsonForbidden('The order is canceled');
            }
        }

        $result = $this->orderRepository->withdrawnPayments($request->all());

        return Response::json($result);
    }

    /**
     * Delete order products.
     *
     * @param int $order_id
     * @param int $product_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException|\Exception
     */
    public function destroyProducts(int $order_id, int $product_id)
    {
        $order = $this->orderRepository->findById($order_id);

        if ($order->isCompleted() || $order->isRefunded() || $order->isPartiallyRefunded() || $order->isCanceled()) {
            return Response::jsonForbidden();
        }

        $this->orderRepository->deleteProduct($order_id, $product_id);

        return Response::jsonDeleted();
    }

    /**
     * Find all orders and make PDF file.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function downloadAll(Request $request)
    {
        // Collect query criteria.
        $criteria = $request->only([
            'id',
            'status',
            'search',
            'date_start',
            'date_end',
            'delivery_method',
            'payment_method',
            'payment_status',
            'product'
        ]);
        $criteria['exists'] = true;

        // Collect options.
        $options = [
            'limit' => $request->input('limit'),
            'page'  => $request->input('page')
        ];
        if (!$options['limit'] || $options['limit'] > 100) {
            $options['limit'] = 15;
        }

        // Collect criteria for admins, managers.
        if (Auth::user()->can('orders.view')) {
            $criteria = array_merge($criteria, $request->only([
                'id',
                'store_id',
                'manager_id',
                'customer'
            ]));
        } // Collect criteria, options for members.
        else {
            $criteria['customer_id'] = Auth::id();
            $criteria['store_id'] = $this->storeService->id();
        }

        // Retrieve orders.
        $orders = $this->orderRepository->findAll($criteria, $options);

        // Calculate total price.
        $total_price = [];
        $defaultCurrencyId = $this->currencyRepository->findDefaultCurrency()->id;
        foreach ($orders as $order) {
            $price = $order->price->where('currency_id', $defaultCurrencyId)->first();

            if (!isset($total_price['currency_id'])) {
                $total_price = $price;
            } else {
                $total_price['value'] += $price['value'];
            }

            $total = ['quantity' => 0];

            foreach ($order->products as $product) {
                $total['quantity'] += $product['sizes']['items']->sum('quantity');
            }

            $order['total'] = $total;
        }

        // Retrieve auth user.
        $user = Auth::user();

        // Make PDF.
        $store = $this->storeService->current();
        $pdf = PDF::loadView('pdf.orders', compact('user', 'orders', 'total_price', 'store'));

        return $pdf->stream();
    }

    /**
     * Find order by ID and make PDF file.
     *
     * @param int $order_id
     * @return \Illuminate\Http\Response
     */
    public function downloadById(int $order_id)
    {
        // Retrieve order.
        $order = $this->orderRepository->findById($order_id);

        // Retrieve total quantity.
        $total = ['quantity' => 0];

        foreach ($order->products as $product) {
            $total['quantity'] += $product['sizes']['items']->sum('quantity');
        }

        // Retrieve auth user.
        $user = Auth::user();

        if ($user->hasRole('member') && $order->customer_id !== $user->id) {
            return Response::jsonForbidden();
        }

        // Make PDF.
        $store = $this->storeService->current();
        $pdf = PDF::loadView('pdf.order', compact('user', 'order', 'total', 'store'));

        return $pdf->stream();
    }

    /**
     * Get orders count.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCount(Request $request)
    {
        // Collect query criteria.
        $criteria = $request->only([
            'store_id',
            'status'
        ]);

        // Retrieve orders count.
        $number = $this->orderRepository->getCount($criteria);

        return Response::json($number);
    }

    /**
     * Get completed orders and products quantity.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatistic(Request $request)
    {
        // Collect query criteria.
        $criteria = $request->only([
            'store_id',
            'manager_id',
            'customer_id',
            'date_start',
            'date_end',
            'date'
        ]);

        // Retrieve completed orders and products.
        $result = $this->orderRepository->getCompletedOrdersAndProductsQuantity($criteria);

        return Response::json($result);
    }

    /**
     * Get not completed orders quantity.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNotCompletedOrders(Request $request)
    {
        // Collect query criteria.
        $criteria = $request->only([
            'store_id',
            'manager_id',
            'customer_id',
            'date_start',
            'date_end',
            'date'
        ]);

        // Retrieve not completed orders quantity.
        $result = $this->orderRepository->getNotCompletedOrdersQuantity($criteria);

        return Response::json($result);
    }
}
