<?php

namespace App\Http\Controllers\Api\Store;

use App\Events\Color\ColorsWasUpdated;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Store\ColorSaveRequest;
use App\Models\Color;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;

class ColorController extends ApiController
{
    /**
     * Get all colors.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $colors = Color::all();

        return response()->json($colors);
    }

    /**
     * Create a color.
     *
     * @param ColorSaveRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function save(ColorSaveRequest $request)
    {
        DB::beginTransaction();

        $data = $request->input('colors');

        try {
            // Remove old (unused) record
            $ids = array_filter(array_pluck($data, 'id'));
            Color::whereNotIn('id', $ids)->delete();

            // Update or create
            $result = [];
            foreach ($data as $item) {
                $id = array_get($item, 'id');

                $model = Color::updateOrCreate(compact('id'), $item);

                $result[] = $model;
            }

            DB::commit();

            Event::dispatch(new ColorsWasUpdated($result));

            return response()->json($result);
        } catch (Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }
}
