<?php

namespace App\Http\Controllers\Api\Store;

use App\Events\Brand\BrandWasCreated;
use App\Events\Brand\BrandWasDeleted;
use App\Events\Brand\BrandWasUpdated;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Common\MediaUploadRequest;
use App\Http\Requests\Store\BrandCreateRequest;
use App\Http\Requests\Store\BrandUpdateRequest;
use App\Repositories\Contracts\BrandRepositoryContract;
use App\Services\MediaManager;
use Exception;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Response;

class BrandController extends ApiController
{
    /**
     * @var BrandRepositoryContract
     */
    protected $brandRepository;

    /**
     * @param BrandRepositoryContract $brandRepository
     */
    public function __construct(BrandRepositoryContract $brandRepository)
    {
        $this->brandRepository = $brandRepository;
    }

    /**
     * Find all brands.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function findAll()
    {
        $brands = $this->brandRepository->findAll();

        return Response::json($brands);
    }

    /**
     * Get brand by id.
     *
     * @param int $brand_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function findById(int $brand_id)
    {
        $brand = $this->brandRepository->findById($brand_id);

        if (!$brand) {
            return Response::jsonNotFound();
        }

        return Response::json($brand);
    }

    /**
     * Create a brand.
     *
     * @param BrandCreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(BrandCreateRequest $request)
    {
        $data = $request->all();

        $result = $this->brandRepository->create($data);

        Event::dispatch(new BrandWasCreated($result));

        return Response::jsonCreated($result);
    }

    /**
     * Update a brand by id.
     *
     * @param int $brand_id
     * @param BrandUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $brand_id, BrandUpdateRequest $request)
    {
        try {
            $data = $request->all();

            $result = $this->brandRepository->update($brand_id, $data);

            Event::dispatch(new BrandWasUpdated($result));

            return Response::json($result);
        } catch (Exception $e) {
            return Response::jsonNotFound();
        }
    }

    /**
     * Delete a brand by id.
     *
     * @param int $brand_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $brand_id)
    {
        $model = $this->brandRepository->findById($brand_id);
        // TODO:
//
        //if (!$model) {
        //    return Response::jsonNotFound();
        //}
//
        //$this->brandRepository->delete($brand_id);

        Event::dispatch(new BrandWasDeleted($model));

        return Response::jsonDeleted();
    }

    /**
     * Upload main image.
     *
     * @param MediaUploadRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadMedia(MediaUploadRequest $request)
    {
        $file['data'] = $request->input('base64');
        preg_match('/data\:image\/([a-zA-Z]+);/', $request->input('base64'), $ext);

        $file['ext'] = $ext[1];

        $media = MediaManager::uploadFromBlob($file, 'image');

        return Response::json($media);
    }
}
