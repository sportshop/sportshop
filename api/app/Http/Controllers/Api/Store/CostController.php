<?php

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Store\CostCreateRequest;
use App\Http\Requests\Store\CostUpdateRequest;
use App\Repositories\Contracts\CostRepositoryContract;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class CostController extends ApiController
{
    /**
     * @var CostRepositoryContract
     */
    protected $costRepository;

    /**
     * @param CostRepositoryContract $costRepository
     */
    public function __construct(CostRepositoryContract $costRepository)
    {
        $this->costRepository = $costRepository;
    }

    /**
     * Get all costs.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function findAll(Request $request)
    {
        $criteria = $request->only([
            'date_start',
            'date_end',
            'date',
            'store_id',
            'type'
        ]);

        $options = $request->only(['limit', 'page']);

        $costs = $this->costRepository->findAll($criteria, $options);
        $amount = $this->costRepository->getTotalAmount($criteria);

        $result = array_merge(
            $costs->toArray(),
            ['total_amount' => $amount]
        );

        return Response::json($result);
    }

    /**
     * Get cost by id.
     *
     * @param int $cost_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function findById(int $cost_id)
    {
        $cost = $this->costRepository->findById($cost_id);

        if (!$cost) {
            return Response::jsonNotFound();
        }

        return Response::json($cost);
    }

    /**
     * Create cost.
     *
     * @param CostCreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(CostCreateRequest $request)
    {
        $data = $request->all();

        $cost = $this->costRepository->create($data);

        return Response::jsonCreated($cost);
    }

    /**
     * Update cost by id.
     *
     * @param int               $cost_id
     * @param CostUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $cost_id, CostUpdateRequest $request)
    {
        $data = $request->all();

        try {
            $result = $this->costRepository->update($cost_id, $data);

            return Response::json($result);
        } catch (Exception $e) {
            return Response::jsonNotFound();
        }
    }

    /**
     * Delete cost by id.
     *
     * @param int $cost_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $cost_id)
    {
        if ($this->costRepository->delete($cost_id)) {
            return Response::jsonDeleted();
        }

        return Response::jsonNotFound();
    }

    /**
     * Get rates of all cost types and group by date.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatistic(Request $request)
    {
        $criteria = $request->only([
            'date_start',
            'date_end',
            'date',
            'store_id'
        ]);
        $criteria['group_by'] = 'type';

        $costs = $this->costRepository->getRatesByDate($criteria);

        return Response::json($costs);
    }
}
