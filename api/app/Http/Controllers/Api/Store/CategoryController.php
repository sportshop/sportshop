<?php

namespace App\Http\Controllers\Api\Store;

use App\Events\Category\CategoryWasCreated;
use App\Events\Category\CategoryWasDeleted;
use App\Events\Category\CategoryWasUpdated;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Common\MediaUploadRequest;
use App\Http\Requests\Store\CategoryCreateRequest;
use App\Http\Requests\Store\CategoryUpdateRequest;
use App\Repositories\Contracts\CategoryRepositoryContract;
use App\Repositories\Contracts\SettingsRepositoryContract;
use App\Services\MediaManager;
use App\Services\StoreService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Response;

class CategoryController extends ApiController
{
    /**
     * @var CategoryRepositoryContract
     */
    protected $categoryRepository;

    /**
     * @var SettingsRepositoryContract
     */
    protected $settingsRepository;

    /**
     * @param CategoryRepositoryContract $categoryRepository
     * @param SettingsRepositoryContract $settingsRepository
     */
    public function __construct(
        CategoryRepositoryContract $categoryRepository,
        SettingsRepositoryContract $settingsRepository
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->settingsRepository = $settingsRepository;
    }

    /**
     * Find all categories.
     *
     * @param Request $request
     * @param StoreService $storeService
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function findAll(Request $request, StoreService $storeService)
    {
        // Collect criteria.
        $criteria = $request->only(['active']);

        if (!config('app.is_dashboard')) {
            $criteria['store_id'] = $storeService->id();
            $criteria['products_exists'] = true;
        }

        if (Auth::guest() || !Auth::user()->can('categories.view')) {
            $criteria['active'] = true;
        }

        // Collect options
        $options = $request->only('flat');

        if ($request->input('type') === 'feature') {
            $featureCategories = $this->settingsRepository->findByName('feature_categories');

            if ($featureCategories) {
                $criteria['ids'] = array_pluck($featureCategories, 'id');
                $options['without_tree'] = true;
            }
        }

        $categories = $this->categoryRepository->findAll($criteria, $options);

        return Response::json($categories);
    }

    /**
     * Find category by id.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function findById(int $id)
    {
        $category = $this->categoryRepository->findById($id);

        if (!$category) {
            return Response::jsonNotFound();
        }

        return Response::json($category);
    }

    /**
     * Create a category.
     *
     * @param CategoryCreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(CategoryCreateRequest $request)
    {
        $data = $request->all();

        $result = $this->categoryRepository->create($data);

        Event::dispatch(new CategoryWasCreated($result));

        return Response::jsonCreated($result);
    }

    /**
     * Update a category by id.
     *
     * @param int $category_id
     * @param CategoryUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $category_id, CategoryUpdateRequest $request)
    {
        try {
            $data = $request->all();

            $result = $this->categoryRepository->update($category_id, $data);

            Event::dispatch(new CategoryWasUpdated($result));

            return Response::json($result);
        } catch (Exception $e) {
            return Response::jsonNotFound();
        }
    }

    /**
     * Delete a category by id.
     *
     * @param int $category_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $category_id)
    {
        $result = $this->categoryRepository->findById($category_id);

        if (!$result) {
            return Response::jsonNotFound();
        }

        $this->categoryRepository->delete($category_id);

        Event::dispatch(new CategoryWasDeleted($result));

        return Response::jsonDeleted();
    }

    /**
     * Upload a main image to category.
     *
     * @param MediaUploadRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadMedia(MediaUploadRequest $request)
    {
        $file['data'] = $request->input('base64');
        preg_match('/data\:image\/([a-zA-Z]+);/', $request->input('base64'), $ext);

        $file['ext'] = $ext[1];

        $media = MediaManager::uploadFromBlob($file, 'image');

        return Response::json($media);
    }
}
