<?php
namespace App\Http\Controllers\Api\Store;

use App\Exceptions\ProductExistException;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Store\SizeCreateRequest;
use App\Http\Requests\Store\SizeUpdateRequest;
use App\Repositories\Contracts\BrandRepositoryContract;
use App\Repositories\Contracts\SizeRepositoryContract;
use Exception;
use Illuminate\Http\Request;

class SizeController extends ApiController
{
    /**
     * @var BrandRepositoryContract
     */
    protected $brandRepository;

    /**
     * @var SizeRepositoryContract
     */
    protected $sizeRepository;

    /**
     * @param BrandRepositoryContract $brandRepository
     * @param SizeRepositoryContract $sizeRepository
     */
    public function __construct(
        BrandRepositoryContract $brandRepository,
        SizeRepositoryContract $sizeRepository
    ) {
        $this->brandRepository = $brandRepository;
        $this->sizeRepository = $sizeRepository;
    }

    /**
     * Get all size groups with types and sizes.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $criteria = $request->only('type');

        $groups = $this->brandRepository->findAll($criteria);

        $groups->transform(function ($table) use (&$as) {
            $table->addVisible('size_groups');

            $table->size_groups->transform(function ($group) use (&$as) {
                $sizes = [];
                foreach ($group->sizes as $size) {
                    foreach ($size->values as $value) {
                        $sizes[$value->title][] = [
                            'id'    => $size->id,
                            'title' => $value->title,
                            'size'  => $value->value
                        ];
                    }
                }
                $group = $group->toArray();

                $group['sizes'] = $sizes;

                return $group;
            });

            return $table;
        });

        $groups = $groups->toArray();

        return response()->json($groups);
    }

    /**
     * Get a size group by id.
     *
     * @param int $group_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $group_id)
    {
        $group = $this->sizeRepository->findById($group_id);

        if (!$group) {
            return response()->jsonNotFound();
        }

        $group->addVisible('brand');

        return response()->json($group);
    }

    /**
     * Create a size group.
     *
     * @param SizeCreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function store(SizeCreateRequest $request)
    {
        $data = $request->all();

        $result = $this->sizeRepository->create($data);

        return response()->json($result);
    }

    /**
     * Update a size group by id.
     *
     * @param int $group_id
     * @param SizeUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function update(int $group_id, SizeUpdateRequest $request)
    {
        $data = $request->all();

        try {
            $result = $this->sizeRepository->update($group_id, $data);

            return response()->json($result);
        } catch (ProductExistException $e) {
            return response()->jsonForbidden($e->getMessage(), 'products_exists');
        }
    }

    /**
     * Delete a size group by id.
     *
     * @param int $group_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $group_id)
    {
        $group = $this->sizeRepository->findById($group_id);

        if (!$group) {
            return response()->jsonNotFound();
        }

        if ($group->productSizesExists()) {
            return response()->jsonForbidden('Please, delete the products first');
        }

        if ($this->sizeRepository->delete($group_id)) {
            return response()->jsonDeleted();
        }

        return response()->jsonError();
    }
}
