<?php
namespace App\Http\Controllers\Api\Store;

use App\Events\Store\StoreWasCreated;
use App\Events\Store\StoreWasDeleted;
use App\Events\Store\StoreWasUpdated;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Common\MediaUploadRequest;
use App\Http\Requests\Store\StoreCreateRequest;
use App\Http\Requests\Store\StoreUpdateRequest;
use App\Repositories\Contracts\StoreRepositoryContract;
use App\Services\MediaManager;
use Exception;
use Illuminate\Support\Facades\Event;
use Response;

class StoreController extends ApiController
{
    /**
     * @var StoreRepositoryContract
     */
    protected $storeRepository;

    /**
     * @param StoreRepositoryContract $storeRepository
     */
    public function __construct(StoreRepositoryContract $storeRepository)
    {
        $this->storeRepository = $storeRepository;
    }

    /**
     * Get all stores.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $stores = $this->storeRepository->findAll();

        return Response::json($stores);
    }

    /**
     * Get a store by id.
     *
     * @param int $store_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $store_id)
    {
        $store = $this->storeRepository->findById($store_id);

        if (!$store) {
            return Response::jsonNotFound();
        }

        return Response::json($store);
    }

    /**
     * Create a store.
     *
     * @param StoreCreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function store(StoreCreateRequest $request)
    {
        $data = $request->all();

        $result = $this->storeRepository->create($data);

        Event::dispatch(new StoreWasCreated($result));

        return Response::json($result);
    }

    /**
     * Update a store by id.
     *
     * @param int $store_id
     * @param StoreUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function update(int $store_id, StoreUpdateRequest $request)
    {
        $data = $request->all();

        $result = $this->storeRepository->update($store_id, $data);

        Event::dispatch(new StoreWasUpdated($result));

        return Response::json($result);
    }

    /**
     * Delete a store.
     *
     * @param int $store_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $store_id)
    {
        $store = $this->storeRepository->findById($store_id);

        if (!$store) {
            return Response::jsonNotFound();
        }

        if ($store->productSizesExists()) {
            return Response::jsonForbidden('The store has products');
        }

        $this->storeRepository->delete($store_id);

        Event::dispatch(new StoreWasDeleted($store));

        return Response::jsonDeleted();
    }

    /**
     * Upload logo.
     *
     * @param MediaUploadRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadLogo(MediaUploadRequest $request)
    {
        $file = $request->file('file');

        $media = MediaManager::upload($file, 'image');

        return Response::json($media);
    }

    /**
     * Upload favicon.
     *
     * @param MediaUploadRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadFavicon(MediaUploadRequest $request)
    {
        $file = $request->file('file');

        $media = MediaManager::upload($file, 'image');

        return Response::json($media);
    }
}
