<?php

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\ApiController;
use App\Repositories\Contracts\AttributeRepositoryContract;
use App\Repositories\Contracts\BrandRepositoryContract;
use App\Repositories\Contracts\CategoryRepositoryContract;
use App\Repositories\Contracts\ColorRepositoryContract;
use App\Repositories\Contracts\ProductRepositoryContract;
use App\Repositories\Contracts\SizeRepositoryContract;
use App\Services\StoreService;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Response;

class ProductsFilterController extends ApiController
{
    /**
     * @var CategoryRepositoryContract
     */
    protected $attributeRepository;

    /**
     * @var BrandRepositoryContract
     */
    protected $brandRepository;

    /**
     * @var CategoryRepositoryContract
     */
    protected $categoryRepository;

    /**
     * @var ColorRepositoryContract
     */
    protected $colorRepository;

    /**
     * @var ProductRepositoryContract
     */
    protected $productRepository;
    /**
     * @var SizeRepositoryContract
     */
    protected $sizeRepository;

    /**
     * @var int
     */
    protected $store_id;

    /**
     * ProductsFilterController constructor.
     *
     * @param AttributeRepositoryContract $attributeRepository
     * @param BrandRepositoryContract $brandRepository
     * @param CategoryRepositoryContract $categoryRepository
     * @param ColorRepositoryContract $colorRepository
     * @param ProductRepositoryContract $productRepository
     * @param SizeRepositoryContract $sizeRepository
     */
    public function __construct(
        AttributeRepositoryContract $attributeRepository,
        BrandRepositoryContract $brandRepository,
        CategoryRepositoryContract $categoryRepository,
        ColorRepositoryContract $colorRepository,
        ProductRepositoryContract $productRepository,
        SizeRepositoryContract $sizeRepository
    ) {
        $this->attributeRepository = $attributeRepository;
        $this->brandRepository = $brandRepository;
        $this->categoryRepository = $categoryRepository;
        $this->colorRepository = $colorRepository;
        $this->productRepository = $productRepository;
        $this->sizeRepository = $sizeRepository;
    }

    /**
     * Get product's entities.
     *
     * @param Request $request
     * @param StoreService $storeService
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, StoreService $storeService)
    {
        $this->store_id = $storeService->id();

        // Retrieve attributes.
        $attributes = $this->retrieveAttributes($request);

        // Retrieve brands
        $brands = $this->retrieveBrands($request);

        // Retrieve categories.
        $categories = $this->retrieveCategories($request);

        // Retrieve colors.
        $colors = $this->retrieveColors($request);

        // Retrieve min, max prices
        $prices = $this->retrievePrices($request);

        // Retrieve sizes.
        $sizes = $this->retrieveSizes($request);

        return Response::json(compact(
            'attributes',
            'brands',
            'categories',
            'colors',
            'prices',
            'sizes'
        ));
    }

    /**
     * Retrieve attributes by request criteria.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Support\Collection
     */
    protected function retrieveAttributes(Request $request)
    {
        $criteria = $request->only([
            'attributes',
            'brands',
            'category_slug',
            'categories',
            'colors',
            'sizes',
            'price_min',
            'price_max',
            'search',
            'type'
        ]);
        $criteria['store_id'] = $this->store_id;
        $criteria['products_exists'] = true;

        $attributes = $this->attributeRepository->findAll($criteria);

        return $attributes;
    }

    /**
     * Retrieve brands by category.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function retrieveBrands(Request $request)
    {
        $criteria = $request->only([
            'attributes',
            'brands',
            'category_slug',
            'categories',
            'colors',
            'sizes',
            'price_min',
            'price_max',
            'search',
            'type'
        ]);
        $criteria['store_id'] = $this->store_id;
        $criteria['products_exists'] = true;

        $brands = $this->brandRepository->findAll($criteria);

        return $brands;
    }

    /**
     * Retrieve top level categories with children by request criteria.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function retrieveCategories(Request $request)
    {
        $criteria = [
            'root_slug'       => $request->input('category_slug'),
            'store_id'        => $this->store_id,
            'products_exists' => true,
            'product_type'    => $request->input('type')
        ];

        $categories = $this->categoryRepository->findAll($criteria);

        return $categories;
    }

    /**
     * Retrieve colors.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function retrieveColors(Request $request)
    {
        $criteria = $request->only([
            'attributes',
            'brands',
            'category_slug',
            'categories',
            'colors',
            'sizes',
            'price_min',
            'price_max',
            'search',
            'type'
        ]);
        $criteria['store_id'] = $this->store_id;
        $criteria['products_exists'] = true;

        $colors = $this->colorRepository->findAll($criteria);

        return $colors;
    }

    /**
     * Retrieve min, max prices.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \stdClass|null
     */
    protected function retrievePrices(Request $request)
    {
        $criteria = $request->only([
            'attributes',
            'brands',
            'category_slug',
            'categories',
            'colors',
            'sizes',
            'currency_id',
            'price_min',
            'price_max',
            'search',
            'type'
        ]);
        $criteria['store_id'] = $this->store_id;

        if (!Auth::guest()) {
            $user = Auth::user();
            $user->load('roles.permissions', 'data.discount');

            if ($user->role_name == 'member' && $user->discount) {
                $criteria['discount_id'] = $user->discount->id;
            }
            if ($user->can('products.view')) {
                $criteria['discount_id'] = $request->input('discount_id');
            }
        }

        $prices = $this->productRepository->getMinMaxOfSalePrice($criteria);

        return $prices;
    }

    /**
     * Retrieve sizes by request criteria.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Support\Collection
     */
    protected function retrieveSizes(Request $request)
    {
        $criteria = $request->only([
            'attributes',
            'brands',
            'category_slug',
            'categories',
            'colors',
            'price_min',
            'price_max',
            'search',
            'type'
        ]);
        $criteria['store_id'] = $this->store_id;
        $criteria['products_exists'] = true;

        $sizes = $this->sizeRepository->findAll($criteria);

        $result = new Collection();

        foreach ($sizes as $group) {
            foreach ($group->sizes as $size) {
                $value = $size->values->firstWhere('is_default', true)->toArray();
                $value['id'] = $size['id'];
                $result->push($value);
            }
        }

        $result = $result->unique('value');
        $result = $result->sortBy('value', SORT_NATURAL);

        return $result;
    }
}
