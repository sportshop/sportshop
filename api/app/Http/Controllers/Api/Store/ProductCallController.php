<?php

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Store\ProductCallSendRequest;
use App\Mail\ProductCall;
use App\Repositories\Contracts\ProductRepositoryContract;
use App\Services\StoreService;
use Illuminate\Support\Facades\Response;
use Mail;

class ProductCallController extends ApiController
{
    /**
     * @var ProductRepositoryContract
     */
    protected $productRepository;

    /**
     * @param ProductRepositoryContract $productRepository
     */
    public function __construct(ProductRepositoryContract $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Create a product.
     *
     * @param int $product_id
     * @param ProductCallSendRequest $request
     * @param StoreService $store
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(int $product_id, ProductCallSendRequest $request, StoreService $store)
    {
        $form_data = $request->all();
        $product = $this->productRepository->findById($product_id);
        $url = $store->url();

        Mail::queue(new ProductCall($form_data, $product, $url));

        return Response::json(null, 204);
    }
}
