<?php

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\ApiController;
use App\Repositories\Contracts\TransferRepositoryContract;
use App\Repositories\Criteria\ProductTransferListCriteria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use PDF;

class TransferController extends ApiController
{
    /**
     * @var TransferRepositoryContract
     */
    protected $transferRepository;

    /**
     * @param TransferRepositoryContract $transferRepository
     */
    public function __construct(TransferRepositoryContract $transferRepository)
    {
        $this->transferRepository = $transferRepository;
    }

    /**
     * Get all product transfers.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        define('DEFAULT_LIMIT', 30);
        define('MAX_LIMIT', 50);

        $criteria = $request->only([
            'from_store_id',
            'to_store_id',
            'user_id',
            'action',
            'date',
            'date_start',
            'date_end',
            'product_name',
            'product_sku',
            'product_size',
            'product_quantity',
            'sort_by',
            'order_by'
        ]);

        $options = [
            'limit' => $request->input('limit', DEFAULT_LIMIT),
            'page'  => $request->input('page')
        ];

        if ($options['limit'] > MAX_LIMIT) {
            $options['limit'] = DEFAULT_LIMIT;
        }

        $transfers = $this->transferRepository->findAll($criteria, $options);

        $total = $this->transferRepository->getTotalPriceAndQuantity($criteria);

        $result = array_merge(['items' => $transfers], $total);

        return Response::json($result);
    }

    public function downloadPdf(Request $request)
    {
        define('DEFAULT_LIMIT', 30);
        define('MAX_LIMIT', 50);

        $criteria = $request->only([
            'from_store_id',
            'to_store_id',
            'user_id',
            'action',
            'date',
            'date_start',
            'date_end',
            'product_name',
            'product_sku',
            'product_size',
            'product_quantity'
        ]);

        $options = [
            'limit' => $request->input('limit', DEFAULT_LIMIT),
            'page'  => $request->input('page')
        ];

        if ($options['limit'] > MAX_LIMIT) {
            $options['limit'] = DEFAULT_LIMIT;
        }

        $transfers = $this->transferRepository->findAll($criteria, $options);

        $total = $this->transferRepository->getTotalPriceAndQuantity($criteria);

        if ($total['price']) {
            $total['price'] = array_where($total['price'], function ($item) {
                return $item['is_default'];
            });
            $total['price'] = reset($total['price']);
        }

        $pdf = PDF::setPaper('A4', 'landscape')->loadView('pdf.transfers', compact('transfers', 'total'));

        return $pdf->stream();
    }
}
