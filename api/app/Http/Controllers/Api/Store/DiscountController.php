<?php

namespace App\Http\Controllers\Api\Store;

use App\Events\Discount\DiscountsWasUpdated;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Store\DiscountSaveRequest;
use App\Repositories\Contracts\DiscountRepositoryContract;
use Exception;
use Illuminate\Support\Facades\Event;

class DiscountController extends ApiController
{
    /**
     * @var DiscountRepositoryContract
     */
    protected $discountRepository;

    /**
     * @param DiscountRepositoryContract $discountRepository
     */
    public function __construct(DiscountRepositoryContract $discountRepository)
    {
        $this->discountRepository = $discountRepository;
    }

    /**
     * Get all discounts.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $discounts = $this->discountRepository->all();

        return response()->json($discounts);
    }

    /**
     * Save all discounts.
     *
     * @param DiscountSaveRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function save(DiscountSaveRequest $request)
    {
        $data = $request->input('discounts');

        $result = $this->discountRepository->save($data);

        Event::dispatch(new DiscountsWasUpdated($result));

        return response()->json($result);
    }
}
