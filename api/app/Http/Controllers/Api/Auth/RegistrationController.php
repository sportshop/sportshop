<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegistrationRequest;
use App\Models\Discount;
use App\Repositories\Contracts\UserRepositoryContract;
use Illuminate\Support\Facades\Response;

class RegistrationController extends Controller
{
    /**
     * @var UserRepositoryContract
     */
    protected $userRepository;

    /**
     * @param UserRepositoryContract $userRepository
     */
    public function __construct(UserRepositoryContract $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param RegistrationRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function register(RegistrationRequest $request)
    {
        $data = $request->only([
            'email',
            'password',
            'first_name',
            'last_name',
            'phone'
        ]);
        $data['role_name'] = 'member';

        $user = $this->userRepository->create($data);

        return Response::json($user);
    }
}
