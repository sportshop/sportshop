<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Auth\PasswordResetRequest;
use App\Repositories\Contracts\UserRepositoryContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Response;

class ResetPasswordController extends ApiController
{
    /**
     * @var UserRepositoryContract
     */
    protected $userRepository;

    /**
     * @param UserRepositoryContract $userRepository
     */
    public function __construct(UserRepositoryContract $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Reset password.
     *
     * @param PasswordResetRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset(PasswordResetRequest $request)
    {
        $credentials = $request->only([
            'token',
            'email',
            'password',
            'password_confirmation'
        ]);

        $response = Password::reset($credentials, function ($userPass, $password) use (&$user) {
            $user = $userPass;
            $this->userRepository->updatePassword($userPass->id, $password);
        });

        if ($response === Password::PASSWORD_RESET) {
            return Response::json(null, 204);
        }

        if ($response === Password::INVALID_TOKEN) {
            return Response::json([
                'message' => 'The given data was invalid.',
                'errors'  => [
                    'token' => 'Invalid token'
                ]
            ], 422);
        }

        return Response::json([
            'message' => 'The given data was invalid.',
            'errors'  => $request->only('email')
        ], 422);
    }
}
