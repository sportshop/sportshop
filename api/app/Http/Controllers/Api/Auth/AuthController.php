<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Repositories\Contracts\UserRepositoryContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Socialite;

class AuthController extends Controller
{
    /**
     * @var UserRepositoryContract
     */
    protected $userRepository;

    /**
     * AuthController constructor.
     *
     * @param UserRepositoryContract $userRepository
     */
    public function __construct(UserRepositoryContract $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param \App\Http\Requests\Auth\LoginRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        $credentials['is_active'] = true;

        if ($request->input('remember_me')) {
            $this->guard()->setTTL(60 * 24 * 365); // 1 year
        }
        $token = $this->guard()->attempt($credentials);

        if (!$token) {
            return Response::jsonError('invalid_credentials', 'Invalid credentials', 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Login by social provider.
     *
     * @param string $provider
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function loginByProvider(string $provider, Request $request)
    {
        // Fetch user from provider.
        $providerResponse = Socialite::driver($provider)
            ->setRequest($request)
            ->stateless()
            ->user();

        // Fetch first name, last name from response
        preg_match('/(.*)\s(.*)/', $providerResponse->name, $full_name);

        // Find user by email.
        $user = $this->userRepository->findByEmail($providerResponse->email);

        // Create new user if not exists.
        if (!$user) {
            $payload = [
                'role_name'  => 'member',
                'email'      => $providerResponse->email,
                'first_name' => $full_name[1],
                'last_name'  => $full_name[2]
            ];

            $user = $this->userRepository->create($payload);
        }

        // Log in user.
        $token = $this->guard()->setTTL(60 * 24 * 365)->login($user);

        return $this->respondWithToken($token);
    }

    /**
     * Refresh a token.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshToken(Request $request)
    {
        if ($request->input('remember_me')) {
            $this->guard()->setTTL(60 * 24 * 365); // 1 year
        }

        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Log out user (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();

        return Response::json(null, 204);
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return Response::json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => $this->guard()->factory()->getTTL(),
            'user'         => $this->guard()->user()
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
