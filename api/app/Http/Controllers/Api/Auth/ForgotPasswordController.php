<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Auth\PasswordEmailRequest;
use App\Mail\ResetPassword;
use App\Repositories\Contracts\UserRepositoryContract;
use App\Services\StoreService;
use Mail;
use Password;
use Response;

class ForgotPasswordController extends ApiController
{
    /**
     * @var UserRepositoryContract
     */
    protected $userRepository;

    /**
     * @var StoreService
     */
    protected $storeService;

    /**
     * @param UserRepositoryContract $userRepository
     * @param StoreService $storeService
     */
    public function __construct(
        UserRepositoryContract $userRepository,
        StoreService $storeService
    ) {
        $this->userRepository = $userRepository;
        $this->storeService = $storeService;
    }

    /**
     * Send a reset link to the given user.
     *
     * @param PasswordEmailRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(PasswordEmailRequest $request)
    {
        $email = $request->input('email');

        $user = $this->userRepository->findByEmail($email);

        $token = Password::createToken($user);

        $url = $this->storeService->url();
        $url .= config('password_reset_url_path');
        $url .= "?modal=reset-password&token=$token&email=$user->email";

        Mail::queue(new ResetPassword($user, $url));

        return Response::json(null, 204);
    }
}
