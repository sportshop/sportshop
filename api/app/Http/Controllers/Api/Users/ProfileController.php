<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Users\PasswordUpdateRequest;
use App\Http\Requests\Users\ProfileUpdateRequest;
use App\Repositories\Contracts\UserRepositoryContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class ProfileController extends ApiController
{
    /**
     * @var UserRepositoryContract
     */
    protected $userRepository;

    /**
     * @param UserRepositoryContract $userRepository
     */
    public function __construct(UserRepositoryContract $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Get authenticated user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProfile()
    {
        $user = Auth::user();

        $criteria = [];

        if ($user->hasAnyRole(['admin', 'manager', 'limited_manager'])) {
            $criteria['with_workday_status'] = true;
        }

        if ($user->hasRole('member')) {
            $criteria['with_sale_percentages'] = true;
        }

        $user = $this->userRepository->findById($user->id, $criteria);

        return Response::json(compact('user'));
    }

    /**
     * Update profile.
     *
     * @param ProfileUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updateProfile(ProfileUpdateRequest $request)
    {
        $user_id = Auth::id();

        $data = $request->only([
            'email',
            'password',
            'first_name',
            'last_name',
            'phone',
            'address',
            'data.company_name',
            'data.store_number'
        ]);

        $result = $this->userRepository->update($user_id, $data);

        return Response::json($result);
    }

    /**
     * Update profile password.
     *
     * @param PasswordUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updatePassword(PasswordUpdateRequest $request)
    {
        $user_id = Auth::id();

        $password = $request->input('password');

        $this->userRepository->updatePassword($user_id, $password);

        return Response::json(null, 204);
    }
}
