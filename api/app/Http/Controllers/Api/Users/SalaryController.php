<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\ApiController;
use App\Repositories\Contracts\UserRepositoryContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class SalaryController extends ApiController
{
    /**
     * @var UserRepositoryContract
     */
    protected $userRepository;

    /**
     * @param UserRepositoryContract $userRepository
     */
    public function __construct(UserRepositoryContract $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Get salary of all users.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsersSalary(Request $request)
    {
        $criteria = $request->only([
            'store_id',
            'manager_id',
            'date'
        ]);
        $criteria['role'] = 'manager,limited_manager';

        $salary = $this->userRepository->getUsersSalary($criteria);

        return Response::json($salary);
    }

    /**
     * Get user salary by year or year+month.
     *
     * @param int $user_id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserSalary(int $user_id, Request $request)
    {
        $year = $request->input('year');
        $month = $request->input('month');

        $criteria = [
            'role'     => 'manager,limited_manager',
            'store_id' => $request->input('store_id')
        ];

        if ($year && $month) {
            $salary = $this->userRepository->getMonthlySalaryOfUser($user_id, $year, $month, $criteria);
        } else {
            $salary = $this->userRepository->getYearlySalaryOfUser($user_id, $year, $criteria);
        }

        return Response::json($salary);
    }
}
