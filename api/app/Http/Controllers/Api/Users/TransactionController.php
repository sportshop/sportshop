<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Users\UserTransactionCreateRequest;
use App\Http\Requests\Users\UserTransactionUpdateRequest;
use App\Repositories\Contracts\UserTransactionRepositoryContract;
use App\Repositories\Criteria\UserTransactionListCriteria;
use Exception;
use Illuminate\Http\Request;
use Response;

class TransactionController extends ApiController
{
    /**
     * @var UserTransactionRepositoryContract
     */
    protected $transactionRepository;

    /**
     * @param UserTransactionRepositoryContract $transactionRepository
     */
    public function __construct(UserTransactionRepositoryContract $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * Get the all user transactions.
     *
     * @param int     $user_id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(int $user_id, Request $request)
    {
        $params = $request->all();
        $params['user_id'] = $user_id;

        $transactions = $this->transactionRepository
            ->pushCriteria(new UserTransactionListCriteria($params))
            ->all();

        return Response::json($transactions);
    }

    /**
     * Create a new user transaction.
     *
     * @param int                          $user_id
     * @param UserTransactionCreateRequest $request
     *
     * @return mixed
     */
    public function store(int $user_id, UserTransactionCreateRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = $user_id;

        try {
            $result = $this->transactionRepository->create($data);

            $result = $result->joinRate('rate')->find($result->id);

            return Response::json($result);
        } catch (Exception $e) {
            return Response::jsonError($e);
        }
    }

    /**
     * Update a user transaction by id.
     *
     * @param int                          $user_id
     * @param int                          $transaction_id
     * @param UserTransactionUpdateRequest $request
     *
     * @return mixed
     */
    public function update(int $user_id, int $transaction_id, UserTransactionUpdateRequest $request)
    {
        $data = $request->all();

        try {
            $result = $this->transactionRepository->update($data, $transaction_id);

            $result = $result->joinRate('rate')->find($result->id);

            return response()->json($result);
        } catch (Exception $e) {
            return response()->jsonError();
        }
    }

    /**
     * Delete a user transaction by id.
     *
     * @param int $user_id
     * @param int $transaction_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $user_id, int $transaction_id)
    {
        if ($this->transactionRepository->delete($transaction_id)) {
            return response()->jsonSuccess(trans('events.deleted'));
        }

        return response()->jsonError();
    }
}
