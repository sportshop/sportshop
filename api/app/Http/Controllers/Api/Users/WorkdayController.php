<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\ApiController;
use App\Repositories\Contracts\UserRepositoryContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class WorkdayController extends ApiController
{
    /**
     * @var UserRepositoryContract
     */
    protected $userRepository;

    /**
     * @param UserRepositoryContract $userRepository
     */
    public function __construct(UserRepositoryContract $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Start workday.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function startWorkday()
    {
        $criteria = ['with_workday_status' => true];

        $user = $this->userRepository->findById(Auth::id(), $criteria);

        if ($user->workday_status == 'started') {
            return Response::jsonForbidden('Session was started');
        }

        $this->userRepository->createWorkdayLog($user->id);

        return Response::json(null, 204);
    }

    /**
     * Finish workday.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function finishWorkday()
    {
        $criteria = ['with_workday_status' => true];

        $user = $this->userRepository->findById(Auth::id(), $criteria);

        if ($user->workday_status == 'not_started') {
            return Response::jsonForbidden('Session wasn\'t started');
        }

        if ($user->workday_status == 'finished') {
            return Response::jsonForbidden('Session was finished');
        }

        $this->userRepository->closeWorkdayLog($user->id);

        return Response::json(null, 204);
    }
}
