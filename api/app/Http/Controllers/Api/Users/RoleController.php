<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\ApiController;
use App\Repositories\Contracts\RoleRepositoryContract;

class RoleController extends ApiController
{
    /**
     * @var RoleRepositoryContract
     */
    protected $roleRepository;

    /**
     * @param RoleRepositoryContract $roleRepository
     */
    public function __construct(RoleRepositoryContract $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * Get the roles without system.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $roles = $this->roleRepository->findCustomRoles();

        return response()->json($roles);
    }
}
