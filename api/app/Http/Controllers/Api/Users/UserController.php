<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Users\UserCreateRequest;
use App\Http\Requests\Users\UserUpdateRequest;
use App\Mail\UserWasCreated;
use App\Models\Store;
use App\Repositories\Contracts\UserRepositoryContract;
use Auth;
use Exception;
use Illuminate\Http\Request;
use Mail;
use Response;

class UserController extends ApiController
{
    /**
     * @var UserRepositoryContract
     */
    protected $userRepository;

    /**
     * @param UserRepositoryContract $userRepository
     */
    public function __construct(UserRepositoryContract $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Find all users.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function findAll(Request $request)
    {
        // Collect query criteria.
        $criteria = $request->only([
            'name',
            'role',
            'discount_id',
            'created_at',
            'purchased_orders_price_min',
            'purchased_orders_price_max',
            'purchased_orders_price_currency_id'
        ]);

        // Collect options.
        $options = [
            'limit' => $request->input('limit'),
            'page'  => $request->input('page')
        ];

        // Retrieve users.
        $users = $this->userRepository->findAll($criteria, $options);

        return Response::json($users);
    }

    /**
     * Get user by id.
     *
     * @param int $user_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function findById(int $user_id)
    {
        $criteria = [
            'with_sale_percentages' => true,
            'with_workday_status'   => true,
        ];

        $user = $this->userRepository->findById($user_id, $criteria);

        if (!$user) {
            return Response::jsonNotFound();
        }

        return Response::json($user);
    }

    /**
     * Create user.
     *
     * @param UserCreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(UserCreateRequest $request)
    {
        $password = str_random(8);

        $data = $request->only([
            'role_name',
            'email',
            'first_name',
            'last_name',
            'nickname',
            'phone',
            'address',
            'data',
            'sale_percentages'
        ]);
        $data['password'] = $password;

        $user = $this->userRepository->create($data);

        $store = Store::first(); // FIXME
        $url = $store->url;
        $url .= '?modal=sign-in';

        Mail::queue(new UserWasCreated($user, $password, $url));

        return Response::json($user);
    }

    /**
     * Update user by id.
     *
     * @param int $user_id
     * @param UserUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(int $user_id, UserUpdateRequest $request)
    {
        $data = $request->only([
            'role_name',
            'email',
            'first_name',
            'last_name',
            'nickname',
            'phone',
            'address',
            'data',
            'sale_percentages'
        ]);

        if (Auth::id() !== $user_id) {
            $data['role'] = $request->input('role');
        }

        try {
            $user = $this->userRepository->update($user_id, $data);

            return Response::json($user);
        } catch (Exception $e) {
            return Response::jsonNotFound();
        }
    }

    /**
     * Delete user by id.
     *
     * @param int $user_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $user_id)
    {
        $user = $this->userRepository->findById($user_id);

        if ($user->hasRole('admin') || $user->id === Auth::id()) {
            return response()->jsonForbidden();
        }

        if ($this->userRepository->delete($user_id)) {
            return Response::jsonDeleted();
        }

        return Response::jsonNotFound();
    }
}
