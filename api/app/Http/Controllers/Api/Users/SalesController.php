<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\ApiController;
use App\Repositories\Contracts\DiscountRepositoryContract;
use App\Repositories\Contracts\UserRepositoryContract;
use App\Repositories\Criteria\UserSaleListCriteria;
use Illuminate\Http\Request;
use Response;

class SalesController extends ApiController
{
    /**
     * @var UserRepositoryContract
     */
    protected $userRepository;

    /**
     * @var DiscountRepositoryContract
     */
    protected $discountRepository;

    /**
     * @param UserRepositoryContract     $userRepository
     * @param DiscountRepositoryContract $discountRepository
     */
    public function __construct(
        UserRepositoryContract $userRepository,
        DiscountRepositoryContract $discountRepository
    ) {
        $this->userRepository = $userRepository;
        $this->discountRepository = $discountRepository;
    }

    /**
     * Get the all user transactions.
     *
     * @param int     $user_id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(int $user_id, Request $request)
    {
        $params = $request->all();

        $user = $this->userRepository
            ->pushCriteria(new UserSaleListCriteria($params))
            ->active()
            ->find($user_id);

        if (!$user) {
            return Response::jsonNotFound(trans('users.not_found'));
        }

        $sales = $user->sales->groupBy('date');

        return Response::json($sales);
    }
}
