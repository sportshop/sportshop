<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\ApiController;
use App\Repositories\Contracts\UserActivityRepositoryContract;
use App\Repositories\Criteria\UserActivityListCriteria;
use Illuminate\Http\Request;

class ActivityController extends ApiController
{
    /**
     * @var UserActivityRepositoryContract
     */
    protected $activityRepository;

    /**
     * @param UserActivityRepositoryContract $activityRepository
     */
    public function __construct(UserActivityRepositoryContract $activityRepository)
    {
        $this->activityRepository = $activityRepository;
    }

    /**
     * Get user activity.
     *
     * @param int     $user_id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(int $user_id, Request $request)
    {
        $params = $request->all();
        $params['user_id'] = $user_id;

        $activity = $this->activityRepository
            ->pushCriteria(new UserActivityListCriteria($params))
            ->all();

        return response()->json($activity);
    }
}
