<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'                 => 'required|email|max:60|unique:users',
            'password'              => 'required|min:6',
            'password_confirmation' => 'required|same:password',
            'first_name'            => 'required|string|between:2,36',
            'last_name'             => 'required|string|between:2,36',
            'phone'                 => 'required|phone:UA',
            'agree'                 => 'required|boolean'
        ];
    }
}
