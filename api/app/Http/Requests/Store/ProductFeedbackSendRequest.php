<?php

namespace App\Http\Requests\Store;

use Illuminate\Foundation\Http\FormRequest;

class ProductFeedbackSendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required|string|between:3,72',
            'email'   => 'sometimes|required|email|max:60',
            'phone'   => 'phone:UA',
            'message' => 'required|string|min:3'
        ];
    }
}
