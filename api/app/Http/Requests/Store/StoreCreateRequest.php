<?php

namespace App\Http\Requests\Store;

use Illuminate\Foundation\Http\FormRequest;

class StoreCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required|string',
            'description'  => 'string|nullable',
            'address'      => 'required|string',
            'website_name' => 'string|nullable|unique:stores,website_name',
            'url'          => 'url|nullable|unique:stores,url',
            'theme'        => 'required|string',
            'logo_id'      => 'required|integer|exists:media,id',
            'favicon_id'   => 'required|integer|exists:media,id'
        ];
    }
}
