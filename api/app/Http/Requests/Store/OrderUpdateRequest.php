<?php

namespace App\Http\Requests\Store;

use App\Models\Order;
use Illuminate\Foundation\Http\FormRequest;

class OrderUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $paymentMethods = implode(',', Order::paymentMethods());
        $deliveryMethods = implode(',', Order::deliveryMethods());

        return [
            'store_id'             => 'integer|exists:stores,id',
            'manager_id'           => 'integer|exists:users,id',
            'customer.id'          => 'nullable|integer|exists:users,id',
            'customer.email'       => 'nullable|email',
            'customer.first_name'  => 'nullable|string|max:36',
            'customer.last_name'   => 'nullable|string|max:36',
            'customer.phone'       => 'nullable|phone:UA',
            'payment.method'       => 'required|in:' . $paymentMethods,
            'delivery.method'      => 'in:' . $deliveryMethods,
            'delivery.city'        => 'string|nullable|required_if:delivery.method,' . Order::DELIVERY_METHOD_NEW_POST,
            'delivery.department'  => 'string|nullable|required_if:delivery.method,' . Order::DELIVERY_METHOD_NEW_POST,
            'delivery.address'     => 'string|nullable|required_if:delivery.method,' . Order::DELIVERY_METHOD_COURIER,
            'delivery.declaration' => 'string|nullable',
            'notes'                => 'string|nullable',
            'is_from_web_store'    => 'required|boolean',

            'products.*'          => 'required|array',
            'products.*.id'       => 'integer|exists:order_products,id',
            'products.*.size_id'  => 'required|integer|exists:product_sizes,id',
            'products.*.quantity' => 'required|integer',
            'products.*.discount' => 'numeric',
            'currency_id'         => 'required_with:products|integer|exists:currencies,id',

            'payments.*'                  => 'required|array',
            'payments.*.id'               => 'integer|exists:order_payments,id',
            'payments.*.rate.currency_id' => 'required|integer|exists:currencies,id',
            'payments.*.rate.value'       => 'required|numeric',
            'is_withdrawn'                => 'boolean'
        ];
    }

    /**
     * Get the validator instance for the request.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $validator->sometimes([
            'store_id',
            'customer.first_name',
            'customer.last_name',
            'customer.phone'
        ], 'required', function ($input) {
            return $input->is_from_web_store && !array_get($input->customer, 'id');
        });

        $validator->sometimes('delivery.method', 'required', function ($input) {
            return $input->is_from_web_store;
        });

        $validator->sometimes('customer.id', 'required', function ($input) {
            return $input->is_from_web_store
                && !array_get($input->customer, 'first_last')
                && !array_get($input->customer, 'last_last')
                && !array_get($input->customer, 'phone');
        });

        return $validator;
    }
}
