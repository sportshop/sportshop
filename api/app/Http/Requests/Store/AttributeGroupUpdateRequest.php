<?php

namespace App\Http\Requests\Store;

use Illuminate\Foundation\Http\FormRequest;

class AttributeGroupUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                        => 'required|string',
            'attributes.*.id'             => 'integer|exists:attributes,id',
            'attributes.*.name'           => 'required|string',
            'attributes.*.values'         => 'required|array',
            'attributes.*.values.*.id'    => 'integer|exists:attribute_values,id',
            'attributes.*.values.*.value' => 'required|string',
        ];
    }
}
