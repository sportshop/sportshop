<?php

namespace App\Http\Requests\Store;

use Illuminate\Foundation\Http\FormRequest;

class ColorSaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'colors'        => 'required|array',
            'colors.*.id'   => 'integer|exists:colors,id',
            'colors.*.name' => 'string',
            'colors.*.hex'  => 'required|string|size:7'
        ];
    }
}
