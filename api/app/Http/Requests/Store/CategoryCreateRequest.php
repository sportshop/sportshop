<?php

namespace App\Http\Requests\Store;

use Illuminate\Foundation\Http\FormRequest;

class CategoryCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug'             => 'required|string|unique:categories,slug',
            'icon'             => 'string|nullable',
            'name'             => 'required|string',
            'description'      => 'required|string',
            'meta_title'       => 'string|nullable',
            'meta_description' => 'string|nullable',
            'thumbnail_id'     => 'integer|exists:media,id'
        ];
    }
}
