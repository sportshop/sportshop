<?php

namespace App\Http\Requests\Store;

use Illuminate\Foundation\Http\FormRequest;

class OrderRefundRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * TODO: need to check quantity
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'products'            => 'array',
            'products.*.size_id'  => 'required|integer|exists:product_sizes,id',
            'products.*.quantity' => 'required|integer'
        ];
    }
}
