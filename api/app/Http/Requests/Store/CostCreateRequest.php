<?php

namespace App\Http\Requests\Store;

use App\Models\Cost;
use Illuminate\Foundation\Http\FormRequest;

class CostCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'store_id'         => 'required|integer|exists:stores,id',
            'type'             => 'required|in:' . implode(Cost::types(), ','),
            'rate'             => 'required|array',
            'rate.currency_id' => 'required|integer|exists:currencies,id',
            'rate.value'       => 'required|numeric',
            'description'      => 'string|nullable'
        ];
    }
}
