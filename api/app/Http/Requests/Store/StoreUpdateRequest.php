<?php

namespace App\Http\Requests\Store;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required|string',
            'description'  => 'string|nullable',
            'address'      => 'required|string',
            'website_name' => 'string|nullable|unique:stores,website_name,' . $this->route('store_id'),
            'url'          => 'url|nullable|unique:stores,url,' . $this->route('store_id'),
            'theme'        => 'required|string',
            'logo_id'      => 'required|integer|exists:media,id',
            'favicon_id'   => 'required|integer|exists:media,id'
        ];
    }
}
