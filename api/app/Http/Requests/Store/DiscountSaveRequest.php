<?php

namespace App\Http\Requests\Store;

use Illuminate\Foundation\Http\FormRequest;

class DiscountSaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'discounts'               => 'required|array',
            'discounts.*'             => 'required|array',
            'discounts.*.id'          => 'integer',
            'discounts.*.title'       => 'required|string',
            'discounts.*.value'       => 'required|numeric',
            'discounts.*.is_increase' => 'required|boolean',
            'discounts.*.is_default'  => 'required|boolean'
        ];
    }
}
