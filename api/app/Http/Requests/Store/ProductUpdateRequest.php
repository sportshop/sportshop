<?php

namespace App\Http\Requests\Store;

use App\Models\Product;
use App\Models\ProductUniform;
use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $types = implode(Product::types(), ',');
        $mediaTags = implode(Product::mediaTags(), ',');
        $uniformTypes = implode(ProductUniform::types(), ',');
        $uniformBuilderPositions = implode(ProductUniform::positions(), ',');

        return [
            'brand_id'         => 'required|integer|exists:brands,id',
            'slug'             => 'required|string|unique:products,slug,' . $this->route('product_id'),
            'sku'              => 'required|string|unique:products,sku,' . $this->route('product_id'),
            'warehouse_number' => 'nullable|string',
            'name'             => 'required|string|between:2,255',
            'description'      => 'required|string',
            'type'             => 'required|in:' . $types,
            'meta_title'       => 'string|max:255|nullable',
            'meta_description' => 'string|max:255|nullable',
            'is_active'        => 'required|boolean',
            'is_visible'       => 'required|boolean',

            'attributes'      => 'array',
            'attributes.*.id' => 'required|integer|exists:attribute_values,id',

            'categories'              => 'required|array',
            'categories.*.id'         => 'required|integer|exists:categories,id',
            'categories.*.is_default' => 'boolean',

            'colors'      => 'array',
            'colors.*.id' => 'required|integer|exists:colors,id',

            'cost_price.currency_id' => 'integer|exists:currencies,id',
            'cost_price.value'       => 'numeric',

            'price.currency_id' => 'integer|exists:currencies,id',
            'price.value'       => 'numeric',

            'media'       => 'required|array',
            'media.*.id'  => 'required|integer|exists:media,id',
            'media.*.tag' => 'required|in:' . $mediaTags,

            'sizes'            => 'required_with:cost_price|array',
            'sizes.*.size_id'  => 'required|integer|exists:sizes,id',
            'sizes.*.store_id' => 'required|integer|exists:stores,id',
            'sizes.*.quantity' => 'required|integer',

            'stores'                                => 'required_with:cost_price|array',
            'stores.*.store_id'                     => 'required|integer|exists:stores,id',
            'stores.*.currency_id'                  => 'required|integer|exists:currencies,id',
            'stores.*.markup'                       => 'required|numeric',
            'stores.*.sale_discount'                => 'required_if:type,' . Product::TYPE_SALE . '|numeric',
            'stores.*.max_sale_discount'            => 'required_if:type,' . Product::TYPE_MAX_SALE . '|numeric',
            'stores.*.markup_price'                 => 'required|numeric',
            'stores.*.sale_price'                   => 'required_if:type,' . Product::TYPE_SALE . '|numeric',
            'stores.*.max_sale_price'               => 'required_if:type,' . Product::TYPE_MAX_SALE . '|numeric',
            'stores.*.user_discounts'               => 'required_unless:type,' . Product::TYPE_MAX_SALE . '|array',
            'stores.*.user_discounts.*.discount_id' => 'required|integer|exists:discounts,id',
            'stores.*.user_discounts.*.user'        => 'required|numeric',
            'stores.*.user_discounts.*.sale'        => 'required_if:type,' . Product::TYPE_SALE . '|numeric',

            'uniform.category_id' => 'required_if:uniform.is_visible,true|exists:categories,id',
            'uniform.type'        => 'required_if:uniform.is_visible,true|in:' . $uniformTypes,
            'uniform.position'    => 'required_if:uniform.is_visible,true|in:' . $uniformBuilderPositions,
            'uniform.model'       => 'required_if:uniform.is_visible,true|string|max:255',
            'uniform.is_visible'  => 'boolean'
        ];
    }
}
