<?php

namespace App\Http\Requests\Store;

use App\Models\SizeGroup;
use Illuminate\Foundation\Http\FormRequest;

class SizeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $types = implode(',', SizeGroup::TYPES);

        return [
            'brand_id'               => 'required|exists:brands,id',
            'type'                   => 'required|in:' . $types,
            'name'                   => 'required|string',
            'sizes'                  => 'required|array',
            'sizes.*.values'         => 'required|array',
            'sizes.*.values.*.title' => 'required|string',
            'sizes.*.values.*.value' => 'required|string'
        ];
    }
}
