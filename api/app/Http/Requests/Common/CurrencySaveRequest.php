<?php

namespace App\Http\Requests\Common;

use Illuminate\Foundation\Http\FormRequest;

class CurrencySaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'currencies'              => 'required|array',
            'currencies.*'            => 'required|array',
            'currencies.*.id'         => 'integer',
            'currencies.*.name'       => 'required|string',
            'currencies.*.code'       => 'required|string|max:16',
            'currencies.*.symbol'     => 'required|string|max:10',
            'currencies.*.value'      => 'required|numeric',
            'currencies.*.is_default' => 'boolean'
        ];
    }
}
