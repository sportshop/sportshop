<?php

namespace App\Http\Requests\Common;

use Illuminate\Foundation\Http\FormRequest;

class BannerCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url'       => 'url|nullable',
            'title'     => 'string|nullable',
            'is_active' => 'required|boolean',
            'image_id'  => 'required|integer|exists:media,id',

            'price'             => 'array',
            'price.currency_id' => 'required_with:cost_price|integer|exists:currencies,id',
            'price.value'       => 'required_with:cost_price|numeric'
        ];
    }
}
