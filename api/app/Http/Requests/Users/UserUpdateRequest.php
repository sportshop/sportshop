<?php

namespace App\Http\Requests\Users;

use App\Models\Role;
use App\Models\UserData;
use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $roles = implode(Role::nonSystemRoles(), ',');
        $rateTypes = implode(UserData::rateTypes(), ',');

        return [
            'role_name'  => 'required|in:' . $roles,
            'email'      => 'required|email|max:60|unique:users,email,' . $this->route('user_id'),
            'first_name' => 'required|string|between:2,36',
            'last_name'  => 'required|string|between:2,36',
            'nickname'   => 'string:between:3:40|nullable',
            'phone'      => 'phone:UA|nullable',
            'address'    => 'required|string|min:5|nullable',

            'data.discount_id'       => 'required_if:role_name,member|integer|exists:discounts,id',
            'data.company_name'      => 'required|string',
            'data.store_number'      => 'required|string',
            'data.is_show_sizes'     => 'boolean',
            'data.rate_type'         => 'required_unless:role_name,member|in:' . $rateTypes,
            'data.rate'              => 'required_unless:role_name,member|array|nullable',
            'data.rate.currency_id'  => 'required_unless:role_name,member|integer|exists:currencies,id',
            'data.rate.value'        => 'required_unless:role_name,member|numeric',

            'sale_percentages'               => 'required_unless:role_name,member|array',
            'sale_percentages.*.discount_id' => 'integer|exists:discounts,id',
            'sale_percentages.*.value'       => 'required|numeric'
        ];
    }
}
