<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class UserTransactionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment'          => 'string',
            'payed_at'         => 'required|date_format:Y-m-d H:i:s',
            'rate'             => 'required|array',
            'rate.currency_id' => 'required|integer|exists:currencies,id',
            'rate.value'       => 'required|numeric'
        ];
    }
}
