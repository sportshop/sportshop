<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'            => 'required|email|max:60|unique:users,email,' . Auth::id(),
            'current_password' => 'check_password',
            'first_name'       => 'required|string|between:3,20',
            'last_name'        => 'required|string|between:3,20',
            'phone'            => 'phone:UA|nullable',
            'address'          => 'string|min:5|nullable',

            'data.company_name' => 'string',
            'data.store_number' => 'string',
        ];
    }

    /**
     * Get the validator instance for the request.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $validator->sometimes('current_password', 'required', function ($input) {
            return (bool) $input->password || ($input->email != Auth::user()->email);
        });

        $validator->sometimes('address', 'required', function ($input) {
            return Auth::user()->hasRole('member');
        });

        $validator->sometimes([
            'data.company_name',
            'data.store_number'
        ], 'required', function () {
            return !Auth::user()->hasAnyRole(['admin', 'member']);
        });

        return $validator;
    }
}
