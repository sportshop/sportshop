<?php

namespace App\Http\Requests\Users;

use App\Models\UserTransaction;
use Illuminate\Foundation\Http\FormRequest;

class UserTransactionCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $types = implode(',', UserTransaction::types());

        return [
            'type'             => 'required|string|in:' . $types,
            'comment'          => 'string',
            'payed_at'         => 'required|date_format:Y-m-d H:i:s',
            'rate'             => 'required|array',
            'rate.currency_id' => 'required|integer|exists:currencies,id',
            'rate.value'       => 'required|numeric',
            'sale_percentage'  => 'numeric'
        ];
    }
}
