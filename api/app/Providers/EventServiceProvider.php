<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        // Order
        'App\Events\OrderWasAccepted' => [],
        'App\Events\OrderWasCanceled' => [],
        'App\Events\OrderWasCompleted' => [],
        'App\Events\Order\OrderWasCreated' => [
            'App\Listeners\Order\SendEmailsWhenOrderCreatedListener'
        ],
        'App\Events\OrderWasRefunded' => [],
        'App\Events\OrderWasSent' => [],
        'App\Events\OrderWasUpdated' => [],

        // Brand
        'App\Events\Brand\BrandWasCreated' => [],
        'App\Events\Brand\BrandWasUpdated' => [],
        'App\Events\Brand\BrandWasDeleted' => [],

        // Category
        'App\Events\Category\CategoryWasCreated' => [],
        'App\Events\Category\CategoryWasUpdated' => [],
        'App\Events\Category\CategoryWasDeleted' => [],

        // Color
        'App\Events\Currency\ColorsWasUpdated' => [],

        // Currency
        'App\Events\Currency\CurrenciesWasUpdated' => [],

        // Discount
        'App\Events\Discount\DiscountsWasUpdated' => [],

        // Settings
        'App\Events\Settings\SettingsWasUpdated' => [],

        // Store
        'App\Events\Store\StoreWasCreated' => [],
        'App\Events\Store\StoreWasUpdated' => [],
        'App\Events\Store\StoreWasDeleted' => [],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
