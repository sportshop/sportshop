<?php

namespace App\Providers;

use App\Repositories\Contracts\SettingsRepositoryContract;
use App\Repositories\Contracts\StoreRepositoryContract;
use App\Services\CurrencyService;
use App\Services\RateService;
use App\Services\SettingsService;
use App\Services\StoreService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \App\Models\Currency::observe(\App\Observers\CurrencyObserver::class);
        \App\Models\Discount::observe(\App\Observers\DiscountObserver::class);
        \App\Models\Order::observe(\App\Observers\OrderObserver::class);
        \App\Models\Product::observe(\App\Observers\ProductObserver::class);
        \App\Models\ProductSize::observe(\App\Observers\ProductSizeObserver::class);
        \App\Models\Store::observe(\App\Observers\StoreObserver::class);
        \App\Models\UserActivity::observe(\App\Observers\UserActivityObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SettingsService::class, function ($app) {
            return new SettingsService(
                $app->make(SettingsRepositoryContract::class)
            );
        });

        $this->app->singleton(CurrencyService::class, function () {
            return new CurrencyService();
        });

        $this->app->singleton(RateService::class, function () {
            return new RateService(new CurrencyService());
        });

        $this->app->singleton(StoreService::class, function ($app) {
            return new StoreService($app->make(StoreRepositoryContract::class));
        });
    }
}
