<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Response;

class ResponseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('jsonSuccess', function (string $message, int $statusCode = 200) {
            return Response::json(compact('message'), $statusCode);
        });

        Response::macro('jsonCreated', function ($data) {
            return Response::json($data, 201);
        });

        Response::macro('jsonDeleted', function () {
            return Response::json(null, 204);
        });

        // Error: Something went wrong
        Response::macro('jsonError', function (string $code = null, string $message = null, int $statusCode = 500) {
            return Response::json([
                'code'    => $code ?? 'server_error',
                'message' => $message ?? 'Something went wrong'
            ], $statusCode);
        });

        // Error: Not Found
        Response::macro('jsonNotFound', function (string $message = 'Not Found', string $code = 'not_found', int $statusCode = 404) {
            return Response::jsonError(
                $code,
                $message,
                $statusCode
            );
        });

        // Error: Forbidden
        Response::macro('jsonForbidden', function (string $message = null, string $code = null, int $statusCode = 403) {
            return Response::jsonError(
                $code ?? 'permission_denied',
                $message ?? 'Permission denied',
                $statusCode
            );
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
