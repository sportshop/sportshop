<?php

namespace App\Providers;

use App\Repositories\Criteria\ActiveCriteria;
use App\Repositories\Criteria\DefaultCriteria;
use Freevital\Repository\Eloquent\BaseRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryMacroServiceProvider extends ServiceProvider
{
    /**
     * Register the application's repository macros.
     *
     * @return void
     */
    public function boot()
    {
        BaseRepository::macro('active', function (BaseRepository $repository) {
            $repository->pushCriteria(new ActiveCriteria());
        });
        BaseRepository::macro('default', function (BaseRepository $repository) {
            $repository->pushCriteria(new DefaultCriteria());
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
