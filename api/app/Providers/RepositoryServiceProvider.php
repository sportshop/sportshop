<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * @var string
     */
    private $contractsNamespace = 'App\Repositories\Contracts';

    /**
     * @var string
     */
    private $classesNamespace = 'App\Repositories\Eloquent';

    /**
     * @var array
     */
    private $repositories = [
        'AttributeRepository',
        'BannerRepository',
        'BrandRepository',
        'CategoryRepository',
        'ColorRepository',
        'CostRepository',
        'CurrencyRepository',
        'DiscountRepository',
        'MediaRepository',
        'OrderPaymentRepository',
        'OrderProductRepository',
        'OrderRepository',
        'PageRepository',
        'PermissionRepository',
        'ProductRepository',
        'RoleRepository',
        'SettingsRepository',
        'SizeRepository',
        'StoreRepository',
        'TransferRepository',
        'UserActivityRepository',
        'UserRepository',
        'UserTransactionRepository'
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->repositories as $class) {
            $this->app->bind(
                $this->contractsNamespace . '\\' . $class . 'Contract',
                $this->classesNamespace . '\\' . $class
            );
        }
    }
}
