<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('check_password', 'App\Validators\PasswordValidator@validate');

        Validator::extend('product_size_quantity', 'App\Validators\ProductSizeQuantityValidator@validate');
        Validator::replacer('product_size_quantity', 'App\Validators\ProductSizeQuantityValidator@replacer');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
