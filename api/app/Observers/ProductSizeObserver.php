<?php

namespace App\Observers;

use App\Models\ProductSize;
use App\Models\StoreProductTransfer;
use Illuminate\Support\Facades\Auth;

class ProductSizeObserver
{
    /**
     * @param ProductSize $model
     */
    public function saving(ProductSize $model)
    {
        if ($model->quantity && $model->deleted_at) {
            $model->deleted_at = null;
        }
    }

    /**
     * Listen to the ProductSize saved event.
     *
     * @param \App\Models\ProductSize $model
     * @return void
     */
    public function saved(ProductSize $model)
    {
        $product = $model->product;

        $totalQuantity = $product->product_sizes->sum('quantity');

        if ($product->is_active && !$totalQuantity) {
            $product->is_active = false;
        } else if (!$product->is_active && $totalQuantity) {
            $product->is_active = true;
        }

        if ($product->isDirty('is_active')) {
            $product->save();
        }
    }

    /**
     * Listen to the ProductSize deleting event.
     *
     * @param ProductSize $model
     * @throws \Exception
     */
    public function deleting(ProductSize $model)
    {
        $this->addTransferRecordAboutDeletingSize($model);
    }

    /**
     * Add record to product transfers.
     *
     * @param ProductSize $model
     * @throws \Exception
     */
    private function addTransferRecordAboutDeletingSize(ProductSize $model)
    {
        $transferModel = new StoreProductTransfer();
        $transferModel->fill([
            'to_store_id'     => $model->store_id,
            'product_size_id' => $model->id,
            'user_id'         => Auth::user()->id,
            'action'          => StoreProductTransfer::ACTION_DELETE,
            'quantity'        => $model->quantity
        ]);
        $transferModel->save();
    }
}