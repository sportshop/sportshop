<?php

namespace App\Observers;

use App\Models\Order;
use App\Models\OrderPayment;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class OrderObserver
{
    /**
     * Listen to the Order saving event.
     *
     * @param \App\Models\Order $model
     *
     * @return void
     */
    public function saving(Order $model)
    {
        // Set "new" status for new order.
        if (!$model->id) {
            $model->status = Order::STATUS_NEW;
        }

        // If status is "new" and order has manager, set "accepted" status.
        if ($model->status === Order::STATUS_NEW && $model->manager_id) {
            $model->status = Order::STATUS_ACCEPTED;
        }

        // If order is not completed.
        // Set payment status by calculating payments and order price.
        if (
            $model->status !== Order::STATUS_COMPLETED
            && $model->status !== Order::STATUS_CANCELED
            && $model->status !== Order::STATUS_REFUNDED
            && $model->status !== Order::STATUS_PARTIALLY_REFUNDED
        ) {
            $payment = $model->payment;
            $payment['status'] = Order::PAYMENT_STATUS_NOT_PAID;

            if ($model->id && !$model->payments->isEmpty()) {
                $totalPrice = $model
                    ->select('orders.id')
                    ->joinRate('price')
                    ->where('orders.id', $model->id)
                    ->value('price');

                $totalPrice = $totalPrice->where('is_default', true)->first();

                if ($totalPrice) {
                    $paymentsSum = 0;

                    $paymentIds = $model->payments->pluck('id');
                    $payments = OrderPayment::query()
                        ->joinRate('rate')
                        ->whereIn('order_payments.id', $paymentIds)
                        ->get();

                    foreach ($payments as $paymentModel) {
                        $paymentsSum += $paymentModel->rate[$totalPrice['currency_id']]['value'];
                    }

                    if (round($totalPrice['value']) <= $paymentsSum) {
                        $payment['status'] = Order::PAYMENT_STATUS_PAID;
                    } else {
                        $payment['status'] = Order::PAYMENT_STATUS_PARTIALLY_PAID;
                    }
                }
            }
            $model->payment = $payment;
        }

        // Set "sent" status.
        if (
            $model->status === Order::STATUS_ACCEPTED
            && $model->manager_id
            && $model->delivery
            && $model->delivery['method'] === Order::DELIVERY_METHOD_NEW_POST
            && isset($model->delivery['declaration'])
            && (
                $model->payment['method'] != Order::PAYMENT_METHOD_CARD
                || ($model->payment['method'] == Order::PAYMENT_METHOD_CARD && $model->is_paid)
            )
        ) {
            $model->status = Order::STATUS_SENT;
        }

        // If order was paid completely.
        if ($model->is_paid) {

            // Set "completed" status if delivery method is "pick up".
            if (
                $model->status === Order::STATUS_ACCEPTED
                && $model->delivery
                && $model->delivery['method'] === Order::DELIVERY_METHOD_PICKUP
            ) {
                $model->status = Order::STATUS_COMPLETED;
            }

            // Set "completed" status if order was created from local store.
            if (
                $model->status === Order::STATUS_ACCEPTED
                && !$model->is_from_web_store
            ) {
                $model->status = Order::STATUS_COMPLETED;
            }

            // Set "completed" status if order was sent.
            if ($model->status === Order::STATUS_SENT) {
                $model->status = Order::STATUS_COMPLETED;
            }
        }

        // Save manager's sale percentages.
        if ($model->status == Order::STATUS_COMPLETED && $model->isDirty('status')) {
            $sale_percentages = $model->manager->sale_percentages;

            if (!$sale_percentages->isEmpty()) {
                $percentage = null;

                $customer = $model->customer;

                if (!empty($customer) && isset($customer['discount'])) {
                    $percentage = $sale_percentages->where('discount_id', $customer['discount']['id'])->first();
                } else {
                    $percentage = $sale_percentages->where('is_local_store', true)->first();
                }

                $percentage = $percentage->value;

                $model->manager_data = [
                    'sale_percentages' => $percentage
                ];
            }
        }
    }
}
