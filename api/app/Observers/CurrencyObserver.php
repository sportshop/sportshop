<?php

namespace App\Observers;

use App\Models\Currency;
use App\Models\EntityRate;
use App\Models\Product;
use App\Services\ProductPriceService;
use Illuminate\Support\Facades\DB;

class CurrencyObserver
{
    /**
     * Re-calculate product prices when currency was updated.
     *
     * @param Currency $model
     *
     * @throws \Exception
     */
    public function updated(Currency $model)
    {
        if ($model->isDirty('value')) {

            DB::beginTransaction();

            try {

                // Retrieve products.
                $products = Product::query()
                    ->joinRate('cost_price')
                    ->with(['stores' => function ($query) {
                        $query
                            ->joinRate('markup_price')
                            ->joinRate('max_sale_price')
                            ->joinRate('sale_price')
                            ->with([
                                'user_discounts' => function ($query) {
                                    $query
                                        ->joinRate('price')
                                        ->joinRate('sale_price');
                                }
                            ]);
                    }])
                    ->whereRaw("JSON_EXTRACT(price, '$.\"{$model->id}\".is_default') = true")
                    ->get();

                foreach ($products as $product) {

                    $cost_price = $product->cost_price;

                    // Convert price to array.
                    if ($cost_price instanceof Collection) {
                        $cost_price = $cost_price->toArray();
                    } elseif ($cost_price instanceof \stdClass) {
                        $cost_price = json_decode(json_encode($cost_price), true);
                    }

                    // Retrieve cost price in default currency.
                    $cost_price = array_first($cost_price, function($item) {
                        return $item['is_default'];
                    });

                    // Update product cost price value.
                    $product->cost_price = [
                        'value'       => $cost_price['value'],
                        'currency_id' => $cost_price['currency_id'],
                        'tag'         => 'cost_price',
                    ];
                    $product->save();
                    $cost_price = $product->joinRate('cost_price')->where('products.id', $product->id)->value('cost_price');

                    foreach ($product->stores as $store) {

                        $price_service = new ProductPriceService($cost_price, $store->currency_id);

                        // Update markup, max_sale and sale prices.
                        $store->markup_price = $price_service->calculateMarkupPrice($store->markup);
                        $store->sale_price = $price_service->calculateSalePrice($store->markup_price['value'], $store->sale_discount);
                        $store->max_sale_price = $price_service->calculateMaxSalePrice($store->max_sale_discount);
                        $store->save();

                        foreach ($store->user_discounts as $user_discount) {

                            // Update user discount prices.
                            $prices = $price_service->calculateDiscountPrices($store, $user_discount);

                            $user_discount->price = $prices['price'];
                            $user_discount->sale_price = $prices['sale_price'];
                            $user_discount->save();
                        }
                    }
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();

                throw $e;
            }
        }
    }

    /**
     * Listen to the Currency deleted event.
     *
     * @param Currency $model
     *
     * @throws \Throwable
     */
    public function deleting(Currency $model)
    {
        $rates = EntityRate::all();

        DB::transaction(function () use ($model, $rates) {
            foreach ($rates as $rateModel) {
                $rate = $rateModel->price;

                unset($rate->{$model->id});

                $rateModel->price = $rate;
                $rateModel->save();
            }
        });
    }
}