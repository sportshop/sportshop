<?php

namespace App\Observers;

use App\Models\Product;

class ProductObserver
{
    /**
     * @param Product $model
     */
    public function deleting(Product $model)
    {
        $model->product_sizes->each(function ($item) {
            $item->delete();
        });
    }
}