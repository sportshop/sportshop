<?php

namespace App\Observers;

use App\Models\Discount;
use App\Models\Product;
use App\Services\ProductPriceService;
use Illuminate\Support\Facades\DB;

class DiscountObserver
{
    /**
     * Update product discount prices.
     *
     * @param Discount $model
     *
     * @throws \Exception
     */
    public function updated(Discount $model)
    {
        if ($model->isDirty('is_increase')) {

            DB::beginTransaction();

            try {
                // Retrieve products with cost price, stores prices: markup, sale, max sale and discounts prices.
                $products = Product::query()
                    ->joinRate('cost_price')
                    ->with(['stores' => function ($query) use ($model) {
                        $query
                            ->joinRate('markup_price')
                            ->joinRate('max_sale_price')
                            ->joinRate('sale_price')
                            ->with(['user_discounts' => function ($query) use ($model) {
                                $query
                                    ->with('discount')
                                    ->joinRate('price')
                                    ->joinRate('sale_price')
                                    ->where('discount_id', $model->id);
                            }]);
                    }])
                    ->get();

                foreach ($products as $product) {
                    foreach($product->stores as $store) {

                        $price_service = new ProductPriceService($product->cost_price, $store->currency_id);

                        $user_discount = $store->user_discounts->first();

                        // Update user discount prices.
                        $prices = $price_service->calculateDiscountPrices($store, $user_discount);

                        $user_discount->price = $prices['price'];
                        $user_discount->sale_price = $prices['sale_price'];
                        $user_discount->save();
                    }
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();

                throw $e;
            }
        }
    }
}