<?php

namespace App\Observers;

use App\Models\UserActivity;

class UserActivityObserver
{
    /**
     * Listen to the UserActivity created event.
     *
     * @param UserActivity $model
     *
     * @return void
     */
    public function creating(UserActivity $model)
    {
        $model->created_at = $model->freshTimestamp();
    }
}