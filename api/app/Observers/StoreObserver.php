<?php

namespace App\Observers;

use App\Models\Store;

class StoreObserver
{
    /**
     * Listen to the Store created event.
     *
     * @param Store $store
     *
     * @return void
     */
    public function creating(Store $store)
    {
        $store->is_active = true;
    }
}