<?php

namespace App\Services;

class RateService
{
    /**
     * @var CurrencyService
     */
    protected $currencyService;

    /**
     * @param CurrencyService $currencyService
     */
    public function __construct(CurrencyService $currencyService)
    {
        $this->currencyService = $currencyService;
    }
}