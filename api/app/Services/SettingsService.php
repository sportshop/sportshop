<?php

namespace App\Services;

use App\Repositories\Contracts\SettingsRepositoryContract;

class SettingsService
{
    /**
     * @var SettingsRepositoryContract
     */
    protected $settingsRepository;

    /**
     * @param SettingsRepositoryContract $settingsRepository
     */
    public function __construct(SettingsRepositoryContract $settingsRepository)
    {
        $this->settingsRepository = $settingsRepository;
    }

    /**
     * Get the value of an attribute.
     *
     * @param string $key
     * @return mixed
     */
    public function get(string $key)
    {
        return $this->settingsRepository->findByName($key);
    }
}