<?php

namespace App\Services;

use App\Exceptions\NoRequestRefererException;
use App\Models\Store;
use App\Repositories\Contracts\StoreRepositoryContract;
use DB;

class StoreService
{
    /**
     * @var StoreRepositoryContract
     */
    protected $storeRepository;

    /**
     * @var CurrencyService
     */
    protected $current;

    /**
     * @var null|string|string[]
     */
    protected $dashboard_url;

    /**
     * StoreService constructor.
     *
     * @param StoreRepositoryContract $storeRepository
     */
    public function __construct(StoreRepositoryContract $storeRepository)
    {
        $this->storeRepository = $storeRepository;

        $this->dashboard_url = preg_replace('/https?:\/\/(.*)/', '${1}', config('app.dashboard_url'));
    }

    /**
     * Determine if given url equal dashboard url.
     *
     * @param $url
     *
     * @return bool
     */
    public function isDashboard($url)
    {
        $url = preg_replace('/https?:\/\/(.*)/', '${1}', $url);

        return $url === $this->dashboard_url;
    }

    /**
     * Get current store data.
     *
     * @return CurrencyService
     */
    public function current()
    {
        return $this->current;
    }

    /**
     * Get current store id.
     *
     * @return CurrencyService
     */
    public function id()
    {
        return $this->current->id;
    }

    /**
     * Get current store url.
     *
     * @return CurrencyService
     */
    public function url()
    {
        return $this->current->url;
    }

    /**
     * Set current store by given url.
     *
     * @param $url
     *
     * @return CurrencyService
     * @throws NoRequestRefererException
     */
    public function setCurrentStore($url)
    {
        $stores = $this->storeRepository->findAll();
        $stores->transform(function ($item) {
            $item->url = preg_replace('/https?:\/\/(.*)/', '${1}', $item->url);

            return $item;
        });

        foreach ($stores as $store) {
            if ($store->url && $store->url == $url) {
                $this->current = $store;

                return $this->current;
            }
        }

        throw new NoRequestRefererException();
    }
}