<?php

namespace App\Services;

use App\Models\Currency as CurrencyModel;
use Exception;
use Session;

class CurrencyService
{
    /**
     * List of all currencies.
     *
     * @var \Illuminate\Support\Collection
     */
    protected $all;

    /**
     * Get all currencies from database.
     *
     * @return mixed
     */
    public function all()
    {
        if (!$this->all) {
            $this->all = CurrencyModel::all();
        }

        return $this->all;
    }

    /**
     * Get currency by code from list.
     *
     * @param mixed $key
     *
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function get($key)
    {
        if (is_int($key)) {
            $currency = $this->getById($key);
        } else {
            $currency = $this->getByCode($key);
        }

        if (!$currency) {
            throw new Exception('CurrencyService ' . $currency . ' does not exists.');
        }

        return $currency;
    }

    /**
     * @param mixed $key
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function set($key)
    {
        $currency = $this->get($key);

        Session::put('currency', $currency);

        return $currency;
    }

    /**
     * Get current currency from session.
     * If currency doesn't exists in the session, push default currency.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function current()
    {
        if ($currency = Session::get('currency')) {
            return $currency;
        }

        $currency = $this->getDefaultCurrency();

        $this->set($currency->id);

        return $currency;
    }

    /**
     * Get default currency.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function default()
    {
        return $this->getDefaultCurrency();
    }

    /**
     * Get default currency from all list.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function getDefaultCurrency()
    {
        return $this->all()->where('is_default', true)->first();
    }

    /**
     * Get currency by code.
     *
     * @param string $code
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    private function getByCode(string $code)
    {
        return $this->all()->where('code', $code)->first();
    }

    /**
     * Get currency by id.
     *
     * @param int $id
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    private function getById(int $id)
    {
        return $this->all()->where('id', $id)->first();
    }
}