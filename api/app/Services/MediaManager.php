<?php

namespace App\Services;

use Intervention\Image\Facades\Image;
use Plank\Mediable\MediaUploaderFacade as MediaUploader;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MediaManager
{
    const THUMBNAIL_QUALITY = 100;

    /**
     * Upload media file.
     *
     * @param        $file
     * @param string $type
     * @param string $destination
     *
     * @return mixed
     */
    public static function upload($file, string $type, string $destination = 'public')
    {
        $path = $type . '/' . date('Y') . '/' . date('m');

        $media = MediaUploader::fromSource($file)
            ->toDestination($destination, $path)
            ->useFilename(str_random(16))
            ->upload();

        $originalName = self::getOriginalName($file);

        $media->original_filename = $originalName;
        $media->save();

        return $media;
    }

    /**
     * Upload media file from base64.
     *
     * @param        $data
     * @param string $type
     * @param string $destination
     *
     * @return mixed
     */
    public static function uploadFromBlob($data, string $type, string $destination = 'public')
    {
        $tmpFile = tempnam(sys_get_temp_dir(), str_random()) . '.' . mb_strtolower($data['ext']);

        $image = Image::make($data['data']);
        $image->save($tmpFile, 100);

        return self::upload($tmpFile, $type, $destination);
    }

    /**
     * Create a thumbnail and attach to original media file.
     *
     * @param        $media
     * @param string $type
     * @param string $mode
     * @param array  $size
     * @param bool   $need_original
     *
     * @return mixed
     */
    public static function thumbnail($media, string $type, string $mode, array $size, bool $need_original = false)
    {
        $tmpFile = tempnam(sys_get_temp_dir(), 'image') . '.' . $media->extension;

        $image = Image::make($media->getAbsolutePath());
        $image = call_user_func_array([$image, $mode], $size);
        $image->save($tmpFile, self::THUMBNAIL_QUALITY);

        $thumbnail = self::upload($tmpFile, $type);

        if ($need_original) {
            $thumbnail->attachMedia($media, 'original');
        }

        return $thumbnail;
    }

    /**
     * Get the original name of file.
     *
     * @param $file
     *
     * @return null|string
     */
    public static function getOriginalName($file)
    {
        $originalName = '';
        if ($file instanceof UploadedFile) {
            $originalName = $file->getClientOriginalName();
        } else if (is_string($file)) {
            $originalName = pathinfo($file)['basename'];
        }

        return $originalName;
    }
}
