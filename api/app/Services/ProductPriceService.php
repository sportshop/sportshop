<?php

namespace App\Services;

use App\Models\Currency;
use App\Models\Discount;
use App\Models\ProductStoreData;
use App\Models\ProductStoreDiscount;

class ProductPriceService
{
    /**
     * @var float
     */
    protected $cost_price_value;

    /**
     * @var Currency
     */
    protected $cost_price_currency;

    /**
     * @var Currency
     */
    protected $currency;

    /**
     * ProductPriceService constructor.
     *
     * @param $cost_price
     * @param $currency_id
     */
    public function __construct($cost_price, $currency_id)
    {
        $cost_price = $cost_price->first(function ($item) use ($currency_id) {
            return $item['currency_id'] == $currency_id;
        });

        $this->cost_price_value = $cost_price['value'];
        $this->cost_price_currency = Currency::find($cost_price['currency_id']);
        $this->currency = Currency::find($currency_id);
    }

    /**
     * Calculate markup price by cost price and given markup percentages.
     *
     * @param float $markup
     *
     * @return array
     */
    public function calculateMarkupPrice(float $markup)
    {
        $price = $this->cost_price_value + $this->cost_price_value * $markup / 100;

        return [
            'value'       => round($price),
            'currency_id' => $this->currency->id,
            'tag'         => ProductStoreData::PRICE_TAG_MARKUP
        ];
    }

    /**
     * Calculate sale price by given markup price and given discount percentages.
     *
     * @param float $markup_price
     * @param float $discount
     *
     * @return array
     */
    public function calculateSalePrice(float $markup_price, float $discount)
    {
        $price_value = $markup_price - $markup_price * $discount / 100;

        if ($price_value < $this->cost_price_value) {
            $price_value = $this->cost_price_value;
        }

        return [
            'value'       => round($price_value),
            'currency_id' => $this->currency->id,
            'tag'         => ProductStoreData::PRICE_TAG_SALE
        ];
    }

    /**
     * Calculate max sale price by cost price and given discount percentages.
     *
     * @param float $discount
     *
     * @return array
     */
    public function calculateMaxSalePrice(float $discount)
    {
        $price_value = $this->cost_price_value + $this->cost_price_value * $discount / 100;

        return [
            'value'       => round($price_value),
            'currency_id' => $this->currency->id,
            'tag'         => ProductStoreData::PRICE_TAG_MAX_SALE
        ];
    }

    /**
     * Calculate discount prices by given params.
     *
     * @param ProductStoreData $store
     * @param mixed $discount
     *
     * @return array
     */
    public function calculateDiscountPrices(ProductStoreData $store, $discount)
    {
        $store = $store
            ->joinRate('markup_price')
            ->joinRate('sale_price')
            ->where('product_store_data.id', $store->id)
            ->first();

        $default_discount = Discount::find($discount['discount_id']);

        // Retrieve "markup price" in default currency id.
        $markup_price = $store->markup_price->first(function ($item) {
            return $item['currency_id'] == $this->currency->id;
        });

        // Retrieve "sale price" in default currency id.
        // Sale price needs for comparison.
        $sale_price = $store->sale_price->first(function ($item) {
            return $item['currency_id'] == $this->currency->id;
        });

        // Calculate "user" price.
        if ($default_discount->is_increase) {
            $user_price_value = $this->cost_price_value + $this->cost_price_value * $discount['user'] / 100;
        } else {
            $user_price_value = $markup_price['value'] - $markup_price['value'] * $discount['user'] / 100;
        }
        if ($user_price_value >= $markup_price['value'] || $user_price_value <= $this->cost_price_value) {
            $user_price_value = $markup_price['value'];
        }

        // Calculate "user sale" price.
        $user_sale_price_value = $this->cost_price_value + $this->cost_price_value * $discount['sale'] / 100;
        if ($user_sale_price_value >= $user_price_value && $user_price_value < $markup_price['value']) {
            $user_sale_price_value = $user_price_value;
        } else if ($user_sale_price_value > $sale_price['value'] || $user_sale_price_value <= $this->cost_price_value) {
            $user_sale_price_value = $sale_price['value'];
        }

        return [
            'price'      => [
                'value'       => round($user_price_value),
                'currency_id' => $this->currency->id,
                'tag'         => ProductStoreDiscount::PRICE_USER_PRICE
            ],
            'sale_price' => [
                'value'       => round($user_sale_price_value),
                'currency_id' => $this->currency->id,
                'tag'         => ProductStoreDiscount::PRICE_SALE_PRICE
            ]
        ];
    }
}