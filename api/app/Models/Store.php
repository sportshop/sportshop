<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Store extends Model
{
    use Mediable, SoftDeletes;

    /**
     * Media tags.
     */
    const MEDIA_LOGO = 'logo';
    const MEDIA_FAVICON = 'favicon';

    /**
     * {@inheritdoc}
     */
    protected $appends = [
        'logo',
        'favicon'
    ];

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'is_active' => 'boolean'
    ];

    /**
     * {@inheritdoc}
     */
    protected $dates = ['deleted_at'];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'name',
        'description',
        'address',
        'website_name',
        'url',
        'theme'
    ];

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'id',
        'name',
        'description',
        'address',
        'website_name',
        'url',
        'theme',
        'is_active',
        'logo',
        'favicon'
    ];

    /**
     * Retrieve product sizes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function product_sizes()
    {
        return $this->hasMany(ProductSize::class);
    }

    /**
     * Determine if store has product sizes relation records.
     *
     * @return bool
     */
    public function productSizesExists()
    {
        return $this->product_sizes()->exists();
    }

    /**
     * Get logo.
     *
     * @return Media|null
     */
    public function getLogoAttribute()
    {
        if (!$this->relationLoaded('media')) {
            return null;
        }

        return $this->firstMedia(self::MEDIA_LOGO);
    }

    /**
     * Get favicon.
     *
     * @return Media|null
     */
    public function getFaviconAttribute()
    {
        if (!$this->relationLoaded('media')) {
            return null;
        }

        return $this->firstMedia(self::MEDIA_FAVICON);
    }
}
