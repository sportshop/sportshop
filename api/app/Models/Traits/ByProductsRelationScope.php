<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait ByProductsRelationScope
{
    /**
     * Filter by products store and category.
     *
     * @param Builder  $query
     * @param int      $store_id
     * @param int|null $category_id
     *
     * @return Builder|static
     */
    public function scopeByProductsStoreAndCategory(Builder $query, $store_id, $category_id = null)
    {
        return $query->whereHas('products', function ($query) use ($category_id, $store_id) {
            if ($category_id) {
                $query->whereHas('categories', function ($query) use ($category_id) {
                    return $query->where('id', $category_id);
                });
            }
            $query
                ->where('is_active', true)
                ->has('cost_price')
                ->whereHas('stores', function ($query) use ($store_id) {
                    return $query->where('id', $store_id);
                })
                ->whereHas('product_sizes', function ($query) use ($store_id) {
                    $query->where('store_id', $store_id);
                });;

            return $query;
        });
    }
}
