<?php

namespace App\Models\Traits;

use App\Models\Currency;
use App\Models\EntityRate;
use Closure;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

trait HasRates
{
    /**
     * Save the model and the rate relations to the database.
     *
     * @param array $options
     *
     * @return bool
     * @throws \Exception
     */
    public function save(array $options = [])
    {
        DB::beginTransaction();

        try {
            $rates = [];

            foreach ($this->getRateFields() as $rateField) {
                if (
                    !$this->{$rateField}
                    || !array_get($this->{$rateField}, 'currency_id')
                    || !array_get($this->{$rateField}, 'value')
                ) {
                    $this->offsetUnset($rateField);
                    continue;
                }

                $fieldValue = [
                    'field' => $this->{$rateField}->toArray(),
                    'tag'   => $rateField
                ];

                array_push($rates, $fieldValue);

                $this->offsetUnset($rateField);
            }

            $result = parent::save($options);

            $currencies = Currency::all();

            // Save rate models.
            foreach ($rates as $rateData) {
                $rateModel = EntityRate::firstOrNew([
                    'entity_id'   => $this->getKey(),
                    'entity_type' => self::class,
                    'tag'         => $rateData['tag']
                ]);

                $fieldValue = $rateData['field'];

                $rate = [];

                $currentCurrency = $currencies->where('id', $fieldValue['currency_id'])->first();

                foreach ($currencies as $currency) {
                    $coefficient = $currentCurrency->value / $currency->value;

                    $rate[$currency->id] = [
                        'currency_id' => $currency->id,
                        'value'       => round($fieldValue['value'] * $coefficient, 2),
                        'code'        => $currency->code,
                        'symbol'      => $currency->symbol,
                        'is_default'  => $currentCurrency->id === $currency->id
                    ];
                }

                $rateModel->price = $rate;
                $rateModel->save();
            }

            DB::commit();

            return $result;
        } catch (Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }

    /**
     * Select converted rate field to all currencies.
     *
     * @param $query
     * @param $fieldName
     * @param Closure|null $callback
     */
    public function scopeJoinRate($query, $fieldName, Closure $callback = null)
    {
        $priceTableName = 'entity_rates_' . $fieldName;

        $mainTableColumns = "{$this->getTable()}.*";

        if (!in_array($mainTableColumns, $query->getQuery()->columns ?? [])) {
            $query->addSelect($mainTableColumns);
        }

        $query
            ->addSelect("$priceTableName.price AS $fieldName")
            ->leftJoin('entity_rates AS ' . $priceTableName, function ($join) use ($priceTableName, $fieldName, $callback) {
                if ($callback) {
                    call_user_func($callback, $join, $priceTableName);
                } else {
                    $join->on($this->getQualifiedKeyName(), '=', $priceTableName . '.entity_id')
                        ->where($priceTableName . '.entity_type', self::class)
                        ->where($priceTableName . '.tag', $fieldName);
                }
            });
    }

    /**
     * Get the casts array.
     * Add rate fields to casts.
     *
     * @return array
     */
    public function getCasts()
    {
        $casts = parent::getCasts();

        foreach ($this->getRateFields() as $rateField) {
            $casts[$rateField] = 'collection';
        }

        return $casts;
    }

    /**
     * Get the name of rate fields.
     *
     * @return array
     */
    protected function getRateFields()
    {
        return (array) ($this->rateFields);
    }
}
