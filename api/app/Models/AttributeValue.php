<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeValue extends Model
{
    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'value'
    ];

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'id',
        'value'
    ];

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * Retrieve attribute.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }

    /**
     * Retrieve products.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function products()
    {
        return $this->morphedByMany(Product::class, 'entity', 'x_attribute_value_entity', 'value_id');
    }
}
