<?php

namespace App\Models;

use App\Models\Traits\HasRates;
use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
    use HasRates;

    /**
     * Types of rate.
     */
    const RATE_TYPE_MONTHLY = 'monthly';
    const RATE_TYPE_DAILY = 'daily';
    const RATE_TYPE_HOURLY = 'hourly';

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'is_show_sizes' => 'boolean'
    ];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'discount_id',
        'company_name',
        'store_number',
        'rate_type',
        'is_show_sizes',
        'rate'
    ];

    /**
     * {@inheritdoc}
     */
    public $incrementing = false;

    /**
     * {@inheritdoc}
     */
    protected $primaryKey = 'user_id';

    /**
     * {@inheritdoc}
     */
    protected $rateFields = [
        'rate'
    ];

    /**
     * {@inheritdoc}
     */
    protected $table = 'user_data';

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'company_name',
        'store_number',
        'rate_type',
        'is_show_sizes',
        'rate'
    ];

    /**
     * Retrieve discount.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }

    /**
     * Get type list of rate.
     *
     * @return array
     */
    public static function rateTypes()
    {
        return [
            self::RATE_TYPE_MONTHLY,
            self::RATE_TYPE_DAILY,
            self::RATE_TYPE_HOURLY
        ];
    }
}
