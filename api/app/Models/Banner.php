<?php

namespace App\Models;

use App\Models\Traits\HasRates;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Banner extends Model
{
    use HasRates, Mediable;

    /**
     * Media tags.
     */
    const MEDIA_MAIN = 'main';

    /**
     * {@inheritdoc}
     */
    protected $appends = [
        'image',
        'url',
        'title'
    ];

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'data'       => 'object',
        'sort_order' => 'integer',
        'is_active'  => 'boolean'
    ];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'data',
        'sort_order',
        'is_active',
        'price'
    ];

    /**
     * {@inheritdoc}
     */
    protected $rateFields = [
        'price'
    ];

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'id',
        'url',
        'title',
        'sort_order',
        'is_active',
        'image',
        'price'
    ];

    /**
     * Filter query by criteria.
     *
     * @param $query
     * @param array $criteria
     */
    public function scopeCriteria($query, array $criteria = [])
    {
        // Is active
        if (array_has($criteria, 'active')) {
            $query->where('is_active', array_get($criteria, 'active'));
        }
    }

    /**
     * Get the main media file.
     *
     * @return \App\Models\Media
     */
    public function getImageAttribute()
    {
        if (!$this->relationLoaded('media')) {
            return null;
        }

        return $this->firstMedia(self::MEDIA_MAIN);
    }

    /**
     * Retrieve url from json data.
     *
     * @return string|null
     */
    public function getUrlAttribute()
    {
        if ($this->data && property_exists($this->data, 'url')) {
            return $this->data->url;
        }

        return null;
    }

    /**
     * Retrieve title from json data.
     *
     * @return string|null
     */
    public function getTitleAttribute()
    {
        if ($this->data && property_exists($this->data, 'title')) {
            return $this->data->title;
        }

        return null;
    }
}
