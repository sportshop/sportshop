<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSalePercentage extends Model
{
    /**
     * {@inheritdoc}
     */
    protected $appends = [
        'is_local_store'
    ];

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'value' => 'float'
    ];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'discount_id',
        'value'
    ];

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    public $visible = [
        'discount_id',
        'value',
        'is_local_store'
    ];

    /**
     * Determine if discount id is present.
     *
     * @return bool
     */
    public function getIsLocalStoreAttribute()
    {
        return (bool) !$this->discount_id;
    }
}
