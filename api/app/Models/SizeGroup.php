<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SizeGroup extends Model
{
    /**
     * "clothes", "shoes"
     */
    const TYPES = [
        'shoes',
        'clothes'
    ];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'brand_id',
        'type',
        'name'
    ];

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'id',
        'type',
        'name',
        'sizes'
    ];

    /**
     * Retrieve brand.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * Retrieve sizes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sizes()
    {
        return $this->hasMany(Size::class, 'group_id');
    }

    /**
     * Determine if sizes has product sizes relation records.
     *
     * @return bool
     */
    public function productSizesExists()
    {
        return $this->sizes()->has('product_sizes')->exists();
    }
}
