<?php

namespace App\Models;

use App\Models\Traits\HasRates;
use Illuminate\Database\Eloquent\Model;

class ProductStoreData extends Model
{
    use HasRates;

    /**
     * Price tags.
     */
    const PRICE_TAG_MARKUP = 'markup';
    const PRICE_TAG_SALE = 'sale';
    const PRICE_TAG_MAX_SALE = 'max_sale';

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'markup'            => 'float',
        'max_sale_discount' => 'float',
        'sale_discount'     => 'float'
    ];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'store_id',
        'currency_id',
        'markup',
        'max_sale_discount',
        'sale_discount',
        'markup_price',
        'max_sale_price',
        'sale_price'
    ];

    /**
     * {@inheritdoc}
     */
    protected $rateFields = [
        'markup_price',
        'max_sale_price',
        'sale_price'
    ];

    /**
     * {@inheritdoc}
     */
    protected $table = 'product_store_data';

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'store_id',
        'currency_id',
        'markup',
        'max_sale_discount',
        'sale_discount',
        'user_discounts'
    ];

    /**
     * Retrieve user discounts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user_discounts()
    {
        return $this->hasMany(ProductStoreDiscount::class, 'product_store_id');
    }

    /**
     * Retrieve product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
