<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SizeValue extends Model
{
    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'is_default' => 'boolean'
    ];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'title',
        'value',
        'is_default'
    ];

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'id',
        'title',
        'value',
        'is_default'
    ];

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;
}
