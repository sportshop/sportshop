<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes;

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'is_active' => 'boolean',
        'is_system' => 'boolean'
    ];

    /**
     * {@inheritdoc}
     */
    protected $dates = ['deleted_at'];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'slug',
        'title',
        'description',
        'meta_title',
        'meta_description',
        'is_active'
    ];

    /**
     * {@inheritdoc}
     */
    protected $hidden = [
        'is_system',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Determine if page is system.
     *
     * @return bool
     */
    public function isSystem()
    {
        return $this->getAttribute('is_system');
    }
}
