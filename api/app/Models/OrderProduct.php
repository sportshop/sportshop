<?php

namespace App\Models;

use App\Models\Traits\HasRates;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    use HasRates;

    /**
     * {@inheritdoc}
     */
    protected $appends = [
        'is_refunded',
        'total_quantity',
        'values'
    ];

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'discount'    => 'float'
    ];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'size_id',
        'quantity',
        'discount'
    ];

    /**
     * {@inheritdoc}
     */
    protected $rateFields = [
        'cost_price',
        'sale_price'
    ];

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'id',
        'size_id',
        'quantity',
        'refunded_quantity',
        'discount',
        'is_refunded',
        'sale_price',
        'total_quantity',
        'values'
    ];

    /**
     * Retrieve order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * Retrieve product size.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_size()
    {
        return $this->belongsTo(ProductSize::class, 'size_id');
    }

    /**
     * Except non-refunded products.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function withoutRefundedScope(Builder $query)
    {
        return $query->where('refunded_quantity', 0);
    }

    /**
     * Determine if refunded quantity more than zero.
     *
     * @return bool
     */
    public function getIsRefundedAttribute()
    {
        return (bool) $this->refunded_quantity;
    }

    /**
     * Retrieve total quantity from original product size.
     *
     * @return integer|null
     */
    public function getTotalQuantityAttribute()
    {
        if (!$this->relationLoaded('product_size')) {
            return null;
        }

        return $this->product_size->quantity;
    }

    /**
     * Retrieve original product size values.
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function getValuesAttribute()
    {
        if (!$this->relationLoaded('product_size') || !$this->product_size->relationLoaded('values')) {
            return null;
        }

        return $this->product_size->values;
    }
}
