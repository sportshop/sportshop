<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    /**
     * {@inheritdoc}
     */
    protected $fillable = [];

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'id',
        'values'
    ];

    /**
     * Retrieve size group.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(SizeGroup::class);
    }

    /**
     * Retrieve size values.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function values()
    {
        return $this->hasMany(SizeValue::class);
    }

    /**
     * Retrieve product sizes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function product_sizes()
    {
        return $this->hasMany(ProductSize::class, 'size_id', 'id');
    }

    /**
     * Determine if size has product sizes relation records.
     *
     * @return bool
     */
    public function productSizesExists()
    {
        return !!$this->product_sizes()->sum('quantity');
    }
}
