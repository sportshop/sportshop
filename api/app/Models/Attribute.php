<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'type',
        'name'
    ];

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'id',
        'name',
        'values'
    ];

    /**
     * Retrieve attribute group.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(AttributeGroup::class, 'group_id');
    }

    /**
     * Retrieve attribute values.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function values()
    {
        return $this->hasMany(AttributeValue::class, 'attribute_id');
    }
}
