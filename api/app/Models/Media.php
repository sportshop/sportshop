<?php

namespace App\Models;
use Illuminate\Support\Facades\Config;
use Plank\Mediable\Media as MediableModel;
use Plank\Mediable\Mediable;

class Media extends MediableModel
{
    use Mediable;

    /**
     * {@inheritdoc}
     */
    protected $appends = [
        'url',
        'tag',
        'path'
    ];

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'id',
        'original_filename',
        'url',
        'path',
        'tag'
    ];

    /**
     * Get the url of file.
     *
     * @return string
     * @throws \Plank\Mediable\Exceptions\MediaUrlException
     */
    public function getUrlAttribute()
    {
        $url = $this->getUrl();

        if (Config::get('app.env') === 'production') {
            return str_replace('http://', 'https://', $url);
        }

        return $url;
    }

    /**
     * Get the tag of file.
     *
     * @return null|string
     */
    public function getTagAttribute()
    {
        if (!$this->pivot) {
            return null;
        }

        return $this->pivot->tag;
    }

    /**
     * Determine if media has relations without given tags.
     *
     * @param array $tags
     *
     * @return mixed
     */
    public function hasMediaWithout($tags = [])
    {
        return $this->media()->whereNotIn('tag', (array) $tags)->exists();
    }

    /**
     * Retrieve disk path.
     *
     * @return string
     */
    public function getPathAttribute()
    {
        return $this->getDiskPath();
    }
}
