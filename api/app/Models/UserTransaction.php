<?php

namespace App\Models;

use App\Models\Traits\HasRates;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserTransaction extends Model
{
    use HasRates, SoftDeletes;

    /**
     * Transaction types.
     */
    const TYPE_PREPAYMENT = 'prepayment';
    const TYPE_ADDITIONAL = 'additional';
    const TYPE_FINAL = 'final';

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'payed_at' => 'date'
    ];

    /**
     * {@inheritdoc}
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'comment',
        'payed_at',
        'rate'
    ];

    /**
     * {@inheritdoc}
     */
    protected $rateFields = [
        'rate'
    ];

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'id',
        'type',
        'comment',
        'payed_at',
        'rate'
    ];

    /**
     * Retrieve user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Retrieve available transaction types.
     *
     * @return array
     */
    public static function types()
    {
        return [
            self::TYPE_PREPAYMENT,
            self::TYPE_ADDITIONAL,
            self::TYPE_FINAL
        ];
    }
}
