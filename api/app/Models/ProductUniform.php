<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductUniform extends Model
{
    /**
     * Uniform types.
     */
    const TYPE_EQUIPMENT = 'equipment';
    const TYPE_UNIFORM = 'uniform';

    /**
     * Position of product in builder.
     */
    const POSITION_TOP = 'top';
    const POSITION_CENTER = 'center';
    const POSITION_BOTTOM = 'bottom';

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'is_visible' => 'boolean'
    ];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'product_id',
        'category_id',
        'type',
        'position',
        'model',
        'is_visible'
    ];

    /**
     * {@inheritdoc}
     */
    public $incrementing = false;

    /**
     * {@inheritdoc}
     */
    protected $primaryKey = 'product_id';

    /**
     * {@inheritdoc}
     */
    protected $table = 'product_uniform';

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'type',
        'position',
        'model',

        'category'
    ];

    /**
     * Retrieve category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Position list.
     *
     * @return array
     */
    public static function positions()
    {
        return [
            self::POSITION_TOP,
            self::POSITION_CENTER,
            self::POSITION_BOTTOM
        ];
    }

    /**
     * Uniform types.
     *
     * @return array
     */
    public static function types()
    {
        return [
            self::TYPE_EQUIPMENT,
            self::TYPE_UNIFORM
        ];
    }
}
