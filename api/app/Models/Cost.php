<?php

namespace App\Models;

use App\Models\Traits\HasRates;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cost extends Model
{
    use HasRates, SoftDeletes;

    /**
     * {@inheritdoc}
     */
    protected $dates = ['deleted_at'];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'store_id',
        'type',
        'description',
        'rate'
    ];

    /**
     * {@inheritdoc}
     */
    protected $rateFields = [
        'rate'
    ];

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'id',
        'type',
        'description',
        'created_at',
        'store',
        'rate'
    ];

    /**
     * Retrieve store.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    /**
     * Filter query by criteria.
     *
     * @param $query
     * @param array $criteria
     */
    public function scopeCriteria($query, array $criteria = [])
    {
        // Year
        if ($value = array_get($criteria, 'year')) {
            $query->whereYear('created_at', $value);
        }

        // Month
        if ($value = array_get($criteria, 'month')) {
            $query->whereMonth('created_at', $value);
        }

        // Date
        if ($value = array_get($criteria, 'date')) {
            preg_match('/(\d*)_?(\d*)/', $value, $value);
            
            if ($year = array_get($value, 1)) {
                $query->whereYear('created_at', $year);

                if ($month = array_get($value, 2)) {
                    $query->whereMonth('created_at', $month);
                }
            }
        }

        // Store ID
        if ($value = array_get($criteria, 'store_id')) {
            $query->where('store_id', $value);
        }

        // Type
        if ($value = array_get($criteria, 'type')) {
            $query->where('type', $value);
        }

        // Sort order
        $query->orderBy('created_at', 'DESC');
    }

    /**
     * Type list of cost.
     *
     * @return array
     */
    public static function types()
    {
        return [
            'salary',
            'hosting',
            'tax',
            'utilities',
            'add',
            'delivery',
            'development',
            'other'
        ];
    }
}
