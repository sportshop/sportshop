<?php

namespace App\Models;

use App\Models\Traits\HasRates;
use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
    use HasRates;

    /**
     * {@inheritdoc}
     */
    protected $appends = [
        'is_withdrawn'
    ];

    /**
     * {@inheritdoc}
     */
    protected $dates = [
        'withdrawn_at'
    ];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'rate'
    ];

    /**
     * {@inheritdoc}
     */
    protected $rateFields = [
        'rate'
    ];

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'id',
        'withdrawn_at',
        'is_withdrawn',
        'rate'
    ];

    /**
     * Retrieve order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * Determine if payment was withdrawn.
     *
     * @return bool
     */
    public function getIsWithdrawnAttribute()
    {
        return (bool) $this->withdrawn_at;
    }
}
