<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'name',
        'value'
    ];

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'name',
        'display_name',
        'value'
    ];

    /**
     * Filter query by criteria.
     *
     * @param $query
     * @param array $criteria
     */
    public function scopeCriteria($query, array $criteria = [])
    {
        if ($value = array_get($criteria, 'name')) {
            $query->whereIn('name', $value);
        }
    }
}
