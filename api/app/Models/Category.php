<?php

namespace App\Models;

use App\Models\Traits\ByProductsRelationScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kalnoy\Nestedset\NodeTrait;
use Plank\Mediable\Mediable;

class Category extends Model
{
    use ByProductsRelationScope, Mediable, SoftDeletes;
    use NodeTrait {
        NodeTrait::newCollection insteadof Mediable;
    }

    /**
     * Media tags.
     */
    const MEDIA_THUMBNAIL = 'thumbnail';

    /**
     * {@inheritdoc}
     */
    protected $appends = [
        'thumbnail'
    ];

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'is_active'  => 'boolean'
    ];

    /**
     * {@inheritdoc}
     */
    protected $dates = ['deleted_at'];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'parent_id',
        'slug',
        'icon',
        'name',
        'description',
        'meta_title',
        'meta_description',
        'is_active'
    ];

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'id',
        'slug',
        'icon',
        'name',
        'description',
        'meta_title',
        'meta_description',
        'is_active',

        'depth_name',
        'is_default',
        'thumbnail',
        'children'
    ];

    /**
     * Retrieve products.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'x_product_category');
    }

    /**
     * Only active categories.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    /**
     * Filter query by criteria.
     *
     * @param $query
     * @param array $criteria
     */
    public function scopeFilter($query, array $criteria = [])
    {
        if ($value = array_get($criteria, 'slug')) {
            $query->where('slug', $value);
        }

        if ($value = array_get($criteria, 'ids')) {
            $query->whereIn('id', $value);
        }
    }

    /**
     * Get a name by depth in a tree.
     *
     * @return mixed|string
     */
    public function getDepthNameAttribute()
    {
        if ($this->depth) {
            $this->name = html_entity_decode('&nbsp;') . $this->name;
            for ($i = 0; $i < $this->depth; $i++) {
                $this->name = html_entity_decode('&mdash;') . $this->name;
            }
        }

        return $this->name;
    }

    /**
     * Get pivot attribute "is_default".
     *
     * @return string
     */
    public function getIsDefaultAttribute()
    {
        if (!$this->pivot) {
            return null;
        }

        return !!$this->pivot->is_default;
    }

    /**
     * Get the thumbnail.
     *
     * @return Media|null
     */
    public function getThumbnailAttribute()
    {
        if (!$this->relationLoaded('media')) {
            return null;
        }

        return $this->firstMedia(self::MEDIA_THUMBNAIL);
    }
}
