<?php

namespace App\Models;

use Spatie\Permission\Models\Role as BaseRole;

class Role extends BaseRole
{
    /**
     * Get the list of roles without system roles.
     *
     * @return array|mixed
     */
    public static function nonSystemRoles()
    {
        $roles = config('laravel-permission.roles');

        $roles = array_pluck($roles, 'name');

        if(($key = array_search('super_admin', $roles)) !== false) {
            unset($roles[$key]);
        }
        if(($key = array_search('admin', $roles)) !== false) {
            unset($roles[$key]);
        }

        return $roles;
    }
}
