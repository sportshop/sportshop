<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'hours'      => 'integer',
        'count_days' => 'integer'
    ];

    /**
     * {@inheritdoc}
     */
    protected $dates = [
        'created_at',
        'finished_at'
    ];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [];

    /**
     * {@inheritdoc}
     */
    protected $table = 'user_activity';

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $visible = [
        'date',
        'hours',
        'count_days'
    ];

    /**
     * Retrieve user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Group rows by month.
     *
     * @param Builder $query
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeGroupByMonth(Builder $query)
    {
        $timeDiffQuery = 'SELECT *, HOUR(TIMEDIFF(finished_at, created_at)) AS diff FROM user_activity WHERE finished_at IS NOT NULL';

        return $query
            ->selectRaw('
                user_activity.*,
                DATE_FORMAT(created_at, "%Y-%m") AS date,
                SUM(user_activity.diff) AS hours,
                COUNT(DISTINCT(DATE(created_at))) AS count_days
            ')
            ->from(DB::raw("($timeDiffQuery) as user_activity"))
            ->groupBy(DB::raw('user_activity.user_id, YEAR(user_activity.created_at), MONTH(user_activity.created_at)'));
    }

    /**
     * Group rows by day.
     *
     * @param Builder $query
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeGroupByDay(Builder $query)
    {
        $timeDiffQuery = 'SELECT *, HOUR(TIMEDIFF(finished_at, created_at)) AS diff FROM user_activity WHERE finished_at IS NOT NULL';

        return $query
            ->selectRaw('
                user_activity.*,
                DATE(created_at) AS date, 
                SUM(user_activity.diff) AS hours
            ')
            ->from(DB::raw("($timeDiffQuery) as user_activity"))
            ->groupBy(DB::raw('user_activity.user_id, DATE(user_activity.created_at)'));
    }
}
