<?php

namespace App\Models;

use App\Models\Traits\HasRates;
use Illuminate\Database\Eloquent\Model;

class ProductStoreDiscount extends Model
{
    use HasRates;

    /**
     * Price tags.
     */
    const PRICE_USER_PRICE = 'price';
    const PRICE_SALE_PRICE = 'sale_price';

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'user' => 'float',
        'sale' => 'float'
    ];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'discount_id',
        'user',
        'sale',
        'price',
        'sale_price'
    ];

    /**
     * {@inheritdoc}
     */
    protected $rateFields = [
        'price',
        'sale_price'
    ];

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'discount_id',
        'user',
        'sale',
        'price',
        'sale_price'
    ];

    /**
     * Retrieve discount.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }
}
