<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EntityRate extends Model
{
    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'price' => 'object'
    ];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'entity_id',
        'entity_type',
        'price',
        'tag'
    ];

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'price'
    ];

    /**
     * Retrieve entity relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function entity()
    {
        return $this->morphTo();
    }
}
