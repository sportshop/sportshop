<?php

namespace App\Models;

use App\Models\Traits\HasRates;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StoreProductTransfer extends Model
{
    use HasRates, SoftDeletes;

    /**
     * Actions.
     */
    const ACTION_ADD = 'add';
    const ACTION_MOVE = 'move';
    const ACTION_DELETE = 'delete';

    /**
     * {@inheritdoc}
     */
    protected $dates = ['deleted_at'];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'from_store_id',
        'to_store_id',
        'product_size_id',
        'user_id',
        'action',
        'quantity',
        'price'
    ];

    /**
     * {@inheritdoc}
     */
    protected $rateFields = [
        'price'
    ];

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'action',
        'quantity',
        'created_at',
        'user',
        'from_store',
        'to_store',
        'product',
        'price'
    ];

    /**
     * Store from which it was transferred.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function from_store()
    {
        return $this->belongsTo(Store::class, 'from_store_id');
    }

    /**
     * Store to which it was transferred.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function to_store()
    {
        return $this->belongsTo(Store::class, 'to_store_id');
    }

    /**
     * Retrieve product size.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_size()
    {
        return $this->belongsTo(ProductSize::class, 'product_size_id')
            ->with('size', 'values');
    }

    /**
     * Retrieve user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Filter query by criteria.
     *
     * @param $query
     * @param array $criteria
     */
    public function scopeCriteria($query, array $criteria = [])
    {
        if ($value = array_get($criteria, 'date')) {
            $query->whereDate('created_at', $value);
        }

        // Date Start
        if ($value = array_get($criteria, 'date_start')) {
            $query->whereDate('created_at', '>=', $value);
        }

        // Date End
        if ($value = array_get($criteria, 'date_end')) {
            $query->whereDate('created_at', '<=', $value);
        }

        if ($value = array_get($criteria, 'manager_id')) {
            $query->where('user_id', $value);
        }

        if ($value = array_get($criteria, 'action')) {
            $query->where('action', $value);
        }

        if ($value = array_get($criteria, 'from_store_id')) {
            $query->where('from_store_id', $value);
        }

        if ($value = array_get($criteria, 'to_store_id')) {
            $query->where('to_store_id', $value);
        }

        if ($value = array_get($criteria, 'product_name')) {
            $query->whereHas('product_size.product', function ($query) use ($value) {
                return $query->where('name', 'like', "%$value%");
            });
        }

        if ($value = array_get($criteria, 'product_sku')) {
            $query->whereHas('product_size.product', function ($query) use ($value) {
                return $query->where('sku', 'like', "%$value%");
            });
        }

        if ($value = array_get($criteria, 'product_quantity')) {
            $query->where('quantity', $value);
        }

        if ($value = array_get($criteria, 'product_size')) {
            $query->whereHas('product_size.values', function ($query) use ($value) {
                return $query->where('value', 'like', "%$value%");
            });
        }

        if ($sort_by = array_get($criteria, 'sort_by') && $order_by = array_get($criteria, 'order_by')) {
            $query->orderBy($sort_by, $order_by);
        } else {
            $query->orderBy('created_at', 'desc');
        }
    }

    /**
     * Transform product.
     *
     * @return mixed
     */
    public function getProductAttribute()
    {
        if (!$this->relationLoaded('product_size')) {
            return null;
        }

        $product = $this->product_size->product;
        $product->size = [
            'group' => $this->product_size->size->group,
            'size'  => $this->product_size
        ];
        $product->addVisible('size');

        return $product;
    }
}
