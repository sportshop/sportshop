<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'value'       => 'float',
        'sale_value'  => 'float',
        'is_increase' => 'boolean',
        'is_default' => 'boolean'
    ];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'title',
        'value',
        'sale_value',
        'is_increase',
        'is_default'
    ];

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    public $visible = [
        'id',
        'title',
        'value',
        'sale_value',
        'is_increase',
        'is_default'
    ];
}
