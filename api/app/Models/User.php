<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasRoles, Notifiable, SoftDeletes;

    /**
     * Status of the user's daily work.
     */
    const WORKDAY_NOT_STARTED = 'not_started';
    const WORKDAY_STARTED = 'started';
    const WORKDAY_FINISHED = 'finished';

    /**
     * {@inheritdoc}
     */
    protected $appends = [
        'full_name',
        'role_name',
        'has_password',
        'discount'
    ];

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'is_active' => 'boolean'
    ];

    /**
     * {@inheritdoc}
     */
    protected $dates = ['deleted_at'];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'email',
        'password',
        'first_name',
        'last_name',
        'nickname',
        'phone',
        'address'
    ];

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'id',
        'email',
        'first_name',
        'last_name',
        'nickname',
        'phone',
        'address',
        'created_at',
        'full_name',
        'role_name',
        'has_password',
        'permissions',
        'data',
        'discount',
        'workday_status'
    ];

    /**
     * Retrieve user activity log.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activity()
    {
        return $this->hasMany(UserActivity::class);
    }

    /**
     * Retrieve user today activity log.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activity_today()
    {
        return $this->activity()
            ->whereDate('created_at', Carbon::now()->toDateString());
    }

    /**
     * Retrieve user data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function data()
    {
        return $this->hasOne(UserData::class)->with('discount');
    }

    /**
     * Retrieve managed orders.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function managed_orders()
    {
        return $this->hasMany(Order::class, 'manager_id');
    }

    /**
     * Retrieve purchase orders.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function purchased_orders()
    {
        return $this->hasMany(Order::class, 'customer_id')
            ->joinRate('price')
            ->whereIn('status', [
                Order::STATUS_COMPLETED,
                Order::STATUS_PARTIALLY_REFUNDED
            ]);
    }

    /**
     * Get user sale percentages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sale_percentages()
    {
        return $this->hasMany(UserSalePercentage::class);
    }

    /**
     * Retrieve user transactions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(UserTransaction::class);
    }

    /**
     * Filter query by criteria.
     *
     * @param $query
     * @param array $criteria
     */
    public function scopeCriteria($query, array $criteria = [])
    {
        // ID
        if ($value = array_get($criteria, 'id')) {
            $query->where('id', $value);
        }

        // Name, Nickname
        if ($value = array_get($criteria, 'name')) {
            $query->where(function ($query) use ($value) {
                $sqlRaw = 'concat_ws(" ",first_name,last_name) like "%' . $value . '%"';

                return $query
                    ->whereRaw($sqlRaw)
                    ->orWhere('nickname', 'like', "%$value%");
            });
        }

        // Registration date
        if ($value = array_get($criteria, 'created_at')) {
            $query->whereDate('created_at', $value);
        }

        // Active status
        if ($value = array_get($criteria, 'active')) {
            $query->whereDate('is_active', $value);
        }

        // Discount ID
        if ($value = array_get($criteria, 'discount_id')) {
            $query->whereHas('data', function ($query) use ($value) {
                return $query->where('discount_id', $value);
            });
        }

        // Role name
        if ($value = array_get($criteria, 'role')) {
            $value = explode(',', $value);

            $query->whereHas('roles', function ($query) use ($value) {
                $query->whereIn('name', $value);
            });
        }

        // Purchased orders summary price
        $price_min = array_get($criteria, 'purchased_orders_price_min');
        $price_max = array_get($criteria, 'purchased_orders_price_max');
        $currency_id = array_get($criteria, 'purchased_orders_price_currency_id');

        if (($price_min || $price_max) && $currency_id) {
            $query->whereHas('purchased_orders', function ($query) use ($price_min, $price_max, $currency_id) {
                $subQuery = 'SUM(JSON_EXTRACT(`entity_rates_price`.`price`, \'$.\"' . $currency_id . '\".value\')) AS purchased_orders_price';

                $query
                    ->addSelect(DB::raw($subQuery))
                    ->joinRate('price');

                if ($price_min) {
                    $query->having('purchased_orders_price', '>=', $price_min);
                }
                if ($price_max) {
                    $query->having('purchased_orders_price', '<=', $price_max);
                }
            });
        }

        // Sort order
        $query->orderBy('created_at', 'DESC');
    }

    /**
     * Load managed completed orders.
     *
     * @param Builder $query
     * @param \Closure $callback
     */
    public function scopeWithManagedCompletedOrders(Builder $query, \Closure $callback)
    {
        $query->with(['managed_orders' => function ($query) use ($callback) {
            $query
                ->whereNotNull(DB::raw('JSON_EXTRACT(manager_data, "$.sale_percentages")'))
                ->joinRate('price')
                ->with([
                    'customer_relation',
                    'order_products'
                ])
                ->whereIn('status', [
                    Order::STATUS_COMPLETED,
                    Order::STATUS_PARTIALLY_REFUNDED
                ]);

            call_user_func($callback, $query);
        }]);
    }

    /**
     * Retrieve user discount.
     *
     * @return \App\Models\Discount|null
     */
    public function getDiscountAttribute()
    {
        if (!$this->relationLoaded('data') || !$this->data || !$this->data->relationLoaded('discount')) {
            return null;
        }

        return $this->data->discount;
    }

    /**
     * Get full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Determine if password exists.
     *
     * @return bool
     */
    public function getHasPasswordAttribute()
    {
        return !!$this->password;
    }

    /**
     * Determine if user can show sizes.
     *
     * @return string
     */
    public function getIsCanShowSizesAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Get user role.
     *
     * @return \App\Models\Role|null
     */
    public function getRoleAttribute()
    {
        if (!$this->relationLoaded('roles')) {
            return null;
        }

        return $this->getRelationValue('roles')->first();
    }

    /**
     * Get role name.
     *
     * @return string|null
     */
    public function getRoleNameAttribute()
    {
        if (!$this->relationLoaded('roles') || !$this->role) {
            return null;
        }

        return $this->role->name;
    }

    /**
     * Set the password encryption.
     *
     * @param string $value
     *
     * @return string
     */
    public function setPasswordAttribute($value)
    {
        if ($value) {
            $this->attributes['password'] = bcrypt($value);
        }
    }

    /**
     * Get user sales.
     *
     * @return \Illuminate\Support\Collection|null
     */
    public function getSalesAttribute()
    {
        if (!$this->relationLoaded('managed_orders') || !$this->relationLoaded('sale_percentages')) {
            return null;
        }

        $orders = $this->managed_orders;

        $sales = new Collection();

        foreach ($orders as $order) {
            $sale_percentages = $order->manager_data->sale_percentages;

            $saleItem = [
                'order_id' => $order->id,
                'date'     => $order->created_at,
                'rate'     => new Collection()
            ];

            foreach ($order->price as $currencyId => $currencyPrice) {
                $currencyPrice['value'] = $currencyPrice['value'] * $sale_percentages / 100;

                $saleItem['rate']->put($currencyId, $currencyPrice);
            }

            $sales->push($saleItem);
        }

        return $sales;
    }

    /**
     * Get the user's daily work log status.
     *
     * @return string
     */
    public function getWorkdayStatusAttribute()
    {
        if (!$this->relationLoaded('activity_today')) {
            return null;
        }

        $activity_today = $this->activity_today;

        if ($activity_today->isEmpty()) {
            return self::WORKDAY_NOT_STARTED;
        } else {
            if (!$activity_today->where('finished_at', null)->isEmpty()) {
                return self::WORKDAY_STARTED;
            } else {
                return self::WORKDAY_FINISHED;
            }
        }
    }

    /**
     * Retrieve purchased orders summary price.
     *
     * @return array
     */
    public function getPurchasedOrdersPriceAttribute()
    {
        if (!$this->relationLoaded('purchased_orders')) {
            return null;
        }

        $result = [];

        foreach ($this->purchased_orders as $order) {
            if (empty($result)) {
                $result = $order->price->toArray();
            } else {
                foreach ($order->price as $currency_id => $currency_rate) {
                    $result[$currency_id]['value'] += $currency_rate['value'];
                }
            }
        }

        return $result;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        $this->load('roles.permissions', 'data.discount');
        $this->permissions = $this->role->permissions->pluck('name');

        if ($this->hasAnyRole(['admin', 'manager', 'limited_manager'])) {
            $this->load('sale_percentages');
            $this->addVisible('sale_percentages');
            $this->load('activity_today');
            $this->append('workday_status');
        }

        return [
            'data' => $this
        ];
    }
}
