<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'value'      => 'float',
        'is_default' => 'boolean'
    ];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'code',
        'value',
        'name',
        'symbol',
        'is_default'
    ];

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    public $visible = [
        'id',
        'code',
        'value',
        'name',
        'symbol',
        'is_default'
    ];

    /**
     * Only default currency.
     *
     * @param $query
     */
    public function scopeDefault($query)
    {
        $query->where('is_default', true);
    }

    /**
     * Format a code attribute to lower case.
     *
     * @param string $value
     */
    public function setCodeAttribute(string $value)
    {
        $this->attributes['code'] = strtoupper($value);
    }

    /**
     * Decode html.
     *
     * @param string $value
     *
     * @return string
     */
    public function getSymbolAttribute(string $value)
    {
        return html_entity_decode($value);
    }
}
