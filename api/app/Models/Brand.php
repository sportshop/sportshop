<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Brand extends Model
{
    use Mediable;

    /**
     * Media tags.
     */
    const MEDIA_THUMBNAIL = 'thumbnail';

    /**
     * {@inheritdoc}
     */
    protected $appends = [
        'thumbnail'
    ];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'name'
    ];

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    public $visible = [
        'id',
        'name',
        'thumbnail',
        'size_groups'
    ];

    /**
     * Retrieve products.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * Retrieve size groups.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function size_groups()
    {
        return $this->hasMany(SizeGroup::class);
    }

    /**
     * Get the thumbnail.
     *
     * @return Media|null
     */
    public function getThumbnailAttribute()
    {
        if (!$this->relationLoaded('media')) {
            return null;
        }

        return $this->firstMedia(self::MEDIA_THUMBNAIL);
    }
}
