<?php

namespace App\Models;

use App\Models\Traits\HasRates;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Plank\Mediable\Mediable;

class Product extends BaseModel
{
    use HasRates, Mediable, SoftDeletes;

    /**
     * Discount types.
     */
    const DISCOUNT_MARKUP = 'markup';
    const DISCOUNT_SALE = 'sale';
    const DISCOUNT_MAX_SALE = 'max_sale';
    const DISCOUNT_USER = 'user';
    const DISCOUNT_USER_SALE = 'user_sale';

    /**
     * Product types.
     */
    const TYPE_NOVELTY = 'novelty';
    const TYPE_SALE = 'sale';
    const TYPE_MAX_SALE = 'max_sale';
    const TYPE_TOP_SALES = 'top_sale';

    /**
     * Media tags.
     */
    const MEDIA_THUMBNAIL = 'thumbnail';
    const MEDIA_ADDITIONAL = 'additional_image';

    /**
     * {@inheritdoc}
     */
    protected $appends = [
        'quantity',
        'attributes',
        'sizes',
        'thumbnail'
    ];

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'is_active'  => 'boolean',
        'is_visible' => 'boolean'
    ];

    /**
     * {@inheritdoc}
     */
    protected $dates = ['deleted_at'];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'brand_id',
        'slug',
        'sku',
        'warehouse_number',
        'name',
        'description',
        'type',
        'meta_title',
        'meta_description',
        'is_visible',
        'is_active',

        'cost_price'
    ];

    /**
     * {@inheritdoc}
     */
    protected $rateFields = [
        'cost_price',
        'sale_price'
    ];

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'id',
        'slug',
        'sku',
        'warehouse_number',
        'name',
        'description',
        'type',
        'meta_title',
        'meta_description',
        'quantity',

        'cost_price',
        'sale_price',

        'attributes',
        'brand',
        'categories',
        'colors',
        'media',
        'sizes',
        'thumbnail',
        'uniform'
    ];

    /**
     * Retrieve attribute values.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function attribute_values()
    {
        return $this->morphToMany(AttributeValue::class, 'entity', 'x_attribute_value_entity', 'entity_id', 'value_id')
            ->with('attribute.group');
    }

    /**
     * Retrieve brand.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class)->withMedia(Brand::MEDIA_THUMBNAIL);
    }

    /**
     * Retrieve categories.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'x_product_category')
            ->withPivot('is_default')
            ->withMedia(Category::MEDIA_THUMBNAIL);
    }

    /**
     * Retrieve colors.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function colors()
    {
        return $this->belongsToMany(Color::class, 'x_product_color');
    }

    /**
     * Retrieve product sizes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function product_sizes()
    {
        return $this->hasMany(ProductSize::class, 'product_id')->with('values');
    }

    /**
     * Retrieve stores data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stores()
    {
        return $this->hasMany(ProductStoreData::class, 'product_id');
    }

    /**
     * Retrieve product transfers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function transfers()
    {
        return $this
            ->hasManyThrough(
                StoreProductTransfer::class,
                ProductSize::class,
                'product_id',
                'product_size_id'
            )->with('price');
    }

    /**
     * Retrieve uniform builder data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uniform()
    {
        return $this->hasOne(ProductUniform::class)->with('category');
    }

    /**
     * Filter query by criteria.
     *
     * @param $query
     * @param array $criteria
     */
    public function scopeCriteria($query, array $criteria = [])
    {
        $storeId = array_get($criteria, 'store_id');
        $currencyId = array_get($criteria, 'currency_id');

        // IDs
        if ($value = array_get($criteria, 'id')) {
            $query->whereIn('products.id', explode(',', $value));
        }

        // Product name
        if ($value = array_get($criteria, 'name')) {
            $query->where('name', 'like', "%$value%");
        }

        // SKU
        if ($value = array_get($criteria, 'sku')) {
            $query->where('sku', 'like', "%$value%");
        }

        // Type
        if ($value = array_get($criteria, 'type')) {
            $query->where('type', $value);
        }

        // Status
        if ($value = array_get($criteria, 'status')) {
            $query->byStatus($value);
        }

        // Visibility status
        if ($value = array_get($criteria, 'is_visible')) {
            $query->where('is_visible', $value);
        }

        // Multiple keys
        if ($value = array_get($criteria, 'search')) {
            $query->where(function ($query) use ($value) {
                $query
                    ->where('name', 'like', "%$value%")
                    ->orWhere('sku', 'like', "%$value%");
            });
        }

        // Uniform type
        if (array_has($criteria, 'uniform_type')) {
            $query->whereHas('uniform', function ($query) use ($criteria) {
                $query
                    ->where('is_visible', true)
                    ->where('type', $criteria['uniform_type']);
            });
        }

        // Attribute IDs
        if ($value = array_get($criteria, 'attributes')) {
            $query->whereHas('attribute_values', function ($query) use ($value) {
                return $query->whereIn('id', explode(',', $value));
            });
        }

        // Brand IDs
        if ($value = array_get($criteria, 'brands')) {
            $query->whereHas('brand', function ($query) use ($value) {
                return $query->whereIn('id', explode(',', $value));
            });
        }

        // Category IDs
        if ($value = array_get($criteria, 'categories')) {
            $query->whereHas('categories', function ($query) use ($value) {
                return $query->whereIn('id', explode(',', $value));
            });
        }

        // Color IDs
        if ($value = array_get($criteria, 'colors')) {
            $query->whereHas('colors', function ($query) use ($value) {
                return $query->whereIn('id', explode(',', $value));
            });
        }

        // Size values
        if ($value = array_get($criteria, 'sizes')) {
            $query->whereHas('product_sizes', function ($query) use ($value) {
                $query->whereHas('values', function ($query) use ($value) {
                    $query->whereIn('value', explode(',', $value));
                });
            });
        }

        // Sale Price
        if (
            $storeId && $currencyId
            && (array_get($criteria, 'price_min') || array_get($criteria, 'price_max'))
        ) {
            $column = "JSON_EXTRACT(entity_rates_sale_price.price, '$.\"{$currencyId}\".value')";

            $priceMin = array_get($criteria, 'price_min');
            $priceMax = array_get($criteria, 'price_max');

            if ($priceMin) {
                $query->havingRaw("$column >= $priceMin");
            }
            if ($priceMax) {
                $query->havingRaw("$column <= $priceMax");
            }
        }

        // Category Slug
        if ($value = array_get($criteria, 'category_slug')) {
            $query->whereHas('categories', function ($query) use ($value) {
                $query
                    ->where('slug', $value)
                    ->orWhereHas('parent', function ($query) use ($value) {
                        $query->where('slug', $value);
                    });
            });
        }

        // Only exists sizes
        if (array_get($criteria, 'exists') && $storeId) {
            $query->whereHas('product_sizes', function ($query) use ($storeId) {
                $query->where('store_id', $storeId)->where('quantity', '>', 0);
            });
        }

        // Excluded slug
        if ($value = array_get($criteria, 'excluded_slug')) {
            $query->whereNotIn('slug', [$value]);
        }

        // Sort order
        switch (array_get($criteria, 'order_by', 'created_at')) {
            case 'novelty':
                $query->orderByRaw('FIELD(type, "' . Product::TYPE_NOVELTY . '") DESC');
                break;

            case 'top_sale':
                $query->orderByRaw('FIELD(type, "' . Product::TYPE_TOP_SALES . '") DESC');
                break;

            case 'lowest_price':
            case 'highest_price':
                if (!$storeId) continue;

                $order = ($criteria['order_by'] === 'highest_price') ? 'desc' : 'asc';

                $query->orderByRaw("JSON_EXTRACT(entity_rates_sale_price.price, '$.\"{$currencyId}\".value') $order");
                break;

            case 'created_at':
            default:
                $query->orderBy('created_at', 'DESC');
        }
    }

    /**
     * Filter query by status.
     *
     * @param $query
     * @param $status
     */
    public function scopeByStatus($query, $status)
    {
        switch ($status) {
            case 'active':
                $query->where('is_active', true);
                break;

            case 'inactive':
                $query->where('is_active', false);
                break;

            case 'hidden':
                $query->where('is_visible', false);
                break;
        }
    }

    /**
     * Join sale price.
     *
     * @param $query
     * @param int $store_id
     * @param int|null $discount_id
     */
    public function scopeJoinSalePrice($query, int $store_id, int $discount_id = null)
    {
        // Join product store data.
        $query->join('product_store_data', function ($join) use ($store_id) {
            $join->on('products.id', '=', 'product_store_data.product_id')
                ->where('product_store_data.store_id', $store_id);
        });

        // If discount_id exists, join user_discount.
        if ($discount_id) {
            $query->join('product_store_discounts', function ($join) use ($discount_id) {
                $join->on('product_store_data.id', '=', 'product_store_discounts.product_store_id')
                    ->where('product_store_discounts.discount_id', $discount_id);
            });
        }

        // Join entity rate.
        $query->joinRate('sale_price', function ($join, $priceTableName) use ($discount_id) {
            if ($discount_id) {
                $join
                    ->on(function ($join) use ($priceTableName) {
                        $join
                            ->on('product_store_data.id', '=', "$priceTableName.entity_id")
                            ->where("$priceTableName.entity_type", ProductStoreData::class)
                            ->where(function ($query) use ($priceTableName) {
                                $query
                                    ->where('products.type', Product::TYPE_MAX_SALE)
                                    ->where("$priceTableName.tag", 'max_sale_price');
                            });
                    })
                    ->orOn(function ($join) use ($priceTableName) {
                        $join
                            ->on('product_store_discounts.id', '=', "$priceTableName.entity_id")
                            ->where("$priceTableName.entity_type", ProductStoreDiscount::class)
                            ->where(function ($query) use ($priceTableName) {
                                $query
                                    ->where(function ($query) use ($priceTableName) {
                                        $query
                                            ->where('products.type', Product::TYPE_SALE)
                                            ->where("$priceTableName.tag", 'sale_price');
                                    })
                                    ->orWhere(function ($query) use ($priceTableName) {
                                        $query
                                            ->whereIn('products.type', [Product::TYPE_NOVELTY, Product::TYPE_TOP_SALES])
                                            ->where("$priceTableName.tag", 'price');
                                    });
                            });
                    });
            } else {
                $join->on('product_store_data.id', '=', "$priceTableName.entity_id")
                    ->where("$priceTableName.entity_type", ProductStoreData::class)
                    ->where(function ($query) use ($priceTableName) {
                        $query
                            ->where(function ($query) use ($priceTableName) {
                                $query
                                    ->where('products.type', Product::TYPE_SALE)
                                    ->where("$priceTableName.tag", 'sale_price');
                            })
                            ->orWhere(function ($query) use ($priceTableName) {
                                $query
                                    ->where('products.type', Product::TYPE_MAX_SALE)
                                    ->where("$priceTableName.tag", 'max_sale_price');
                            })
                            ->orWhere(function ($query) use ($priceTableName) {
                                $query
                                    ->whereIn('products.type', [Product::TYPE_NOVELTY, Product::TYPE_TOP_SALES])
                                    ->where("$priceTableName.tag", 'markup_price');
                            });
                    });
            }
        });
    }

    /**
     * Check if product is available for customers by store.
     *
     * @param $query
     * @param $store_id
     *
     * @return mixed
     */
    public function scopeStoreAvailable($query, $store_id)
    {
        return $query
            ->where('is_active', true)
            ->where('is_visible', true)
            ->whereHas('product_sizes', function ($query) use ($store_id) {
                $query->where('store_id', $store_id)->where('quantity', '>', 0);
            });
    }

    /**
     * Get product attributes.
     *
     * @return mixed
     */
    public function getAttributesAttribute()
    {
        if (!$this->relationLoaded('attribute_values') || $this->attribute_values->isEmpty()) {
            return null;
        }

        $values = $this->attribute_values;
        $group = $values->first()->attribute->group;

        $values = $values->groupBy('attribute_id');

        $attributes = new Collection();
        $values->transform(function ($item) use ($attributes) {
            $attribute = [
                'name' => $item[0]->attribute->name
            ];

            $item->each(function ($valueItem) use ($attributes, &$attribute) {
                $attribute['values'][] = [
                    'id'    => $valueItem->id,
                    'value' => $valueItem->value
                ];
            });

            $attributes->push($attribute);
        });

        $result = [
            'group' => $group,
            'items' => $attributes
        ];

        return $result;
    }

    /**
     * Get product quantity.
     *
     * @return integer
     */
    public function getQuantityAttribute()
    {
        if (!$this->relationLoaded('product_sizes') || $this->product_sizes->isEmpty()) {
            return null;
        }

        $product_sizes = $this->product_sizes;

        return $product_sizes->sum('quantity');
    }

    /**
     * Get product sizes.
     *
     * @return mixed|null
     */
    public function getSizesAttribute()
    {
        if (!$this->relationLoaded('product_sizes') || $this->product_sizes->isEmpty()) {
            return null;
        }

        $sizes = $this->product_sizes;
        $group = $sizes->first()->size->group;

        $result = [
            'group' => $group,
            'items' => $sizes
        ];

        return $result;
    }

    /**
     * Convert to slug format.
     *
     * @param $value
     */
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    /**
     * Get the thumbnail.
     *
     * @return Media|null
     */
    public function getThumbnailAttribute()
    {
        if (!$this->relationLoaded('media')) {
            return null;
        }

        return $this->firstMedia(self::MEDIA_THUMBNAIL);
    }

    /**
     * Determine if product has quantity.
     *
     * @return boolean
     */
    public function hasQuantity()
    {
        return !!$this->quantity;
    }

    /**
     * Discount type list.
     *
     * @return array
     */
    public static function discountTypes()
    {
        return [
            self::DISCOUNT_MARKUP,
            self::DISCOUNT_SALE,
            self::DISCOUNT_MAX_SALE,
            self::DISCOUNT_USER,
            self::DISCOUNT_USER_SALE
        ];
    }

    /**
     * Product media tags.
     *
     * @return array
     */
    public static function mediaTags()
    {
        return [
            self::MEDIA_THUMBNAIL,
            self::MEDIA_ADDITIONAL
        ];
    }

    /**
     * Product type list.
     *
     * @return array
     */
    public static function types()
    {
        return [
            self::TYPE_NOVELTY,
            self::TYPE_SALE,
            self::TYPE_MAX_SALE,
            self::TYPE_TOP_SALES
        ];
    }
}
