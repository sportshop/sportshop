<?php

namespace App\Models;

use App\Models\Traits\HasRates;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

class Order extends Model
{
    use HasRates, SoftDeletes;

    /**
     * Order statuses.
     */
    const STATUS_NEW = 'new';
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_SENT = 'sent';
    const STATUS_COMPLETED = 'completed';
    const STATUS_CANCELED = 'canceled';
    const STATUS_REFUNDED = 'refunded';
    const STATUS_PARTIALLY_REFUNDED = 'partially_refunded';

    /**
     * Payment methods.
     */
    const PAYMENT_METHOD_COD = 'cod';
    const PAYMENT_METHOD_CARD = 'card';
    const PAYMENT_METHOD_CASH = 'cash';

    /**
     * Payment statuses.
     */
    const PAYMENT_STATUS_PAID = 'paid';
    const PAYMENT_STATUS_NOT_PAID = 'not_paid';
    const PAYMENT_STATUS_PARTIALLY_PAID = 'partially_paid';

    /**
     * Delivery methods.
     */
    const DELIVERY_METHOD_COURIER = 'courier';
    const DELIVERY_METHOD_NEW_POST = 'new_post';
    const DELIVERY_METHOD_PICKUP = 'pickup';

    /**
     * {@inheritdoc}
     */
    protected $appends = [
        'customer',
        'is_paid',
        'products',
        'residual_price'
    ];

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'customer_data'     => 'array',
        'manager_data'      => 'object',
        'payment'           => 'array',
        'delivery'          => 'array',
        'is_from_web_store' => 'boolean'
    ];

    /**
     * {@inheritdoc}
     */
    protected $dates = ['deleted_at'];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'store_id',
        'manager_id',
        'customer',
        'payment',
        'delivery',
        'notes',
        'is_from_web_store'
    ];

    /**
     * {@inheritdoc}
     */
    protected $rateFields = [
        'price'
    ];

    /**
     * {@inheritdoc}
     */
    protected $visible = [
        'id',
        'customer',
        'status',
        'payment',
        'delivery',
        'notes',
        'created_at',
        'price',
        'is_paid',
        'products',
        'residual_price',
        'residual_payments_rate'
    ];

    /**
     * Retrieve customer by relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer_relation()
    {
        return $this->belongsTo(User::class, 'customer_id')
            ->with('data.discount')
            ->withTrashed();
    }

    /**
     * Retrieve manager.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id');
    }

    /**
     * Retrieve order payments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany(OrderPayment::class)->joinRate('rate');
    }

    /**
     * Retrieve order products.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order_products()
    {
        return $this->hasMany(OrderProduct::class)
            ->joinRate('cost_price')
            ->joinRate('sale_price')
            ->with([
                'product_size' => function ($query) {
                    $query
                        ->withTrashed()
                        ->with([
                            'product' => function ($query) {
                                $query
                                    ->withTrashed()
                                    ->withMedia(Product::MEDIA_THUMBNAIL);
                            },
                            'values'
                    ]);
                }
            ]);
    }

    /**
     * Retrieve store.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    /**
     * Filter query by criteria.
     *
     * @param $query
     * @param array $criteria
     */
    public function scopeCriteria($query, array $criteria = [])
    {
        // IDs
        if ($value = array_get($criteria, 'id')) {
            $query->whereIn('orders.id', explode(',', $value));
        }

        // Status
        if ($value = array_get($criteria, 'status')) {
            $query->where(function ($query) use ($value) {
                $query->where('status', $value);

                if ($value === 'refunded') {
                    $query->orWhere('status', Order::STATUS_PARTIALLY_REFUNDED);
                }
            });
        }

        // Store ID
        if ($value = array_get($criteria, 'store_id')) {
            $query->where('store_id', $value);
        }

        // Manager ID
        if ($value = array_get($criteria, 'manager_id')) {
            $query->where('manager_id', $value);
        }

        // Payment Status
        if ($value = array_get($criteria, 'payment_status')) {
            $query->where('payment->status', $value);
        }

        // Payment Method
        if ($value = array_get($criteria, 'payment_method')) {
            $query->where('payment->method', $value);
        }

        // Delivery Method
        if ($value = array_get($criteria, 'delivery_method')) {
            if ($value === 'local_store') {
                $query->where('is_from_web_store', false);
            } elseif ($value === 'web_store') {
                $query->where('is_from_web_store', true);
            } else {
                $query
                    ->where('is_from_web_store', true)
                    ->where('delivery->method', $value);
            }
        }

        // Customer ID
        if ($value = array_get($criteria, 'customer_id')) {
            $query->where('customer_id', $value);
        }

        // Customer
        if ($value = array_get($criteria, 'customer')) {
            $value = mb_strtolower($value);

            $query->where(function ($query) use ($value) {
                $query
                    ->whereHas('customer_relation', function ($query) use ($value) {
                        $query
                            ->whereRaw('LOWER(concat_ws(" ",first_name,last_name)) LIKE "%' . $value . '%"')
                            ->orWhereRaw('LOWER(email) LIKE "%' . $value . '%"')
                            ->orWhere('phone', 'like', "%$value%")
                            ->orWhereRaw('LOWER(address) LIKE "%' . $value . '%"');
                    })
                    ->orWhere(function ($query) use ($value) {
                        $query
                            ->whereNull('customer_id')
                            ->whereRaw('LOWER(concat_ws(" ",JSON_EXTRACT(customer_data, "$.first_name"),JSON_EXTRACT(customer_data, "$.last_name"))) LIKE "%' . $value . '%"')
                            ->orWhereRaw('LOWER(customer_data -> "$.email") LIKE "%' . $value . '%"')
                            ->orWhere('customer_data->phone', 'like', "%$value%")
                            ->orWhereRaw('LOWER(customer_data -> "$.address") LIKE "%' . $value . '%"');
                    });
            });
        }

        // Created At
        if ($value = array_get($criteria, 'created_at')) {
            $query->whereDate('created_at', $value);
        }

        // Date Start
        if ($value = array_get($criteria, 'date_start')) {
            $query->whereDate('created_at', '>=', $value);
        }

        // Date End
        if ($value = array_get($criteria, 'date_end')) {
            $query->whereDate('created_at', '<=', $value);
        }

        // Date
        if ($value = array_get($criteria, 'date')) {
            preg_match('/(\d*)_?(\d*)/', $value, $value);

            if ($year = array_get($value, 1)) {
                $query->whereYear('created_at', $year);

                if ($month = array_get($value, 2)) {
                    $query->whereMonth('created_at', $month);
                }
            }
        }

        // Delivery Declaration
        if ($value = array_get($criteria, 'declaration')) {
            $query->where('delivery->declaration', 'like', "%$value%");
        }

        // Product SKU or product size values
        if ($value = array_get($criteria, 'product')) {
            $query->whereHas('order_products.product_size', function ($query) use ($value) {
                $query
                    ->withTrashed()
                    ->whereHas('product', function ($query) use ($value) {
                        $query->where('sku', 'like', "%$value%");
                    })
                    ->orWhereHas('values', function ($query) use ($value) {
                        $query->where('value', 'like', "%$value%");
                    });
            });
        }

        // Withdrawal status
        if ($value = array_get($criteria, 'is_withdrawn')) {
            $value = $value == 'true';

            $query->where(function ($query) use ($value) {
                if ($value) {
                    $query
                        ->has('payments')
                        ->whereDoesntHave('payments', function ($query) use ($value) {
                            $query->whereNull('withdrawn_at');
                        });
                } else {
                    $query
                        ->doesntHave('payments')
                        ->orWhereHas('payments', function ($query) use ($value) {
                            $query->whereNull('withdrawn_at');
                        });
                }
            });
        }

        // Sort order
        $query->orderBy('created_at', 'DESC');
    }

    /**
     * Retrieve customer by relation or custom data.
     *
     * @return \App\Models\User|array|null
     */
    public function getCustomerAttribute()
    {
        $customer_data = $this->customer_data;

        if ($this->customer_id) {
            $relation_data = $this->customer_relation->toArray();

            if (isset($customer_data['discount_id'])) {
                $discount = Discount::find($customer_data['discount_id']);
                $relation_data['discount'] = $discount;
            }

            return $relation_data;
        }

        if (isset($customer_data['discount_id'])) {
            $customer_data['discount'] = Discount::find($customer_data['discount_id']);
            unset($customer_data['discount_id']);
        }

        return $customer_data;
    }

    /**
     * Set customer data.
     *
     * @param $value
     */
    public function setCustomerAttribute($value)
    {
        if (!$value || !is_array($value)) {
            return;
        }
        if (array_get($value, 'id')) {
            $this->attributes['customer_id'] = array_get($value, 'id');
            array_forget($value, 'id');
        }

        $this->attributes['customer_data'] = json_encode($value);
    }

    /**
     * Determine if an order is paid.
     *
     * @return bool
     */
    public function getIsPaidAttribute()
    {
        return array_get($this->payment, 'status') == Order::PAYMENT_STATUS_PAID;
    }

    /**
     * Transform order products.
     *
     * @return \Illuminate\Support\Collection|null
     */
    public function getProductsAttribute()
    {
        if (!$this->relationLoaded('order_products')) {
            return null;
        }

        $result = new Collection();

        foreach ($this->order_products as $orderSize) {
            $orderSize->addVisible('cost_price');

            $productId = $orderSize->product_size->product_id;

            $product = $result
                ->where('id', $productId)
                ->where('sale_price', $orderSize->sale_price)
                ->first();

            if (!$product) {
                $product = $orderSize->product_size->product->toArray();
                $product['sizes'] = [
                    'group' => $orderSize->product_size->size->group,
                    'items' => new Collection()
                ];
                $product['cost_price'] = $orderSize->cost_price;
                $product['sale_price'] = $orderSize->sale_price;

                $result->push($product);
            }

            $orderSize->addHidden([
                'cost_price',
                'sale_price'
            ]);

            if (!$product['sizes']['items']->where('id', $orderSize->id)->count()) {
                $product['sizes']['items']->push($orderSize);
            }
        }

        $result->transform(function ($product) {
            $product['is_refunded'] = false;

            if ($product['sizes']['items']->firstWhere('is_refunded', true)) {
                $product['is_refunded'] = true;
            }

            return $product;
        });

        return $result;
    }

    /**
     * Retrieve residual price by difference between order price and products prices.
     *
     * @return \Illuminate\Support\Collection|null
     */
    public function getResidualPriceAttribute()
    {
        if (!$this->price || !$this->relationLoaded('payments')) {
            return null;
        }

        $residualPrice = $this->price->toArray();

        foreach ($residualPrice as $currencyId => $priceValue) {
            foreach ($this->payments as $orderPayment) {
                $paymentRate = $orderPayment->rate[$currencyId];

                $residualPrice[$currencyId]['value'] -= $paymentRate['value'];
            }

            $residualPrice[$currencyId]['value'] = round($residualPrice[$currencyId]['value']);

            if ($residualPrice[$currencyId]['value'] < 0) {
                $residualPrice[$currencyId]['value'] = 0;
            }
        }

        return $residualPrice;
    }

    /**
     * Retrieve residual payments rate.
     *
     * @return \Illuminate\Support\Collection|null
     */
    public function getResidualPaymentsRateAttribute()
    {
        if (!$this->price || !$this->relationLoaded('payments')) {
            return null;
        }

        $result = new Collection();

        $payments = $this->payments->where('is_withdrawn', false);

        foreach ($payments as $orderPayment) {
            if (!$orderPayment->rate) {
                continue;
            }

            foreach ($orderPayment->rate as $currencyId => $priceValue) {
                $rate = $result->where('currency_id', $currencyId)->first();

                if (!$rate) {
                    $rate = $priceValue;
                } else {
                    $rate['value'] += $priceValue['value'];
                }

                $result->put($currencyId, $rate);
            }
        }

        return $result;
    }

    /**
     * Determine if order status is accepted.
     *
     * @return bool
     */
    public function isAccepted()
    {
        return $this->status === self::STATUS_ACCEPTED;
    }

    /**
     * Determine if order status is completed.
     *
     * @return bool
     */
    public function isCompleted()
    {
        return $this->status === self::STATUS_COMPLETED;
    }

    /**
     * Determine if order status is new.
     *
     * @return bool
     */
    public function isNew()
    {
        return $this->status === self::STATUS_NEW;
    }

    /**
     * Determine if order status is sent.
     *
     * @return bool
     */
    public function isSent()
    {
        return $this->status === self::STATUS_SENT;
    }

    /**
     * Determine if order is refunded.
     *
     * @return bool
     */
    public function isRefunded()
    {
        return $this->status === self::STATUS_REFUNDED;
    }

    /**
     * Determine if order status is partially refunded.
     *
     * @return bool
     */
    public function isPartiallyRefunded()
    {
        return $this->status === self::STATUS_PARTIALLY_REFUNDED;
    }

    /**
     * Determine if order was payed in full.
     *
     * @return bool
     */
    public function isPaid()
    {
        return $this->is_paid;
    }

    /**
     * Determine if order is canceled.
     *
     * @return bool
     */
    public function isCanceled()
    {
        return $this->status === self::STATUS_CANCELED;
    }

    /**
     * Determine if order has manager.
     *
     * @return bool
     */
    public function hasManager()
    {
        return (bool) $this->manager_id;
    }

    /**
     * Order's status list.
     *
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_NEW,
            self::STATUS_ACCEPTED,
            self::STATUS_SENT,
            self::STATUS_COMPLETED,
            self::STATUS_CANCELED,
            self::STATUS_REFUNDED,
            self::STATUS_PARTIALLY_REFUNDED
        ];
    }

    /**
     * Order's payment method list.
     *
     * @return array
     */
    public static function paymentMethods()
    {
        return [
            self::PAYMENT_METHOD_COD,
            self::PAYMENT_METHOD_CARD,
            self::PAYMENT_METHOD_CASH
        ];
    }

    /**
     * Order's payment status list.
     *
     * @return array
     */
    public static function paymentStatuses()
    {
        return [
            self::PAYMENT_STATUS_PAID,
            self::PAYMENT_STATUS_NOT_PAID,
            self::PAYMENT_STATUS_PARTIALLY_PAID
        ];
    }

    /**
     * Delivery methods.
     *
     * @return array
     */
    public static function deliveryMethods()
    {
        return [
            self::DELIVERY_METHOD_COURIER,
            self::DELIVERY_METHOD_NEW_POST,
            self::DELIVERY_METHOD_PICKUP
        ];
    }
}
