<?php

namespace App\Console;

use App\Console\Commands\PurgeMediaCommand;
use App\Jobs\DropDBUnrelatedRecords;
use App\Jobs\FinishWorkdays;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        PurgeMediaCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('media:purge')->daily();
        $schedule->job(new FinishWorkdays())->daily();
        $schedule->job(new DropDBUnrelatedRecords())->daily();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console/index.php');
    }
}
