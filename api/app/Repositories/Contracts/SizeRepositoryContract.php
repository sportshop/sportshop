<?php

namespace App\Repositories\Contracts;

interface SizeRepositoryContract
{
    /**
     * Find all sizes by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Support\Collection
     */
    public function findAll(array $criteria = [], array $options = []);

    /**
     * Find size by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\SizeGroup
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id, array $criteria = []);

    /**
     * Create size.
     *
     * @param array $attributes
     *
     * @return \App\Models\SizeGroup
     * @throws \Exception
     */
    public function create(array $attributes);

    /**
     * Update size by id.
     *
     * @param int $id
     * @param array $attributes
     *
     * @return \App\Models\SizeGroup
     * @throws \Exception
     */
    public function update(int $id, array $attributes);

    /**
     * Delete size by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id);
}
