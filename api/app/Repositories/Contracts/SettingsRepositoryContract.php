<?php

namespace App\Repositories\Contracts;

interface SettingsRepositoryContract
{
    /**
     * Find all settings.
     *
     * @param array $criteria
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = []);

    /**
     * Find settings value by name.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function findByName(string $name);

    /**
     * Update all entities by key.
     *
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Collection
     * @throws \Exception
     */
    public function updateAll(array $data);
}
