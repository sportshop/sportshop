<?php

namespace App\Repositories\Contracts;

interface TransferRepositoryContract
{
    /**
     * Find product transfers by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = []);

    /**
     * Get summary price and total quantity.
     *
     * @param array $criteria
     * @return array
     */
    public function getTotalPriceAndQuantity(array $criteria = []);
}
