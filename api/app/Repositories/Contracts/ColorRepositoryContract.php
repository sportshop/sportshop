<?php

namespace App\Repositories\Contracts;

interface ColorRepositoryContract
{
    /**
     * Find all colors by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Support\Collection
     */
    public function findAll(array $criteria = [], array $options = []);

    /**
     * Find color by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\Color
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id, array $criteria = []);

    /**
     * Create color.
     *
     * @param array $attributes
     *
     * @return \App\Models\Color
     * @throws \Exception
     */
    public function create(array $attributes);

    /**
     * Update color by id.
     *
     * @param int $id
     * @param array $attributes
     *
     * @return \App\Models\Color
     * @throws \Exception
     */
    public function update(int $id, array $attributes);

    /**
     * Delete color by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id);
}
