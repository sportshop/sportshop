<?php

namespace App\Repositories\Contracts;

use Exception;
use Freevital\Repository\Contracts\RepositoryContract;

interface CurrencyRepositoryContract extends RepositoryContract
{
    /**
     * Save all currencies.
     *
     * @param array $records
     *
     * @return array
     * @throws Exception
     */
    public function save(array $records);
}
