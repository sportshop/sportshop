<?php

namespace App\Repositories\Contracts;

interface StoreRepositoryContract
{
    /**
     * Find stores by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = []);

    /**
     * Find store by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\Store|null
     */
    public function findById(int $id, array $criteria = []);

    /**
     * Create store.
     *
     * @param array $attributes
     *
     * @return \App\Models\Store
     * @throws \Exception
     */
    public function create(array $attributes);

    /**
     * Update store.
     *
     * @param $id
     * @param array $attributes
     *
     * @return \App\Models\Store
     * @throws \Exception
     */
    public function update($id, array $attributes);

    /**
     * Delete store by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id);
}
