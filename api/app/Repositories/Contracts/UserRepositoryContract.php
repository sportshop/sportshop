<?php

namespace App\Repositories\Contracts;

interface UserRepositoryContract
{
    /**
     * Find all users by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = []);

    /**
     * Find user by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\User|null
     */
    public function findById(int $id, array $criteria = []);

    /**
     * Find user by email.
     *
     * @param string $email
     * @param array $criteria
     *
     * @return \App\Models\User|null
     */
    public function findByEmail(string $email, array $criteria = []);

    /**
     * Create user.
     *
     * @param array $attributes
     *
     * @return \App\Models\User
     * @throws \Exception
     */
    public function create(array $attributes);

    /**
     * Update user by id.
     *
     * @param int $id
     * @param array $attributes
     *
     * @return \App\Models\User
     * @throws \Exception
     */
    public function update(int $id, array $attributes);

    /**
     * Update user password.
     *
     * @param int $id
     * @param string $password
     *
     * @return bool
     * @throws \Exception
     */
    public function updatePassword(int $id, string $password);

    /**
     * Delete user by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id);

    /**
     * Create workday log.
     *
     * @param int $id
     *
     * @return \App\Models\UserActivity
     */
    public function createWorkdayLog(int $id);

    /**
     * Close (finish) workday log.
     *
     * @param int $id
     *
     * @return \App\Models\UserActivity
     */
    public function closeWorkdayLog(int $id);

    /**
     * Get salary of all users compiling user activity, transactions, sales.
     * Filter and transform all data by date.
     *
     * @param array $criteria
     *
     * @return array
     */
    public function getUsersSalary(array $criteria = []);

    /**
     * Get user salary compiling user activity, transactions, sales.
     * Filter and transform all data by months.
     *
     * @param int $user_id
     * @param int|null $year
     * @param array $criteria
     *
     * @return array
     */
    public function getYearlySalaryOfUser(int $user_id, int $year = null, array $criteria = []);

    /**
     * Get user salary compiling user activity, transactions, sales.
     * Filter and transform all data by monthly days.
     *
     * @param int $user_id
     * @param int $year
     * @param int $month
     * @param array $criteria
     *
     * @return array
     */
    public function getMonthlySalaryOfUser(int $user_id, int $year, int $month, array $criteria = []);
}
