<?php

namespace App\Repositories\Contracts;

use Exception;
use Freevital\Repository\Contracts\RepositoryContract;

interface DiscountRepositoryContract extends RepositoryContract
{
    /**
     * Save all discounts.
     *
     * @param array $records
     *
     * @return array
     * @throws Exception
     */
    public function save(array $records);
}
