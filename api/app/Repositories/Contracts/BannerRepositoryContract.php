<?php

namespace App\Repositories\Contracts;

interface BannerRepositoryContract
{
    /**
     * Find all banners by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = []);

    /**
     * Find banner by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\Banner
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id, array $criteria = []);

    /**
     * Create banner.
     *
     * @param array $attributes
     *
     * @return \App\Models\Banner
     * @throws \Exception
     */
    public function create(array $attributes);

    /**
     * Update banner by id.
     *
     * @param int $id
     * @param array $attributes
     *
     * @return \App\Models\Banner
     * @throws \Exception
     */
    public function update(int $id, array $attributes);

    /**
     * Delete banner by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id);
}
