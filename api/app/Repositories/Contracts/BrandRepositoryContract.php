<?php

namespace App\Repositories\Contracts;

interface BrandRepositoryContract
{
    /**
     * Find all brands.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = []);

    /**
     * Find brand by id.
     *
     * @param int $id
     *
     * @return \App\Models\Brand
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id);

    /**
     * Create brand.
     *
     * @param array $attributes
     *
     * @return \App\Models\Brand
     * @throws \Exception
     */
    public function create(array $attributes);

    /**
     * Update brand.
     *
     * @param int $id
     * @param array $attributes
     *
     * @return \App\Models\Brand
     * @throws \Exception
     */
    public function update(int $id, array $attributes);

    /**
     * Delete brand by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id);
}
