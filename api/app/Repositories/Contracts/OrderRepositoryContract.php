<?php

namespace App\Repositories\Contracts;

interface OrderRepositoryContract
{
    /**
     * Find all orders by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = []);

    /**
     * Find order by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\Order|null
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id, array $criteria = []);

    /**
     * Create order.
     *
     * @param array $attributes
     *
     * @return \App\Models\Order
     * @throws \Exception
     */
    public function create(array $attributes);

    /**
     * Update order by id.
     *
     * @param int $id
     * @param array $attributes
     *
     * @return \App\Models\Order
     * @throws \Exception
     */
    public function update(int $id, array $attributes);

    /**
     * Update order manager and set "accepted" status.
     *
     * @param int $id
     * @param int $user_id
     *
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function accept(int $id, int $user_id);

    /**
     * Update delivery declaration and set "sent" status.
     *
     * @param int $id
     * @param string $declaration
     *
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function send(int $id, string $declaration);

    /**
     * Set "completed" and "paid" status.
     *
     * @param int $id
     *
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function complete(int $id);

    /**
     * Set "partially_refunded" or "refunded" for order by given products.
     *
     * @param int $id
     * @param array $products
     *
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    public function refund(int $id, array $products = []);

    /**
     * Set order status to "cancel".
     *
     * @param int $id
     *
     * @return bool|\Illuminate\Support\Collection
     */
    public function cancel(int $id);

    /**
     * Withdrawn order(s) payments.
     *
     * @param array $criteria
     *
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    public function withdrawnPayments(array $criteria = []);

    /**
     * Get orders count.
     *
     * @param array $criteria
     *
     * @return integer
     */
    public function getCount(array $criteria = []);

    /**
     * Delete order product.
     *
     * @param int $id
     * @param int $product_id
     *
     * @return int
     * @throws \Exception
     */
    public function deleteProduct(int $id, int $product_id);

    /**
     * Get completed orders and products quantity and transform by date.
     *
     * @param array $criteria
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCompletedOrdersAndProductsQuantity(array $criteria = []);

    /**
     * Get completed orders and products cost prices and transform by date.
     *
     * @param array $criteria
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCompletedOrdersAndProductsPrices(array $criteria = []);

    /**
     * Get not completed orders quantity and transform by date.
     *
     * @param array $criteria
     *
     * @return array
     */
    public function getNotCompletedOrdersQuantity(array $criteria = []);

    /**
     * Get summary non-withdrawal payments.
     *
     * @param array $criteria
     *
     * @return array
     */
    public function getNonWithdrawalPaymentsSum(array $criteria = []);
}
