<?php

namespace App\Repositories\Contracts;

use Freevital\Repository\Contracts\RepositoryContract;
use Illuminate\Database\Eloquent\Collection;

interface RoleRepositoryContract extends RepositoryContract
{
    /**
     * Find all system roles.
     *
     * @return Collection
     */
    public function findSystemRoles();

    /**
     * Find all custom roles.
     *
     * @return Collection
     */
    public function findCustomRoles();
}
