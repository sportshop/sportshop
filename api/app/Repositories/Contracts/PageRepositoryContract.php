<?php

namespace App\Repositories\Contracts;

interface PageRepositoryContract
{
    /**
     * Find pages by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = []);

    /**
     * Find page by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\Page|null
     */
    public function findById(int $id, array $criteria = []);

    /**
     * Find page by slug.
     *
     * @param string $slug
     * @param array $criteria
     *
     * @return \App\Models\Page|null
     */
    public function findBySlug(string $slug, array $criteria = []);

    /**
     * Create page.
     *
     * @param array $attributes
     *
     * @return \App\Models\Page
     * @throws \Exception
     */
    public function create(array $attributes);

    /**
     * Update page.
     *
     * @param $id
     * @param array $attributes
     *
     * @return \App\Models\Page
     * @throws \Exception
     */
    public function update($id, array $attributes);

    /**
     * Delete page by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id);
}
