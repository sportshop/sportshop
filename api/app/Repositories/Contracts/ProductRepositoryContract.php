<?php

namespace App\Repositories\Contracts;

interface ProductRepositoryContract
{
    /**
     * Find all products by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = []);

    /**
     * Find product by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\Product
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id, array $criteria = []);

    /**
     * Find product by slug.
     *
     * @param string $slug
     * @param array $criteria
     *
     * @return \App\Models\Product
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findBySlug(string $slug, array $criteria = []);

    /**
     * Create product.
     *
     * @param array $attributes
     *
     * @return \App\Models\Product
     * @throws \Exception
     */
    public function create(array $attributes);

    /**
     * Update product by id.
     *
     * @param int $id
     * @param array $attributes
     *
     * @return \App\Models\Product
     * @throws \Exception
     */
    public function update(int $id, array $attributes);

    /**
     * Update product type.
     *
     * @param int $id
     * @param string $type
     *
     * @return \App\Models\Product
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function updateType(int $id, string $type);

    /**
     * Update product visibility status.
     *
     * @param int $id
     * @param bool $status
     *
     * @return \App\Models\Product
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function updateVisibilityStatus(int $id, bool $status);

    /**
     * Delete product by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id);

    /**
     * Retrieve min, max values of sale price.
     *
     * @param array $criteria
     *
     * @return \stdClass|null
     */
    public function getMinMaxOfSalePrice(array $criteria);

    /**
     * Get summary cost price and total quantity of all products.
     *
     * @param array $criteria
     *
     * @return array
     */
    public function getTotalCostPriceAndQuantity(array $criteria = []);
}
