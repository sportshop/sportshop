<?php

namespace App\Repositories\Contracts;

interface CategoryRepositoryContract
{
    /**
     * Find categories by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = []);

    /**
     * Find category by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\Category|null
     */
    public function findById(int $id, array $criteria = []);

    /**
     * Find category by slug.
     *
     * @param string $slug
     * @param array $criteria
     *
     * @return \App\Models\Category|null
     */
    public function findBySlug(string $slug, array $criteria = []);

    /**
     * Create category.
     *
     * @param array $attributes
     *
     * @return \App\Models\Category
     * @throws \Exception
     */
    public function create(array $attributes);

    /**
     * Update category.
     *
     * @param $id
     * @param array $attributes
     *
     * @return \App\Models\Category
     * @throws \Exception
     */
    public function update($id, array $attributes);

    /**
     * Delete category by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id);
}
