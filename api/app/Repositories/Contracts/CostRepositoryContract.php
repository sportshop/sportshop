<?php

namespace App\Repositories\Contracts;

interface CostRepositoryContract
{
    /**
     * Find all costs by criteria.
     *
     * @param array $criteria
     * @param array $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = []);

    /**
     * Find cost by id.
     *
     * @param int $id
     * @param array $criteria
     * @return \App\Models\Cost
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id, array $criteria = []);

    /**
     * Create cost.
     *
     * @param array $attributes
     * @return \App\Models\Cost
     * @throws \Exception
     */
    public function create(array $attributes);

    /**
     * Update cost by id.
     *
     * @param int $id
     * @param array $attributes
     * @return \App\Models\Cost
     * @throws \Exception
     */
    public function update(int $id, array $attributes);

    /**
     * Delete cost by id.
     *
     * @param int $id
     * @return bool|int|null
     */
    public function delete(int $id);

    /**
     * Get only rates of cost types and group date.
     *
     * @param array $criteria
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getRatesByDate(array $criteria = []);

    /**
     * Get total amount.
     *
     * @param array $criteria
     * @return array
     */
    public function getTotalAmount(array $criteria = []);
}
