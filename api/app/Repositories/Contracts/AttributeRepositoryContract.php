<?php

namespace App\Repositories\Contracts;

interface AttributeRepositoryContract
{
    /**
     * Find all attributes by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Support\Collection
     */
    public function findAll(array $criteria = [], array $options = []);

    /**
     * Find attribute by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\AttributeGroup
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id, array $criteria = []);

    /**
     * Create attribute.
     *
     * @param array $attributes
     *
     * @return \App\Models\AttributeGroup
     * @throws \Exception
     */
    public function create(array $attributes);

    /**
     * Update attribute by id.
     *
     * @param int $id
     * @param array $attributes
     *
     * @return \App\Models\AttributeGroup
     * @throws \Exception
     */
    public function update(int $id, array $attributes);

    /**
     * Delete attribute by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id);
}
