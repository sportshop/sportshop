<?php

namespace App\Repositories\Eloquent;

use App\Models\Banner;
use App\Repositories\Contracts\BannerRepositoryContract;
use Exception;
use Illuminate\Support\Facades\DB;

class BannerRepository implements BannerRepositoryContract
{
    /**
     * Find all banners by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = [])
    {
        $collection = Banner::query()
            ->withMedia(Banner::MEDIA_MAIN)
            ->joinRate('price')
            ->criteria($criteria)
            ->orderBy('created_at', 'DESC')
            ->get();

        return $collection;
    }

    /**
     * Find banner by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\Banner
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id, array $criteria = [])
    {
        $model = Banner::query()
            ->withMedia(Banner::MEDIA_MAIN)
            ->joinRate('price')
            ->find($id);

        return $model;
    }

    /**
     * Create banner.
     *
     * @param array $attributes
     *
     * @return \App\Models\Banner
     * @throws \Exception
     */
    public function create(array $attributes)
    {
        DB::beginTransaction();

        try {
            $attributes['data'] = array_only($attributes, ['url', 'title']);

            $model = Banner::create($attributes);

            // Attach a media
            if ($image_id = array_get($attributes, 'image_id')) {
                $model->attachMedia($image_id, Banner::MEDIA_MAIN);
            }

            DB::commit();

            return $model;
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * Update banner by id.
     *
     * @param int $id
     * @param array $attributes
     *
     * @return \App\Models\Banner
     * @throws \Exception
     */
    public function update(int $id, array $attributes)
    {
        DB::beginTransaction();

        try {
            $attributes['data'] = array_only($attributes, ['url', 'title']);

            $model = Banner::findOrFail($id);
            $model->fill($attributes);
            $model->save();

            // Sync the media
            if ($image_id = array_get($attributes, 'image_id')) {
                $model->syncMedia([$image_id], Banner::MEDIA_MAIN);
            }

            DB::commit();

            return $model;
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * Delete banner by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id)
    {
        $deleted = Banner::destroy($id);

        return $deleted;
    }
}
