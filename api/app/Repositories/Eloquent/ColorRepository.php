<?php

namespace App\Repositories\Eloquent;

use App\Models\Color;
use App\Repositories\Contracts\ColorRepositoryContract;
use Exception;

class ColorRepository implements ColorRepositoryContract
{
    /**
     * Find all colors by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Support\Collection
     */
    public function findAll(array $criteria = [], array $options = [])
    {
        $query = Color::query();

        if (array_get($criteria, 'products_exists') && array_get($criteria, 'store_id')) {
            $query->whereHas('products', function ($query) use ($criteria) {
                $query
                    ->joinSalePrice($criteria['store_id'], array_get($criteria, 'discount_id'))
                    ->criteria($criteria)
                    ->storeAvailable($criteria['store_id']);
            });
        }

        $collection = $query->get();

        return $collection;
    }

    /**
     * Find color by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\Color
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id, array $criteria = [])
    {
        $model = Color::query()
            ->where('id', $id)
            ->first();

        return $model;
    }

    /**
     * Create color.
     *
     * @param array $attributes
     *
     * @return \App\Models\Color
     * @throws \Exception
     */
    public function create(array $attributes)
    {
        try {
            $model = Color::create($attributes);

            return $model;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Update color by id.
     *
     * @param int $id
     * @param array $attributes
     *
     * @return \App\Models\Color
     * @throws \Exception
     */
    public function update(int $id, array $attributes)
    {
        try {
            $model = Color::findOrFail($id);
            $model->fill($attributes);
            $model->save();

            return $model;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Delete color by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id)
    {
        $deleted = Color::destroy($id);

        return $deleted;
    }
}
