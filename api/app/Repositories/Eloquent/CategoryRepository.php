<?php

namespace App\Repositories\Eloquent;

use App\Models\Category;
use App\Repositories\Contracts\CategoryRepositoryContract;
use Exception;
use Illuminate\Support\Facades\DB;

class CategoryRepository implements CategoryRepositoryContract
{
    /**
     * Find categories by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = [])
    {
        $query = Category::query()
            ->withMedia(Category::MEDIA_THUMBNAIL)
            ->withDepth()
            ->filter($criteria);

        if (array_get($criteria, 'root_slug')) {
            $rootCategory = Category::where('slug', $criteria['root_slug'])->first();

            $query->descendantsAndSelf($rootCategory->id);
        }

        if (array_get($criteria, 'products_exists') && array_get($criteria, 'store_id')) {
            $query
                ->active()
                ->with(['children' => function ($query) use ($criteria) {
                    $query
                        ->active()
                        ->whereHas('products', function ($query) use ($criteria) {
                            $query->storeAvailable($criteria['store_id']);

                            if (array_get($criteria, 'product_type')) {
                                $query->criteria(['type' => $criteria['product_type']]);
                            }
                        });
                }])
                ->where(function ($query) use ($criteria) {
                    $query
                        ->whereHas('products', function ($query) use ($criteria) {
                            $query->storeAvailable($criteria['store_id']);

                            if (array_get($criteria, 'product_type')) {
                                $query->criteria(['type' => $criteria['product_type']]);
                            }
                        })
                        ->orWhereHas('children', function ($query) use ($criteria) {
                            $query
                                ->active()
                                ->whereHas('products', function ($query) use ($criteria) {
                                    $query->storeAvailable($criteria['store_id']);

                                    if (array_get($criteria, 'product_type')) {
                                        $query->criteria(['type' => $criteria['product_type']]);
                                    }
                                });
                        });
                });
        }

        $collection = $query->get();

        if (array_get($options, 'flat')) {
            $collection = $collection->toFlatTree();

            $collection->transform(function ($item) {
                $item->append('depth_name');

                return $item;
            });
        } else if (!array_get($options, 'without_tree')) {
            $collection = $collection->toTree();
        }

        return $collection;
    }

    /**
     * Find category by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\Category|null
     */
    public function findById(int $id, array $criteria = [])
    {
        $model = Category::query()
            ->with('children')
            ->withMedia(Category::MEDIA_THUMBNAIL)
            ->find($id);

        if (!$model) {
            return null;
        }

        $model->addVisible('parent_id');

        return $model;
    }

    /**
     * Find category by slug.
     *
     * @param string $slug
     * @param array $criteria
     *
     * @return \App\Models\Category|null
     */
    public function findBySlug(string $slug, array $criteria = [])
    {
        $model = Category::query()
            ->with('children')
            ->withMedia(Category::MEDIA_THUMBNAIL)
            ->where('slug', $slug)
            ->first();

        return $model;
    }

    /**
     * Create category.
     *
     * @param array $attributes
     *
     * @return \App\Models\Category
     * @throws \Exception
     */
    public function create(array $attributes)
    {
        DB::beginTransaction();

        try {
            $model = Category::create($attributes);

            // Attach a media
            if ($thumbnail_id = array_get($attributes, 'thumbnail_id')) {
                $model->attachMedia($thumbnail_id, Category::MEDIA_THUMBNAIL);
                $model->loadMedia(Category::MEDIA_THUMBNAIL);
            }

            DB::commit();

            $model->addVisible('parent_id');

            return $model;
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * Update category.
     *
     * @param $id
     * @param array $attributes
     *
     * @return \App\Models\Category
     * @throws \Exception
     */
    public function update($id, array $attributes)
    {
        DB::beginTransaction();

        try {
            $model = Category::findOrFail($id);
            $model->fill($attributes);
            $model->save();

            // Sync a media
            if ($thumbnail_id = array_get($attributes, 'thumbnail_id')) {
                $model->syncMedia([$thumbnail_id], Category::MEDIA_THUMBNAIL);
                $model->loadMedia(Category::MEDIA_THUMBNAIL);
            }

            DB::commit();

            $model->addVisible('parent_id');

            return $model;
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * Delete category by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id)
    {
        $deleted = Category::destroy($id);

        return $deleted;
    }
}
