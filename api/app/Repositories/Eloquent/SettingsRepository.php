<?php

namespace App\Repositories\Eloquent;

use App\Models\Settings;
use App\Repositories\Contracts\SettingsRepositoryContract;
use Exception;
use Illuminate\Support\Facades\DB;

class SettingsRepository implements SettingsRepositoryContract
{
    /**
     * Find all settings.
     *
     * @param array $criteria
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [])
    {
        $collection = Settings::query()
            ->criteria($criteria)
            ->pluck('value', 'name');

        $collection->transform(function ($value, $key) {
            if ($key === 'feature_categories') {
                $value = json_decode($value, true);
            }
            return $value;
        });

        return $collection;
    }

    /**
     * Find settings value by name.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function findByName(string $name)
    {
        $value = Settings::where('name', $name)->value('value');

        if ($name === 'feature_categories') {
            $value = json_decode($value, true);
        }

        return $value;
    }

    /**
     * Update all entities by key.
     *
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Collection
     * @throws \Exception
     */
    public function updateAll(array $data)
    {
        DB::beginTransaction();

        try {
            foreach ($data as $key => $value) {
                if (is_object($value) || is_array($value)) {
                    $value = json_encode($value);
                }

                $settingsModel = Settings::firstOrCreate(['name' => $key]);
                $settingsModel->value = $value;
                $settingsModel->save();
            }

            DB::commit();

            $collection = $this->findAll();

            return $collection;
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
    }
}
