<?php

namespace App\Repositories\Eloquent;

use App\Models\Discount;
use App\Models\Role;
use App\Models\User;
use App\Models\UserActivity;
use App\Models\UserTransaction;
use App\Repositories\Contracts\UserRepositoryContract;
use Carbon\Carbon;
use Exception;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class UserRepository implements UserRepositoryContract
{
    /**
     * Find all users by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = [])
    {
        // Collect query.
        $query = User::query()
            ->with([
                'roles.permissions',
                'data',
                'purchased_orders'
            ])
            ->criteria($criteria);

        // Build paginator by "limit" param or get collection with all records.
        if ($limit = array_get($options, 'limit')) {
            $page = array_get($options, 'page');

            $total = DB::table(DB::raw("({$query->toSql()}) as sub"))
                ->mergeBindings($query->getQuery())
                ->count();

            $items = $query
                ->forPage($page, $limit)
                ->get();

            $collection = new LengthAwarePaginator($items, $total, $limit, $page);
        } else {
            $collection = $query->get();
        }

        $collection->transform(function ($user) {
            $user->append('purchased_orders_price');
            $user->addVisible('purchased_orders_price');

            return $user;
        });

        return $collection;
    }

    /**
     * Find user by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\User|null
     */
    public function findById(int $id, array $criteria = [])
    {
        $model = User::query()
            ->with([
                'roles.permissions',
                'data' => function ($query) {
                    $query->joinRate('rate');
                }
            ])
            ->find($id);

        $model->load('roles.permissions');
        $model->permissions = $model->role->permissions->pluck('name');

        if (array_get($criteria, 'with_sale_percentages')) {
            $model->load('sale_percentages');
            $model->addVisible('sale_percentages');
        }

        if (array_get($criteria, 'with_workday_status')) {
            $model->load('activity_today');
            $model->append('workday_status');
        }

        return $model;
    }

    /**
     * Find user by email.
     *
     * @param string $email
     * @param array $criteria
     *
     * @return \App\Models\User|null
     */
    public function findByEmail(string $email, array $criteria = [])
    {
        $model = User::query()
            ->where('email', $email)
            ->criteria($criteria)
            ->first();

        return $model;
    }

    /**
     * Create user.
     *
     * @param array $attributes
     *
     * @return \App\Models\User
     * @throws \Exception
     */
    public function create(array $attributes)
    {
        DB::beginTransaction();

        try {
            $model = new User();
            $model->fill($attributes);
            $model->is_active = true;
            $model->save();

            // Save user role.
            if ($roleName = array_get($attributes, 'role_name')) {
                $this->saveRole($model, $roleName);
            }

            // Save user additional data.
            $data = array_get($attributes, 'data', []);
            if (!array_get($data, 'discount_id')) {
                $data['discount_id'] = Discount::where('is_default', true)->value('id');
            }
            $this->saveUserData($model, $data);

            // Save sale percentages.
            if ($percentages = array_get($attributes, 'sale_percentages')) {
                $this->saveSalePercentages($model, $percentages);
            }

            DB::commit();

            return $model;
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * Update user by id.
     *
     * @param int $id
     * @param array $attributes
     *
     * @return \App\Models\User
     * @throws \Exception
     */
    public function update(int $id, array $attributes)
    {
        DB::beginTransaction();

        try {
            $model = User::findOrFail($id);
            $model->fill($attributes);
            $model->save();

            // Save user role.
            if ($roleName = array_get($attributes, 'role_name')) {
                $this->saveRole($model, $roleName);
            }

            // Save user additional data.
            if ($data = array_get($attributes, 'data')) {
                $this->saveUserData($model, $data);
            }

            // Save sale percentages.
            if ($percentages = array_get($attributes, 'sale_percentages')) {
                $this->saveSalePercentages($model, $percentages);
            }

            // Delete discount if role was updated from "member" to some managers.
            // TODO: Should optimize.
            if ($roleName != 'member' && $model->data && $model->data->discount) {
                $model->data->discount()->dissociate();
                $model->data->save();
            }

            DB::commit();

            return $model;
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * Update user password.
     *
     * @param int $id
     * @param string $password
     *
     * @return bool
     * @throws \Exception
     */
    public function updatePassword(int $id, string $password)
    {
        $user = User::query()
            ->where('is_active', true)
            ->where('id', $id)
            ->firstOrFail();

        $user->password = $password;

        return $user->save();
    }

    /**
     * Delete user by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id)
    {
        $deleted = User::destroy($id);

        return $deleted;
    }

    /**
     * Create workday log.
     *
     * @param int $id
     *
     * @return \App\Models\UserActivity
     */
    public function createWorkdayLog(int $id)
    {
        $model = new UserActivity();
        $model->user_id = $id;
        $model->save();

        return $model;
    }

    /**
     * Close (finish) workday log.
     * Update finished_at.
     *
     * @param int $id
     *
     * @return \App\Models\UserActivity
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function closeWorkdayLog(int $id)
    {
        $model = UserActivity::query()
            ->whereDate('created_at', date('Y-m-d'))
            ->latest()
            ->firstOrFail();

        $model->finished_at = time();
        $model->save();

        return $model;
    }

    /**
     * Get salary of all users compiling user activity, transactions, sales.
     * Filter and transform all data by date.
     * TODO: MUST optimize code.
     *
     * @param array $criteria
     *
     * @return array
     */
    public function getUsersSalary(array $criteria = [])
    {
        $store_id = array_get($criteria, 'store_id');
        $date = array_get($criteria, 'date');
        preg_match('/(\d*)_?(\d*)/', $date, $date);

        $criteria['id'] = array_get($criteria, 'manager_id');

        $users = User::query()
            ->with([
                'data' => function ($query) {
                    $query->joinRate('rate');
                },
                'sale_percentages',
                'activity'        => function ($query) use ($date) {
                    $query->groupByDay();

                    if ($year = array_get($date, 1)) {
                        $query->whereYear('created_at', $year);

                        if ($month = array_get($date, 2)) {
                            $query->whereMonth('created_at', $month);
                        }
                    }
                },
                'transactions'    => function ($query) use ($date) {
                    $query
                        ->joinRate('rate')
                        ->where('type', UserTransaction::TYPE_ADDITIONAL);

                    if ($year = array_get($date, 1)) {
                        $query->whereYear('payed_at', $year);

                        if ($month = array_get($date, 2)) {
                            $query->whereMonth('payed_at', $month);
                        }
                    }
                }
            ])
            ->withManagedCompletedOrders(function ($query) use ($date, $store_id) {
                if ($store_id) {
                    $query->where('store_id', $store_id);
                }
                if ($year = array_get($date, 1)) {
                    $query->whereYear('created_at', $year);

                    if ($month = array_get($date, 2)) {
                        $query->whereMonth('created_at', $month);
                    }
                }
            })
            ->criteria($criteria)
            ->get();

        // Result structure.
        $resultDataTemplate = [
            'salary'      => null,
            'salary_rate' => null,
            'sales'       => null
        ];
        $result = [
            'data'  => [],
            'total' => $resultDataTemplate
        ];

        foreach ($users as $user) {

            // Collect user activity.
            $userTempSalaryMonths = [];
            foreach ($user->activity as $activityItem) {
                $rate = null;
                if ($user->data && $user->data->rate) {
                    $rate = $user->data->rate;

                    foreach ($rate as $currencyId => $currencyRate) {
                        if ($user->data->rate_type == 'hourly') {
                            $currencyRate['value'] *= $activityItem->hours;

                            $rate->put($currencyId, $currencyRate);
                        } elseif ($user->data->rate_type == 'monthly') {
                            $salaryMonth = Carbon::parse($activityItem->date)->format('Y-m');

                            if (!in_array($salaryMonth, $userTempSalaryMonths)) {
                                $rate->put($currencyId, $currencyRate);

                                array_push($userTempSalaryMonths, $salaryMonth);
                            }
                        }
                    }
                }

                $rate = $rate->toArray();

                if (!isset($result['data'][$activityItem->date])) {
                    $result['data'][$activityItem->date] = $resultDataTemplate;
                }

                if ($result['data'][$activityItem->date]['salary_rate']) {
                    foreach ($rate as $currencyId => $currencyRate) {
                        $result['data'][$activityItem->date]['salary_rate'][$currencyId]['value'] += $currencyRate['value'];
                    }
                } else {
                    $result['data'][$activityItem->date]['salary_rate'] = $rate;
                }

                if ($result['data'][$activityItem->date]['salary']) {
                    foreach ($rate as $currencyId => $currencyRate) {
                        $result['data'][$activityItem->date]['salary'][$currencyId]['value'] += $currencyRate['value'];
                    }
                } else {
                    $result['data'][$activityItem->date]['salary'] = $rate;
                }

                if ($result['total']['salary_rate']) {
                    foreach ($rate as $currencyId => $currencyRate) {
                        $result['total']['salary_rate'][$currencyId]['value'] += $currencyRate['value'];
                    }
                } else {
                    $result['total']['salary_rate'] = $rate;
                }

                if ($result['total']['salary']) {
                    foreach ($rate as $currencyId => $currencyRate) {
                        $result['total']['salary'][$currencyId]['value'] += $currencyRate['value'];
                    }
                } else {
                    $result['total']['salary'] = $rate;
                }
            }

            // Collect and transform user sales.
            $sales = $user->sales;
            $sales->transform(function ($sale) {
                $sale['date'] = $sale['date']->format('Y-m-d');

                return $sale;
            });
            $sales = $sales->groupBy('date');
            foreach ($sales as $date => $monthSales) {
                $rate = new Collection();

                foreach ($monthSales as $sale) {
                    if (!empty($sale['rate'])) {
                        foreach ($sale['rate'] as $currencyId => $currencyRate) {
                            if (isset($rate[$currencyId])) {
                                $currencyRate['value'] += $rate[$currencyId]['value'];
                            }
                            $rate->put($currencyId, $currencyRate);
                        }
                    }
                }

                $rate = $rate->toArray();

                if (!isset($result['data'][$date])) {
                    $result['data'][$date] = $resultDataTemplate;
                }

                if ($result['data'][$date]['sales']) {
                    foreach ($rate as $currencyId => $currencyRate) {
                        $result['data'][$date]['sales'][$currencyId]['value'] += $currencyRate['value'];
                    }
                } else {
                    $result['data'][$date]['sales'] = $rate;
                }

                if ($result['data'][$date]['salary']) {
                    foreach ($rate as $currencyId => $currencyRate) {
                        $result['data'][$date]['salary'][$currencyId]['value'] += $currencyRate['value'];
                    }
                } else {
                    $result['data'][$date]['salary'] = $rate;
                }

                if ($result['total']['sales']) {
                    foreach ($rate as $currencyId => $currencyRate) {
                        $result['total']['sales'][$currencyId]['value'] += $currencyRate['value'];
                    }
                } else {
                    $result['total']['sales'] = $rate;
                }

                if ($result['total']['salary']) {
                    foreach ($rate as $currencyId => $currencyRate) {
                        if (!array_key_exists($currencyId, $result['total']['salary'])) {
                            $result['total']['salary'][$currencyId] = ['value' => 0];
                        }
                        $result['total']['salary'][$currencyId]['value'] += $currencyRate['value'];
                    }
                } else {
                    $result['total']['salary'] = $rate;
                }
            }

            // Collect and transform user transactions.
            $transactions = $user->transactions;
            $transactions->transform(function ($transaction) {
                $payedAt = $transaction->payed_at->format('Y-m-d');

                $transaction = $transaction->toArray();
                $transaction['payed_at'] = $payedAt;

                return $transaction;
            });
            $transactions = $transactions->groupBy('payed_at');
            foreach ($transactions as $date => $monthTransactions) {
                $rate = new Collection();

                foreach ($monthTransactions as $transaction) {
                    foreach ($transaction['rate'] as $currencyId => $currencyRate) {
                        if (isset($rate[$currencyId])) {
                            $currencyRate['value'] += $rate[$currencyId]['value'];
                        }
                        $rate->put($currencyId, $currencyRate);
                    }
                }

                $rate = $rate->toArray();

                if (!isset($result['data'][$date])) {
                    $result['data'][$date] = $resultDataTemplate;
                }

                if ($result['data'][$date]['salary']) {
                    foreach ($rate as $currencyId => $currencyRate) {
                        $result['data'][$date]['salary'][$currencyId]['value'] += $currencyRate['value'];
                    }
                } else {
                    $result['data'][$date]['salary'] = $rate;
                }

                if ($result['total']['salary']) {
                    foreach ($rate as $currencyId => $currencyRate) {
                        $result['total']['salary'][$currencyId]['value'] += $currencyRate['value'];
                    }
                } else {
                    $result['total']['salary'] = $rate;
                }
            }
        }

        // Sort by date.
        ksort($result['data']);

        return $result;
    }

    /**
     * Get user salary compiling user activity, transactions, sales.
     * Filter and transform all data by months.
     * TODO: MUST optimize code.
     *
     * @param int $user_id
     * @param int|null $year
     * @param array $criteria
     *
     * @return array
     */
    public function getYearlySalaryOfUser(int $user_id, int $year = null, array $criteria = [])
    {
        $user = User::query()
            ->with([
                'data' => function ($query) {
                    $query->joinRate('rate');
                },
                'sale_percentages',
                'activity'        => function ($query) use ($year) {
                    $query->groupByMonth();

                    if ($year) {
                        $query->whereYear('created_at', $year);
                    }
                },
                'transactions'    => function ($query) use ($year) {
                    $query->joinRate('rate');

                    if ($year) {
                        $query->whereYear('payed_at', $year);
                    }
                }
            ])
            ->withManagedCompletedOrders(function ($query) use ($year, $criteria) {
                $query->whereYear('created_at', $year);

                if (array_get($criteria, 'store_id')) {
                    $query->where('store_id', $criteria['store_id']);
                }
            })
            ->criteria($criteria)
            ->findOrFail($user_id);

        // Result structure.
        $result = [];
        $resultDataTemplate = [
            'salary'       => null,
            'sales'        => null,
            'transactions' => []
        ];

        // Collect user activity.
        foreach ($user->activity as $activityItem) {
            $rate = null;
            if ($user->data && $user->data->rate) {
                $rate = $user->data->rate->toArray();

                if (empty($rate)) continue;

                foreach ($rate as $currencyId => $currencyRate) {
                    switch ($user->data->rate_type) {
                        case 'daily':
                            $currencyRate['value'] *= $activityItem->count_days;
                            break;

                        case 'hourly':
                            $currencyRate['value'] *= $activityItem->hours;
                            break;
                    }
                    $rate[$currencyId]['value'] = $currencyRate['value'];
                }
            }

            if (!isset($result[$activityItem->date])) {
                $result[$activityItem->date] = $resultDataTemplate;
            }

            $result[$activityItem->date]['salary'] = $rate;
        }

        // Collect and transform user sales.
        $sales = $user->sales;
        $sales->transform(function ($sale) {
            $sale['date'] = $sale['date']->format('Y-m');

            return $sale;
        });
        $sales = $sales->groupBy('date');
        foreach ($sales as $date => $monthSales) {
            $rate = null;

            foreach ($monthSales as $sale) {
                if (!empty($sale['rate'])) {
                    if ($rate) {
                        foreach ($sale['rate'] as $currencyId => $currencyRate) {
                            $rate[$currencyId]['value'] += $currencyRate['value'];
                        }
                    } else {
                        $rate = $sale['rate']->toArray();
                    }
                }
            }

            if (!isset($result[$date])) {
                $result[$date] = $resultDataTemplate;
            }

            $result[$date]['sales'] = $rate;
        }

        // Collect and transform user transactions.
        $transactions = $user->transactions;
        $transactions->transform(function ($transaction) {
            $payedAt = $transaction->payed_at->format('Y-m');

            $transaction = $transaction->toArray();
            $transaction['payed_at'] = $payedAt;

            return $transaction;
        });
        $transactions = $transactions->groupBy('payed_at');

        foreach ($transactions as $date => $monthlyTransactions) {
            if (!isset($result[$date])) {
                $result[$date] = $resultDataTemplate;
            }

            $result[$date]['transactions'] = $monthlyTransactions;
        }

        // Sort by date.
        krsort($result);

        return $result;
    }

    /**
     * Get user salary compiling user activity, transactions, sales.
     * Filter and transform all data by monthly days.
     * TODO: MUST optimize code.
     *
     * @param int $user_id
     * @param int $year
     * @param int $month
     * @param array $criteria
     *
     * @return array
     */
    public function getMonthlySalaryOfUser(int $user_id, int $year, int $month, array $criteria = [])
    {
        $user = User::query()
            ->with([
                'data' => function ($query) {
                    $query->joinRate('rate');
                },
                'sale_percentages',
                'activity'        => function ($query) use ($year, $month) {
                    $query
                        ->groupByDay()
                        ->whereYear('created_at', $year)
                        ->whereMonth('created_at', $month);
                },
                'transactions'    => function ($query) use ($year, $month) {
                    $query
                        ->where('type', UserTransaction::TYPE_ADDITIONAL)
                        ->joinRate('rate')
                        ->whereYear('payed_at', $year)
                        ->whereMonth('payed_at', $month);
                }
            ])
            ->withManagedCompletedOrders(function ($query) use ($year, $month, $criteria) {
                $query
                    ->whereYear('created_at', $year)
                    ->whereMonth('created_at', $month);

                if (array_get($criteria, 'store_id')) {
                    $query->where('store_id', $criteria['store_id']);
                }
            })
            ->criteria($criteria)
            ->findOrFail($user_id);

        // Result structure.
        $resultDataTemplate = [
            'hours'      => 0,
            'salary'     => null,
            'additional' => null,
            'sales'      => null,
            'total'      => null
        ];
        $result = [
            'data'  => [],
            'total' => $resultDataTemplate
        ];

        // Collect user activity.
        foreach ($user->activity as $activityItem) {
            $rate = null;
            if ($user->data && $user->data->rate) {
                if ($user->data->rate_type != 'monthly') {
                    if ($user->data->rate_type == 'hourly' && !$activityItem->hours) {
                        continue;
                    }

                    $rate = $user->data->rate;

                    foreach ($rate as $currencyId => $currencyRate) {
                        if ($user->data->rate_type == 'hourly') {
                            $currencyRate['value'] *= $activityItem->hours;
                        }
                        $rate->put($currencyId, $currencyRate);
                    }
                    $rate = $rate->toArray();
                } elseif (!$result['total']['salary']) {
                    $result['total']['salary'] = $user->data->rate->toArray();
                    $result['total']['total'] = $user->data->rate->toArray();
                }
            }

            if (!isset($result['data'][$activityItem->date])) {
                $result['data'][$activityItem->date] = $resultDataTemplate;
            }

            $result['data'][$activityItem->date]['hours'] = $activityItem->hours;

            if ($rate) {
                $result['data'][$activityItem->date]['salary'] = $rate;
            }
        }

        // Collect and transform user sales.
        $sales = $user->sales;
        $sales->transform(function ($sale) {
            $sale['date'] = $sale['date']->format('Y-m-d');

            return $sale;
        });
        $sales = $sales->groupBy('date');
        foreach ($sales as $date => $monthSales) {
            $rate = new Collection();

            foreach ($monthSales as $sale) {
                foreach ($sale['rate'] as $currencyId => $currencyRate) {
                    if (isset($rate[$currencyId])) {
                        $currencyRate['value'] += $rate[$currencyId]['value'];
                    }
                    $rate->put($currencyId, $currencyRate);
                }
            }

            if (!isset($result['data'][$date])) {
                $result['data'][$date] = $resultDataTemplate;
            }

            $result['data'][$date]['sales'] = $rate->toArray();
        }

        // Collect and transform user transactions.
        $transactions = $user->transactions;
        $transactions->transform(function ($transaction) {
            $payedAt = $transaction->payed_at->format('Y-m-d');

            $transaction = $transaction->toArray();
            $transaction['payed_at'] = $payedAt;

            return $transaction;
        });
        $transactions = $transactions->groupBy('payed_at');
        foreach ($transactions as $date => $monthTransactions) {
            $rate = new Collection();

            foreach ($monthTransactions as $transaction) {
                foreach ($transaction['rate'] as $currencyId => $currencyRate) {
                    if (isset($rate[$currencyId])) {
                        $currencyRate['value'] += $rate[$currencyId]['value'];
                    }
                    $rate->put($currencyId, $currencyRate);
                }
            }

            if (!isset($result['data'][$date])) {
                $result['data'][$date] = $resultDataTemplate;
            }

            $result['data'][$date]['additional'] = $rate->toArray();
        }

        // Calculate total rates.
        $result['data'] = array_map(function ($item) use (&$result) {
            $entities = ['salary', 'additional', 'sales'];

            foreach ($entities as $entity) {
                if (!$item[$entity]) {
                    continue;
                }

                if ($item['total']) {
                    foreach ($item[$entity] as $currencyId => $currencyRate) {
                        $item['total'][$currencyId]['value'] += $currencyRate['value'];
                    }
                } else {
                    $item['total'] = $item[$entity];
                }

                if ($result['total'][$entity]) {
                    foreach ($item[$entity] as $currencyId => $currencyRate) {
                        $result['total'][$entity][$currencyId]['value'] += $currencyRate['value'];
                    }
                } else {
                    $result['total'][$entity] = $item[$entity];
                }
            }

            if ($result['total']['total']) {
                if ($item['total']) {
                    foreach ($item['total'] as $currencyId => $currencyRate) {
                        if (!array_key_exists($currencyId, $result['total']['total'])) {
                            $result['total']['total'][$currencyId] = ['value' => 0];
                        }
                        $result['total']['total'][$currencyId]['value'] += $currencyRate['value'];
                    }
                }
            } else {
                $result['total']['total'] = $item['total'];
            }

            $result['total']['hours'] += $item['hours'];

            return $item;
        }, $result['data']);

        // Sort by date.
        ksort($result['data']);

        return $result;
    }

    /**
     * Save user role.
     *
     * @param User $model
     * @param string $roleName
     *
     * @return \App\Models\Role
     */
    protected function saveRole(User $model, string $roleName)
    {
        $roleId = Role::where('name', $roleName)->value('id');

        $model->roles()->sync([$roleId]);

        $model->load('roles.permissions');

        return $model->role;
    }

    /**
     * Save user additional data.
     *
     * @param User $model
     * @param array $data
     *
     * @return \App\Models\UserData
     */
    protected function saveUserData(User $model, array $data)
    {
        $userData = $model->data()->firstOrNew([]);
        $userData->fill($data);
        $userData->save();

        $model->load('data');

        return $model->data;
    }

    /**
     * Save user sale percentages.
     *
     * @param User $model
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function saveSalePercentages(User $model, array $data)
    {
        // Delete unrelated records.
        $discountIds = array_filter(array_pluck($data, 'discount_id'));
        $model->sale_percentages()->whereNotIn('discount_id', $discountIds)->delete();

        // Save new records.
        foreach ($data as $percentage) {
            $discountId = array_get($percentage, 'discount_id');

            $criteria = ['discount_id' => $discountId];

            $percentageModel = $model->sale_percentages()->firstOrNew($criteria);
            $percentageModel->fill($percentage);
            $percentageModel->save();
        }

        $model->load('sale_percentages');
        $model->addVisible('sale_percentages');

        return $model->sale_percentages;
    }
}
