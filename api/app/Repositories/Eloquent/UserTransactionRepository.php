<?php

namespace App\Repositories\Eloquent;

use App\Models\UserTransaction;
use App\Repositories\Contracts\UserTransactionRepositoryContract;
use Freevital\Repository\Eloquent\BaseRepository;

class UserTransactionRepository extends BaseRepository implements UserTransactionRepositoryContract
{
    /**
     * {@inheritdoc}
     */
    public function model(): string
    {
        return UserTransaction::class;
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $attributes)
    {
        $model = $this->model->newInstance();

        $model->fill($attributes);
        $model->forceFill([
            'user_id' => $attributes['user_id'],
            'type'    => $attributes['type']
        ]);

        $model->save();

        return $model;
    }
}
