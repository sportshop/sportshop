<?php

namespace App\Repositories\Eloquent;

use App\Models\StoreProductTransfer;
use App\Repositories\Contracts\TransferRepositoryContract;

class TransferRepository implements TransferRepositoryContract
{
    /**
     * Find product transfers by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = [])
    {
        $query = StoreProductTransfer::query()
            ->with([
                'user',
                'from_store',
                'to_store',
                'product_size' => function ($query) {
                    $query
                        ->withTrashed()
                        ->with([
                            'product' => function ($query) {
                                $query->withTrashed();
                            }
                        ]);
                }
            ])
            ->joinRate('price')
            ->criteria($criteria);

        if ($limit = array_get($options, 'limit')) {
            $page = array_get($options, 'page');

            $collection = $query->paginate($limit, ['*'], 'page', $page);
        } else {
            $collection = $query->get();
        }

        $collection->transform(function ($item) {
            $item->append('product');

            return $item;
        });

        return $collection;
    }

    /**
     * Get summary price and total quantity.
     *
     * @param array $criteria
     * @return array
     */
    public function getTotalPriceAndQuantity(array $criteria = [])
    {
        if (array_get($criteria, 'available_for_order')) {
            $criteria['exists'] = true;
            $criteria['status'] = 'active';
            $criteria['is_visible'] = true;
        }

        // Get transfers.
        $rows = StoreProductTransfer::query()
            ->criteria($criteria)
            ->joinRate('price')
            ->get(['id']);

        $result = [
            'price'    => null,
            'quantity' => $rows->sum('quantity')
        ];

        foreach ($rows as $row) {

            // Get price.
            if (!$row->price) {
                continue;
            }

            $price = $row->price->toArray();
            foreach ($price as $currency_id => $currency_value) {
                $price[$currency_id]['value'] = $currency_value['value'] * $row->quantity;
            }

            if (!$result['price']) {
                $result['price'] = $price;
            } else {
                foreach ($price as $currency_id => $currency_value) {
                    if (!array_key_exists($currency_id, $result['price'])) {
                        $result['price'][$currency_id] = $currency_value;
                    }
                    $result['price'][$currency_id]['value'] += $currency_value['value'];
                }
            }
        }

        return $result;
    }
}
