<?php

namespace App\Repositories\Eloquent;

use App\Models\Attribute;
use App\Models\AttributeGroup;
use App\Models\AttributeValue;
use App\Repositories\Contracts\AttributeRepositoryContract;
use Exception;
use Illuminate\Support\Facades\DB;

class AttributeRepository implements AttributeRepositoryContract
{
    /**
     * Find all attributes by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Support\Collection
     */
    public function findAll(array $criteria = [], array $options = [])
    {
        $query = AttributeGroup::with('attributes.values');

        if (array_get($criteria, 'products_exists') && array_get($criteria, 'store_id')) {
            $query->whereHas('attributes.values.products', function ($query) use ($criteria) {
                $query
                    ->joinSalePrice($criteria['store_id'], array_get($criteria, 'discount_id'))
                    ->criteria($criteria)
                    ->storeAvailable($criteria['store_id']);
            });
        }

        $collection = $query->get();

        return $collection;
    }

    /**
     * Find attribute by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\AttributeGroup
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id, array $criteria = [])
    {
        $model = AttributeGroup::query()
            ->with('attributes.values')
            ->where('id', $id)
            ->first();

        return $model;
    }

    /**
     * Create attribute group.
     *
     * @param array $attributes
     *
     * @return \App\Models\AttributeGroup
     * @throws \Exception
     */
    public function create(array $attributes)
    {
        DB::beginTransaction();

        try {
            // Create attribute group
            $model = AttributeGroup::create($attributes);

            // Create attributes
            if ($attrs = array_get($attributes, 'attributes')) {
                foreach ($attrs as $attr) {
                    $attributeModel = new Attribute();
                    $attributeModel->fill($attr);

                    $model->attributes()->save($attributeModel);

                    // Create attribute values
                    if ($values = array_get($attr, 'values')) {
                        foreach ($values as $value) {
                            $valueModel = new AttributeValue();
                            $valueModel->fill($value);

                            $attributeModel->values()->save($valueModel);
                        }
                    }
                }
                $model->load('attributes.values');
            }

            DB::commit();

            return $model;
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * Update attribute group by id.
     *
     * @param int $id
     * @param array $attributes
     *
     * @return \App\Models\AttributeGroup
     * @throws \Exception
     */
    public function update(int $id, array $attributes)
    {
        DB::beginTransaction();

        try {
            // Update attribute group
            $model = AttributeGroup::findOrFail($id);
            $model->fill($attributes);
            $model->save();

            // Delete unrelated attributes
            $this->deleteUnrelatedAttributes($model, $attributes['attributes']);

            // Update attributes
            foreach ($attributes['attributes'] as $attr) {
                $params = ['id' => array_get($attr, 'id')];

                $attributeModel = $model->attributes()->firstOrNew($params);
                $attributeModel->fill($attr);

                $model->attributes()->save($attributeModel);

                // Delete unrelated records
                $this->deleteUnrelatedValues($attributeModel, $attr['values']);

                // Update attribute values
                if ($values = array_get($attr, 'values')) {
                    foreach ($attr['values'] as $value) {
                        $params = ['id' => array_get($value, 'id')];

                        $valueModel = $attributeModel->values()->firstOrNew($params);
                        $valueModel->fill($value);

                        $attributeModel->values()->save($valueModel);
                    }
                }
            }
            $model->load('attributes.values');

            DB::commit();

            return $model;
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * Delete attribute by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id)
    {
        $deleted = AttributeGroup::destroy($id);

        return $deleted;
    }

    /**
     * Delete unrelated attributes.
     *
     * @param \App\Models\AttributeGroup $model
     * @param array $attributes
     */
    protected function deleteUnrelatedAttributes(AttributeGroup $model, array $attributes)
    {
        $ids = array_filter(array_pluck($attributes, 'id'));

        $model->attributes()->whereNotIn('id', $ids)->delete();
    }

    /**
     * Delete unrelated attributes.
     *
     * @param \App\Models\Attribute $model
     * @param array $values
     */
    protected function deleteUnrelatedValues(Attribute $model, array $values)
    {
        $ids = array_filter(array_pluck($values, 'id'));

        $model->values()->whereNotIn('id', $ids)->delete();
    }
}
