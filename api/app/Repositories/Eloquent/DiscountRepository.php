<?php

namespace App\Repositories\Eloquent;

use App\Models\Discount;
use App\Repositories\Contracts\DiscountRepositoryContract;
use DB;
use Exception;
use Freevital\Repository\Eloquent\BaseRepository;

class DiscountRepository extends BaseRepository implements DiscountRepositoryContract
{
    /**
     * {@inheritdoc}
     */
    public function model(): string
    {
        return Discount::class;
    }

    /**
     * Save all discounts.
     *
     * @param array $records
     *
     * @return array
     * @throws Exception
     */
    public function save(array $records)
    {
        DB::beginTransaction();

        try {
            // Remove old (unused) records
            $ids = array_pluck($records, 'id');
            $this->model->whereNotIn('id', $ids)->delete();

            // Update or create
            $result = [];
            foreach ($records as $item) {
                $attributes = array_only($item, 'id');

                $model = $this->updateOrCreate($attributes, $item);

                $result[] = $model;
            }

            DB::commit();

            return $result;
        } catch (Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }
}
