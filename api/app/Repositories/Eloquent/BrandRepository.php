<?php

namespace App\Repositories\Eloquent;

use App\Models\Brand;
use App\Repositories\Contracts\BrandRepositoryContract;
use Exception;
use Illuminate\Support\Facades\DB;

class BrandRepository implements BrandRepositoryContract
{
    /**
     * Find all brands.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = [])
    {
        $type = array_get($criteria, 'type');

        $query = Brand::query()
            ->withMedia(Brand::MEDIA_THUMBNAIL)
            ->with(['size_groups' => function ($query) use ($type) {
                if ($type) {
                    $query->where('type', $type);
                }
            }])
            ->orderBy('name');

        if ($type) {
            $query->whereHas('size_groups', function ($query) use ($type) {
                $query->where('type', $type);
            });
        }

        if (array_get($criteria, 'products_exists') && array_get($criteria, 'store_id')) {
            $query->whereHas('products', function ($query) use ($criteria) {
                $query
                    ->joinSalePrice($criteria['store_id'], array_get($criteria, 'discount_id'))
                    ->criteria($criteria)
                    ->storeAvailable($criteria['store_id']);
            });
        }

        $collection = $query->get();

        return $collection;
    }

    /**
     * Find brand by id.
     *
     * @param int $id
     *
     * @return \App\Models\Brand
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id)
    {
        $model = Brand::query()
            ->withMedia(Brand::MEDIA_THUMBNAIL)
            ->find($id);

        return $model;
    }

    /**
     * Create brand.
     *
     * @param array $attributes
     *
     * @return \App\Models\Brand
     * @throws \Exception
     */
    public function create(array $attributes)
    {
        DB::beginTransaction();

        try {
            $model = Brand::create($attributes);

            // Attach media
            if ($thumbnail_id = array_get($attributes, 'thumbnail_id')) {
                $model->attachMedia($thumbnail_id, Brand::MEDIA_THUMBNAIL);
            }

            DB::commit();

            return $model;
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * Update brand.
     *
     * @param int $id
     * @param array $attributes
     *
     * @return \App\Models\Brand
     * @throws \Exception
     */
    public function update(int $id, array $attributes)
    {
        DB::beginTransaction();

        try {
            $model = Brand::findOrFail($id);
            $model->fill($attributes);
            $model->save();

            // Sync a media
            if ($thumbnail_id = array_get($attributes, 'thumbnail_id')) {
                $model->syncMedia([$thumbnail_id], Brand::MEDIA_THUMBNAIL);
            }

            DB::commit();

            return $model;
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * Delete brand by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id)
    {
        $deleted = Brand::destroy($id);

        return $deleted;
    }
}
