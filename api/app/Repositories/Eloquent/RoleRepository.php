<?php

namespace App\Repositories\Eloquent;

use App\Models\Role;
use App\Repositories\Contracts\RoleRepositoryContract;
use Freevital\Repository\Eloquent\BaseRepository;
use Illuminate\Database\Eloquent\Collection;

class RoleRepository extends BaseRepository implements RoleRepositoryContract
{
    /**
     * {@inheritdoc}
     */
    public function model(): string
    {
        return Role::class;
    }

    /**
     * Find all system roles.
     *
     * @return Collection
     */
    public function findSystemRoles()
    {
        return Role::query()
            ->where('is_system', true)
            ->all();
    }

    /**
     * Find all custom roles.
     *
     * @return Collection
     */
    public function findCustomRoles()
    {
        return Role::query()
            ->where('is_system', false)
            ->all();
    }
}
