<?php

namespace App\Repositories\Eloquent;

use App\Models\Cost;
use App\Repositories\Contracts\CostRepositoryContract;
use Illuminate\Support\Collection;

class CostRepository implements CostRepositoryContract
{
    /**
     * Find all costs by criteria.
     *
     * @param array $criteria
     * @param array $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = [])
    {
        $query = Cost::query()
            ->select('costs.*')
            ->with('store')
            ->joinRate('rate')
            ->criteria($criteria);

        if ($limit = array_get($options, 'limit')) {
            $page = array_get($options, 'page');

            $collection = $query->paginate($limit, ['*'], 'page', $page);
        } else {
            $collection = $query->get();
        }

        return $collection;
    }

    /**
     * Find cost by id.
     *
     * @param int $id
     * @param array $criteria
     * @return \App\Models\Cost
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id, array $criteria = [])
    {
        $model = Cost::query()
            ->select('costs.*')
            ->with('store')
            ->joinRate('rate')
            ->find($id);

        return $model;
    }

    /**
     * Create cost.
     *
     * @param array $attributes
     * @return \App\Models\Cost
     * @throws \Exception
     */
    public function create(array $attributes)
    {
        $model = Cost::create($attributes);

        $model->load('store');

        return $model;
    }

    /**
     * Update cost by id.
     *
     * @param int $id
     * @param array $attributes
     * @return \App\Models\Cost
     * @throws \Exception
     */
    public function update(int $id, array $attributes)
    {
        $model = Cost::findOrFail($id);
        $model->fill($attributes);
        $model->save();

        $model->load('store');

        return $model;
    }

    /**
     * Delete cost by id.
     *
     * @param int $id
     * @return bool|int|null
     */
    public function delete(int $id)
    {
        $deleted = Cost::destroy($id);

        return $deleted;
    }

    /**
     * Get only rates of cost types and group date.
     * TODO: MUST optimize code.
     *
     * @param array $criteria
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getRatesByDate(array $criteria = [])
    {
        $collection = Cost::query()
            ->joinRate('rate')
            ->criteria($criteria)
            ->get();

        $collection = $collection->groupBy(function ($item) {
            return $item->created_at->format('Y-m-d');
        });
        $collection->transform(function ($group) use ($criteria) {
            if (array_get($criteria, 'group_by') == 'type') {
                $group = $group->groupBy('type');

                $group->transform(function ($costs) {
                    $rate = new Collection();

                    foreach ($costs as $cost) {
                        if ($cost->rate) {
                            foreach ($cost->rate as $currencyId => $currencyRate) {
                                if (isset($rate[$currencyId])) {
                                    $currencyRate['value'] += $rate[$currencyId]['value'];
                                }
                                $rate->put($currencyId, $currencyRate);
                            }
                        }
                    }

                    return $rate;
                });
            } else {
                $rate = new Collection();

                foreach ($group as $cost) {
                    if ($cost->rate) {
                        foreach ($cost->rate as $currencyId => $currencyRate) {
                            if (isset($rate[$currencyId])) {
                                $currencyRate['value'] += $rate[$currencyId]['value'];
                            }
                            $rate->put($currencyId, $currencyRate);
                        }
                    }
                }

                return $rate;
            }

            return $group;
        });

        return $collection;
    }

    /**
     * Get total amount.
     *
     * @param array $criteria
     * @return array
     */
    public function getTotalAmount(array $criteria = [])
    {
        $collection = Cost::query()
            ->joinRate('rate')
            ->criteria($criteria)
            ->get();

        $rate = [];
        $collection->map(function ($cost) use (&$rate) {
            if ($cost->rate) {
                foreach ($cost->rate as $currencyRate) {
                    if (!isset($rate[$currencyRate['currency_id']])) {
                        $rate[$currencyRate['currency_id']] = $currencyRate;
                    } else {
                        $rate[$currencyRate['currency_id']]['value'] += $currencyRate['value'];
                    }
                }
            }

            return $rate;
        });

        return $rate;
    }
}
