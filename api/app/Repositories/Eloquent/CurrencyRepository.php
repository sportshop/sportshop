<?php

namespace App\Repositories\Eloquent;

use App\Models\Currency;
use App\Repositories\Contracts\CurrencyRepositoryContract;
use DB;
use Exception;
use Freevital\Repository\Eloquent\BaseRepository;

class CurrencyRepository extends BaseRepository implements CurrencyRepositoryContract
{
    /**
     * {@inheritdoc}
     */
    public function model(): string
    {
        return Currency::class;
    }

    /**
     * Save all currencies.
     *
     * @param array $records
     *
     * @return array
     * @throws Exception
     */
    public function save(array $records)
    {
        DB::beginTransaction();

        try {
            // Remove old (unused) records
            $ids = array_pluck($records, 'id');
            $this->model->whereNotIn('id', $ids)->get()->each(function($model) {
                $model->delete();
            });

            // Update or create
            $result = [];
            foreach ($records as $item) {
                $attributes = array_only($item, 'id');

                $model = $this->updateOrCreate($attributes, $item);

                $result[] = $model;
            }

            DB::commit();

            return $result;
        } catch (Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }

    public function findDefaultCurrency()
    {
        return $this->model->where('is_default', true)->first();
    }
}
