<?php

namespace App\Repositories\Eloquent;

use App\Models\Product;
use App\Models\StoreProductTransfer;
use App\Models\ProductStoreData;
use App\Repositories\Contracts\ProductRepositoryContract;
use App\Services\ProductPriceService;
use Exception;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ProductRepository implements ProductRepositoryContract
{
    /**
     * Find all products by criteria.
     *
     * @param array $criteria
     * @param array $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = [])
    {
        $storeId = array_get($criteria, 'store_id');

        // Collect criteria.
        if (array_get($criteria, 'available_for_order')) {
            $criteria['exists'] = true;
            $criteria['status'] = 'active';
        }

        // Collect query.
        $query = Product::query()
            ->with([
                'attribute_values',
                'brand',
                'categories',
                'colors',
                'product_sizes' => function ($query) use ($storeId) {
                    if ($storeId) {
                        $query->where('store_id', $storeId);
                    }
                }
            ])
            ->withMedia(Product::MEDIA_THUMBNAIL);

        // Load uniform relation.
        if (array_get($criteria, 'uniform_type')) {
            $query->with('uniform');
        }

        // Join cost price.
        if (array_get($criteria, 'with_cost_price')) {
            $query->joinRate('cost_price');
        }

        // Join sale price.
        if ($storeId) {
            $discountId = array_get($criteria, 'discount_id');

            $query->joinSalePrice($storeId, $discountId);
        }

        $query->criteria($criteria);

        // Build paginator by "limit" param or get collection with all records.
        if ($limit = array_get($options, 'limit')) {
            $page = array_get($options, 'page');

            $total = DB::table(DB::raw("({$query->toSql()}) as sub"))
                ->mergeBindings($query->getQuery())
                ->count();

            $items = $query
                ->forPage($page, $limit)
                ->get();

            $collection = new LengthAwarePaginator($items, $total, $limit, $page);
        } else {
            $collection = $query->get();
        }

        // Transform collection.
        $collection->transform(function ($item) use ($criteria) {
            if (array_get($criteria, 'with_status')) {
                $item->addVisible('is_visible', 'is_active');
            };

            $item->categories->transform(function ($item) {
                $item->append('is_default');

                return $item;
            });

            return $item;
        });

        return $collection;
    }

    /**
     * Find product by id.
     *
     * @param int $id
     * @param array $criteria
     * @return \App\Models\Product
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id, array $criteria = [])
    {
        $storeId = array_get($criteria, 'store_id');

        // Collect query.
        $query = Product::query()
            ->with([
                'attribute_values',
                'brand',
                'categories',
                'colors',
                'product_sizes' => function ($query) use ($storeId) {
                    if ($storeId) {
                        $query->where('store_id', $storeId);
                    }
                },
                'stores.user_discounts',
                'uniform'
            ])
            ->withMedia(Product::mediaTags())
            ->joinRate('cost_price');

        $model = $query->find($id);

        if (!$model) {
            return null;
        }

        $model->addVisible([
            'is_visible',
            'is_active',
            'stores'
        ]);

        if ($model->uniform) {
            $model->uniform->addVisible([
                'category_id',
                'is_visible'
            ]);
        }

        // Transform categories collection.
        $model->categories->transform(function ($item) {
            $item->append('is_default');

            return $item;
        });

        return $model;
    }

    /**
     * Find product by slug.
     *
     * @param string $slug
     * @param array $criteria
     * @return \App\Models\Product
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findBySlug(string $slug, array $criteria = [])
    {
        $criteria['status'] = 'active';
        $criteria['is_visible'] = true;
        $criteria['exists'] = true;

        $storeId = array_get($criteria, 'store_id');

        // Collect query.
        $query = Product::query()
            ->with([
                'attribute_values',
                'brand.size_groups.sizes.values',
                'categories',
                'colors',
                'product_sizes' => function ($query) use ($storeId) {
                    $query
                        ->where('store_id', $storeId)
                        ->where('quantity', '>', 0);
                }
            ])
            ->withMedia(Product::mediaTags())
            ->where('slug', $slug)
            ->criteria($criteria);

        // Join sale price.
        if ($storeId) {
            $discountId = array_get($criteria, 'discount_id');

            $query->joinSalePrice($storeId, $discountId);
        }

        $model = $query->first();

        if (!$model) {
            return null;
        }

        // Transform categories collection.
        $model->categories->transform(function ($item) {
            $item->append('is_default');

            return $item;
        });

        return $model;
    }

    /**
     * Create product.
     *
     * @param array $attributes
     * @return \App\Models\Product
     * @throws \Exception
     */
    public function create(array $attributes)
    {
        DB::beginTransaction();

        try {
            $model = Product::create($attributes);
            $model = $model->joinRate('cost_price')->where('products.id', $model->id)->first();

            // Load hidden attributes.
            $model->addVisible([
                'is_visible',
                'is_active',
                'stores'
            ]);

            // Load brand relation.
            $model->load('brand');

            // Save attributes.
            if ($attrs = array_get($attributes, 'attributes')) {
                $this->saveAttributes($model, $attrs);
            }

            // Save categories.
            if ($categories = array_get($attributes, 'categories')) {
                $this->saveCategories($model, $categories);
            }

            // Save colors.
            if ($colors = array_get($attributes, 'colors')) {
                $this->saveColors($model, $colors);
            }

            // Save media files.
            if ($media = array_get($attributes, 'media')) {
                $this->saveMedia($model, $media);
            }

            // Save sizes.
            if ($sizes = array_get($attributes, 'sizes')) {
                $this->saveSizes($model, $sizes, $attributes['manager_id'], $attributes['price']);
            }

            // Save stores data.
            if ($stores_data = array_get($attributes, 'stores')) {
                $this->saveStoresData($model, $stores_data);
            }

            // Save uniform data.
            if ($uniform = array_get($attributes, 'uniform')) {
                $this->saveUniform($model, $uniform);
            }

            $model->load('uniform');

            if ($model->uniform) {
                $model->uniform->addVisible([
                    'category_id',
                    'is_visible'
                ]);
            }

            DB::commit();

            return $model;
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * Update product by id.
     *
     * @param int $id
     * @param array $attributes
     * @return \App\Models\Product
     * @throws \Exception
     */
    public function update(int $id, array $attributes)
    {
        DB::beginTransaction();

        try {
            $model = Product::findOrFail($id);
            $model->fill($attributes);
            $model->save();
            $model = $model->joinRate('cost_price')->where('products.id', $model->id)->first();

            // Load hidden attributes.
            $model->addVisible([
                'is_visible',
                'is_active',
                'stores'
            ]);

            // Load brand relation.
            $model->load('brand');

            // Save attributes.
            $attrs = array_get($attributes, 'attributes', []);
            $this->saveAttributes($model, $attrs);

            // Save categories.
            $categories = array_get($attributes, 'categories', []);
            $this->saveCategories($model, $categories);

            // Save colors.
            $colors = array_get($attributes, 'colors', []);
            $this->saveColors($model, $colors);

            // Save media files.
            $media = array_get($attributes, 'media', []);
            $this->saveMedia($model, $media);

            // Save sizes.
            $sizes = array_get($attributes, 'sizes', []);
            $this->saveSizes($model, $sizes, $attributes['manager_id'], array_get($attributes, 'price', []));

            // Save stores data.
            $stores_data = array_get($attributes, 'stores', []);
            $this->saveStoresData($model, $stores_data);

            // Save uniform data.
            if ($uniform = array_get($attributes, 'uniform')) {
                $this->saveUniform($model, $uniform);
                $model->load('uniform');
                $model->uniform->addVisible([
                    'category_id',
                    'is_visible'
                ]);
            }

            DB::commit();

            return $model;
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * Update product type.
     *
     * @param int $id
     * @param string $type
     * @return \App\Models\Product
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function updateType(int $id, string $type)
    {
        $model = Product::findOrFail($id);
        $model->type = $type;
        $model->save();

        return $model;
    }

    /**
     * Update product visibility status.
     *
     * @param int $id
     * @param bool $status
     * @return \App\Models\Product
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function updateVisibilityStatus(int $id, bool $status)
    {
        $model = Product::findOrFail($id);
        $model->is_visible = $status;
        $model->save();

        $model->addVisible('is_visible');

        return $model;
    }

    /**
     * Delete product by id.
     *
     * @param int $id
     * @return bool|int|null
     */
    public function delete(int $id)
    {
        $deleted = Product::destroy($id);

        return $deleted;
    }

    /**
     * Retrieve min, max values of sale price.
     *
     * @param array $criteria
     * @return \stdClass|array
     */
    public function getMinMaxOfSalePrice(array $criteria)
    {
        $storeId = array_get($criteria, 'store_id');
        $currencyId = array_get($criteria, 'currency_id');
        $discountId = array_get($criteria, 'discount_id');

        if (!$storeId || !$currencyId) {
            return [
                'min' => null,
                'max' => null
            ];
        }

        $query = Product::query()
            ->joinSalePrice($storeId, $discountId)
            ->storeAvailable($storeId)
            ->criteria($criteria);

        $result = DB::table(DB::raw("({$query->toSql()}) as sub"))
            ->select(DB::raw("MIN(JSON_EXTRACT(sub.sale_price, '$.\"$currencyId\".value')) AS min"))
            ->addSelect(DB::raw("MAX(JSON_EXTRACT(sub.sale_price, '$.\"$currencyId\".value')) AS max"))
            ->mergeBindings($query->getQuery())
            ->first();

        return $result;
    }

    /**
     * Get summary cost price and total quantity of all products.
     *
     * @param array $criteria
     * @return array
     */
    public function getTotalCostPriceAndQuantity(array $criteria = [])
    {
        unset($criteria['price_min'], $criteria['price_max']);

        $storeId = array_get($criteria, 'store_id');

        if (array_get($criteria, 'available_for_order')) {
            $criteria['exists'] = true;
            $criteria['status'] = 'active';
            $criteria['is_visible'] = true;
        }

        // Get products.
        $products = Product::query()
            ->criteria($criteria)
            ->joinRate('cost_price')
            ->with([
                'product_sizes' => function ($query) use ($storeId) {
                    if ($storeId) {
                        $query->where('store_id', $storeId);
                    }
                }
            ])
            ->get();

        $result = [
            'total_cost_price' => null,
            'total_quantity'   => 0
        ];

        foreach ($products as $product) {

            // Get quantity.
            $product_quantity = $product->product_sizes->sum('quantity');
            $result['total_quantity'] += $product_quantity;

            // Get cost price.
            if (!$product->cost_price) {
                continue;
            }

            $product_cost_price = $product->cost_price->toArray();
            foreach ($product_cost_price as $currency_id => $currency_value) {
                $product_cost_price[$currency_id]['value'] = $currency_value['value'] * $product_quantity;
            }

            if (!$result['total_cost_price']) {
                $result['total_cost_price'] = $product_cost_price;
            } else {
                foreach ($product_cost_price as $currency_id => $currency_value) {
                    if (!array_key_exists($currency_id, $result['total_cost_price'])) {
                        $result['total_cost_price'][$currency_id] = $currency_value;
                    }
                    $result['total_cost_price'][$currency_id]['value'] += $currency_value['value'];
                }
            }
        }

        return $result;
    }

    /**
     * Save attributes.
     *
     * @param \App\Models\Product $model
     * @param array $attributes
     * @return \Illuminate\Support\Collection
     */
    protected function saveAttributes(Product $model, array $attributes)
    {
        $ids = array_pluck($attributes, 'id');

        $model->attribute_values()->sync($ids);

        $model->load('attribute_values');

        return $model->attribute_values;
    }

    /**
     * Save categories.
     *
     * @param \App\Models\Product $model
     * @param array $categories
     * @return \Illuminate\Support\Collection
     */
    protected function saveCategories(Product $model, array $categories)
    {
        $model->categories()->detach();

        foreach ($categories as $category) {
            $values = array_only($category, ['is_default']);

            $model->categories()->attach($category['id'], $values);
        }

        $model->load('categories');

        $model->categories->transform(function ($item) {
            $item->append('is_default');

            return $item;
        });

        return $model->categories;
    }

    /**
     * Save colors.
     *
     * @param \App\Models\Product $model
     * @param array $colors
     * @return \Illuminate\Support\Collection
     */
    protected function saveColors(Product $model, array $colors)
    {
        $ids = array_pluck($colors, 'id');

        $model->colors()->sync($ids);

        $model->load('colors');

        return $model->colors;
    }

    /**
     * Save media.
     *
     * @param \App\Models\Product $model
     * @param array $media
     * @return \Illuminate\Support\Collection
     */
    protected function saveMedia(Product $model, array $media)
    {
        // Detach old files.
        $model->media()->detach();

        // Attach new files.
        foreach ($media as $item) {
            $model->attachMedia($item['id'], $item['tag']);
        }

        $model->loadMedia(Product::mediaTags(), true);

        return $model->media;
    }

    /**
     * Save sizes.
     *
     * @param \App\Models\Product $model
     * @param array $sizesData
     * @param int $manager_id
     * @param array $original_price
     *
     * @return array
     * @throws \Exception
     */
    protected function saveSizes(Product $model, array $sizesData, int $manager_id, array $original_price)
    {
        // Group sizes.
        $sizesGroup = new Collection($sizesData);
        $sizesGroup = $sizesGroup->groupBy('size_id');

        foreach ($sizesGroup as $sizes) {
            $transferredSizes = new Collection();

            foreach ($sizes as $size) {

                // Retrieve "ProductSize" object.
                $sizeModel = $model->product_sizes()->firstOrNew([
                    'size_id'  => $size['size_id'],
                    'store_id' => $size['store_id']
                ]);

                // Fill values.
                $sizeModel->fill($size);

                // Collect old quantity.
                $originalQuantity = null;

                if ($sizeModel->isDirty('quantity')) {
                    $originalQuantity = $sizeModel->getOriginal('quantity', 0);
                }

                // Save product size.
                $model->product_sizes()->save($sizeModel);

                // Collect transferred sizes.
                if (!is_null($originalQuantity)) {
                    $transferredSize = clone $sizeModel;
                    $transferredSize['id'] = $sizeModel->id;
                    $transferredSize['old_quantity'] = $originalQuantity;

                    $transferredSizes->push($transferredSize);
                }
            }

            $this->saveSizeTransfers($transferredSizes, $manager_id, $original_price);
        }

        // Delete unrelated records
        $this->deleteUnrelatedSizes($model, $sizesData);

        $model->load('product_sizes');

        return $model->product_sizes;
    }

    /**
     * Delete unrelated sizes.
     *
     * @param \App\Models\Product $model
     * @param array $sizes
     */
    protected function deleteUnrelatedSizes(Product $model, array $sizes)
    {
        $ids = [];
        foreach ($sizes as $size) {
            $params = array_only($size, ['store_id', 'size_id']);

            $id = $model->product_sizes()
                ->where($params)
                ->value('id');

            if ($id) {
                $ids[] = $id;
            }
        }

        $model->product_sizes()->whereNotIn('id', $ids)->orWhere('quantity', 0)->delete();
    }

    /**
     * Save transferred sizes.
     *
     * @param \Illuminate\Support\Collection $sizes
     * @param int $manager_id
     * @param array $original_price
     *
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    protected function saveSizeTransfers(Collection $sizes, int $manager_id, array $original_price)
    {
        // Retrieve size from which the quantity was written off.
        $sizeFrom = $sizes->filter(function ($item) {
            return $item['quantity'] < $item['old_quantity'];
        })->first();

        // Retrieve a few sizes to which the quantity was added.
        $sizesTo = $sizes->whereNotIn('store_id', $sizeFrom['store_id']);

        $transfers = new Collection();

        // Create transfers records.
        foreach ($sizesTo as $sizeTo) {
            $transferModel = new StoreProductTransfer();
            $transferModel->fill([
                'from_store_id'   => $sizeFrom['store_id'],
                'to_store_id'     => $sizeTo['store_id'],
                'product_size_id' => $sizeTo['id'],
                'user_id'         => $manager_id,
                'quantity'        => $sizeTo['quantity'] - $sizeTo['old_quantity'],
                'price'           => $original_price
            ]);

            if (!$sizeFrom['store_id']) {
                $transferModel->action = StoreProductTransfer::ACTION_ADD;
            } else {
                $transferModel->action = StoreProductTransfer::ACTION_MOVE;
            }

            $transferModel->save();

            $transfers->push($transferModel);
        }

        return $transfers;
    }

    /**
     * Save stores data.
     *
     * @param \App\Models\Product $model
     * @param array $stores
     * @return \Illuminate\Support\Collection
     */
    protected function saveStoresData(Product $model, array $stores)
    {
        // Delete unrelated records.
        $this->deleteUnrelatedStoresData($model, $stores);

        // Save stores data.
        foreach ($stores as $storeData) {

            // Retrieve "ProductStoreData" object.
            $store_model = $model->stores()->firstOrNew([
                'product_id' => $model->id,
                'store_id'   => $storeData['store_id']
            ]);

            // Fill values.
            $attributes = array_only($storeData, [
                'currency_id',
                'markup',
                'sale_discount',
                'max_sale_discount'
            ]);
            $store_model->fill($attributes);

            // Fill prices.
            $price_service = new ProductPriceService($model->cost_price, $storeData['currency_id']);

            $store_model->markup_price = $price_service->calculateMarkupPrice($attributes['markup']);
            $store_model->sale_price = $price_service->calculateSalePrice($store_model->markup_price['value'], $attributes['sale_discount']);
            $store_model->max_sale_price = $price_service->calculateMaxSalePrice($attributes['max_sale_discount']);

            // Save model.
            $model->stores()->save($store_model);

            // Save user discounts.
            if ($userDiscounts = array_get($storeData, 'user_discounts')) {
                $this->saveUserDiscounts(
                    $userDiscounts,
                    $store_model,
                    $price_service
                );
            }
        }

        $model->load('stores');

        return $model->stores;
    }

    /**
     * Delete unrelated stores data.
     *
     * @param \App\Models\Product $model
     * @param array $stores
     */
    protected function deleteUnrelatedStoresData(Product $model, array $stores)
    {
        $ids = [];
        foreach ($stores as $store) {
            $id = $model->stores()
                ->where('product_id', $model->id)
                ->where('store_id', $store['store_id'])
                ->value('id');

            if ($id) {
                $ids[] = $id;
            }
        }

        $model->stores()->whereNotIn('id', $ids)->delete();
    }

    /**
     * Save user discounts.
     *
     * @param array $discounts
     * @param ProductStoreData $store
     * @param ProductPriceService $price_service
     * @return \Illuminate\Support\Collection
     */
    protected function saveUserDiscounts(array $discounts, ProductStoreData $store, ProductPriceService $price_service)
    {
        // Delete unrelated user discounts.
        $this->deleteUnrelatedUserDiscounts($store, $discounts);

        // Save user discounts.
        foreach ($discounts as $discount) {

            // Retrieve "ProductStoreDiscount" object.
            $discount_model = $store->user_discounts()->firstOrNew([
                'discount_id' => $discount['discount_id']
            ]);

            // Fill values.
            $discount_model->fill($discount);

            // Calculate prices.
            $prices = $price_service->calculateDiscountPrices($store, $discount);

            $discount_model->price = $prices['price'];
            $discount_model->sale_price = $prices['sale_price'];

            // Save model.
            $store->user_discounts()->save($discount_model);
        }

        $store->load('user_discounts');

        return $store->user_discounts;
    }

    /**
     * Delete unrelated user discounts.
     *
     * @param \App\Models\ProductStoreData $storeModel
     * @param array $discounts
     */
    protected function deleteUnrelatedUserDiscounts(ProductStoreData $storeModel, array $discounts)
    {
        $ids = [];
        foreach ($discounts as $discountData) {
            $id = $storeModel->user_discounts()
                ->where('discount_id', $discountData['discount_id'])
                ->value('id');

            if ($id) {
                $ids[] = $id;
            }
        }

        $storeModel->user_discounts()->whereNotIn('id', $ids)->delete();
    }

    /**
     * Save uniform data.
     *
     * @param \App\Models\Product $model
     * @param array $uniformData
     * @return \Illuminate\Database\Eloquent\Model;
     */
    protected function saveUniform(Product $model, array $uniformData)
    {
        // Retrieve uniform instance.
        $uniformModel = $model->uniform()->firstOrNew(['product_id' => $model->id]);

        $uniformModel->fill($uniformData);
        $uniformModel->save();

        return $uniformModel;
    }
}
