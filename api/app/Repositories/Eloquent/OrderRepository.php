<?php

namespace App\Repositories\Eloquent;

use App\Models\Order;
use App\Models\OrderPayment;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Repositories\Contracts\OrderRepositoryContract;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class OrderRepository implements OrderRepositoryContract
{
    /**
     * Find all orders by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = [])
    {
        // Collect query.
        $query = Order::query()
            ->joinRate('price')
            ->with([
                'customer_relation',
                'order_products'
            ])
            ->criteria($criteria);

        if (array_get($criteria, 'with_full_info')) {
            $query->with([
                'store',
                'payments',
                'manager'
            ]);
        }

        // Build paginator by "limit" param or get collection with all records.
        if ($limit = array_get($options, 'limit')) {
            $page = array_get($options, 'page');

            $total = DB::table(DB::raw("({$query->toSql()}) as sub"))
                ->mergeBindings($query->getQuery())
                ->count();

            $items = $query
                ->forPage($page, $limit)
                ->get();

            $collection = new LengthAwarePaginator($items, $total, $limit, $page);
        } else {
            $collection = $query->get();
        }

        // Transform collection.
        $collection->transform(function ($item) use ($criteria) {
            if (array_get($criteria, 'with_full_info')) {
                $item->append('residual_payments_rate');
                $item->addVisible([
                    'is_from_web_store',
                    'store',
                    'manager',
                    'payments'
                ]);
            }

            return $item;
        });

        return $collection;
    }

    /**
     * Find order by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\Order|null
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id, array $criteria = [])
    {
        $model = Order::query()
            ->joinRate('price')
            ->with([
                'store',
                'manager',
                'customer_relation',
                'order_products',
                'payments'
            ])
            ->find($id);

        if (!$model) {
            return null;
        }

        $model->append('residual_payments_rate');

        $model->addVisible([
            'store',
            'manager',
            'is_from_web_store',
            'payments',
            'residual_payments_rate'
        ]);

        return $model;
    }

    /**
     * Create order.
     *
     * @param array $attributes
     *
     * @return \App\Models\Order
     * @throws \Exception
     */
    public function create(array $attributes)
    {
        DB::beginTransaction();

        try {
            $attributes['payment']['status'] = Order::PAYMENT_STATUS_NOT_PAID;

            $model = new Order();
            $model->fill($attributes);
            $model->save();

            // Save products.
            if ($products = array_get($attributes, 'products')) {
                $storeId = array_get($attributes, 'store_id');
                $discountId = array_get($attributes, 'customer.discount_id');
                $currencyId = array_get($attributes, 'currency_id');

                if (!$discountId && $model->customer_id && isset($model->customer['discount_id'])) {
                    $discountId = $model->customer['discount_id'];
                }

                $this->saveProducts($model, $products, $currencyId, $storeId, $discountId);

                // Save order price.
                $this->saveOrderPrice($model, $currencyId);
            }

            // Save payments.
            if ($payments = array_get($attributes, 'payments')) {
                $is_withdrawn = array_get($attributes, 'is_withdrawn', false);

                $this->savePayments($model, $payments, $is_withdrawn);
            }

            // Update order.
            $model->touch();

            DB::commit();

            $model = $this->findById($model->id);

            return $model;
        } catch (Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }

    /**
     * Update order by id.
     *
     * @param int $id
     * @param array $attributes
     *
     * @return \App\Models\Order
     * @throws \Exception
     */
    public function update(int $id, array $attributes)
    {
        DB::beginTransaction();

        try {
            $model = Order::findOrFail($id);
            $model->fill($attributes);
            $model->save();

            // Save products.
            if ($products = array_get($attributes, 'products')) {
                $storeId = array_get($attributes, 'store_id');
                $discountId = array_get($attributes, 'customer.discount_id');
                $currencyId = array_get($attributes, 'currency_id');

                if (!$discountId && $model->customer_id && isset($model->customer['discount_id'])) {
                    $discountId = $model->customer['discount_id'];
                }

                info($discountId);
                $this->saveProducts($model, $products, $currencyId, $storeId, $discountId);

                // Save order price.
                $this->saveOrderPrice($model, $currencyId);
            }

            // Save payments.
            if ($payments = array_get($attributes, 'payments')) {
                $is_withdrawn = array_get($attributes, 'is_withdrawn', false);

                $this->savePayments($model, $payments, $is_withdrawn);
            }

            // Update order.
            $model->touch();

            DB::commit();

            $model->load('manager');
            $model->addVisible([
                'store_id',
                'manager',
                'status',
                'is_from_web_store',
                'payments'
            ]);

            return $model;
        } catch (Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }

    /**
     * Update order manager and set "accepted" status.
     *
     * @param int $id
     * @param int $user_id
     *
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function accept(int $id, int $user_id)
    {
        $model = Order::findOrFail($id);
        $model->manager_id = $user_id;
        $model->status = Order::STATUS_ACCEPTED;
        $model->save();

        return $model;
    }

    /**
     * Update delivery declaration and set "sent" status.
     *
     * @param int $id
     * @param string $declaration
     *
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function send(int $id, string $declaration)
    {
        $model = Order::findOrFail($id);

        $delivery = $model->delivery;
        $delivery['declaration'] = $declaration;

        $model->delivery = $delivery;
        $model->status = Order::STATUS_SENT;
        $model->save();

        return $model;
    }

    /**
     * Set "completed" and "paid" status.
     *
     * @param int $id
     *
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function complete(int $id)
    {
        $model = Order::findOrFail($id);
        $model->status = Order::STATUS_COMPLETED;

        $payment = $model->payment;
        $payment['status'] = Order::PAYMENT_STATUS_PAID;

        $model->payment = $payment;
        $model->save();

        return $model;
    }

    /**
     * Set "partially_refunded" or "refunded" for order by given products.
     *
     * @param int $id
     * @param array $products
     *
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    public function refund(int $id, array $products = [])
    {
        $model = Order::with('order_products')
            ->joinRate('price')
            ->findOrFail($id);

        DB::beginTransaction();

        try {
            $products = $this->saveRefundedProducts($model, $products);

            $productsExists = $products->where('quantity', '>', 0)->count();

            if ($productsExists) {
                $model->status = Order::STATUS_PARTIALLY_REFUNDED;
            } else {
                $model->status = Order::STATUS_REFUNDED;
            }

            $model->save();

            DB::commit();

            return $model;
        } catch (Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }

    /**
     * Set order status to "cancel".
     * Revert products quantity.
     *
     * @param int $id
     *
     * @return bool|Collection|\Illuminate\Database\Eloquent\Model|\Illuminate\Support\Collection
     * @throws Exception
     */
    public function cancel(int $id)
    {
        DB::beginTransaction();

        try {
            $model = Order::findOrFail($id);
            $model->status = Order::STATUS_CANCELED;
            $model->save();

            foreach ($model->order_products as $product) {
                $product_size = $product->product_size;
                $product_size->quantity += $product->quantity;
                $product_size->save();
            }

            DB::commit();

            return $model;
        } catch (Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }

    /**
     * Withdrawn order(s) payments.
     *
     * @param array $criteria
     *
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    public function withdrawnPayments(array $criteria = [])
    {
        $collection = Order::query()
            ->with([
                'payments' => function ($query) {
                    return $query->whereNull('withdrawn_at');
                }
            ])
            ->criteria($criteria)
            ->get();

        DB::beginTransaction();

        try {
            $result = new Collection();

            foreach ($collection as $order) {
                foreach ($order->payments as $payment) {
                    $payment->withdrawn_at = time();
                    $payment->save();

                    $result->push($payment);
                }
            }

            DB::commit();

            return $result;
        } catch (Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }

    /**
     * Get orders count.
     *
     * @param array $criteria
     *
     * @return integer
     */
    public function getCount(array $criteria = [])
    {
        $count = Order::criteria($criteria)->count();

        return $count;
    }

    /**
     * Delete order product.
     *
     * @param int $id
     * @param int $product_id
     *
     * @return int
     * @throws \Exception
     */
    public function deleteProduct(int $id, int $product_id)
    {
        $model = Order::with('order_products')->findOrFail($id);

        $product = $model->products->where('id', $product_id)->first();

        if (!$product) {
            return 0;
        }

        $sizeIds = $product['sizes']->pluck('id');

        $saved = $model->order_products()->whereIn('order_products.id', $sizeIds)->delete();

        return $saved;
    }

    /**
     * Get completed orders and products quantity and transform by date.
     *
     * @param array $criteria
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCompletedOrdersAndProductsQuantity(array $criteria = [])
    {
        // Collect query.
        $collection = Order::query()
            ->with('order_products')
            ->criteria($criteria)
            ->whereIn('status', [
                Order::STATUS_COMPLETED,
                Order::STATUS_PARTIALLY_REFUNDED
            ])
            ->get(['id', 'is_from_web_store', 'created_at']);

        $collection = $collection->groupBy(function ($item) {
            return $item->created_at->format('Y-m-d');
        });
        $collection->transform(function ($groupByDate) {
            $localStoreOrders = $groupByDate->where('is_from_web_store', false);
            $webStoreOrders = $groupByDate->where('is_from_web_store', true);

            $localStoreProductsQnty = $localStoreOrders->sum(function ($item) {
                return $item->order_products->sum('quantity');
            });
            $webStoreProductsQnty = $webStoreOrders->sum(function ($item) {
                return $item->order_products->sum('quantity');
            });

            return [
                'orders' => [
                    'local_store' => $localStoreOrders->count(),
                    'web_store'   => $webStoreOrders->count()
                ],
                'products' => [
                    'local_store' => $localStoreProductsQnty,
                    'web_store'   => $webStoreProductsQnty
                ]
            ];
        });

        return $collection;
    }

    /**
     * Get completed orders and products cost prices and transform by date.
     * TODO: MUST optimize code.
     *
     * @param array $criteria
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCompletedOrdersAndProductsPrices(array $criteria = [])
    {
        // Collect query.
        $collection = Order::query()
            ->joinRate('price')
            ->with('order_products')
            ->criteria($criteria)
            ->whereIn('status', [
                Order::STATUS_COMPLETED,
                Order::STATUS_PARTIALLY_REFUNDED
            ])
            ->get(['price']);

        $collection = $collection->groupBy(function ($item) {
            return $item->created_at->format('Y-m-d');
        });

        $collection->transform(function ($groupByDate) {
            $orders_price = [];
            $products_price = null;

            foreach ($groupByDate as $order) {

                // Collect orders prices.
                foreach ($order->price as $currencyId => $currencyPrice) {
                    $currencyPrice['value'] = round($currencyPrice['value']);

                    if (!array_key_exists($currencyId, $orders_price)) {
                        $orders_price[$currencyId] = $currencyPrice;
                    } else {
                        $orders_price[$currencyId]['value'] += $currencyPrice['value'];
                    }
                }

                // Collect order products prices.
                foreach ($order->order_products as $order_product) {
                    foreach ($order_product->cost_price as $currencyId => $currencyPrice) {
                        if (!isset($products_price[$currencyId])) {
                            $products_price[$currencyId] = [
                                'value' => round($currencyPrice['value']) * $order_product->quantity
                            ];
                        } else {
                            $products_price[$currencyId]['value'] += round($currencyPrice['value']) * $order_product->quantity;
                        }
                    }
                }
            }

            return [
                'orders'   => $orders_price,
                'products' => $products_price
            ];
        });

        return $collection;
    }

    /**
     * Get not completed orders quantity and transform by date.
     *
     * @param array $criteria
     *
     * @return array
     */
    public function getNotCompletedOrdersQuantity(array $criteria = [])
    {
        // Collect query.
        $collection = Order::query()
            ->criteria($criteria)
            ->whereNotIn('status', [
                Order::STATUS_COMPLETED,
                Order::STATUS_PARTIALLY_REFUNDED,
                Order::STATUS_REFUNDED,
                Order::STATUS_CANCELED
            ])
            ->get(['id', 'payment', 'created_at']);

        $resultDataTemplate = [
            'not_paid'       => 0,
            'partially_paid' => 0,
            'paid'           => 0
        ];
        $result = [];

        $collection = $collection->groupBy(function ($item) {
            return $item->created_at->format('Y-m-d');
        });

        foreach ($collection as $date => $group) {
            $group = $group->groupBy('payment.status');

            if (!isset($result[$date])) {
                $result[$date] = $resultDataTemplate;
            }

            foreach ($group as $status => $orders) {
                if (!$status) {
                    $status = Order::PAYMENT_STATUS_NOT_PAID;
                }

                $result[$date][$status] = $orders->count();
            }
        }

        return $result;
    }

    /**
     * Get summary non-withdrawal payments.
     *
     * @param array $criteria
     *
     * @return array
     */
    public function getNonWithdrawalPaymentsSum(array $criteria = [])
    {
        $payments = OrderPayment::query()
            ->joinRate('rate')
            ->whereNull('withdrawn_at')
            ->whereHas('order', function ($query) use ($criteria) {
                $query->criteria($criteria);
            })
            ->get();

        $result = null;
        foreach ($payments as $payment) {
            if (!$result) {
                $result = $payment->rate->toArray();
            } elseif ($payment->rate) {
                foreach ($payment->rate as $currency_id => $rate) {
                    if (!isset($result[$currency_id])) {
                        continue;
                    }

                    $result[$currency_id]['value'] += $rate['value'];
                }
            }
        }

        return $result;
    }

    /**
     * Save products.
     *
     * @param Order $model
     * @param array $orderProducts
     * @param int $currencyId
     * @param int $storeId
     * @param int $discountId
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function saveProducts(Order $model, array $orderProducts, int $currencyId, int $storeId, int $discountId = null)
    {
        // Delete unrelated order products.
        if ($model->exists()) {
            $this->deleteUnrelatedProducts($model, $orderProducts);
        }

        // Retrieve products.
        $sizeIds = array_pluck($orderProducts, 'size_id');
        $products = Product::query()
            ->with(['product_sizes' => function ($query) use ($sizeIds) {
                $query->whereIn('id', $sizeIds)->withTrashed();
            }])
            ->joinSalePrice($storeId, $discountId)
            ->joinRate('cost_price')
            ->whereHas('product_sizes', function ($query) use ($sizeIds) {
                $query->whereIn('id', $sizeIds)->withTrashed();
            })
            ->whereNotNull('entity_rates_sale_price.price')
            ->get();

        // Save order products.
        foreach ($products as $product) {
            $costPrice = $product->cost_price->where('is_default', true)->first();
            $salePrice = $product->sale_price->where('currency_id', $currencyId)->first();

            foreach ($product->product_sizes as $productSize) {

                // Retrieve order product data.
                $productData = array_where($orderProducts, function ($item) use ($productSize) {
                    return $item['size_id'] === $productSize->id;
                });
                $productData = reset($productData);

                // Make orderProduct model.
                $orderProductModel = $model->order_products()->firstOrNew([
                    'size_id' => $productSize->id
                ]);
                $orderProductModel->fill($productData);
                if (!$orderProductModel->exists) {
                    $orderProductModel->cost_price = $costPrice;
                    $orderProductModel->sale_price = $salePrice;
                }

                // Collect difference of new quantity.
                $originalQuantity = $orderProductModel->getOriginal('quantity');
                $dirtyQuantity = $orderProductModel->quantity;
                $diffQuantity = $dirtyQuantity - $originalQuantity;

                // Save order product.
                $model->order_products()->save($orderProductModel);

                // Update quantity of product size.
                $this->updateProductSize($orderProductModel, $diffQuantity);
            }
        }

        $model->load('order_products');

        return $model->products;
    }

    /**
     * Delete unrelated products.
     *
     * @param \App\Models\Order $model
     * @param array $products
     */
    protected function deleteUnrelatedProducts(Order $model, array $products)
    {
        $size_ids = array_filter(array_pluck($products, 'size_id'));

        $order_products = $model->order_products()->whereNotIn('size_id', $size_ids)->get();

        foreach ($order_products as $product) {
            $product->product_size->quantity += $product->quantity;
            $product->product_size->save();
            $product->delete();
        }
    }

    /**
     * Save order total price.
     *
     * @param Order $model
     * @param int $currencyId
     *
     * @return \App\Models\Order
     * @throws \Exception
     */
    protected function saveOrderPrice(Order &$model, int $currencyId)
    {
        // Retrieve order products data.
        $orderProducts = $model->order_products()
            ->joinRate('price')
            ->get();

        $orderPrice = [
            'currency_id' => $currencyId,
            'value'       => 0
        ];

        // Collect order price by order products prices.
        foreach ($orderProducts as $product) {
            $price = $product->sale_price[$currencyId];

            $productPrice = $price['value'] - ($price['value'] * $product->discount / 100);
            $productPrice *= $product->quantity;

            $orderPrice['value'] += $productPrice;
        }

        $model->price = $orderPrice;
        $model->save();

        return $model;
    }

    /**
     * Update quantity of products size.
     *
     * @param OrderProduct $orderProduct
     * @param int $quantity
     */
    protected function updateProductSize(OrderProduct $orderProduct, int $quantity)
    {
        $orderProduct->product_size->quantity -= $quantity;
        $orderProduct->product_size->save();
    }

    /**
     * Save payments.
     *
     * @param Order $model
     * @param array $payments
     * @param bool $withdrawn
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function savePayments(Order $model, array $payments, bool $withdrawn = false)
    {
        // Delete unrelated payments.
        if ($model->exists()) {
            $this->deleteUnrelatedPayments($model, $payments);
        }

        // Save payments.
        $time = time();
        foreach ($payments as $payment) {
            $id = array_get($payment, 'id');
            $rate = array_get($payment, 'rate');

            $paymentModel = $model->payments()->findOrNew($id);
            $paymentModel->rate = $rate;

            if ($withdrawn) {
                $paymentModel->withdrawn_at = $time;
            }

            $model->payments()->save($paymentModel);
        }

        $model->load('payments');

        return $model->payments;
    }

    /**
     * Delete unrelated payments.
     *
     * @param \App\Models\Order $model
     * @param array $payments
     */
    protected function deleteUnrelatedPayments(Order $model, array $payments)
    {
        $ids = array_filter(array_pluck($payments, 'id'));

        $model->payments()
            ->whereNotIn('order_payments.id', $ids)
            ->delete();
    }

    /**
     * Save refunded products.
     *
     * @param Order $model
     * @param array $products
     *
     * @return mixed
     * @throws \Exception
     */
    protected function saveRefundedProducts(Order $model, array $products = [])
    {
        foreach ($products as $productData) {
            $product = $model->order_products
                ->where('size_id', $productData['size_id'])
                ->first();

            $product->refunded_quantity += $productData['quantity'];
            $product->quantity -= $productData['quantity'];
            $product->save();

            // Update original product quantity.
            $product->product_size->quantity += $productData['quantity'];
            $product->product_size->save();
        }

        // Re-save order price.
        $currency_id = $model->price->where('is_default', true)->first()['currency_id'];
        $this->saveOrderPrice($model, $currency_id);

        $model->load('order_products');

        return $model->order_products;
    }
}
