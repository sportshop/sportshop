<?php

namespace App\Repositories\Eloquent;

use App\Models\Store;
use App\Repositories\Contracts\StoreRepositoryContract;
use DB;
use Exception;

class StoreRepository implements StoreRepositoryContract
{
    /**
     * Find all stores by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = [])
    {
        $collection = Store::query()
            ->withMedia([
                Store::MEDIA_LOGO,
                Store::MEDIA_FAVICON
            ])
            ->get();

        return $collection;
    }

    /**
     * Find banner by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\Banner
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id, array $criteria = [])
    {
        $model = Store::query()
            ->withMedia([
                Store::MEDIA_LOGO,
                Store::MEDIA_FAVICON
            ])
            ->find($id);

        return $model;
    }

    /**
     * Create store.
     *
     * @param array $attributes
     *
     * @return \App\Models\Store
     * @throws \Exception
     */
    public function create(array $attributes)
    {
        DB::beginTransaction();

        try {
            $model = Store::create($attributes);

            // Attach logo
            if ($logo_id = array_get($attributes, 'logo_id')) {
                $model->attachMedia($logo_id, Store::MEDIA_LOGO);
            }

            // Attach favicon
            if ($favicon_id = array_get($attributes, 'favicon_id')) {
                $model->attachMedia($favicon_id, Store::MEDIA_FAVICON);
            }

            $model->loadMedia([
                Store::MEDIA_LOGO,
                Store::MEDIA_FAVICON
            ]);

            DB::commit();

            return $model;
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * Update store.
     *
     * @param $id
     * @param array $attributes
     *
     * @return \App\Models\Store
     * @throws \Exception
     */
    public function update($id, array $attributes)
    {
        DB::beginTransaction();

        try {
            $model = Store::findOrFail($id);
            $model->fill($attributes);
            $model->save();

            // Sync logo
            if ($logo_id = array_get($attributes, 'logo_id')) {
                $model->syncMedia([$logo_id], Store::MEDIA_LOGO);
            }

            // Sync favicon
            if ($favicon_id = array_get($attributes, 'favicon_id')) {
                $model->syncMedia([$favicon_id], Store::MEDIA_FAVICON);
            }

            $model->loadMedia([
                Store::MEDIA_LOGO,
                Store::MEDIA_FAVICON
            ]);

            DB::commit();

            return $model;
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * Delete store by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id)
    {
        $deleted = Store::destroy($id);

        return $deleted;
    }
}
