<?php

namespace App\Repositories\Eloquent;

use App\Exceptions\ProductExistException;
use App\Models\Brand;
use App\Models\Size;
use App\Models\SizeGroup;
use App\Models\SizeValue;
use App\Repositories\Contracts\SizeRepositoryContract;
use DB;
use Exception;
use Freevital\Repository\Eloquent\BaseRepository;

class SizeRepository implements SizeRepositoryContract
{
    /**
     * Find all sizes by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Support\Collection
     */
    public function findAll(array $criteria = [], array $options = [])
    {
        $query = SizeGroup::query()
            ->with([
                'sizes' => function ($query) use ($criteria) {
                    $query
                        ->with([
                            'values',
                            'product_sizes' => function ($query) use ($criteria) {
                                $query
                                    ->where('store_id', $criteria['store_id'])
                                    ->where('quantity', '>', 0);
                            }
                        ])
                        ->has('product_sizes');
                }
            ])
            ->whereHas('sizes.product_sizes.product', function ($query) use ($criteria) {
                $query
                    ->joinSalePrice($criteria['store_id'], array_get($criteria, 'discount_id'))
                    ->criteria($criteria)
                    ->storeAvailable($criteria['store_id']);
            });

        $collection = $query->get();

        return $collection;
    }

    /**
     * Find size by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\SizeGroup
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id, array $criteria = [])
    {
        $model = SizeGroup::query()
            ->with([
                'sizes.values',
                'brand' => function ($query) {
                    return $query->withMedia(Brand::MEDIA_THUMBNAIL);
                }
            ])
            ->where('id', $id)
            ->first();

        return $model;
    }

    /**
     * Create size.
     *
     * @param array $attributes
     *
     * @return \App\Models\SizeGroup
     * @throws \Exception
     */
    public function create(array $attributes)
    {
        DB::beginTransaction();

        try {
            // Create a size group
            $group = SizeGroup::create($attributes);

            // Creating the sizes
            $sizes = array_get($attributes, 'sizes');
            foreach ($sizes as $size) {
                $sizeModel = new Size();

                $group->sizes()->save($sizeModel);

                // Creating the size values
                $values = array_get($size, 'values');
                foreach ($values as $value) {
                    $valueModel = new SizeValue();
                    $valueModel->fill($value);

                    $sizeModel->values()->save($valueModel);
                }
            }

            $group->load('sizes.values');

            DB::commit();

            return $group;
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * Update size by id.
     *
     * @param int $id
     * @param array $attributes
     *
     * @return \App\Models\SizeGroup|bool
     * @throws Exception
     */
    public function update(int $id, array $attributes)
    {
        DB::beginTransaction();

        try {
            // Update a size group
            $group = SizeGroup::findOrFail($id);
            $group->fill($attributes);
            $group->save();

            $sizes = array_get($attributes, 'sizes');

            // Delete unrelated sizes
            $ids = array_pluck($sizes, 'id');
            $ids = array_filter($ids);
            $unrelatedSizes = $group->sizes()->whereNotIn('id', $ids)->get();
            foreach ($unrelatedSizes as $size) {
                if ($size->productSizesExists()) {
                    throw new ProductExistException('Please, remove products related with this size.');
                }

                $size->delete();
            }

            // Update the sizes
            foreach ($sizes as $size) {
                $sizeModel = $group->sizes()->findOrNew(array_get($size, 'id'));

                $group->sizes()->save($sizeModel);

                $values = array_get($size, 'values');

                // Delete unrelated size values
                $ids = array_pluck($values, 'id');
                $ids = array_filter($ids);
                $sizeModel->values()->whereNotIn('id', $ids)->delete();

                // Update the sizes
                foreach ($values as $value) {
                    $valueModel = SizeValue::findOrNew(array_get($value, 'id'));
                    $valueModel->fill($value);

                    $sizeModel->values()->save($valueModel);
                }
            }

            $group->load('sizes.values');

            DB::commit();

            return $group;
        } catch (Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * Delete size by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id)
    {
        $deleted = SizeGroup::destroy($id);

        return $deleted;
    }
}
