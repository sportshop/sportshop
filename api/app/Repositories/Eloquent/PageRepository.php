<?php

namespace App\Repositories\Eloquent;

use App\Models\Page;
use App\Repositories\Contracts\PageRepositoryContract;

class PageRepository implements PageRepositoryContract
{
    /**
     * Find pages by criteria.
     *
     * @param array $criteria
     * @param array $options
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function findAll(array $criteria = [], array $options = [])
    {
        $collection = Page::paginate();

        return $collection;
    }

    /**
     * Find page by id.
     *
     * @param int $id
     * @param array $criteria
     *
     * @return \App\Models\Page|null
     */
    public function findById(int $id, array $criteria = [])
    {
        $model = Page::find($id);

        return $model;
    }

    /**
     * Find page by slug.
     *
     * @param string $slug
     * @param array $criteria
     *
     * @return \App\Models\Page|null
     */
    public function findBySlug(string $slug, array $criteria = [])
    {
        $model = Page::where('slug', $slug)->first();

        return $model;
    }

    /**
     * Create page.
     *
     * @param array $attributes
     *
     * @return \App\Models\Page
     * @throws \Exception
     */
    public function create(array $attributes)
    {
        $model = Page::create($attributes);

        return $model;
    }

    /**
     * Update page.
     *
     * @param $id
     * @param array $attributes
     *
     * @return \App\Models\Page
     * @throws \Exception
     */
    public function update($id, array $attributes)
    {
        $model = Page::findOrFail($id);
        $model->fill($attributes);
        $model->save();

        return $model;
    }

    /**
     * Delete page by id.
     *
     * @param int $id
     *
     * @return bool|int|null
     */
    public function delete(int $id)
    {
        $deleted = Page::destroy($id);

        return $deleted;
    }
}
