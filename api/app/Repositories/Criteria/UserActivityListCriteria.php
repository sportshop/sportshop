<?php

namespace App\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Freevital\Repository\Contracts\CriteriaContract;
use Freevital\Repository\Contracts\RepositoryContract;

class UserActivityListCriteria implements CriteriaContract
{
    /**
     * @var array
     */
    protected $params;

    /**
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * Apply criteria in query repository.
     *
     * @param Builder            $query
     * @param RepositoryContract $repository
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Builder $query, RepositoryContract $repository): Builder
    {
        if ($value = array_get($this->params, 'user_id')) {
            $query->where('user_id', $value);
        }

        if ($value = array_get($this->params, 'year')) {
            $query->whereYear('created_at', $value);
        }

        if ($value = array_get($this->params, 'group_by')) {
            switch ($value) {
                case 'month':
                    $query->groupMonth();
                    break;

                case 'day':
                default:
                    $query->groupDay();
                    break;
            }
        } else {
            $query->groupDay();
        }

        $query->orderBy('created_at', 'desc');

        return $query;
    }
}
