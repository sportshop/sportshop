<?php

namespace App\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Freevital\Repository\Contracts\CriteriaContract;
use Freevital\Repository\Contracts\RepositoryContract;

class UserSaleListCriteria implements CriteriaContract
{
    /**
     * @var array
     */
    protected $params;

    /**
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * Apply criteria in query repository.
     *
     * @param Builder            $query
     * @param RepositoryContract $repository
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Builder $query, RepositoryContract $repository): Builder
    {
        $query->with([
            'local_sale_percentage',
            'remote_sale_percentages',
            'orders' => function ($query) {
                if ($value = array_get($this->params, 'year')) {
                    $query->whereYear('created_at', $value);
                }

                return $query->with('customer_relation', 'order_products');
            }
        ]);

        return $query;
    }
}
