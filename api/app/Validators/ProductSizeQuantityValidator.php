<?php

namespace App\Validators;

use App\Models\ProductSize;

class ProductSizeQuantityValidator
{
    /**
     * Check if product size's quantity exist.
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     *
     * @return bool
     * @throws \Exception
     */
    public function validate($attribute, $value, $parameters, $validator)
    {
        if (!isset($parameters[0])) {
            throw new \Exception('No parameter');
        }

        $index = $this->getArrayIndex($attribute);

        $id = $validator->attributes()['products'][$index][$parameters[0]];

        return ProductSize::query()
            ->where('id', $id)
            ->where('quantity', '>=', $value)
            ->exists();
    }

    /**
     * @param $message
     * @param $attribute
     * @param $rule
     * @param $parameters
     * @param $validator
     *
     * @return mixed
     */
    public function replacer($message, $attribute, $rule, $parameters, $validator)
    {
        $index = $this->getArrayIndex($attribute);

        $id = $validator->attributes()['products'][$index][$parameters[0]];

        $model = ProductSize::query()
            ->where('id', $id)
            ->first();

        if (!$model) {
            return $message;
        }

        return [
            'id'       => $model->id,
            'quantity' => $model->quantity
        ];
    }

    /**
     * @param $attribute
     *
     * @return mixed
     */
    protected function getArrayIndex($attribute)
    {
        preg_match('/\d+/', $attribute, $key);

        return reset($key);
    }
}
