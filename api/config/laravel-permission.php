<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authorization Models
    |--------------------------------------------------------------------------
    */

    'models' => [

        /*
        |--------------------------------------------------------------------------
        | Permission Model
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | Eloquent model should be used to retrieve your permissions. Of course, it
        | is often just the "Permission" model but you may use whatever you like.
        |
        | The model you want to use as a Permission model needs to implement the
        | `Spatie\Permission\Contracts\Permission` contract.
        |
        */

        'permission' => App\Models\Permission::class,

        /*
        |--------------------------------------------------------------------------
        | Role Model
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | Eloquent model should be used to retrieve your roles. Of course, it
        | is often just the "Role" model but you may use whatever you like.
        |
        | The model you want to use as a Role model needs to implement the
        | `Spatie\Permission\Contracts\Role` contract.
        |
        */

        'role' => App\Models\Role::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Authorization Tables
    |--------------------------------------------------------------------------
    */

    'table_names' => [

        /*
        |--------------------------------------------------------------------------
        | Users Table
        |--------------------------------------------------------------------------
        |
        | The table that your application uses for users. This table's model will
        | be using the "HasRoles" and "HasPermissions" traits.
        |
        */
        'users' => 'users',


        /*
        |--------------------------------------------------------------------------
        | Roles Table
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | table should be used to retrieve your roles. We have chosen a basic
        | default value but you may easily change it to any table you like.
        |
        */

        'roles' => 'roles',

        /*
        |--------------------------------------------------------------------------
        | Permissions Table
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | table should be used to retrieve your permissions. We have chosen a basic
        | default value but you may easily change it to any table you like.
        |
        */

        'permissions' => 'permissions',

        /*
        |--------------------------------------------------------------------------
        | User Permissions Table
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | table should be used to retrieve your users permissions. We have chosen a
        | basic default value but you may easily change it to any table you like.
        |
        */

        'user_has_permissions' => 'x_permission_user',

        /*
        |--------------------------------------------------------------------------
        | User Roles Table
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | table should be used to retrieve your users roles. We have chosen a
        | basic default value but you may easily change it to any table you like.
        |
        */

        'user_has_roles' => 'x_role_user',

        /*
        |--------------------------------------------------------------------------
        | Role Permissions Table
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | table should be used to retrieve your roles permissions. We have chosen a
        | basic default value but you may easily change it to any table you like.
        |
        */

        'role_has_permissions' => 'x_permission_role',

    ],

    'roles' => [
        ['name' => 'super_admin',     'is_system' => true],
        ['name' => 'admin',           'is_system' => true],
        ['name' => 'manager',         'is_system' => false],
        ['name' => 'limited_manager', 'is_system' => false],
        ['name' => 'member',          'is_system' => false]
    ],

    'permissions' => [
        'attributes.view',
        'attributes.manage',

        'banners.view',
        'banners.manage',

        'brands.view',
        'brands.manage',

        'categories.view',
        'categories.manage',

        'costs.view',
        'costs.manage',

        'currencies.view',
        'currencies.manage',

        'discounts.view',
        'discounts.manage',

        'media.manage',

        'orders.view',
        'orders.manage',
        'orders.refund',
        'orders.custom_price.manage',
        'orders.payments.manage',

        'pages.view',
        'pages.manage',

        'products.view',
        'products.manage',
        'products.status.view',
        'products.cost_price.view',

        'settings.view',
        'settings.manage',

        'sizes.view',
        'sizes.manage',

        'statistic.costs.view',
        'statistic.incomplete_orders.view',
        'statistic.sales.view',
        'statistic.store_income.view',
        'statistic.transfers.view',
        'statistic.wage.view',

        'stores.view',
        'stores.manage',

        'users.view',
        'users.manage',
        'users.delete',
        'users.created_at.view',
        'users.purchase_amount.view',
        'users.data.manage',

        'wage.view',
        'wage.manage'
    ],

    'roles_permissions' => [

        'super_admin' => [
            'banners.view',
            'banners.manage',

            'media.manage',

            'pages.view',
            'pages.manage',

            'settings.view',
            'settings.manage',

            'stores.view',
            'stores.manage',

            'users.view',
            'users.manage',
            'users.delete',
            'users.created_at.view',
            'users.purchase_amount.view',
            'users.data.manage'
        ],

        'admin' => [
            'attributes.view',
            'attributes.manage',

            'banners.view',
            'banners.manage',

            'brands.view',
            'brands.manage',

            'categories.view',
            'categories.manage',

            'costs.view',
            'costs.manage',

            'currencies.view',
            'currencies.manage',

            'discounts.view',
            'discounts.manage',

            'media.manage',

            'orders.view',
            'orders.manage',
            'orders.refund',
            'orders.custom_price.manage',
            'orders.payments.manage',

            'pages.view',
            'pages.manage',

            'products.view',
            'products.manage',
            'products.status.view',
            'products.cost_price.view',

            'settings.view',
            'settings.manage',

            'sizes.view',
            'sizes.manage',

            'statistic.costs.view',
            'statistic.incomplete_orders.view',
            'statistic.sales.view',
            'statistic.store_income.view',
            'statistic.transfers.view',
            'statistic.wage.view',

            'stores.view',
            'stores.manage',

            'users.view',
            'users.manage',
            'users.delete',
            'users.created_at.view',
            'users.purchase_amount.view',
            'users.data.manage',

            'wage.view',
            'wage.manage'
        ],

        'manager' => [
            'attributes.view',
            'attributes.manage',

            'banners.view',
            'banners.manage',

            'brands.view',
            'brands.manage',

            'categories.view',
            'categories.manage',

            'costs.view',
            'costs.manage',

            'currencies.view',
            'currencies.manage',

            'discounts.view',
            'discounts.manage',

            'media.manage',

            'orders.view',
            'orders.manage',
            'orders.refund',
            'orders.custom_price.manage',

            'pages.view',
            'pages.manage',

            'products.view',
            'products.manage',
            'products.status.view',
            'products.cost_price.view',

            'settings.view',
            'settings.manage',

            'sizes.view',
            'sizes.manage',

            'statistic.costs.view',
            'statistic.incomplete_orders.view',
            'statistic.sales.view',
            'statistic.transfers.view',

            'stores.view',
            'stores.manage',

            'users.view',
            'users.manage'
        ],

        'limited_manager' => [
            'attributes.view',
            'attributes.manage',

            'banners.view',
            'banners.manage',

            'brands.view',
            'brands.manage',

            'categories.view',
            'categories.manage',

            'currencies.view',
            'currencies.manage',

            'discounts.view',
            'discounts.manage',

            'media.manage',

            'orders.view',
            'orders.manage',

            'pages.view',
            'pages.manage',

            'products.view',

            'sizes.view',
            'sizes.manage',

            'statistic.sales.view',

            'stores.view',
            'stores.manage'
        ],

        'member' => [
            //
        ]
    ]

];
