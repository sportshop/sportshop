<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'imagick',

    'size' => [
        'dashboard' => [
            'thumbnail' => [
                'width'  => 300,
                'height' => 300
            ]
        ],
        'category'  => [
            'thumbnail' => [
                'width'  => 300
            ]
        ],
        'product' => [
            'small' => [
                'width'  => 300
            ],
            'medium' => [
                'width'  => 600
            ]
        ],
        'size' => [
            'medium' => [
                'width'  => 600
            ]
        ],
        'brand' => [
            'medium' => [
                'width'  => 600
            ]
        ],
        'banner' => [
            'large' => [
                'width'  => 1140,
                'height' => 400
            ]
        ]
    ]

];
