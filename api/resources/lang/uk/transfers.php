<?php

return [
    'actions'        => [
        'add'    => 'Додавання',
        'move'   => 'Переміщення',
        'delete' => 'Видалення'
    ],
    'action'         => 'Дія',
    'date'           => 'Дата',
    'from_store'     => 'З магазину',
    'manager'        => 'Менеджер',
    'price'          => 'Ціна',
    'product'        => 'Товар / Артикул',
    'qnty'           => 'К-сть',
    'size'           => 'Розмір',
    'to_store'       => 'В магазин',
    'total'          => 'Сума',
    'total_quantity' => 'Загальна кількість',
    'total_sum'      => 'Загальна сума',
    'transfer_list'  => 'Переміщення товарів',
];
