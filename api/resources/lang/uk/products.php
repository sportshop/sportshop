<?php

return [
    'image' => 'Фото',
    'name'  => 'Назва',
    'sku'   => 'Артикул',
    'size'  => 'Розмір',
    'qnty'  => 'Кiлькiсть',
    'price' => 'Ціна',
    'total' => 'Всього'
];
