<?php

return [
    'title'             => 'Cпортивне взуття та одяг',
    'feedback'          => [
        'subject' => 'Зворотній зв’язок',
        'title'   => 'Зворотній зв’язок - нове повідомлення',
        'name'    => 'Ім’я',
        'email'   => 'Email',
        'phone'   => 'Телефон',
        'message' => 'Повідомлення',
    ],
    'product_feedback'  => [
        'subject' => 'Нове запитання по товару'
    ],
    'product_call'      => [
        'subject' => 'Запит на дзвінок'
    ],
    'password_link'     => [
        'subject' => 'Відновлення паролю'
    ],
    'user_was_created'  => [
        'subject' => 'Ваш аккаунт зареєстрований'
    ],
    'order_created_to_manager' => [
        'subject'  => 'Нове замовлення #:id',
        'title'    => 'Ви отримали нове замовлення #:id'
    ],
    'order_created_to_customer' => [
        'subject'  => 'Нове замовлення #:id',
        'title'    => 'Ваше замовлення #:id'
    ]
];
