<?php

return [
    'comment'            => 'Коментар',
    'customer'           => 'Клієнт',
    'customer_discount'  => 'Знижка',
    'declaration_number' => '№ декларації',
    'delivery'           => 'Доставка',
    'delivery_method'    => [
        'courier'  => 'Доставка курєром',
        'new_post' => 'Нова Пошта',
        'pickup'   => 'Самовивіз'
    ],
    'manager'            => 'Менеджер',
    'orders_list'        => 'Список замовлень',
    'orders_quantity'    => 'Кількість замовлень',
    'order_number'       => 'Замовлення № :order',
    'payment'            => 'Оплата',
    'payment_method'     => [
        'cod'  => 'Наложений платіж',
        'card' => 'Карта',
        'cash' => 'Оплата готівкою'
    ],
    'payment_status'     => [
        'paid'           => 'Оплачено',
        'not_paid'       => 'Неоплачено',
        'partially_paid' => 'Частково оплачено'
    ],
    'status'             => [
        'new'                => 'Новий',
        'accepted'           => 'Прийнятий',
        'sent'               => 'Відправлено',
        'completed'          => 'Завершено',
        'canceled'           => 'Відмінено',
        'refunded'           => 'Повернено',
        'partially_refunded' => 'Частково повернено'
    ],
    'total_sum'          => 'Загальна сума',
    'total_quantity'     => 'Загальна кількість'
];
