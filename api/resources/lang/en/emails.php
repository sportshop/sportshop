<?php

return [
    'feedback'         => [
        'subject' => 'Feedback'
    ],
    'product_feedback' => [
        'subject' => 'Product feedback'
    ],
    'product_call'     => [
        'subject' => 'Product call'
    ],
    'password_link'    => [
        'subject' => 'Reset password'
    ],
    'user_was_created' => [
        'subject' => 'Your account is ready.'
    ],
    'order_was_created' => [
        'subject' => 'Order was created.'
    ]
];
