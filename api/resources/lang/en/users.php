<?php

return [
    'not_found'                     => 'The user not found',
    'session_finished'              => 'Your session has been finished',
    'session_has_not_been_finished' => 'Your previous session has not been finished',
    'session_has_not_been_started'  => 'You did not start the session today',
    'workday_started'               => 'Daily worklog has been started',
    'workday_finished'              => 'Daily worklog has been finished',
];
