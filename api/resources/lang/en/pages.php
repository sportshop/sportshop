<?php

return [
    'not_found'                      => 'The page not found',
    'system_page_can_not_be_deleted' => 'The system page can not be deleted'
];
