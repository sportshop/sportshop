<?php

return [
    'status'          => [
        'new'                => 'Новий',
        'accepted'           => 'Прийнятий',
        'sent'               => 'Відправлено',
        'completed'          => 'Завершено',
        'canceled'           => 'Відмінено',
        'refunded'           => 'Повернено',
        'partially_refunded' => 'Частково повернено'
    ],
    'payment_status'  => [
        'paid'           => 'Оплачено',
        'not_paid'       => 'Неоплачено',
        'partially_paid' => 'Частково оплачено'
    ],
    'payment_method'  => [
        'cod'  => 'Наложений платіж',
        'card' => 'Карта',
        'cash' => 'Оплата готівкою'
    ],
    'delivery_method' => [
        'courier'  => 'Доставка курєром',
        'new_post' => 'Нова Пошта',
        'pickup'   => 'Самовивіз'
    ]
];
