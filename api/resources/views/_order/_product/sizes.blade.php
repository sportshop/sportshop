@if (isset($is_refunded) && $is_refunded)
    {{ $product['sizes']['items']->where('refunded_quantity', '>', 0)[0]->values->where('is_default', true)->first()->title }}:
    @foreach ($product['sizes']['items']->where('refunded_quantity', '>', 0) as $size)
        @if ($loop->index)
            /
        @endif
        {{ $size->values->where('is_default', true)->first()->value }}
        -
        {{ $size->refunded_quantity }} шт.
    @endforeach
@elseif (isset($product['sizes']['items']->where('quantity', '>', 0)[0]))
    {{ $product['sizes']['items']->where('quantity', '>', 0)[0]->values->where('is_default', true)->first()->title }}:
    @foreach ($product['sizes']['items']->where('quantity', '>', 0) as $size)
        @if ($loop->index)
            /
        @endif
        {{ $size->values->where('is_default', true)->first()->value }}
        -
        {{ $size->quantity }} шт.
    @endforeach
@endif