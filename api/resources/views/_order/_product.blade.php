<tr>
    {{-- Thumbnail --}}
    <td width="1">
        @include('_order._product.thumbnail', ['path' => $product['media'][0]['path']])
    </td>

    {{-- Name, sku --}}
    <td>
        <span>{{ $product['name'] }}</span>, {{ $product['sku'] }}<br>
        @if (isset($is_refunded) && $is_refunded)
            <span style="color: #FF6857">Повернено</span>
        @endif
    </td>

    {{-- Sizes --}}
    <td>
        @include('_order._product.sizes', ['sizes' => $product['sizes']])
    </td>

    {{-- Price --}}
    <td width="1" class="nowrap">
        @include('_order.amount', ['amount' => $product['sale_price']])
    </td>

    {{-- Total quantity, price --}}
    <td width="1" class="nowrap">
        @if (isset($is_refunded) && $is_refunded)
            <strong>{{ $product['sizes']['items']->sum('refunded_quantity') }} шт.</strong>
            -
            <strong>{{ $product['sale_price']->where('is_default', true)->first()['value'] * $product['sizes']['items']->sum('refunded_quantity') }}</strong>
        @else
            <strong>{{ $product['sizes']['items']->sum('quantity') }} шт.</strong>
            -
            <strong>{{ $product['sale_price']->where('is_default', true)->first()['value'] * $product['sizes']['items']->sum('quantity') }}</strong>
        @endif
        <strong>{{ $product['sale_price']->where('is_default', true)->first()['symbol'] }}</strong>
    </td>
</tr>