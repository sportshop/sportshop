{{ trans('orders.delivery_method.' . $data['method']) }}

@if ($data['method'] !== 'pickup')
    {{ $data['city'] }}
    @if (isset($data['department']))
        , {{ $data['department'] }}
    @endif
    @if (isset($data['address']))
        , {{ $data['address'] }}
    @endif
@endif

{{-- Declaration --}}
@if (isset($data['declaration']))
    / <strong>@lang('orders.declaration_number'):</strong> {{ $data['declaration'] }}
@endif