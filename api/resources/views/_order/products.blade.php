<table class="order-products">
    <thead>
        <tr>
            <th>@lang('products.image')</th>
            <th>@lang('products.name'), @lang('products.sku')</th>
            <th>@lang('products.size') / @lang('products.qnty')</th>
            <th>@lang('products.price')</th>
            <th>@lang('products.total')</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($products as $product)
            @include('_order._product', compact('product'))
        @endforeach
    </tbody>
</table>