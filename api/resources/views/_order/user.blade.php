@if (isset($data['phone']))
    <strong>{{ $data['phone'] }}</strong>
@endif

@if (isset($data['first_name']) && isset($data['last_name']))
    {{ $data['first_name'] }} {{ $data['last_name'] }}
@endif

@if (isset($data['email']))
    {{ $data['email'] }}
@endif