@extends('emails.layout')

@section('content')
    <p>
        Вітаємо! Для вас створено аккаунт.
        <br>
        Дані для входу:
    </p>
    <ul>
        <li><strong>Email:</strong> {{ $user->email }}</li>
        <li><strong>Пароль:</strong> {{ $password }}</li>
    </ul>
    <br>
    <p>
        Ви можете
        <a href="{{ $url }}">увійти в особистий кабінет</a>
    </p>
@endsection