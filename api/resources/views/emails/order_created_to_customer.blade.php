@extends('emails.layout')

@section('content')
    <h2>@lang('emails.order_created_to_customer.title', ['id' => $order['id']])</h2>

    <table class="order-additional">

        {{-- Payment information --}}
        <tr>
            <td class="label">@lang('orders.payment'):</td>
            <td>
                @include('_order.payment', ['data' => $order['payment']])
            </td>
        </tr>

        {{-- Delivery information --}}
        <tr>
            <td class="label">@lang('orders.delivery'):</td>
            <td>
                @include('_order.delivery', ['data' => $order['delivery']])
            </td>
        </tr>
    </table>

    {{-- Products --}}
    @include('_order.products', ['products' => $order['products']])
@endsection