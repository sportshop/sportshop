@extends('emails.layout')

@section('content')
    <h1>Доброго дня!</h1>
    <p>Ви надіслали запит на відновлення паролю.</p>
    <p>
        Будь ласка, перейдіть за посиланням нижче для підтвердження вашої електронної адреси та відновлення паролю:
        <br>
        <a href="{{ $url }}">Відновити пароль</a>
    </p>
@endsection