@extends('emails.layout')

@section('content')
    <p>
        Ви отримали нове запитання по товару
        <a href="{{ $product['link'] }}">{{ $product['name'] }} - {{ $product['sku'] }}.</a>
    </p>

    <ul>
        <li><strong>Імя:</strong> {{ $form_data['name'] }}</li>
        <li><strong>Email:</strong> {{ $form_data['email'] }}</li>
        <li><strong>Запитання:</strong> {!! $form_data['text'] !!}</li>
    </ul>
@endsection