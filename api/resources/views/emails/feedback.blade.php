@extends('emails.layout')

@section('content')
    <h2>@lang('emails.feedback.title')</h2>
    <ul>
        <li><strong>@lang('emails.feedback.name'):</strong> {{ $name }}</li>
        <li><strong>@lang('emails.feedback.email'):</strong> {{ $email }}</li>
        <li><strong>@lang('emails.feedback.phone'):</strong> {{ $phone }}</li>
        <li><strong>@lang('emails.feedback.message'):</strong> {!! $text !!}</li>
    </ul>
@endsection