@extends('emails.layout')

@section('content')
    <p>
        Ви отримали новий запит на дзвінок по товару
        <a href="{{ $product['link'] }}">{{ $product['name'] }} - {{ $product['sku'] }}.</a>
    </p>

    <p>
        <strong>Телефон:</strong> {{ $form_data['phone'] }}
    </p>
@endsection