@extends('pdf._layout')

@section('title', trans('orders.orders_list'))

@section('content')
    <div class="head">

        {{-- Customer --}}
        @if ($user->hasRole('member'))
            <div><strong>@lang('orders.client'):</strong> {{ $user->full_name }}</div>
        @endif

        {{-- Total quantity, sum --}}
        @if (!$orders->isEmpty())
            <div>@lang('orders.orders_quantity'): <strong>{{ $orders->count() }}</strong></div>
            <div>
                @lang('orders.total_sum'):
                <strong>{{ $total_price['value'] }} {{ $total_price['symbol'] }}</strong>
            </div>
        @endif
    </div>

    {{-- Order list --}}
    @foreach($orders as $order)
        <div class="order">
            <div class="order-title">
                <strong>@lang('orders.order_number', ['order' => $order->id])</strong>
            </div>

            @include('pdf._order', ['order' => $order, 'total' => $order['total']])
        </div>
    @endforeach
@endsection