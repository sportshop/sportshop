@extends('pdf._layout')

@section('title', trans('orders.order_number', ['order' => $order->id]))

@section('content')
    @include('pdf._order', compact('order', 'total'))
@endsection