@extends('pdf._layout')

@section('title', trans('transfers.transfer_list'))

@section('content')
    <div class="head">

        {{-- Total quantity, sum --}}
        @if (!$transfers->isEmpty())
            <div>@lang('transfers.total_quantity'): <strong>{{ $total['quantity'] }}</strong></div>
            <div>
                @lang('transfers.total_sum'):
                <strong>{{ $total['price']['value'] }} {{ $total['price']['symbol'] }}</strong>
            </div>
        @endif
    </div>

    {{-- Transfer list --}}
    <div class="order">
        <table class="order-products">
            <thead>
                <tr>
                    <th>@lang('transfers.action')</th>
                    <th class="nowrap">@lang('transfers.from_store')</th>
                    <th class="nowrap">@lang('transfers.to_store')</th>
                    <th>@lang('transfers.manager')</th>
                    <th>@lang('transfers.date')</th>
                    <th>@lang('transfers.product')</th>
                    <th>@lang('transfers.size')</th>
                    <th>@lang('transfers.qnty')</th>
                    <th>@lang('transfers.price')</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($transfers as $transfer)
                    @include('pdf._transfer_item', compact('transfer'))
                @endforeach
            </tbody>
        </table>
    </div>
@endsection