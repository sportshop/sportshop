<style>
    body {
        margin: 0;
        padding: 0;
        width: 100%;
        font-family: DejaVu Sans, sans-serif;
        font-size: 14px;
    }

    h1 { margin: 5px 0; text-align: center; }
    img { max-height: 50px; max-width: 50px; }

    table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 2px;
        border: 0;
    }
    table th {
        padding: 10px 5px;
        line-height: 1;
        white-space: nowrap;
        font-weight: bold;
        color: #444;
    }
    table td {
        vertical-align: top;
    }
    .label {
        padding-right: 5px;
        width: 1px;
        white-space: nowrap;
    }
    .nowrap {
        white-space: nowrap;
    }

    .main-logo {
        background-color: #3c76bf;
        text-align: center;
        padding: 10px;
        max-width: 150px;
        display: block;
        margin-left: auto;
        margin-right: auto;
        border-radius: 5px;
    }

    .main-logo img {
        max-width: 150px;
        height: 41px;
    }

    .head {
        margin-bottom: 5px;
    }

    .order {
        margin-top: 20px;
        padding-bottom: 20px;
        border-bottom: 1px dashed #666;
    }
    .order-title {
        margin-bottom: 5px;
        font-size: 16px;
    }
    .order-products {
        margin: 15px 0;
    }
    .order-products th {
        border: 1px solid #222;
    }
    .order-products td {
        padding: 5px;
        border: 1px solid #222;
    }
    .order-additional td {
        padding: 5px;
    }
</style>