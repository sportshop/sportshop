<tr>
    {{-- Action --}}
    <td>@lang('transfers.actions.' . $transfer['action'])</td>

    {{-- From Store --}}
    <td style="max-width: 150px;">{{ $transfer->from_store ? $transfer->from_store->name : '-' }}</td>

    {{-- To Store --}}
    <td style="max-width: 150px;">{{ $transfer->to_store->name }}</td>

    {{-- Manager --}}
    <td class="nowrap">{{ $transfer->user->first_name }} {{ $transfer->user->last_name }}</td>

    {{-- Date --}}
    <td class="nowrap">{{ $transfer->created_at->format('d.m.Y - H:i') }}</td>

    {{-- Product Name, SKU --}}
    <td>{{ $transfer->product->name }}, {{ $transfer->product->sku }}</td>

    {{-- Size --}}
    <td class="nowrap">
        {{ $transfer->product_size->size->values->where('is_default', true)->first()->value }}
        {{ $transfer->product_size->size->values->where('is_default', true)->first()->title }}
    </td>

    {{-- Quantity --}}
    <td>
        <strong>{{ $transfer->quantity }} шт.</strong>
    </td>

    {{-- Price --}}
    <td width="1" class="nowrap">
        @if ($transfer->price)
            <strong>@include('_order.amount', ['amount' => $transfer->price])</strong>
        @else
            -
        @endif
    </td>
</tr>