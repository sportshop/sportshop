<table class="order-additional">

    {{-- Date --}}
    <tr>
        <td class="label">@lang('common.datetime'):</td>
        <td><strong>{{ $order->created_at->format('d.m.Y - H:i') }}</strong></td>
    </tr>

    {{-- Customer --}}
    <tr>
        <td class="label">@lang('orders.customer'):</td>
        <td>@include('_order.user', ['data' => $order->customer])</td>
    </tr>

    {{-- Manager --}}
    @if ($order->manager)
        <tr>
            <td class="label">@lang('orders.manager'):</td>
            <td>@include('_order.user', ['data' => $order->manager])</td>
        </tr>
    @endif

    {{-- Status --}}
    <tr>
        <td class="label">@lang('common.status'):</td>
        <td><strong>{{ trans('orders.status.' . $order->status) }}</strong></td>
    </tr>

    {{-- Payment information --}}
    <tr>
        <td class="label">@lang('orders.payment'):</td>
        <td>
            @include('_order.payment', ['data' => $order['payment']])
        </td>
    </tr>

    {{-- Delivery information --}}
    @if ($order['delivery'])
        <tr>
            <td class="label">@lang('orders.delivery'):</td>
            <td>
                @include('_order.delivery', ['data' => $order['delivery']])
            </td>
        </tr>
    @endif

    {{-- Comment --}}
    @if ($total['quantity'])
        <tr>
            <td class="label">@lang('orders.total_quantity'):</td>
            <td><strong>{{ $total['quantity'] }}</strong></td>
        </tr>
    @endif
</table>

@include('_order.products', ['products' => $order->products])
