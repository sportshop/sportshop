<!DOCTYPE html>
<html lang="uk">
<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>

    @include('pdf._styles')
</head>
<body>
    <h1>
        @yield('title')
    </h1>

    @yield('content')
</body>
</html>