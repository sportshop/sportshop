<?php

$this->patterns([
    'banner_id'     => '\d+',
    'category_slug' => '[a-zа-я0-9-]+',
    'slug'          => '[a-zа-я0-9-]+',
    'size_id'       => '\d+',
    'attribute_id'  => '\d+',
    'category_id'   => '\d+',
    'media_id'      => '\d+',
    'page_id'       => '\d+',
    'cost_id'       => '\d+',
    'order_id'      => '\d+',
    'page_id'       => '\d+',
    'page_slug'     => '[a-zа-я]+[a-zа-я0-9-]+',
    'payment_id'    => '\d+',
    'product_id'    => '\d+',
    'product_slug'  => '[a-zа-я0-9-]+',
    'store_id'      => '\d+',
    'user_id'       => '\d+',
]);
