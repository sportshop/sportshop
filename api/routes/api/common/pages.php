<?php

Route::group([
    'namespace'  => 'Common'
], function () {

    Route::get('pages/{page_slug}', 'PageController@getPageBySlug');
});

Route::group([
    'namespace'  => 'Common',
    'middleware' => ['auth:api', 'perm:pages.manage']
], function () {

    Route::get('pages', 'PageController@index');
    Route::get('pages/{page_id}', 'PageController@getPageById');
    Route::post('pages', 'PageController@store');
    Route::put('pages/{page_id}', 'PageController@update');
    Route::delete('pages/{page_id}', 'PageController@destroy');
});
