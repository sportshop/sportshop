<?php

Route::group([
    'namespace'  => 'Common'
], function () {
    Route::get('banners', 'BannerController@findAll');
});

Route::group([
    'namespace'  => 'Common',
    'middleware' => ['auth:api', 'perm:banners.manage']
], function () {
    Route::get('banners/{banner_id}', 'BannerController@findById');
    Route::post('banners', 'BannerController@store');
    Route::put('banners/{banner_id}', 'BannerController@update');
    Route::delete('banners/{banner_id}', 'BannerController@destroy');
    Route::post('banners/upload-media', 'BannerController@upload');
});
