<?php

Route::group([
    'namespace'  => 'Common',
    'middleware' => ['auth:api', 'perm:settings.manage']
], function () {

    Route::get('settings', 'SettingsController@index');
    Route::put('settings', 'SettingsController@update');
});
