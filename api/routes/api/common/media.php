<?php

Route::group([
    'namespace'  => 'Common',
    'middleware' => ['auth:api', 'perm:media.manage']
], function () {

    Route::delete('media/{media_id}', 'MediaController@destroy');
});
