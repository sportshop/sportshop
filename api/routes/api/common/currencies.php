<?php

Route::group([
    'namespace' => 'Common'
], function () {

    Route::get('currencies', 'CurrencyController@index');
});

Route::group([
    'namespace'  => 'Common',
    'middleware' => ['auth:api', 'perm:currencies.manage']
], function () {

    Route::post('currencies', 'CurrencyController@save');
});
