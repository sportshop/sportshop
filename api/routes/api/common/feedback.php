<?php

Route::group([
    'namespace'  => 'Common'
], function () {

    Route::post('feedback', 'FeedbackController@store');
});
