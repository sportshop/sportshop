<?php

Route::group([
    'namespace'  => 'Store'
], function () {

    Route::post('products/{product_id}/feedback', 'ProductFeedbackController@send');
});
