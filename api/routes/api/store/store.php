<?php

Route::group([
    'namespace'  => 'Store',
    'middleware' => ['auth:api', 'perm:stores.manage']
], function () {

    Route::get('stores', 'StoreController@index');
    Route::get('stores/{store_id}', 'StoreController@show');
    Route::post('stores', 'StoreController@store');
    Route::put('stores/{store_id}', 'StoreController@update');
    Route::delete('stores/{store_id}', 'StoreController@destroy');
    Route::post('stores/upload-logo', 'StoreController@uploadLogo');
    Route::post('stores/upload-favicon', 'StoreController@uploadFavicon');
});
