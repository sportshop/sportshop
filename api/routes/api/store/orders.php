<?php

Route::group(['namespace' => 'Store'], function () {

    Route::post('orders', 'OrderController@create');
});


Route::group([
    'namespace'  => 'Store',
    'middleware' => ['jwt.auth']
], function () {

    Route::get('orders', 'OrderController@findAll');
    Route::get('orders/{order_id}', 'OrderController@findById');
    Route::post('orders/download', 'OrderController@downloadAll');
    Route::post('orders/{order_id}/download', 'OrderController@downloadById');
});


Route::group([
    'namespace'  => 'Store',
    'middleware' => ['jwt.auth', 'perm:orders.manage']
], function () {

    Route::get('orders/count', 'OrderController@getCount');
    Route::put('orders/{order_id}', 'OrderController@update');
    Route::post('orders/{order_id}/accept', 'OrderController@accept');
    Route::post('orders/{order_id}/send', 'OrderController@send');
    Route::post('orders/{order_id}/complete', 'OrderController@complete');
    Route::post('orders/{order_id}/refund', 'OrderController@refund');
    Route::post('orders/{order_id}/cancel', 'OrderController@cancel');
    Route::post('orders/withdrawn-payments', 'OrderController@withdrawnPayments');
    Route::delete('orders/{order_id}/products/{product_id}', 'OrderController@destroyProducts');
    Route::get('orders/statistic', 'OrderController@getStatistic');
    Route::get('orders/not-completed', 'OrderController@getNotCompletedOrders');
});
