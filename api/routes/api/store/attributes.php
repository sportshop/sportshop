<?php

Route::group([
    'namespace'  => 'Store',
    'middleware' => ['auth:api', 'perm:attributes.manage']
], function () {

    Route::get('attributes', 'AttributeController@findAll');
    Route::get('attributes/{attribute_id}', 'AttributeController@findById');
    Route::post('attributes', 'AttributeController@store');
    Route::put('attributes/{attribute_id}', 'AttributeController@update');
    Route::delete('attributes/{attribute_id}', 'AttributeController@destroy');
});
