<?php

Route::group([
    'namespace'  => 'Store'
], function () {

    Route::get('sizes', 'SizeController@index');
});

Route::group([
    'namespace'  => 'Store',
    'middleware' => ['auth:api', 'perm:sizes.manage']
], function () {

    Route::get('sizes/{size_id}', 'SizeController@show');
    Route::post('sizes', 'SizeController@store')->middleware(['auth:api', 'perm:sizes.manage']);
    Route::put('sizes/{size_id}', 'SizeController@update')->middleware(['auth:api', 'perm:sizes.manage']);
    Route::delete('sizes/{size_id}', 'SizeController@destroy')->middleware(['auth:api', 'perm:sizes.manage']);
});
