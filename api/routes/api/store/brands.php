<?php

Route::group([
    'namespace'  => 'Store',
    'middleware' => ['auth:api', 'perm:brands.manage']
], function () {

    Route::get('brands', 'BrandController@findAll');
    Route::get('brands/{brand_id}', 'BrandController@findById');
    Route::post('brands', 'BrandController@store');
    Route::put('brands/{brand_id}', 'BrandController@update');
    Route::delete('brands/{brand_id}', 'BrandController@destroy');
    Route::post('brands/upload-media', 'BrandController@uploadMedia');
});
