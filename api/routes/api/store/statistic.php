<?php

Route::group([
    'namespace'  => 'Store',
    'middleware' => ['auth:api', 'perm:products.manage']
], function () {

    Route::get('statistic', 'StatisticController@index');
});
