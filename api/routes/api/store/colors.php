<?php

Route::group([
    'namespace'  => 'Store',
    'middleware' => ['auth:api', 'perm:products.manage']
], function () {

    Route::get('colors', 'ColorController@index');
    Route::post('colors', 'ColorController@save');
});
