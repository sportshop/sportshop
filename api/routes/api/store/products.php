<?php

Route::group([
    'namespace'  => 'Store',
    'middleware' => ['auth:api', 'perm:products.manage']
], function () {

    Route::get('products/{product_id}', 'ProductController@findById');
    Route::post('products', 'ProductController@store');
    Route::put('products/{product_id}', 'ProductController@update');
    Route::put('products/{product_id}/update-type', 'ProductController@updateType');
    Route::put('products/{product_id}/update-visibility', 'ProductController@updateVisibilityStatus');
    Route::delete('products/{product_id}', 'ProductController@destroy');
    Route::post('products/upload-media', 'ProductController@upload');
    Route::get('products/transfers', 'TransferController@index');
    Route::post('products/transfers/download', 'TransferController@downloadPdf');
});

Route::group([
    'namespace'  => 'Store'
], function () {

    Route::get('products', 'ProductController@findAll');
    Route::get('products/{category_slug}/{product_slug}', 'ProductController@findBySlug');
    Route::get('products/filter', 'ProductsFilterController@index');
});
