<?php

Route::group([
    'namespace'  => 'Store'
], function () {

    Route::get('categories', 'CategoryController@findAll');
});

Route::group([
    'namespace'  => 'Store',
    'middleware' => ['auth:api', 'perm:categories.manage']
], function () {

    Route::get('categories/{category_id}', 'CategoryController@findById');
    Route::post('categories', 'CategoryController@store');
    Route::put('categories/{category_id}', 'CategoryController@update');
    Route::delete('categories/{category_id}', 'CategoryController@destroy');
    Route::post('categories/upload-media', 'CategoryController@uploadMedia');
});
