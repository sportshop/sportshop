<?php

Route::group([
    'namespace'  => 'Store',
    'middleware' => ['auth:api', 'perm:costs.manage']
], function () {

    Route::get('costs', 'CostController@findAll');
    Route::get('costs/{cost_id}', 'CostController@findById');
    Route::post('costs', 'CostController@store');
    Route::put('costs/{cost_id}', 'CostController@update');
    Route::delete('costs/{cost_id}', 'CostController@destroy');
    Route::get('costs/statistic', 'CostController@getStatistic');
});
