<?php

require_once 'attributes.php';
require_once 'brands.php';
require_once 'categories.php';
require_once 'colors.php';
require_once 'costs.php';
require_once 'discounts.php';
require_once 'orders.php';
require_once 'product-call.php';
require_once 'product-feedback.php';
require_once 'products.php';
require_once 'sizes.php';
require_once 'statistic.php';
require_once 'store.php';
