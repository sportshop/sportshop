<?php

Route::group([
    'namespace'  => 'Store'
], function () {

    Route::get('discounts', 'DiscountController@index');
    Route::post('discounts', 'DiscountController@save');
});
