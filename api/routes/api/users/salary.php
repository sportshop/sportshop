<?php

Route::group([
    'namespace'  => 'Users',
    'middleware' => ['auth:api', 'perm:users.manage'],
], function () {
    Route::get('users/salary', 'SalaryController@getUsersSalary');
    Route::get('users/{user_id}/salary', 'SalaryController@getUserSalary');
});
