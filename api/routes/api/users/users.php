<?php

Route::group([
    'namespace'  => 'Users',
    'middleware' => ['auth:api', 'perm:users.view|orders.view'],
], function () {
    Route::get('users', 'UserController@findAll');
    Route::get('users/{user_id}', 'UserController@findById');
});


Route::group([
    'namespace'  => 'Users',
    'middleware' => ['auth:api', 'perm:users.manage'],
], function () {
    Route::post('users', 'UserController@store');
    Route::put('users/{user_id}', 'UserController@update');
    Route::delete('users/{user_id}', 'UserController@destroy');
});