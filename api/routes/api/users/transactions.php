<?php

Route::group([
    'prefix'     => 'users/{user_id}',
    'namespace'  => 'Users',
    'middleware' => ['auth:api', 'perm:users.manage'],
], function () {

    Route::get('transactions', 'TransactionController@findAll');
    Route::post('transactions', 'TransactionController@store');
    Route::put('transactions/{transaction_id}', 'TransactionController@update');
    Route::delete('transactions/{transaction_id}', 'TransactionController@destroy');
});
