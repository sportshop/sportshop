<?php

Route::group([
    'namespace'  => 'Users',
    'middleware' => ['auth:api', 'role:admin,manager,limited_manager']
], function () {

    Route::post('start-workday', 'WorkdayController@startWorkday');
    Route::post('finish-workday', 'WorkdayController@finishWorkday');
});
