<?php

Route::group([
    'namespace'  => 'Users',
    'middleware' => ['auth:api', 'perm:users.manage'],
], function () {

    Route::get('roles', 'RoleController@index');
});
