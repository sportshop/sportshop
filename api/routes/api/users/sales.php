<?php

Route::group([
    'prefix'     => 'users/{user_id}',
    'namespace'  => 'Users',
    'middleware' => ['auth:api', 'perm:users.manage'],
], function () {

    Route::get('sales', 'SalesController@index');
});
