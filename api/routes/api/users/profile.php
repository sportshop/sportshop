<?php

Route::group([
    'namespace'  => 'Users',
    'middleware' => ['auth:api']
], function () {

    Route::get('profile', 'ProfileController@getProfile');
    Route::put('profile', 'ProfileController@updateProfile');
    Route::put('profile/update-password', 'ProfileController@updatePassword');
});
