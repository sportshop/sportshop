<?php

Route::group([
    'prefix'    => 'auth',
    'namespace' => 'Auth'
], function () {

    Route::post('register', 'RegistrationController@register');
});
