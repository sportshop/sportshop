<?php

Route::group([
    'prefix'    => 'auth',
    'namespace' => 'Auth'
], function () {

    Route::post('password/send', 'ForgotPasswordController@send');
    Route::post('password/reset', 'ResetPasswordController@reset');
});
