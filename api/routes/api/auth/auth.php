<?php

Route::group([
    'prefix'     => 'auth',
    'namespace'  => 'Auth'
], function () {

    Route::post('login', 'AuthController@login');
    Route::post('login/{token}', 'AuthController@loginByProvider');
});


Route::group([
    'prefix'     => 'auth',
    'namespace'  => 'Auth',
    'middleware' => ['auth:api']
], function () {

    Route::post('refresh', 'AuthController@refreshToken');
    Route::post('logout', 'AuthController@logout');
});
