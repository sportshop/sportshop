# Sportshop
Folder structure and technologies in use:
- `api` - Laravel 5.5, MySQL 5.7
- `dashboard` - Angular 1.5.8
- `web` - Nuxt 1.4 (Vue)

## Table of contents
* [Prerequisites](#prerequisites)
* [Get the source code](#get-the-source-code)
* [Deployment](#deployment)
* [Development](#development)

## Prerequisites
- Node >= 8 (only for development)
- Docker >= 17
- Docker Compose >= 1.2

## Get the Source Code
Clone the repository using the following command:
```
git clone git@bitbucket.org:sportshop/sportshop.git
```

## Deployment
**Create environment files**
```bash
cp api/.env.example api/.env
cp api/laravel-echo-server.example.json api/laravel-echo-server.json
cp dashboard/.env.example dashboard/.env
cp web/.env.example web/.env
```
If you use `docker-compose`, set `DB_PASSWORD`, `MAIL_*` variables only in `api/.env.example`.

**Create docker network and volume**
```bash
docker network create sportshop && \
docker volume create --name=sportshop.db_data
```

**Build dashboard app**
```bash
npm install --no-package-lock --prefix dashboard
npm run build --prefix dashboard
```

**Build web app**
```bash
npm install --no-package-lock --prefix web
npm run build --prefix web
```

**Define database password and start docker services**
```bash
source api/.env && \
export DB_PASSWORD && \
docker-compose up -d
```

**Install PHP dependencies and configure app**
```bash
docker exec sportshop.php_fpm sh -c "composer install && php artisan key:generate && php artisan jwt:secret --force && php artisan storage:link -q"
```

**Run the migrations**
```bash
docker exec sportshop.php_fpm php artisan migrate --force
```

**Run the seeds**
```bash
docker exec sportshop.php_fpm php artisan db:seed
```

Open `http://localhost:3110` (Dashboard)<br>
Open `http://localhost:3120` (API)

## Development
#### Dashboard app
**Install dependencies**
```bash
npm install --no-package-lock --prefix dashboard
```

**Run application**
```bash
npm run dev --prefix dashboard
```

#### Web app
**Install dependencies**
```bash
npm install --no-package-lock --prefix web
```

**Run application**
```bash
npm run dev --prefix web
```