FROM php:7.2-fpm-alpine

LABEL maintainer="Vitaliy Kravchyshyn <vital.kravchyshyn@gmail.com>"

ARG UID=1000
ARG GID=1000

# Set timezone
ENV TZ=Europe/Kiev
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY ./php.ini /usr/local/etc/php/conf.d/php.ini

RUN apk --no-cache add \
        tzdata \
        wget \
        shadow \
        freetype-dev \
        libpng-dev \
        libjpeg-turbo-dev \
        libxml2-dev \
        autoconf \
        g++ \
        imagemagick \
        imagemagick-dev \
        make \
    && docker-php-ext-configure gd \
        --with-gd \
        --with-freetype-dir=/usr/include/ \
        --with-png-dir=/usr/include/ \
        --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd pdo_mysql \
    && pecl install imagick xdebug-2.6.0 \
    && docker-php-ext-enable imagick xdebug \
    && apk del autoconf g++ make

RUN usermod -u $UID www-data && groupmod -g $GID www-data

# Set timezone
ENV TZ=Europe/Kiev
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Install composer
RUN wget https://getcomposer.org/download/1.6.5/composer.phar && \
    mv ./composer.phar /usr/local/bin/composer && \
    chmod +x /usr/local/bin/composer