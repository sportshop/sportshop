FROM php:7.2-alpine

LABEL maintainer="Vitaliy Kravchyshyn <vital.kravchyshyn@gmail.com>"

ARG UID=1000
ARG GID=1000

COPY ./supervisord.conf /etc/supervisord.conf

RUN apk --no-cache --virtual add \
        tzdata \
        shadow \
        wget \
        supervisor \
    && docker-php-ext-install pdo_mysql

RUN usermod -u $UID www-data && groupmod -g $GID www-data

# Set timezone
ENV TZ=Europe/Kiev
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ENTRYPOINT ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]