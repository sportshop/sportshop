FROM php:7.2-alpine

LABEL maintainer="Vitaliy Kravchyshyn <vital.kravchyshyn@gmail.com>"

ARG UID=1000
ARG GID=1000

COPY ./crontab /var/spool/cron/crontabs/www-data

RUN apk --no-cache --virtual add \
        shadow \
        wget \
    && docker-php-ext-install pdo_mysql

RUN usermod -u $UID www-data && groupmod -g $GID www-data

ENTRYPOINT ["/usr/sbin/crond", "-f"]