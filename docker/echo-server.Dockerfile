FROM node:alpine

LABEL maintainer="Vitaliy Kravchyshyn <vital.kravchyshyn@gmail.com>"

WORKDIR /app

RUN npm install -g laravel-echo-server

CMD ["laravel-echo-server", "start", "--force"]