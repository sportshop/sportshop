const path = require('path');
const webpack = require('webpack');
const Dotenv = require('dotenv-webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');


require('dotenv').config();

module.exports = {
    entry: {
        dashboard: [
            path.resolve('src/bootstrap'),
            path.resolve('src/assets/scss/dashboard')
        ]
    },
    output: {
        path: path.resolve('dist/assets'),
        filename: '[name].js'
    },
    resolve: {
        extensions: ['', '.js', '.css', '.scss']
    },
    resolveLoader: {
        root: path.join(__dirname, 'node_modules')
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                include: /\/src/,
                loader: 'babel-loader'
            },
            {
                test: /\.html$/,
                exclude: [
                    path.resolve('src/public/index.html')
                ],
                loader: 'ngtemplate!html'
            },
            {
                test: /\.css$/,
                loader: 'style!css!postcss!resolve-url'
            },
            {
                test: /src\/assets\/scss\/dashboard\.scss/,
                loader: 'style!css!postcss!resolve-url!sass?sourceMap'
            },
            {
                test: /\.(jpg|png|svg|ico)$/,
                loader: 'file?name=../assets/images/[name].[ext]'
            },
            {
                test: /\.(ttf|eot|svg|woff|woff2)/,
                loader: 'file?name=../assets/fonts/[name].[ext]'
            }
        ]
    },
    babel: {
        presets: ['es2015', 'stage-3'],
        plugins: ['transform-runtime'],
        cacheDirectory: true
    },
    plugins: [
        new ExtractTextPlugin('../css/[name].css'),
        new Dotenv({
            path: './.env',
            safe: true
        }),
        new webpack.ProvidePlugin({
            _: 'underscore',
            moment: 'moment',
            deferredBootstrapper: 'angular-deferred-bootstrap',
            $: 'jquery',
            jQuery: 'jquery',
            'window.$': 'jquery',
            'window.jQuery': 'jquery'
        }),
        new CopyWebpackPlugin([
            {
                from: path.resolve('src/assets/fonts'),
                to:   path.resolve('dist/assets/fonts')
            },
            {
                from: path.resolve('src/assets/images'),
                to:   path.resolve('dist/assets/images')
            },
            {
                from: path.resolve('src/languages'),
                to:   path.resolve('dist/languages')
            },
            {
                from: path.resolve('src/public'),
                to:   path.resolve('dist')
            }
        ]),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production')
            }
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'src/public/index.html',
            favicon:  process.env.FAVICON,
            theme:  process.env.THEME_NAME
        })
    ]
};