const merge = require('webpack-merge');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const historyFallback = require('connect-history-api-fallback');

var commonConfig = require('./webpack.common.js');

module.exports = merge(commonConfig, {
    watch: true,
    devtool: 'source-map',
    plugins: [
        new BrowserSyncPlugin({
            server: {
                baseDir: "dist",
                index: "index.html",
                middleware: [
                    historyFallback()
                ]
            }
        })
    ]
});