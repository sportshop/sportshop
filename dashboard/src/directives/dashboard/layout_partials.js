import navbarTemplate from './navbar.html';
import sidebarTemplate from './sidebar.html';

angular
    .module('app.layout_partials', [])
    .directive('appNavbar', appNavbar)
    .directive('appSidebar', appSidebar);

function appNavbar() {
    var directive = {
        restrict: 'EA',
        replace: true,
        templateUrl: navbarTemplate,
        controller: ['$scope', '$rootScope', '$state', '$stateParams', 'HttpService', 'Notify', navbarCtrl]
    };

    return directive;

    function navbarCtrl($scope, $rootScope, $state, $stateParams, HttpService, Notify)
    {
        $rootScope.isCollapseSidebar = false;

        var active_store = localStorage.getItem('active_store');
        var allStores = localStorage.getItem('all_stores');
        if (active_store) {
            $rootScope.active_store_id = active_store;
        }

        if ($rootScope.currentUser && $rootScope.currentUser.user && ($rootScope.currentUser.user.role_name == 'admin' || $rootScope.currentUser.user.role_name == 'manager' || $rootScope.currentUser.user.role_name == 'limited_manager')) {
            $scope.admin = true;

            if (!$rootScope.globalStores) {
                HttpService.get('/stores')
                    .success(function(response) {
                        $rootScope.storesList = response || [];

                        // Set stores
                        $rootScope.globalStores = JSON.parse(JSON.stringify(response || []));

                        $rootScope.active_store = null;

                        var active_store = $rootScope.active_store_id;

                        if (!allStores && !$rootScope.active_store_id && $rootScope.storesList && $rootScope.storesList.length) {
                            $rootScope.active_store_id = active_store = $rootScope.storesList[0].id;
                            localStorage.setItem('active_store', $rootScope.active_store_id);
                            $state.go($state.current, {}, { reload: true });
                        }

                        if (active_store) {
                            var tempStore = _.find($rootScope.storesList, function(item) {
                                if (+active_store == item.id) {
                                    return item;
                                }
                            });
                            $rootScope.active_store = tempStore;
                        } else {

                        }
                    });
            } else {
                $rootScope.storesList = JSON.parse(JSON.stringify($rootScope.globalStores || []));

                $rootScope.active_store = null;

                var active_store = $rootScope.active_store_id;

                if (!allStores && !$rootScope.active_store_id && $rootScope.storesList && $rootScope.storesList.length) {
                    $rootScope.active_store_id = active_store = $rootScope.storesList[0].id;
                    localStorage.setItem('active_store', $rootScope.active_store_id);
                    $state.go($state.current, {}, { reload: true });
                }

                if (active_store) {
                    var tempStore = _.find($rootScope.storesList, function(item) {
                        if (+active_store == item.id) {
                            return item;
                        }
                    });
                    $rootScope.active_store = tempStore;
                } else {

                }
            }

        } else {
            $scope.superadmin = true;
        }

        $scope.changeStore = function(item) {
            localStorage.removeItem('active_store');

            if (item) {
                $rootScope.active_store = item;
                $rootScope.active_store_id = item.id;
                localStorage.setItem('active_store', $rootScope.active_store.id);
                localStorage.removeItem('all_stores');
            } else {
                $rootScope.active_store = null;
                $rootScope.active_store_id = null;
                localStorage.setItem('active_store', '');
                localStorage.setItem('all_stores', true);
            }
           
            $state.go($state.current, {}, { reload: true });
        };

        $scope.collapseSidebar = collapseSidebarFn;
        $scope.startTime = startTimeFn;
        $scope.endTime = endTimeFn;
        $scope.continueTime = continueTimeFn;

        function startTimeFn() {
            HttpService.post('/start-workday')
                .success(function(resp) {
                    $rootScope.currentUser.user.workday_status = 'started';
                    // Notify.success(resp.message);
                })
                .error(function(errors) {
                    // Notify.warning(errors.message);
                });
        }

        function endTimeFn() {
            HttpService.post('/finish-workday')
                .success(function(resp) {
                    $rootScope.currentUser.user.workday_status = 'finished';
                    // Notify.success(resp.message);
                })
                .error(function(errors) {
                    // Notify.warning(errors.message);
                });
        }

        function continueTimeFn() {
            //TODO: uncomment and set URL
            HttpService.post('/start-workday')
                .success(function(resp) {
                    $rootScope.currentUser.user.workday_status = 'started';
                    // Notify.success(resp.message);
                })
                .error(function(errors) {
                    // Notify.warning(errors.message);
                });
        }

        function collapseSidebarFn() {
            $rootScope.isCollapseSidebar = !$rootScope.isCollapseSidebar;
            var sidebar = document.getElementsByClassName('main-sidebar')[0];
            sidebar.classList.toggle('collapse');
        }
    }
}

function appSidebar() {
    var directive = {
        restrict: 'EA',
        replace: true,
        templateUrl: sidebarTemplate,
        controller: ['$scope', '$rootScope', '$state', 'ProfileService', sidebarCtrl]
    };

    return directive;

    function sidebarCtrl($scope, $rootScope, $state, ProfileService) {
        // var $sidebar = angular.element(element);
        // $sidebar.sidebar($sidebar);

        // All menu items for dashboard
        var menuList = [
            {
                state: 'home.main',
                state_active: 'home.*',
                name: 'home.title.main',
                icon: 'fa fa-home'
            },
            {
                state: 'orders.list',
                state_active: 'orders.*',
                name: 'menu.orders',
                icon: 'fa fa-shopping-cart',
                perm: 'ORDERS.VIEW'
            },
            {
                state: 'products.list',
                state_active: 'products.*',
                name: 'menu.products',
                icon: 'fa fa-th-list',
                perm: 'PRODUCTS.VIEW'
            },
            {
                state: 'categories.list',
                state_active: 'categories.*',
                name: 'menu.categories',
                icon: 'fa fa-bars',
                perm: 'CATEGORIES.VIEW'
            },
            {
                state: 'pages.list',
                state_active: 'pages.*',
                name: 'menu.pages',
                icon: 'fa fa-book',
                perm: 'PAGES.VIEW'
            },
            {
                state: 'slides.list',
                state_active: 'slides.*',
                name: 'menu.slides',
                icon: 'fa fa-book',
                perm: 'CATEGORIES.VIEW'
            },
            {
                state: 'users.list',
                state_active: 'users.*',
                name: 'menu.users',
                icon: 'fa fa-users',
                perm: 'USERS.VIEW'
            },
            {
                state: 'tables.list',
                state_active: 'tables.*',
                name: 'menu.tables',
                icon: 'fa fa-table',
                perm: 'SIZES.VIEW'
            },
            {
                state: 'attributes.list',
                state_active: 'attributes.*',
                name: 'menu.attributes',
                icon: 'fa fa-table',
                perm: 'SIZES.VIEW'
            },
            {
                state: 'brands.list',
                state_active: 'brands.*',
                name: 'menu.brands',
                icon: 'fa fa-table',
                perm: 'SIZES.VIEW'
            },
            {
                state: 'stores.list',
                state_active: 'stores.*',
                name: 'menu.stores',
                icon: 'fa fa-building',
                perm: 'STORES.VIEW'
            },
            {
                state: 'costs.list',
                state_active: 'costs.*',
                name: 'menu.costs',
                icon: 'fa fa-money',
                perm: 'COSTS.VIEW'
            },
            {
                state: 'salaries.list',
                state_active: 'salaries.*',
                name: 'menu.salaries',
                icon: 'fa fa-dollar',
                perm: 'WAGE.VIEW'
            },
            {
                state: 'settings.edit',
                state_active: 'settings.*',
                name: 'menu.settings',
                icon: 'fa fa-cogs',
                perm: 'SETTINGS.VIEW'
            },
            {
                state: 'statistics.costs',
                state_active: 'statistics.*',
                name: 'menu.statistics',
                icon: 'fa fa-cog',
                perm: 'STATISTIC.COSTS.VIEW'
            }
        ];

        initNav();

        function initNav() {
            // Sidebar menu
            var new_menu = [];

            if ($rootScope.currentUser && $rootScope.currentUser.user && $rootScope.currentUser.user.permissions) {
                $rootScope.perms = {};
                $rootScope.currentUser.user.permissions.forEach(function (elem) {
                    $rootScope.perms[elem] = true;
                });

                // if ($rootScope.currentUser && $rootScope.currentUser.permissions) {
                if ($rootScope.currentUser && $rootScope.currentUser.user) {
                    if ($rootScope.currentUser.user.permissions) {
                        $rootScope.currentUser.permissions = $rootScope.currentUser.user.permissions;
                    }
                    // User permissions
                    var perms = $rootScope.currentUser.permissions;

                    // Mapping permissions for new menu
                    _.map(menuList, function (item) {

                        _.each(perms, function(perm) {
                            if (item.perm == perm.toUpperCase() ) {
                                new_menu.push(item);
                            }
                        });

                    });

                    // User menu list
                    $scope.menu = new_menu;
                }
            } else {
                ProfileService.get()
                    .success(function(resp) {
                        if (resp && resp.user && resp.user.permissions) {
                            // Grab the user from local storage and parse it to an object
                            var user = JSON.parse(localStorage.getItem('user'));

                            // Putting the user's data on $rootScope allows
                            // us to access it anywhere across the app. Here
                            // we are grabbing what is in local storage
                            $rootScope.currentUser = user;
                            if ($rootScope.currentUser.user && $rootScope.currentUser.user.permissions) {
                                $rootScope.currentUser.user.permissions = resp.user.permissions;
                                $rootScope.perms = {};
                                $rootScope.currentUser.user.permissions.forEach(function (elem) {
                                    $rootScope.perms[elem] = true;
                                });

                                // if ($rootScope.currentUser && $rootScope.currentUser.permissions) {
                                if ($rootScope.currentUser && $rootScope.currentUser.user) {
                                    if ($rootScope.currentUser.user.permissions) {
                                        $rootScope.currentUser.permissions = $rootScope.currentUser.user.permissions;
                                    }
                                    // User permissions
                                    var perms = $rootScope.currentUser.permissions;

                                    // Mapping permissions for new menu
                                    _.map(menuList, function (item) {

                                        _.each(perms, function(perm) {
                                            if (item.perm == perm.toUpperCase() ) {
                                                new_menu.push(item);
                                            }
                                        });

                                    });

                                    // User menu list
                                    $scope.menu = new_menu;
                                }
                            }

                            localStorage.setItem('user', JSON.stringify($rootScope.currentUser));

                        }
                    });
            }
        }

        $rootScope.$on('nav:init', function() {
            initNav();
        });
    }
}