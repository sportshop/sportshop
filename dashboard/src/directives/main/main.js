import breadcrumbsView from '../partials/breadcrumbs.html';
import placeholderView from '../partials/placeholder.html';
import mainHeaderView from '../partials/main_header.html';
import currencyIconView from './currency_icon.html';
import currencyTitleView from './currency_title.html';
import sizeDropdownButton from './sizes-dropdown-button.html';
import sizeDropdownButtonInDialog from './sizes-dropdown-button-in-dialog.html';
import thumbnailView from './thumbnail.html';
import cropImageView from './crop-image.html';

angular
    .module("app.directives_main", [])

    .directive('breadcrumbs', breadcrumbs)
    .directive('placeholderNoItems', placeholderNoItems)
    .directive('imageonload', imageonload)
    .directive('mainHeader', mainHeader)
    .directive('icheck', icheck)
    .directive('sizesDropdownButtton', sizesDropdownButtton)
    .directive('sizesDropdownButttonInDialog', sizesDropdownButttonInDialog)
    .directive('currencyIcon', currencyIcon)
    .directive('thumbnail', thumbnail)
    .directive('currencyTitle', currencyTitle)
    .directive('format', format);


    /**
     * Limit to show currency input
     *
     * @type {string[]}
     */
    format.$inject = [];
    function format() {
        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ctrl) {
                if (!ctrl) return;

                if (attrs.format === 'numberLimit') {
                    ctrl.$formatters.unshift(function (a) {
                        if (ctrl.$modelValue) {
                            return (+ctrl.$modelValue).toFixed(2);
                        } else {
                            return '';
                        }
                    });

                    elem.bind('change', function(event) {
                        if (ctrl.$modelValue) {
                            elem.val((+ctrl.$modelValue).toFixed(2));
                        } else {
                            elem.val('');
                        }
                    });
                }

                if (attrs.format === 'numberInteger') {
                    ctrl.$formatters.unshift(function (a) {
                        if (ctrl.$modelValue) {
                            return (+ctrl.$modelValue).toFixed(0);
                        } else {
                            return '';
                        }
                    });

                    elem.bind('change', function(event) {
                        if (ctrl.$modelValue) {
                            elem.val((+ctrl.$modelValue).toFixed(0));
                        } else {
                            elem.val('');
                        }
                    });
                }
            }
        };
    }

    /**
     * Bread crumbs
     * @returns {*[]}
     */
    function breadcrumbs() {
        var directive = {
            restrict: 'EA',
            replace: true,
            scope: {
                items: '='
            },
            templateUrl: breadcrumbsView,
            controller: ['$scope', '$state', breadcrumbsCtrl]
        };

        return directive;

        function breadcrumbsCtrl($scope, $state) {}
    }

    /**
     * Stub for no items
     * @returns {*[]}
     */
    function placeholderNoItems() {
        var directive = {
            restrict: 'E',
            replace: true,
            templateUrl: placeholderView,
            scope: {
                label: '@',
                btnurl: '@',
                btnname: '@',
                sizeicon: '=?'
            },
            controller: ['$scope', '$state', placeholderCtrl]
        };

        return directive;

        function placeholderCtrl($scope, $state) {}
    }

    /**
     * Load image
     * @type {string[]}
     */
    imageonload.$inject = ['$timeout'];
    function imageonload($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.addClass('s-image--loading');

                var img = new Image();

                img.onload = function() {
                    element.addClass('s-image--loading_success');
                    element.removeClass('s-image--loading');
                };

                img.onerror = function() {
                    element.addClass('s-image--loading_success');
                    element.removeClass('s-image--loading');
                }
            }
        };
    }

    /**
     * Show currency icon
     * @returns {*[]}
     */
    function currencyIcon() {
        var directive = {
            restrict: 'EA',
            replace: true,
            scope: {
                item: '='
            },
            templateUrl: currencyIconView,
            controller: ['$rootScope', '$scope', '$state', currencyIconCtrl]
        };

        return directive;

        function currencyIconCtrl($rootScope, $scope, $state) {
            $scope.showSymbol = showSymbolFn;

            function showSymbolFn(item) {
                $scope.currency  = _.find($rootScope.staticCurrencies, function(el) {
                    if (el.id == item) {
                        return el;
                    }
                });

                if ($scope.currency && $scope.currency.symbol) {
                    return $scope.currency.symbol;
                }

                return ' '
            }
        }
    }

    /**
     * Show currency name
     * @returns {*[]}
     */
    currencyTitle.$inject = ['$rootScope'];
    function currencyTitle($rootScope) {
        var directive = {
            restrict: 'EA',
            replace: true,
            scope: {
                item: '='
            },
            templateUrl: currencyTitleView,
            controller: ['$scope', '$state', '$rootScope', currencyTitleCtrl]
        };

        return directive;

        function currencyTitleCtrl($scope, $state, $rootScope) {
            $scope.showSymbol = showSymbolFn;

            function showSymbolFn(item) {
                $scope.currency  = _.find($rootScope.staticCurrencies, function(el) {
                    if (el.id == item) {
                        return el;
                    }
                });

                if ($scope.currency && $scope.currency.symbol) {
                    return $scope.currency.symbol;
                }

                return ' '
            }
        }
    }

    /**
     * Dropdown button for sizes list
     * @type {Array}
     */
    sizesDropdownButtton.$inject = [];
    function sizesDropdownButtton() {
        var directive = {
            restrict: 'EA',
            replace: true,
            scope: {
                elem: '=',
                group: '='
            },
            templateUrl: sizeDropdownButton,
            controller: ['$scope', '$rootScope', sizeDropdownButtonCtrl]
        };

        return directive;

        function sizeDropdownButtonCtrl($scope, $rootScope) {}
    }

/**
 * Dropdown button for sizes list in Add product dialog
 * @type {Array}
 */
sizesDropdownButttonInDialog.$inject = [];
function sizesDropdownButttonInDialog() {
    var directive = {
        restrict: 'EA',
        replace: true,
        scope: {
            elem: '=',
            group: '='
        },
        templateUrl: sizeDropdownButtonInDialog,
        controller: ['$scope', '$rootScope', sizeDropdownButtonInDialogCtrl]
    };

    return directive;

    function sizeDropdownButtonInDialogCtrl($scope, $rootScope) {}
}

    /**
     * Show header
     * @returns {*[]}
     */
    function mainHeader() {
        var directive = {
            restrict: 'EA',
            replace: true,
            scope: {
                item: '='
            },
            templateUrl: mainHeaderView,
            controller: ['$scope', '$state', mainHeaderCtrl]
        };

        return directive;

        function mainHeaderCtrl($scope, $state) {}
    }

    /**
     * Thumbnail for images
     * @returns {*[]}
     */
    function thumbnail() {
        var directive = {
            restrict: 'EA',
            replace: true,
            scope: {
                media: '=',
                name: '=',
                temp: '=',
                width: '=',
                url: '=',
                errors: '=',
                id: '=',
                aspectratio: '=',
                isrequired: '@',
                areaminrelativesize: '='
            },
            templateUrl: thumbnailView,
            controller: ['$scope', '$state', '$timeout', 'MyFileUploader', 'HttpService', 'ngDialog', 'CONFIG', thumbnailCtrl]
        };

        return directive;

        function thumbnailCtrl($scope, $state, $timeout, MyFileUploader, HttpService, ngDialog, CONFIG) {
            // if ($scope.media && $scope.media.url) {
            //     $scope.media.url = $scope.media.url.replace("https", "http");
            // }

            // Additional form data for file
            var formData;

            var onCompleteItem;
            // Set image
            onCompleteItem = function(fileItem, response, status, headers) {
                if (status == 200) {
                    $scope.media = response;

                    // if ($scope.media && $scope.media.url) {
                    //     $scope.media.url = $scope.media.url.replace("https", "http");
                    // }

                    $scope.id = response.id;
                }
            };

            $scope.uploadUrl = CONFIG.host + $scope.url;

            $scope.defaultImage = $scope.url == '/banners/upload-media' ?
                '/assets/images/dashboard/no-banner-image.png' : '/assets/images/dashboard/no-image.png';
            if ($scope.url == '/stores/upload-logo') {
                $scope.defaultImage = '/assets/images/dashboard/no-banner-image.png';
            }

            // Init MyFileUploader for image
            // var upload =  MyFileUploader.init(true, 1, false, {onCompleteItem: onCompleteItem});
            // Upload image
            $scope.upload = MyFileUploader.init(
                false,
                1,
                false,
                {onCompleteItem: onCompleteItem},
                [formData]
            );

            $scope.upload.onAfterAddingFile = function(item) {
                ngDialog.open({
                    templateUrl: cropImageView,
                    showClose: false,
                    scope: $scope,
                    className: 'dialog-theme-default',
                });
            };

            $scope.uploadImage = uploadImageFn;
            $scope.cropImage = cropImageFn;
            $scope.updateImage = updateImageFn;
            $scope.deleteImage = deleteImageFn;

            /**
             * start
             */
            $scope.myCroppedImage = '';
            $scope.myImage = null;
            $scope.aspectRatio = $scope.aspectratio || 1;

            $scope.myImage = undefined;

            /**
             * Insert image for crop
             *
             * @param evt
             */
            var handleFileSelect = function(evt) {
                var file = evt.currentTarget.files[0];
                var reader = new FileReader();
                reader.onload = function(evt) {
                    $scope.$apply(function($scope) {
                        $scope.myImage = evt.target.result;
                    });
                };
                reader.readAsDataURL(file);

                $scope.upload.clearQueue();
            };
            angular.element(document.querySelector('#file-upload')).on('change', handleFileSelect);

            /**
             * Update image after crop
             */
            function updateImageFn(cropper, myCroppedImage) {
                // Update formData before upload image to server
                $scope.upload.onBeforeUploadItem = function(item) {
                    // item.formData = [croppercropper.areaCoords];
                    item.formData = [{
                        base64: myCroppedImage
                    }];
                };

                // Upload image to server
                $scope.upload.uploadAll();
                ngDialog.closeAll();
            }

            /**
             * Crop image
             */
            function cropImageFn() {

            }

            /**
             * Upload image
             */
            function uploadImageFn() {
                $timeout(function() {
                    var el = '.'+ $scope.temp;
                    var element;
                    if ($scope.temp) {
                        element = angular.element(el);
                    } else {
                        element = angular.element('#file-upload');
                    }

                    element.triggerHandler('click');
                    element.trigger('click');
                });
            }

            /**
             * Remove image
             * @param item
             */
            function deleteImageFn(item) {
                HttpService.delete('/media/'+ item.id)
                    .success(function() {
                        delete $scope.media;
                        delete $scope.id;
                    });
            }
        }
    }

    /**
     * Modify default checkbox
     * @param $timeout
     * @param $parse
     * @returns {{require: string, scope: {}, link: link}}
     */
    function icheck($rootScope, $timeout, $parse) {
        return {
            require: 'ngModel',
            scope: {},
            link: function(scope, element, $attrs, ngModel) {
                return $timeout(function() {
                    var value;
                    value = $attrs['value'];

                    if ($attrs['disabled'])
                        $(element).iCheck($attrs['disabled'] ? 'disable' : 'enable');

                    scope.$watch(function() { return ngModel.$modelValue; }, function(newValue) {
                        if (newValue != undefined) {
                            $(element).iCheck('update');
                        }
                    });

                    scope.$watch($attrs['ngDisabled'], function(newValue) {
                        if (newValue != undefined) {
                            $(element).iCheck(newValue ? 'disable' : 'enable');
                            $(element).iCheck('update');
                        }
                    });

                    return $(element).iCheck({
                        checkboxClass: $attrs['checkboxClass'] ? $attrs['checkboxClass'] : 'icheckbox_square-'+ 'blue',//$rootScope.themeName,
                        radioClass: $attrs['radioClass'] ? $attrs['radioClass'] : 'iradio_square-'+ 'blue',//$rootScope.themeName

                    }).on('ifChanged', function(event) {
                        if ($(element).attr('type') === 'checkbox') {
                            ngModel.$setViewValue(event.target.checked);
                        }
                    }).on('ifClicked', function(event) {
                        if ($(element).attr('type') === 'radio') {
                            //set up for radio buttons to be de-selectable
                            if (ngModel.$viewValue != value) {
                                $(element).iCheck('check');
                                ngModel.$setViewValue(value);
                            }

                            ngModel.$render();
                        }
                    });
                });
            }
        };
    }