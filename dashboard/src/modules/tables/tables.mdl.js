import layoutTemplate from "../layouts/dashboard.html";
import listTemplate   from "./list.html";
import formTemplate   from "./form.html";

angular
    .module('app.tables', [])
    .config(configure);


configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('tables', {
            abstract: true,
            url: '/tables',
            views: {
                '': {
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('tables.list', {
            url: '',
            views: {
                'content': {
                    controller: 'TablesController.list as ctrl',
                    templateUrl: listTemplate
                }
            },
            resolve: {
                items: [
                    '$rootScope', 'HttpService',
                    function ($rootScope, HttpService) {
                        if (!$rootScope.globalSizes) {
                            return HttpService.get('/sizes');
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalSizes));
                    }
                ]
            }
        })
        .state('tables.create', {
            url: '/create',
            views: {
                'content': {
                    controller: 'TablesController.create as ctrl',
                    templateUrl: formTemplate
                }
            },
            resolve: {
                item: [
                    function () {
                        return {}
                    }
                ],
                brands : [
                    '$rootScope', 'BrandsService',
                    function($rootScope, BrandsService) {
                        if (!$rootScope.globalBrands) {
                            return BrandsService.getList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalBrands));
                    }
                ]
            }
        })
        .state('tables.edit', {
            url: '/:id',
            views: {
                'content': {
                    controller: 'TablesController.edit as ctrl',
                    templateUrl: formTemplate
                }
            },
            resolve: {
                item: [
                    '$stateParams', 'TablesService',
                    function ($stateParams, TablesService) {
                        return TablesService.get($stateParams.id);
                    }
                ],
                brands : [
                    '$rootScope', 'BrandsService',
                    function($rootScope, BrandsService) {
                        if (!$rootScope.globalBrands) {
                            return BrandsService.getList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalBrands));
                    }
                ]
            }
        });
}