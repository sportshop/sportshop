angular
    .module('app.tables')
    .service('TablesService', TablesService);


TablesService.$inject = ['HttpService'];
function TablesService(HttpService) {
    var baseUrl = '/sizes';

    var types = [
        {key: 'shoes', value: 'Взуття'},
        {key: 'clothes', value: 'Одяг'}
    ];

    return {
        getList:  getListFn,
        getTypes: getTypesFn,
        get:      getFn,
        save:     saveFn,
        update:   updateFn,
        delete:   deleteFn,
    };

    /**
     * Get table list
     *
     * @returns {*}
     */
    function getListFn() {
        return HttpService.get(baseUrl);
    }

    /**
     * Get table types
     *
     * @returns {*}
     */
    function getTypesFn() {
        return types;
    }

    /**
     * Get table size
     *
     * @returns {*}
     */
    function getFn(id) {
        return HttpService.get(baseUrl +'/'+ id);
    }

    /**
     * Save table size
     *
     * @param data
     *
     * @returns {*}
     */
    function saveFn(data) {
        return HttpService.post(baseUrl, data)
    }

    /**
     * Update table size
     *
     * @param id
     * @param data
     *
     * @returns {*}
     */
    function updateFn(id, data) {
        return HttpService.put(baseUrl +'/'+ id, data)
    }

    /**
     * Delete table size
     *
     * @param id
     *
     * @returns {*}
     */
    function deleteFn(id) {
        return HttpService.delete(baseUrl +'/'+ id)
    }
}