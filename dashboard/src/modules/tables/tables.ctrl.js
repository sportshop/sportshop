angular
    .module('app.tables')
    .controller('TablesController.list', TablesController_List)
    .controller('TablesController.create', TablesController_Create)
    .controller('TablesController.edit', TablesController_Edit);


TablesController_List.$inject = ['$rootScope', '$state', 'BreadcrumbsService', 'MainHeaderService', 'TablesService', 'Notify', 'items'];
function TablesController_List($rootScope, $state, BreadcrumbsService, MainHeaderService, TablesService, Notify, items) {
    BreadcrumbsService.push('tables.title.list');
    MainHeaderService.set('tables.title.list');

    var vm = this;

    // Tables Model
    if (items && items.data) {
        vm.list = items.data || [];
        $rootScope.globalSizes = JSON.parse(JSON.stringify(items.data));
    } else {
        vm.list = items || {};
        $rootScope.globalSizes = items || {};
    }


    vm.remove = removeFn;

    function removeFn(item) {
        Notify.confirm(function () {
            TablesService.delete(item.id)
                .success(function() {
                    $state.transitionTo('tables.list', null, { reload: true });
                    Notify.success('general.removed');
                })
                .error(function(errors) {
                    if (errors.message && errors.message == 'Please, delete the products first') {
                        Notify.warning('Будь ласка, спочатку видаліть продукти');
                    } else {
                        Notify.warning(errors.message);
                    }
                });
        }, 'general.are_you_sure')
    }
}

TablesController_Create.$inject = ['$rootScope', '$state', 'BreadcrumbsService', 'MainHeaderService', 'TablesService', 'Notify', 'brands'];
function TablesController_Create($rootScope, $state, BreadcrumbsService, MainHeaderService, TablesService, Notify, brands) {
    BreadcrumbsService.push('tables.title.list', 'tables.list');
    BreadcrumbsService.push('tables.title.create');
    MainHeaderService.set('tables.title.create');

    // On enter is dvovymirnui masyv
    function TransMatrix(A) {
        var m = A.length, n = A[0].length, AT = [];

        for (var i = 0; i < n; i++) {
            AT[i] = [];

            for (var j = 0; j < m; j++) AT[i][j] = A[j][i];
        }

        return AT;
    }

    var vm = this;

    if (brands && brands.data) {
        vm.brands = brands.data || [];
        $rootScope.globalBrands = JSON.parse(JSON.stringify(brands.data));
    } else {
        vm.brands = brands || [];
    }

    vm.types = TablesService.getTypes();

    // Store Model
    vm.item = {
        sizes: [
            {title: ''}
        ],
        values: [
            [{}]
        ]
    };
    vm.item.sizes.is_default = 0;

    vm.ui = {
        addSize: function() {
            vm.submit = false;
            vm.item.sizes.push({});
            vm.item.values.push(new Array(vm.item.values[0].length));
        },
        addValues: function() {
            vm.submit = false;
            _.each(vm.item.values, function(el, index) {
                el.push({});
            })
        },
        delCols: function(index) {
            Notify.confirm(function () {
                _.each(vm.item.values, function(item) {
                    item.splice(index, 1);
                });
            }, 'general.are_you_sure');
        },
        delRows: function(index) {
            Notify.confirm(function () {
                vm.item.values.splice(index, 1);
                vm.item.sizes.splice(index, 1);
            }, 'general.are_you_sure');
        }
    };

    vm.save = saveFn;

    /**
     * Save table size
     */
    function saveFn() {
        vm.submit = true;
        var newValues = TransMatrix(vm.item.values);

        var tempResult = [];
        var sizeLen = vm.item.sizes.length;
        var valuesLen = newValues.length;

        if (sizeLen && valuesLen) {
            // Add title to values
            for (var i = 0; i < valuesLen; i++) {
                tempResult[i] = _.map(newValues[i], function(item, index) {
                    if (index < sizeLen) {
                        return {
                            title: vm.item.sizes[index].title,
                            value: item.value,
                            is_default: (vm.item.sizes.is_default == index) ? true : false
                        }
                    }
                });
            }

            // Modified values
            for (var j = 0; j < tempResult.length; j++) {
                tempResult[j] = {
                    values: tempResult[j]
                }
            }
        }

        var data = {
            name: vm.item.name,
            type: vm.item.type,
            sizes: tempResult
        };

        if (vm.item.brand_id) {
            data.brand_id = vm.item.brand_id;
        }

        TablesService.save(data)
            .success(function() {
                Notify.success('general.saved');
                $state.go('tables.list');
            })
            .error(function(errors) {
                vm.generalErrors = errors.errors;
            });
    }
}

TablesController_Edit.$inject = ['$state', '$stateParams','BreadcrumbsService', 'MainHeaderService', 'TablesService', 'Notify', 'item', 'brands'];
function TablesController_Edit($state, $stateParams, BreadcrumbsService, MainHeaderService, TablesService, Notify, item, brands) {
    BreadcrumbsService.push('tables.title.list', 'tables.list');
    BreadcrumbsService.push('tables.title.edit');
    MainHeaderService.set('tables.title.edit');

    // On enter is dvovymirnui masyv
    function TransMatrix(A) {
        var m = A.length, n = A[0].length, AT = [];

        for (var i = 0; i < n; i++) {
            AT[i] = [];

            for (var j = 0; j < m; j++) AT[i][j] = A[j][i];
        }

        return AT;
    }

    var vm = this;

    vm.brands = brands.data || brands;
    vm.types = TablesService.getTypes();

    // Table Model
    var table = item.data || {};
    var is_default = 0;

    // Get size titles
    if (table.sizes && table.sizes[0] && table.sizes[0] && table.sizes[0]['values']) {
        var len = table.sizes[0]['values'].length;

        var tempSizes = _.map(table.sizes[0]['values'], function(item, index) {
            if (item.is_default) {
                is_default = index || 0;
            }
            
            var data = {
                title: item.title,
                is_default: item.is_default ? index : false
            };
            
            if (item.id) {
                data.id = item.id;
            }

            return data;
        });

        // Get values for sizes
        var tempValues = [];
        var newTempValues = [];
        for (var i = 0; i < table.sizes.length; i++) {
                tempValues[i] = _.map(table.sizes[i]['values'], function(item, index) {

                    if (item.is_default) {
                        is_default = index || 0;
                    }

                    if (table.sizes[i] && table.sizes[i].id) {
                        item.value_id = table.sizes[i].id;
                    }

                    return item;
                });
        }

        newTempValues = TransMatrix(tempValues);

        // Set table data
        vm.item = {
                name: table.name,
                type: table.type,
                sizes: tempSizes,
                values: newTempValues
            };
        vm.item.sizes.is_default = is_default;
    } else {
        vm.item = {
            name: '',
            type: null,
            sizes: [{title: '', is_default: 0}],
            values: [[]]
        };
        vm.item.sizes.is_default = 0;
    }

    vm.item.brand_id = (table && table.brand && table.brand.id) ? table.brand.id : null;

    vm.ui = {
        addSize: function() {
            vm.submit = false;
            vm.item.sizes.push({});
            vm.item.values.push(new Array(vm.item.values[0].length));
        },
        addValues: function() {
            vm.submit = false;
            _.each(vm.item.values, function(el, index) {
                el.push({});
            })
        },
        delCols: function(index) {
            Notify.confirm(function () {
                _.each(vm.item.values, function(item) {
                    item.splice(index, 1);
                });
            }, 'general.are_you_sure');
        },
        delRows: function(index) {
            Notify.confirm(function () {
                vm.item.values.splice(index, 1);
                vm.item.sizes.splice(index, 1);
            }, 'general.are_you_sure');
        }
    };

    vm.save = saveFn;

    function saveFn() {
        vm.submit = true;

        var newValues = TransMatrix(vm.item.values);

        var tempResult = [];
        var sizeLen = vm.item.sizes.length;
        var valuesLen = newValues.length;

        if (sizeLen && valuesLen) {
            // Add title to values
            for (var i = 0; i < valuesLen; i++) {
                tempResult[i] = _.map(newValues[i], function(item, index) {
                    if (index < sizeLen) {

                        var data = {
                            title: vm.item.sizes[index].title,
                            value: item.value,
                            is_default: (vm.item.sizes.is_default == index) ? true : false
                        };

                        if (item.id) {
                            data.id = item.id;
                        }

                        if (item.value_id) {
                            data.value_id = item.value_id;
                        }

                        return data;
                    }
                });
            }

            // Modified values
            for (var j = 0; j < tempResult.length; j++) {
                var data = {
                    values: tempResult[j]
                };

                if (tempResult[j] && tempResult[j].length && tempResult[j][0].value_id) {
                    data.id = tempResult[j][0].value_id;
                }

                tempResult[j] = data;
            }
        }

        var data = {
            name: vm.item.name,
            type: vm.item.type,
            sizes: tempResult
        };

        if (vm.item.brand_id) {
            data.brand_id = vm.item.brand_id;
        }

        TablesService.update($stateParams.id, data)
            .success(function() {
                Notify.success('general.updated');
                $state.go('tables.list');
            })
            .error(function(errors) {
                if (errors.code && errors.code == 'products_exists') {
                    Notify.warning('Будь ласка, видаліть продукти, пов\'язані з цим розміром.');
                    $state.reload();
                }

                if (errors.errors) {
                    vm.generalErrors = errors.errors;
                }
            });
    }
}