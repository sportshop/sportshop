angular
    .module('app.banners')
    .controller('SlidesController.list', SlidesController_List)
    .controller('SlidesController.edit', SlidesController_Edit);


SlidesController_List.$inject = ['$state', 'BreadcrumbsService', 'MainHeaderService', 'SlidesService', 'Notify', 'items'];
function SlidesController_List($state, BreadcrumbsService, MainHeaderService, SlidesService, Notify, items) {
    BreadcrumbsService.push('slides.title.list');
    MainHeaderService.set('slides.title.list');

    var vm = this;

    // Banners list
    vm.list = items.data || [];

    vm.remove = removeFn;
    vm.changePage = changePageFn;

    /**
     * Remove banner
     *
     * @param item
     */
    function removeFn(item) {
        Notify.confirm(function () {
            SlidesService.delete(item.id)
                .success(function() {
                    var index = vm.list.indexOf(item);
                    vm.list.splice(index, 1);

                    Notify.success('general.removed');
                })
                .error(function(errors) {
                    Notify.warning(errors.message);
                });
        }, 'general.are_you_sure')
    }

    /**
     * Change page
     */
    function changePageFn() {
        var params = {
            page: vm.list.current_page
        };

        SlidesService.getList(params)
            .success(function(response){
                vm.list = response.data || {};
                $state.go($state.current, params, { notify: false });
            });
    }
}

SlidesController_Edit.$inject = ['$rootScope', '$state', '$stateParams', 'BreadcrumbsService', 'MainHeaderService', 'SlidesService', 'CategoriesService', 'Notify', 'item', 'currencies'];
function SlidesController_Edit($rootScope, $state, $stateParams, BreadcrumbsService, MainHeaderService, SlidesService, CategoriesService, Notify, item, currencies) {
    BreadcrumbsService.push('slides.title.list', 'slides.list');

    var vm = this;

    // Banner model
    vm.item = item.data || {};

    // Statuses list
    vm.statuses = CategoriesService.getStatuses();
    // Currencies list
    if (currencies && currencies.data) {
        vm.currencies = currencies.data || [];
        $rootScope.globalCurrencies = JSON.parse(JSON.stringify(currencies.data));
    } else {
        vm.currencies = currencies || [];
    }

    if (!$stateParams.id) {
        BreadcrumbsService.push('slides.title.create');
        MainHeaderService.set('slides.title.create');

        vm.item = {
            is_active: true
        };
    } else {
        if (vm.item.price) {
            var price = _.find(vm.item.price, function(elem) {
                return elem.is_default
            });

            vm.item.price = price;
        } else {
            delete vm.item.price;
        }

        BreadcrumbsService.push('slides.title.edit');
        MainHeaderService.set('slides.title.edit');
    }

    vm.save = saveFn;

    /**
     * Update or create banner
     */
    function saveFn() {
        if (vm.item.image && vm.item.image.id) {
            vm.item.image_id = vm.item.image.id;
        }

        if ($stateParams.id) {
            SlidesService.update(vm.item.id, vm.item)
                .success(function() {
                    Notify.success('general.updated');
                    $state.go('slides.list');
                })
                .error(function(errors) {
                    vm.errors = errors.errors;

                    if (vm.errors && vm.errors.image_id) {
                        vm.errors.media_id = vm.errors.image_id;
                    }
                });
        } else {
            SlidesService.save(vm.item)
                .success(function() {
                    Notify.success('general.saved');
                    $state.go('slides.list');
                })
                .error(function(errors) {
                    vm.errors = errors.errors;

                    if (vm.errors && vm.errors.image_id) {
                        vm.errors.media_id = vm.errors.image_id;
                    }
                });
        }
    }
}