angular
    .module('app.banners')
    .service('SlidesService', SlidesService);


SlidesService.$inject = ['HttpService'];
function SlidesService(HttpService) {
    var baseUrl = '/banners';

    return {
        getList: getListFn,
        get:     getFn,
        save:    saveFn,
        update:  updateFn,
        delete:  deleteFn
    };

    /**
     * Get banner list
     *
     * @param currentPage
     *
     * @returns {*}
     */
    function getListFn(params) {
        return HttpService.getWParams(baseUrl, { params: params });
    }

    /**
     * Get banner
     *
     * @param id
     *
     * @returns {*}
     */
    function getFn(id) {
        return HttpService.get(baseUrl +'/'+ id)
    }

    /**
     * Save banner
     *
     * @param data
     *
     * @returns {*}
     */
    function saveFn(data) {
        return HttpService.post(baseUrl, data)
    }

    /**
     * Update banner
     *
     * @param id
     * @param data
     *
     * @returns {*}
     */
    function updateFn(id, data) {
        return HttpService.put(baseUrl +'/'+ id, data)
    }

    /**
     * Remove banner
     *
     * @param id
     *
     * @returns {*}
     */
    function deleteFn(id) {
        return HttpService.delete(baseUrl +'/'+ id)
    }
}