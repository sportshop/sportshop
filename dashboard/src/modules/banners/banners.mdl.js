import layoutTemplate from "../layouts/dashboard.html";
import listTemplate from "./list.html";
import formTemplate from "./form.html";
import generalTemplate from "./_general.html";
import sidebarTemplate from "./_sidebar.html"

angular
    .module('app.banners', [])
    .config(configure);


configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('slides', {
            abstract: true,
            url: '/banners',
            views: {
                '': {
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('slides.list', {
            url: '?page',
            views: {
                'content': {
                    controller: 'SlidesController.list as ctrl',
                    templateUrl: listTemplate
                }
            },
            resolve: {
                items: [
                    '$stateParams', 'SlidesService',
                    function ($stateParams, SlidesService) {
                        var params = {
                            page: $stateParams.page
                        };

                        return SlidesService.getList(params);
                    }
                ]
            }
        })
        .state('slides.create', {
            url: '/create',
            views: {
                'content': {
                    controller: 'SlidesController.edit as ctrl',
                    templateUrl: formTemplate
                },
                'general@slides.create': {
                    templateUrl: generalTemplate
                },
                'sidebar@slides.create': {
                    templateUrl: sidebarTemplate
                }
            },
            resolve: {
                item: [
                    function () {
                        return {
                            data: {}
                        };
                    }
                ],
                currencies : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalCurrencies) {
                            return SettingsService.getCurrenciesList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalCurrencies));
                    }
                ]
            }
        })
        .state('slides.edit', {
            url: '/:id',
            views: {
                'content': {
                    controller: 'SlidesController.edit as ctrl',
                    templateUrl: formTemplate
                },
                'general@slides.edit': {
                    templateUrl: generalTemplate
                },
                'sidebar@slides.edit': {
                    templateUrl: sidebarTemplate
                }
            },
            resolve: {
                item: [
                    '$stateParams', 'SlidesService',
                    function ($stateParams, SlidesService) {
                        return SlidesService.get($stateParams.id);
                    }
                ],
                currencies : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalCurrencies) {
                            return SettingsService.getCurrenciesList();
                        }

                        return $rootScope.globalCurrencies;
                    }
                ]
            }
        });
}