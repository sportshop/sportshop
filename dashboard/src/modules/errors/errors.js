import layoutTemplate from "./layout.html";
import template403 from "./403.html";
import template404 from "./404.html";
import template500 from "./500.html";

angular
    .module('app.errors', [])
    .config(configure)
    .controller('ErrorsController.main', ErrorsController_Main);


configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('errors', {
            abstract: true,
            url: '',
            views: {
                '': {
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('errors.403', {
            url: '/403',
            views: {
                'content': {
                    controller: 'ErrorsController.main',
                    templateUrl: template403
                }
            }
        })
        .state('errors.404', {
            url: '/404',
            views: {
                'content': {
                    controller: 'ErrorsController.main',
                    templateUrl: template404
                }
            }
        })
        .state('errors.500', {
            url: '/500',
            views: {
                'content': {
                    controller: 'ErrorsController.main',
                    templateUrl: template500
                }
            }
        });
}


ErrorsController_Main.$inject = [];
function ErrorsController_Main() {}