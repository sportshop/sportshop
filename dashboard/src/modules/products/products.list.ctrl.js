angular
    .module('app.products')
    .controller('ProductsController.list', ProductsController_List);


ProductsController_List.$inject = ['$rootScope', '$scope', '$state', '$stateParams', '$filter', 'BreadcrumbsService', 'MainHeaderService', 'HttpService', 'Notify', 'items', 'categories_list'];
function ProductsController_List($rootScope, $scope, $state, $stateParams, $filter, BreadcrumbsService, MainHeaderService, HttpService, Notify, items, categories_list) {
    BreadcrumbsService.push('products.title.list');
    MainHeaderService.set('products.title.list');

    // products Model
    $scope.list = items.data || {};

    $scope.isManager = $rootScope.currentUser.user.role_name == 'manager' || $rootScope.currentUser.user.role_name == 'limited_manager';

    // Transform list
    function transformList(list) {
        _.each(list, function (item) {
            if (item.media && item.media.length) {
                item.thumbnail = _.find(item.media, function(el) {
                    return el.tag == 'thumbnail';
                });
            }

            if (item.categories && item.categories.length) {
                item.category = _.find(item.categories, function (el) {
                    return el.is_default;
                });
            }

            if (item.cost_price) {
                item.cost_price = _.find(item.cost_price, function(el) {
                    return el.is_default;
                });
            }

            if (item.sale_price) {
                item.sale_price = _.find(item.sale_price, function(el) {
                    return el.is_default;
                });
            }

            return item;
        });

        return list;
    }

    transformList($scope.list.products.data);

    // [options] Categories
    if (categories_list && categories_list.data) {
        $scope.options_categories = categories_list.data || [];
        $rootScope.globalCategoriesFlatList = JSON.parse(JSON.stringify(categories_list.data));
    } else {
        $scope.options_categories = categories_list || [];
    }

    // [options] Product Statuses
    $scope.options_product_types = [
        {id: 'novelty',  name: 'products.product_type.novelty'},
        {id: 'sale',     name: 'products.product_type.sale'},
        {id: 'max_sale', name: 'products.product_type.max_sale'},
        {id: 'top_sale', name: 'products.product_type.top_sale'}
    ];

    $scope.options_product_default_statuses = [
        {key: 'active', value: 'products.general.status_active'},
        {key: 'inactive', value: 'products.general.status_inactive'},
        {key: 'hidden', value: 'products.general.hidden'},
    ];

    // Filter
    $scope.searchForm = {
        limit:     $stateParams.limit || 20,
        name:      $stateParams.name || null,
        sku:       $stateParams.sku || null,
        categories:  $stateParams.categories || null,
        status:    $stateParams.status || null,
        type:      $stateParams.type || null,
        store_id:  localStorage.getItem('active_store') || null,
    };

    $scope.changeHiddenStatus = changeHiddenStatusFn;
    $scope.changeType = changeTypeFn;
    $scope.showType = showTypeFn;
    $scope.processSearch = processSearchFn;
    $scope.remove = removeFn;
    $scope.changePage = changePageFn;
    $scope.finalPrice = getFinalPriceFn;

    function changeHiddenStatusFn(item) {
        var data = {
            is_visible: !item.is_visible
        };

        HttpService.put('/products/'+ item.id +'/update-visibility', data)
            .success(function() {
                Notify.success('Статус продукта '+ item.name +' змінено');
            });
    }

    function changeTypeFn(item) {
        var data = {
            type: item.type
        };

        HttpService.put('/products/'+ item.id +'/update-type', data)
            .success(function() {
                Notify.success('Тип продукта '+ item.name +' змінено');
        });
    }

    function showTypeFn(id) {
        switch(id) {
            case 'novelty':
                return 'products.product_type.novelty';
                break;
            case 'sale':
                return 'products.product_type.sale';
                break;
            case 'max_sale':
                return 'products.product_type.max_sale';
                break;
            case 'top_sale':
                return 'products.product_type.top_sale';
                break;
        }
    }

    // Change Filter
    function processSearchFn() {
        var params = {
            paginate: true,
            limit:    $scope.list.products.per_page || 20,
            name:     $scope.searchForm.name,
            sku:      $scope.searchForm.sku,
            type:     $scope.searchForm.type,
            categories: $scope.searchForm.categories,
            status:   $scope.searchForm.status,
            store_id: localStorage.getItem('active_store'),
            page:     null
        };

        HttpService.getWParams('/products', {params: params})
            .success(function(response){
                $scope.list = response || {};
                transformList($scope.list.products.data);
                $state.go($state.current, params, { notify: false });
            });
    }

    function removeFn(item) {
        if (item.quantity && item.quantity > 0) {
            Notify.warning('products.general.isset_products_quantity');
        } else {
            Notify.confirm(function () {
                HttpService.delete('/products/'+ item.id)
                    .success(function(response) {
                        if ($scope.list.products.current_page > 1 && $scope.list.products.data.length == 1) {
                            $state.go($state.current, { page: +$scope.list.products.current_page - 1 }, { reload: true });
                        } else {
                            $state.go($state.current, {}, { reload: true });
                        }

                        Notify.success('general.removed');
                    })
                    .error(function(errors) {
                        Notify.warning('products.general.isset_products_quantity');
                        // Notify.warning(errors.message);
                    });
            }, 'products.general.are_you_sure');
        }
    }

    function changePageFn() {
        var params = {
            paginate: true,
            limit:    $scope.list.products.per_page || 20,
            name:     $scope.searchForm.name,
            sku:      $scope.searchForm.sku,
            type:     $scope.searchForm.type,
            categories: $scope.searchForm.categories,
            status:   $scope.searchForm.status,
            page:     $scope.list.products.current_page,
            store_id: localStorage.getItem('active_store')
        };

        HttpService.getWParams('/products', { params: params })
            .success(function(response){
                $scope.list = response || {};
                transformList($scope.list.products.data);
                $state.go($state.current, params, { notify: false });
            });
    }
    
    function getFinalPriceFn(item) {
        var result = {};
        _.each(item.final_prices, function (item) {
            if (item.store_id == localStorage.getItem('active_store')) {
                result = item;
            }
        });
        
        return result;
    }
}