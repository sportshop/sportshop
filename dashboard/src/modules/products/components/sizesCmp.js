import sizeListTemplate from './size_list.html'
import sizeItemTemplate from './size_item.html'

(function () {
    'use strict';

angular
    .module('app.products.components', [])
    .component('sizeList', {
        bindings: {
            stores: '<',
            sizes: '<',
            productquantities: '<',
            productdefaultsizes: '<',
            currencies: '<',
            currency: "=",
            price: '=',
            freshprice: '=',
            sizeerrors: '=',
            priceserrors: '=',
            totalquantity: '=',
            item: '='
        },
        templateUrl: sizeListTemplate,
        controller: function ($rootScope, $scope, $stateParams, $timeout, ProductsService, HttpService) {
            var vm = this;

            vm.totalQuantity = 0; // Total quantity of products

            function changeProductSizes() {
                vm.store_sizes = [];

                _.each(vm.productdefaultsizes.sizes, function(size, sizeIndex) {
                    vm.store_sizes.push({
                        stores: [],
                        total: 0,
                        isLocked: false,
                        lockedIndex: null
                    });

                    _.each(vm.stores, function(store, storeIndex) {
                        var newValue = {
                            original_quantity: 0,
                            new_quantity: null,
                            quantity: 0,
                            size_id: size.id,
                            store_id: store.id
                        };

                        // For Edit product if isset quantity
                        if (vm.productquantities && vm.productquantities.sizes) {
                            _.each(vm.productquantities.sizes, function(productSize) {
                                if (size.id == productSize.size_id && store.id == productSize.store_id) {
                                    newValue.original_quantity = productSize.quantity;
                                    newValue.quantity = productSize.quantity;
                                    newValue.id = productSize.id;
                                }
                            })
                        }

                        vm.store_sizes[sizeIndex].stores.push(newValue);
                    });
                });
            }

            changeProductSizes(); // Initialize store_sizes

            vm.showSizes = showSizesFn;
            vm.changeMainPrice = changeMainPriceFn;

            function showSizesFn() {
                if (!vm.productdefaultsizes.id) {
                    vm.productdefaultsizes = [];
                    vm.store_sizes = [];
                    return;
                }

                HttpService.get('/sizes/'+ vm.productdefaultsizes.id)
                    .success(function(response) {
                        vm.productdefaultsizes = response || [];
                        vm.productdefaultsizes.id = vm.productdefaultsizes.id.toString();

                        changeProductSizes(); // Modified store_sizes
                    });
            }

            // Set new price in sales
            function changeMainPriceFn() {

                $rootScope.$emit('prices:init');
            }

            // Send all data for save product
            $scope.$on('sizes:init', function () {
                var data = {
                    store_sizes: vm.store_sizes,
                    price: vm.price
                };

                ProductsService.setSizesData(data);
            });
        }
    })

    .component('sizesItem', {
        bindings: {
            stores: '<',
            sizes: '<',
            parentindex: '<',
            size: '=',
            storesizes: '=',
            price: '=',
            totalquantity: '=',
            productdefaultsizes: '='
        },
        templateUrl: sizeItemTemplate,
        controller: function ($rootScope, $scope, $stateParams, $timeout, ProductsService, HttpService) {
            var vm = this;

            // Count total quantity
            function totalQuantity() {
                vm.totalquantity = 0;
                vm.totalquantity = _.reduce(vm.storesizes, function(memo, elem){
                    if (elem.total && elem.total >= 0) {
                        return memo + (+elem.total);
                    } else {
                        return memo;
                    }
                }, 0);
            }

            // Initialize sizeItem
            _.each(vm.productdefaultsizes.sizes, function(size, sizeIndex) {
                _.each(vm.stores, function (store, storeIndex) {
                    changeQuantitiesSizeAmountFn(sizeIndex, storeIndex); // Initialize total Quantity for one size
                })
            });

            totalQuantity(); // Initialize total quantity

            vm.changeQuantities = changeQuantitiesFn;
            vm.changeQuantitiesSizeAmount = changeQuantitiesSizeAmountFn;
            vm.moveQuantities = moveQuantitiesFn;

            // Move quantities between stores
            function moveQuantitiesFn(parentindex, index, event) {
                if (vm.storesizes[parentindex].isLocked && vm.storesizes[parentindex].lockedIndex && vm.storesizes[parentindex].lockedIndex != index) {
                    return;
                }

                // For icheck
                if (vm.storesizes[parentindex].stores[index].original_quantity == 0 && !vm.storesizes[parentindex].isLocked) {
                    vm.storesizes[parentindex].isLocked = true;
                    vm.storesizes[parentindex].lockedIndex = index;
                    return;
                }

                vm.storesizes[parentindex].isLocked = !vm.storesizes[parentindex].isLocked;
                vm.storesizes[parentindex].lockedIndex = index;

                if (vm.storesizes[parentindex].isLocked || !vm.storesizes[parentindex].isLocked) {
                    _.each(vm.storesizes[parentindex].stores, function(elem, index){
                        vm.storesizes[parentindex].stores[index].original_quantity = elem.quantity;
                        vm.storesizes[parentindex].stores[index].new_quantity = '';

                        changeQuantitiesFn(parentindex, index);
                    });
                }
            }

            // Total Quantity for one size
            function changeQuantitiesSizeAmountFn(parentindex, index) {
                vm.storesizes[parentindex].total = _.reduce(vm.storesizes[parentindex].stores, function(memo, elem){
                    var new_quantity = 0;
                    var original_quantity = 0;

                    if (elem.new_quantity && +elem.new_quantity) {
                        new_quantity = +elem.new_quantity
                    }
                    if (elem.original_quantity && +elem.original_quantity) {
                        original_quantity = +elem.original_quantity
                    }

                    return memo + (+original_quantity+ +new_quantity);
                }, 0);

                totalQuantity(); // Modified total quantity
            }

            // Modify one size quantity
            function changeQuantitiesFn(parentindex, index) {
                var item = vm.storesizes[parentindex].stores[index];
                var lockedIndex = vm.storesizes[parentindex].lockedIndex || null;

                // Set original quantities if unLocked
                function setOriginalQuantityIfIsLocked() {
                    if (vm.storesizes[parentindex].isLocked) {
                        var amoutSizeQuantities = _.reduce(vm.storesizes[parentindex].stores, function (memo, elem, index) {
                            if (+elem.new_quantity && (index != lockedIndex)) {
                                return memo + (+elem.new_quantity);
                            } else {
                                return memo;
                            }
                        }, 0);
                        vm.storesizes[parentindex].stores[+lockedIndex].original_quantity = vm.storesizes[parentindex].stores[+lockedIndex].quantity - +amoutSizeQuantities;
                    }
                }

                if ((item.new_quantity <= 0) || item.new_quantity == '') {
                    item.original_quantity = +item.quantity;
                    item.new_quantity = '';
                    setOriginalQuantityIfIsLocked();
                } else if (item.new_quantity > 0 && !vm.storesizes[parentindex].isLocked) {
                } else if (item.new_quantity > 0 && vm.storesizes[parentindex].isLocked) {
                    setOriginalQuantityIfIsLocked();
                    if (vm.storesizes[parentindex].stores[+lockedIndex].original_quantity < 0) {
                        item.new_quantity = '';
                        item.original_quantity = +item.quantity;
                        setOriginalQuantityIfIsLocked();
                        return;
                    }
                }
                changeQuantitiesSizeAmountFn(parentindex, index);

                $rootScope.$emit('prices:init', {totalquantity: vm.totalquantity});
            }
        }
    });

})();