import cropImageView from '../../directives/main/crop-image.html';

angular
    .module('app.products')

    .controller('ProductsController.edit', ProductsController_Edit);


ProductsController_Edit.$inject = ['$rootScope', '$scope', '$state', '$stateParams', '$timeout', '$filter', 'ngDialog', 'MainHeaderService', 'HttpService', 'Notify', 'BreadcrumbsService', 'MyFileUploader', 'ProductsService', 'AttributesService', 'HelperService','item', 'categories_list', 'sizes', 'stores', 'currencies', 'discounts', 'product_quantities', 'product_default_sizes', 'settings', 'attributes', 'brands', 'colors', 'CONFIG'];
function ProductsController_Edit($rootScope, $scope, $state, $stateParams, $timeout, $filter, ngDialog, MainHeaderService, HttpService, Notify, BreadcrumbsService, MyFileUploader, ProductsService, AttributesService, HelperService, item, categories_list, sizes, stores, currencies, discounts, product_quantities, product_default_sizes, settings, attributes, brands, colors, CONFIG) {
    BreadcrumbsService.push('products.title.list', 'products.list');

    $scope.uploadUrl = CONFIG.host + '/products/upload-media';

    // Model Settings
    if (settings && settings.data) {
        $scope.settings = settings.data || {};
        $rootScope.globalSettings = JSON.parse(JSON.stringify(settings.data));
    } else {
        $scope.settings = settings || {};
    }


    if (attributes && attributes.data) {
        $scope.attributes = attributes.data || {};
        $rootScope.globalAttributes = JSON.parse(JSON.stringify(attributes.data));
    } else {
        $scope.attributes = attributes || [];
    }

    if (brands && brands.data) {
        $scope.brands = brands.data || {};
        $rootScope.globalBrands = JSON.parse(JSON.stringify(brands.data));
    } else {
        $scope.brands = brands || {};
    }

    $scope.builder_types = ProductsService.getBuilderTypes();
    $scope.builder_positions = ProductsService.getBuilderPositions();

    var markup = $scope.settings.markup;
    var discount_sale = $scope.settings.discount_sale;
    var discount_max_sale = $scope.settings.discount_max_sale;

    if ($stateParams.id) {
        BreadcrumbsService.push('products.title.edit');
        MainHeaderService.set('products.title.edit');
    } else {
        BreadcrumbsService.push('products.title.create');
        MainHeaderService.set('products.title.create');
    }

    // Additional form data for file
    var formData;

    var onCompleteItem;
    // Set main image
    onCompleteItem = function(fileItem, response, status, headers) {
        if (status == 200) {
            $scope.item.thumbnail = response.data;
            $scope.item.media_id = response.data.id;
        }
    };
    // Init MyFileUploader for main image
    var uploadMainImage =  MyFileUploader.init(true, 5, false, { onCompleteItem: onCompleteItem });
    // Set additional gallery images
    onCompleteItem = function(fileItem, response, status, headers) {
        if (status == 200) {
            if (!$scope.item.additional_media) {
                $scope.item.additional_media = [];
            }
            var data = response;
            $scope.item.additional_media.push(data);
        }
    };
    // Init MyFileUploader for gallery images
    // var uploadAdditionalImages =  MyFileUploader.init(true, 10, false, { onCompleteItem: onCompleteItem });
    var uploadAdditionalImages =  MyFileUploader.init(false, 1, false, { onCompleteItem: onCompleteItem }, [formData]);

    // Upload main image
    $scope.uploadMainImage = uploadMainImage;
    // Upload additional gallery images
    $scope.uploadAdditionalImages = uploadAdditionalImages;

    $scope.uploadAdditionalImages.onAfterAddingFile = function(item) {
        ngDialog.open({
            templateUrl: cropImageView,
            showClose: false,
            scope: $scope,
            className: 'dialog-theme-default',
        });
    };

    // Currencies List
    if (currencies && currencies.data) {
        $scope.currencies = currencies.data || [];
        $rootScope.globalCurrencies = JSON.parse(JSON.stringify(currencies.data));
    } else {
        $scope.currencies = currencies || [];
    }

    // Discounts List
    if (discounts && discounts.data) {
        $scope.discounts = discounts.data || [];
        $rootScope.globalDiscounts = JSON.parse(JSON.stringify(discounts.data));
    } else {
        $scope.discounts = discounts || [];
    }

    // Personal discounts
    $scope.personal_discounts = _.map($scope.discounts, function(item) {
        return {
            id: item.id,
            is_increase: item.is_increase,
            title: item.title,
            value: item.value
        }
    });

    // Sale discounts
    $scope.sale_discounts = _.map(discounts.data || discounts, function(item) {
        return {
            id: item.id,
            is_increase: item.is_increase,
            title: item.title,
            value: item.sale_value || 0
        }
    });

    // [options] Categories List
    if (categories_list && categories_list.data) {
        $scope.options_categories = categories_list.data || [];
        $rootScope.globalCategoriesFlatList = JSON.parse(JSON.stringify(categories_list.data));
    } else {
        $scope.options_categories = categories_list || [];
    }

    $scope.options_additional_categories = JSON.parse(JSON.stringify($scope.options_categories));

    // [options] Colors List
    if (colors && colors.data) {
        $scope.options_colors = colors.data || [];
        $rootScope.globalColors = JSON.parse(JSON.stringify(colors.data));
    } else {
        $scope.options_colors = colors || [];
    }

    // [options] Product Types List
    $scope.options_product_types = [
        {id: 'novelty', name: 'Новинка'},
        {id: 'sale', name: 'Sale'},
        {id: 'max_sale', name: 'Max Sale'},
        {id: 'top_sale', name: 'Хіт продаж'}
    ];

    // Sizes List
    if (sizes && sizes.data) {
        $scope.sizes = sizes.data || [];
        $rootScope.globalSizes = JSON.parse(JSON.stringify(sizes.data));
    } else {
        $scope.sizes = sizes || [];
    }

    // Stores List
    if (stores && stores.data) {
        $scope.stores = stores.data || [];
        $rootScope.globalStores = JSON.parse(JSON.stringify(stores.data));
    } else {
        $scope.stores = stores || [];
    }

    // Quantities of products
    $scope.product_quantities = product_quantities.data || {};
    // Default product sizes
    $scope.product_default_sizes = product_default_sizes.data ?  product_default_sizes.data : {};
    if ($scope.product_default_sizes && $scope.product_default_sizes.id) {
        $scope.product_default_sizes.id = $scope.product_default_sizes.id.toString();
    }

    $scope.selectCategoryChanged = function (item) {
        if (item && item.originalObject) {
            $scope.item.default_category_id = item.originalObject.id;

            // $scope.options_additional_categories = _.filter($scope.options_additional_categories, function(elem) {
            //     return elem.id != $scope.item.default_category_id;
            // });
        }
    };

    if ($stateParams.id) {
        // Product Model for Edit Product
        $scope.item = item.data || {};
        $scope.item.brand_id = $scope.item.brand ?  $scope.item.brand.id : null;

        if ($scope.item.cost_price) {
            $scope.item.cost_price = _.find($scope.item.cost_price, function(el) {
                return el.is_default;
            });
        }

        if ($scope.item.attributes && $scope.item.attributes.items) {
            var tempProdAttributes = [];
            _.each($scope.item.attributes.items, function(elem) {
                if (elem.values) {
                    _.each(elem.values, function(prop) {
                        if (prop.id) {
                            elem.id = prop.id;
                            tempProdAttributes.push({
                                value_id: prop.id
                            });
                        }
                        return prop;
                    })
                }
            });

            if ($scope.item.attributes && $scope.item.attributes.group && $scope.item.attributes.group.id) {
                AttributesService.get($scope.item.attributes.group.id)
                    .success(function(resp) {
                        $scope.item.allAttributes = resp.attributes || {};
                        $scope.item.allAttributes = _.map($scope.item.allAttributes, function(elem) {
                            var data = {
                                attribute_id: elem.id,
                                name: elem.name,
                                values: elem.values
                            };

                            return data;
                        });

                        _.each($scope.item.allAttributes, function(attr) {
                            _.each(tempProdAttributes, function(prodAttr) {
                                if (attr.values) {
                                    _.each(attr.values, function(prop) {
                                        if (prop.id == prodAttr.value_id) {
                                            prop.value_id = true;
                                        }
                                        return prop;
                                    })
                                }
                            })
                        });

                        if (!$scope.item.attributes) {
                            $scope.item.attributes = {};
                        }
                        $scope.item.attributes.items = $scope.item.allAttributes;
                    });
            }
        }

        $scope.item.category = _.find($scope.item.categories, function(el) {
            return el.is_default;
        });
        $scope.item.additional_categories = _.filter($scope.item.categories, function(el) {
            return el.is_default == false;
        });
        $scope.item.categories = $scope.item.additional_categories;

        $scope.item.default_category_id = ($scope.item.category && $scope.item.category.id) ? $scope.item.category.id : null;

        if ($scope.item.default_category_id) {
            // $scope.options_additional_categories = _.filter($scope.options_additional_categories, function(elem) {
            //     return elem.id != $scope.item.default_category_id;
            // });
        }

        if ($scope.item.sizes && $scope.item.sizes.items) {
            $scope.product_quantities = { sizes: $scope.item.sizes.items };
        }

        if (!$scope.item.cost_price || !$scope.item.cost_price.currency_id) {
            $scope.item.cost_price = {
                value: 0,
                currency_id: ($scope.currencies && $scope.currencies.length && $scope.currencies[0].id) ? $scope.currencies[0].id : $rootScope.defaultCurrencyId
            };
        }

        $scope.item.additional_images =  _.filter($scope.item.media, function(el) {
            return el.tag == 'additional_image';
        });

        $scope.item.additional_media = $scope.item.additional_images || []; // Product galleries
        $scope.item.thumbnail = _.find($scope.item.media, function(el) {
            return el.tag == 'thumbnail';
        });
        $scope.item.media_id = $scope.item.thumbnail ? $scope.item.thumbnail.id : null; // Product main image
        $scope.item.type = $scope.item.type || "novelty";

        if (($scope.item.cost_price && $scope.stores) || ($scope.item.stores && $scope.item.stores.length)) {

            var stores = HelperService.cloneObject($scope.stores);
            var prices = HelperService.cloneObject($scope.item.cost_price);

            var stores_items = _.map(HelperService.cloneObject($scope.stores), function(elem) {
                elem.store_id = elem.id;
                elem.personal_discounts = HelperService.cloneObject($scope.personal_discounts);
                elem.sale_discounts = HelperService.cloneObject($scope.sale_discounts);

                elem.sale = markup || '';
                elem.sale_discount = discount_sale || '';
                elem.max_sale = discount_max_sale || '';
                return elem;
            });

            if (stores_items) {
                $scope.stores_items = _.map(stores_items, function(storeItem, storeIndex) {
                    _.each($scope.item.stores, function(elem, elemIndex) {
                        if (elem.markup && elem.store_id == storeItem.id) {
                            storeItem.currency_id = elem.currency_id;
                            storeItem.sale = +elem.markup;
                        }
                        if (elem.sale_discount && elem.store_id == storeItem.id) {
                            storeItem.sale_discount = +elem.sale_discount;
                        }
                        if (elem.max_sale_discount && elem.store_id == storeItem.id) {
                            storeItem.max_sale = +elem.max_sale_discount;
                        }

                        if (elem.store_id == storeItem.id) {
                            _.each(storeItem.personal_discounts, function(personalElem, pesonalElemIndex) {
                                var personal_discounts = _.find(elem.user_discounts, function(discount) {
                                    return personalElem.id == discount.discount_id;
                                });

                                if (personal_discounts) {
                                    if (personal_discounts.user) {
                                        personalElem.value = personal_discounts.user;
                                    }
                                }

                                return personalElem;
                            });
                        }

                        if (elem.store_id == storeItem.id) {
                            _.each(storeItem.sale_discounts, function(salesElem, salesElemIndex) {
                                var sale_discounts = _.find(elem.user_discounts, function(discount) {
                                    return salesElem.id == discount.discount_id;
                                });

                                if (sale_discounts) {
                                    if (sale_discounts.sale) {
                                        salesElem.value = sale_discounts.sale;
                                    }
                                }

                                return salesElem;
                            });
                        }

                        return elem;
                    });
                    if (!storeItem.currency_id) {
                        storeItem.currency_id = $scope.currencies[0].id;
                    }
                    return storeItem;
                });

                if ($scope.item.cost_price && $scope.item.cost_price.value) {
                    $timeout(function() {
                        _.each($scope.stores_items, function(elem, index) {
                            changeSaleFn(index);
                            changePersonalPriceFn(index);
                            changeSalesPriceFn(index);
                            changeTotalPriceFn(index);
                            changeMaxSaleFn(index);
                        });
                    }, 100);
                }

            } else {

                $scope.stores_items = _.map($scope.stores, function(elem) {
                    elem.store_id = elem.id;
                    elem.currencies = HelperService.cloneObject($scope.currencies);
                    elem.currency_id = $scope.currencies[0] ? $scope.currencies[0].id : null;
                    elem.personal_discounts = HelperService.cloneObject($scope.personal_discounts);
                    elem.sale_discounts = HelperService.cloneObject($scope.sale_discounts);
                    elem.sale = markup || '';
                    elem.sale_discount = discount_sale || '';
                    elem.max_sale = discount_max_sale || '';
                    return elem;
                });
            }

        } else {
            $scope.stores_items = _.map($scope.stores, function(elem) {
                elem.store_id = elem.id;
                elem.currencies = HelperService.cloneObject($scope.currencies);
                elem.currency_id = $scope.currencies[0] ? $scope.currencies[0].id : '';
                elem.price = 0;
                elem.personal_discounts = HelperService.cloneObject($scope.personal_discounts);
                elem.sale_discounts = HelperService.cloneObject($scope.sale_discounts);
                elem.sale = markup || '';
                elem.sale_discount = discount_sale || '';
                elem.max_sale = discount_max_sale || '';
                return elem;
            });
        }

    } else {
        // Product Model for Create Product
        $scope.item = {
            type: "novelty",
            is_visible: true,
            is_active: true,
            cost_price: {
                currency_id: $rootScope.defaultCurrencyId,
                value: 0
            }
        };

        $scope.item.additional_images = [];
        $scope.item.additional_media = $scope.item.additional_images;

        $scope.stores_items = _.map($scope.stores, function(elem) {
            elem.store_id = elem.id;
            elem.currencies = HelperService.cloneObject($scope.currencies);
            elem.currency_id = $scope.currencies[0] ? $scope.currencies[0].id : null;
            elem.price = 0;
            elem.personal_discounts = HelperService.cloneObject($scope.personal_discounts);
            elem.sale_discounts = HelperService.cloneObject($scope.sale_discounts);
            elem.sale = markup || '';
            elem.sale_discount = discount_sale || '';
            elem.max_sale = discount_max_sale || '';
            return elem;
        });
    }

    $scope.tempPrice = []; // temp price for show orange if discount price < updated price

    // Initialize prices for discounts if updates
    $rootScope.$on('prices:init', function(event, products) {
        if (products) {
            changeMainPriceFn(products.totalquantity);
        } else {
            changeMainPriceFn();
        }
    });

    $scope.freshprice = {
        value: 0,
        currency_id: $rootScope.defaultCurrencyId
    };

    if (!$scope.item.transfers) {
        $scope.item.transfers = [];
    }

    if (!$scope.item.quantity) {
        $scope.item.quantity = 0;
    }

    if ($scope.item.cost_price && !$scope.item.cost_price.value) {
        $scope.item.cost_price.value = 0;
    }
    var costPrice = JSON.parse(JSON.stringify($scope.item.cost_price));


    // Change main price for discounts
    function changeMainPriceFn(actualQuantity) {
        // Update price if different currency
        function convertPriceFn(elem, lastIndex) {
            var sumPrice = 0;

            var costPriceCurrency = _.findWhere($scope.currencies, { id: costPrice.currency_id});

            // if ($scope.item.quantity != 0) {
            sumPrice = +costPrice.value * +costPriceCurrency.value * +$scope.item.quantity;
            // } else {
            //     sumPrice = +costPrice.value * +costPriceCurrency.value;
            // }

            if ($scope.item.cost_price == 0) {
                var freshPriceCurrency = _.findWhere($scope.currencies, { id: $scope.freshprice.currency_id });
                $scope.item.cost_price.value = +$scope.freshprice.value * +freshPriceCurrency.value;
            }

            if (actualQuantity) {
                $scope.actualQuantity = +actualQuantity;
                $scope.freshQuantity = +actualQuantity - +$scope.item.quantity;
            }

            if (typeof $scope.actualQuantity == 'undefined') {
                $scope.actualQuantity = 0;
            }

            if ($scope.freshprice.value && $scope.freshQuantity && $scope.actualQuantity != $scope.item.quantity) {

                if ($scope.freshprice.currency_id != $scope.item.cost_price.currency_id) {
                    var costPriceCurrency = _.findWhere($scope.currencies, { id: $scope.item.cost_price.currency_id});
                    var freshPriceCurrency = _.findWhere($scope.currencies, { id: $scope.freshprice.currency_id });

                    if ((costPriceCurrency && freshPriceCurrency) && ((+costPriceCurrency.value) > (+freshPriceCurrency.value))) {
                        $scope.item.cost_price.value = roundToTwo(((sumPrice +(+$scope.freshprice.value * (+freshPriceCurrency.value) * +$scope.freshQuantity)) / $scope.actualQuantity) / (+costPriceCurrency.value));
                    } else {
                        $scope.freshpriceConverted = roundToTwo((+$scope.freshprice.value) * (+freshPriceCurrency.value) / (+costPriceCurrency.value));
                        $scope.item.cost_price.value = roundToTwo(((sumPrice +(+$scope.freshprice.value * (+freshPriceCurrency.value) * +$scope.freshQuantity)) / $scope.actualQuantity) / (+costPriceCurrency.value));
                    }
                } else {
                    var freshPriceCurrency = _.findWhere($scope.currencies, { id: $scope.freshprice.currency_id });
                    var costPriceCurrency = _.findWhere($scope.currencies, { id: $scope.item.cost_price.currency_id});

                    $scope.item.cost_price.value = roundToTwo(((sumPrice +(+$scope.freshprice.value *(+freshPriceCurrency.value) * +$scope.freshQuantity)) / $scope.actualQuantity) / (+costPriceCurrency.value));
                }
            } else if ($scope.actualQuantity || $scope.actualQuantity == 0) {
                if ($scope.item.quantity == 0) {
                    var freshPriceCurrency = _.findWhere($scope.currencies, { id: $scope.freshprice.currency_id });
                    var costPriceCurrency = _.findWhere($scope.currencies, { id: $scope.item.cost_price.currency_id});

                    if ($scope.freshprice && $scope.freshprice.value) {
                        $scope.item.cost_price.value = roundToTwo(+$scope.freshprice.value * +freshPriceCurrency.value / (+costPriceCurrency.value));
                    }
                } else {
                    var freshPriceCurrency = _.findWhere($scope.currencies, { id: $scope.freshprice.currency_id });
                    var costPriceCurrency = _.findWhere($scope.currencies, { id: $scope.item.cost_price.currency_id});
                    $scope.item.cost_price.value = roundToTwo(((sumPrice) / $scope.item.quantity) / costPriceCurrency.value);
                }
            }

            if (elem.currency_id != $scope.item.cost_price.currency_id) {
                var priceCurrency = _.findWhere($scope.currencies, { id: $scope.item.cost_price.currency_id});
                var saleCurrency = _.findWhere($scope.currencies, { id: elem.currency_id });
                if ((priceCurrency && saleCurrency) && ((+priceCurrency.value) > (+saleCurrency.value))) {
                    elem.price = roundToTwo((+elem.price) * (+priceCurrency.value) / (+saleCurrency.value));
                } else if ((priceCurrency && saleCurrency) && ((+priceCurrency.value) < (+saleCurrency.value))) {
                    elem.price = roundToTwo((+elem.price) * (+priceCurrency.value) / (+saleCurrency.value));
                }

            } else {
                var priceCurrency = _.findWhere($scope.currencies, { id: $scope.item.cost_price.currency_id});
                var saleCurrency = _.findWhere($scope.currencies, { id: elem.currency_id });
                if ((priceCurrency && saleCurrency) && ((+priceCurrency.value) > (+saleCurrency.value))) {
                    elem.price = roundToTwo((+elem.price) * (+priceCurrency.value) / (+saleCurrency.value));
                } else if ((priceCurrency && saleCurrency) && ((+priceCurrency.value) < (+saleCurrency.value))) {
                    elem.price = roundToTwo((+elem.price) * (+priceCurrency.value) / (+saleCurrency.value));
                }
            }
            return elem;
        }

        // Update discounts
        _.each($scope.stores_items, function(elem, index) {
            if ($scope.item.cost_price && ($scope.item.cost_price.value || $scope.item.cost_price.value == 0)) {
                convertPriceFn(elem);
                var priceCurrency = _.findWhere($scope.currencies, { id: $scope.item.cost_price.currency_id});
                var saleCurrency = _.findWhere($scope.currencies, { id: elem.currency_id});

                elem.price = roundToTwo($scope.item.cost_price.value * priceCurrency.value/saleCurrency.value);

            } else if ($scope.item.cost_price && $scope.item.cost_price.value > 0) {
                convertPriceFn(elem);
                var priceCurrency = _.findWhere($scope.currencies, { id: $scope.item.cost_price.currency_id});
                var saleCurrency = _.findWhere($scope.currencies, { id: elem.currency_id});

                elem.price = roundToTwo($scope.item.cost_price.value * priceCurrency.value/saleCurrency.value);
            } else if ($scope.item.cost_price && $scope.item.cost_price.value) {
                convertPriceFn(elem);
                var priceCurrency = _.findWhere($scope.currencies, { id: $scope.item.cost_price.currency_id});
                var saleCurrency = _.findWhere($scope.currencies, { id: elem.currency_id});

                elem.price = roundToTwo($scope.item.cost_price.value * priceCurrency.value/saleCurrency.value);
            }

            $scope.tempPrice[index] = roundToTwo(elem.price);

            if ((+elem.sale == 0 || elem.sale > 0)) {
                changeSaleFn(index);
            } else {
                return;
            }

            return elem;
        });
    }

    if (!$stateParams.id) {
        changeMainPriceFn(); // Initialize main price for discounts
    } else {
        changeMainPriceFn(); // Initialize main price for discounts
    }

    $scope.focusInBuilder = focusInBuilderFn;
    $scope.focusInDefaultCategory = focusInDefaultCategoryFn;

    function focusInBuilderFn() {
        if ($scope.item.uniform && $scope.item.uniform.category_id) {
            if (!$scope.item.uniform) {
                $scope.item.uniform = {};
            }
            $scope.item.uniform.category_id = null;
            $scope.$broadcast('angucomplete-alt:clearInput', 'category_builder');
        }
    }

    function focusInDefaultCategoryFn() {
        if ($scope.item.default_category_id) {
            $scope.item.default_category_id = null;
            $scope.$broadcast('angucomplete-alt:clearInput', 'default_category');

            $scope.options_additional_categories = JSON.parse(JSON.stringify($scope.options_categories));
        }
    }

    $scope.selectCategoryForBuilderChanged = function (item) {
        if (item && item.originalObject && item.originalObject.id) {
            if (!$scope.item.uniform) {
                $scope.item.uniform = {};
            }

            $scope.item.uniform.category_id = item.originalObject.id;
        }
    };

    // Methods to works with images
    $scope.uploadImage = uploadImageFn;
    $scope.deleteImage = deleteImageFn;
    $scope.uploadGalleryImage = uploadGalleryImageFn;
    $scope.deleteGalleryImage = deleteGalleryImageFn;

    // Copy all prices to others stores
    $scope.copyToOtherStores = copyToOtherStoresFn;
    // Change main sale or main last price in discounts
    $scope.changeSale = changeSaleFn;
    $scope.changeLastPrice = changeLastPriceFn;
    // Change personal sale or personal price in discounts
    $scope.changePersonalSale = changePersonalSaleFn;
    $scope.changePersonalPrice = changePersonalPriceFn;
    // Change sales sale or sales price in discounts
    $scope.changeSalesSale = changeSalesSaleFn;
    $scope.changeSalesPrice = changeSalesPriceFn;
    // Change total sale or total price in discounts
    $scope.changeTotalSale = changeTotalSaleFn;
    $scope.changeTotalPrice = changeTotalPriceFn;
    // Change max sale or max price in discounts
    $scope.changeMaxSale = changeMaxSaleFn;
    $scope.changeMaxPrice = changeMaxPriceFn;
    // Show actual price for discounts
    $scope.showActualPrice = showActualPriceFn;

    // Save product
    $scope.save = saveFn;

    $scope.showAttributes = showAttributesFn;
    function showAttributesFn() {
        if ($scope.item.attributes && $scope.item.attributes.group && $scope.item.attributes.group.id) {
            AttributesService.get($scope.item.attributes.group.id)
                .success(function(resp) {
                    if (!$scope.item.attributes) {
                        $scope.item.attributes = {};
                    }
                    $scope.item.attributes.items = resp.attributes || {};
                    $scope.item.attributes.items = _.map($scope.item.attributes.items, function(elem) {
                        var data = {
                            attribute_id: elem.id,
                            name: elem.name,
                            values: elem.values
                        };

                        return data;
                    });
                })
        } else {
            if (!$scope.item.attributes) {
                $scope.item.attributes = {};
            }
            $scope.item.attributes.items = [];
        }
    }

    // Upload main product image
    function uploadImageFn() {
        $timeout(function() {
            var element = angular.element('#file-upload');
            element.triggerHandler('click');
            element.trigger('click');
        });
    }
    // Delete main product image
    function deleteImageFn(item) {
        HttpService.delete('/media/'+ item.id)
            .success(function() {
                delete $scope.item.thumbnail;
                delete $scope.item.media_id;
            });
    }

    /**
     * start
     */
    $scope.myCroppedImage = '';
    $scope.myImage = null;
    $scope.aspectRatio = $scope.aspectratio || 1;
    $scope.areaminrelativesize = {
        w: 600, height: 600
    };
    $scope.myImage = undefined;

    $scope.updateImage = updateImageFn;

    /**
     * Insert image for crop
     *
     * @param evt
     */
    var handleFileSelect = function(evt) {
        var file = evt.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.myImage = evt.target.result;
            });
        };
        reader.readAsDataURL(file);

        // $scope.uploadAdditionalImages.clearQueue();
    };
    angular.element(document.querySelector('#files-upload')).on('change', handleFileSelect);

    /**
     * Update image after crop
     */
    function updateImageFn(cropper, myCroppedImage) {
        // Update formData before upload image to server
        $scope.uploadAdditionalImages.onBeforeUploadItem = function(item) {
            // item.formData = [croppercropper.areaCoords];
            item.formData = [{
                base64: myCroppedImage
            }];
        };

        // Upload image to server
        $scope.uploadAdditionalImages.uploadAll();
        ngDialog.closeAll();
    }

    // Upload image for product's gallery
    function uploadGalleryImageFn() {
        $timeout(function() {
            var element = angular.element('#files-upload');
            element.triggerHandler('click');
            element.trigger('click');
            element.on('change', handleFileSelect);
        });
    }
    // Delete image from product's gallery
    function deleteGalleryImageFn(item, index) {
        HttpService.delete('/media/'+ item.id)
            .success(function() {
                $scope.item.additional_media.splice(index, 1);
            });
    }

    // Round number to two decimals after point
    function roundToTwo(num) {
        return Math.round(num * 100) / 100;
    }

    // Round number to int
    function roundToInt(num) {
        return Math.round(num);
    }

    // ----------------------------- START Discounts methods ------------------------------ //
    _.each($scope.stores_items, function(elem, storeIndex) {

        $scope.$watch('stores_items['+storeIndex+'].currency_id', function(newValue, oldValue) {
            var oldCurrency, newCurrency;

            oldCurrency = HelperService.cloneObject(_.findWhere($scope.currencies, { id: +$scope.item.cost_price.currency_id }));
            newCurrency = HelperService.cloneObject(_.findWhere($scope.currencies, { id: +newValue }));
            if (newValue != oldValue) {
                if (+oldCurrency.value < newCurrency.value) {
                    $scope.stores_items[storeIndex].price = roundToTwo((+$scope.item.cost_price.value)/(+newCurrency.value)*(+oldCurrency.value));
                } else {
                    $scope.stores_items[storeIndex].price = roundToTwo((+$scope.item.cost_price.value)*(+oldCurrency.value)/(+newCurrency.value));
                }
            } else {
                if (+oldCurrency.value < newCurrency.value) {
                    $scope.stores_items[storeIndex].price = roundToTwo((+$scope.item.cost_price.value)/(+newCurrency.value)*(+oldCurrency.value));
                } else {
                    $scope.stores_items[storeIndex].price = roundToTwo((+$scope.item.cost_price.value)*(+oldCurrency.value)/(+newCurrency.value));
                }
            }

            if ($scope.stores_items[storeIndex].sale && +$scope.stores_items[storeIndex].sale) {
                changeSaleFn(storeIndex);
            }

            $scope.tempPrice[storeIndex] = roundToTwo($scope.stores_items[storeIndex].price);
        });
    });

    function copyToOtherStoresFn(index) {
        _.each($scope.stores_items, function(store, storeItemIndex) {
            if (storeItemIndex != index) {
                $scope.stores_items[storeItemIndex].sale = HelperService.cloneObject($scope.stores_items[index].sale);
                $scope.stores_items[storeItemIndex].currency_id = HelperService.cloneObject($scope.stores_items[index].currency_id);
                $scope.stores_items[storeItemIndex].max_price = HelperService.cloneObject($scope.stores_items[index].max_price);
                $scope.stores_items[storeItemIndex].max_sale = HelperService.cloneObject($scope.stores_items[index].max_sale);
                $scope.stores_items[storeItemIndex].max_sale_price = HelperService.cloneObject($scope.stores_items[index].max_sale_price);
                $scope.stores_items[storeItemIndex].sale_discount = HelperService.cloneObject($scope.stores_items[index].sale_discount);
                $scope.stores_items[storeItemIndex].sale_price = HelperService.cloneObject($scope.stores_items[index].sale_price);

                _.each(store.personal_discounts, function(personalDiscountItem, personalDiscountIndex) {
                    $scope.stores_items[storeItemIndex].personal_discounts[personalDiscountIndex].price = HelperService.cloneObject($scope.stores_items[index].personal_discounts[personalDiscountIndex].price);
                    $scope.stores_items[storeItemIndex].personal_discounts[personalDiscountIndex].value = HelperService.cloneObject($scope.stores_items[index].personal_discounts[personalDiscountIndex].value);
                });

                _.each(store.sale_discounts, function(salesDiscountItem, salesDiscountIndex) {
                    $scope.stores_items[storeItemIndex].sale_discounts[salesDiscountIndex].sale_price = HelperService.cloneObject($scope.stores_items[index].sale_discounts[salesDiscountIndex].sale_price);
                    $scope.stores_items[storeItemIndex].sale_discounts[salesDiscountIndex].value = HelperService.cloneObject($scope.stores_items[index].sale_discounts[salesDiscountIndex].value);
                });
            }
        });
    }

    function changeSaleFn(parent_index) {
        if ($scope.stores_items[parent_index] && ($scope.stores_items[parent_index].sale || +$scope.stores_items[parent_index].sale == 0)) {
            $scope.stores_items[parent_index].max_price = roundToInt(+$scope.stores_items[parent_index].price * (+$scope.stores_items[parent_index].sale) / 100 + +$scope.stores_items[parent_index].price);
        } else {
            $scope.stores_items[parent_index].max_price = '';
        }

        if ($scope.stores_items[parent_index].sale || +$scope.stores_items[parent_index].sale == 0) {
            changePersonalSaleFn(parent_index);
            changeTotalSaleFn(parent_index);
            changeMaxSaleFn(parent_index);
        }
    }
    function changeLastPriceFn(parent_index) {

        if ($scope.stores_items[parent_index] && ($scope.stores_items[parent_index].max_price || +$scope.stores_items[parent_index].max_price == 0) && $scope.stores_items[parent_index].price) {
            $scope.stores_items[parent_index].sale = roundToTwo(+$scope.stores_items[parent_index].max_price * 100 / +$scope.stores_items[parent_index].price - 100);
        } else {
            $scope.stores_items[parent_index].sale = 0;
            return;
        }

        if ($scope.stores_items[parent_index].sale == -100) {
            $scope.stores_items[parent_index].max_price = '';
            $scope.stores_items[parent_index].sale = '';
            $scope.stores_items[parent_index].max_sale = '';
            $scope.stores_items[parent_index].sale_discount = '';
            _.each($scope.stores_items[parent_index].sale_discounts, function(elem) {
                $timeout(function(){
                    changeTotalSaleFn(parent_index);
                    changePersonalSaleFn(parent_index);
                    changeMaxSaleFn(parent_index);
                }, 100);
            });
        }

        $timeout(function(){
            changeTotalSaleFn(parent_index);
            changePersonalSaleFn(parent_index);
            changeMaxSaleFn(parent_index);
        }, 100);

    }
    function changePersonalSaleFn(parent_index) {

        _.each($scope.stores_items[parent_index].personal_discounts, function(prop, index) {
            if (prop.is_increase == 1 && (prop.value || +prop.value == 0)) {
                $scope.stores_items[parent_index].personal_discounts[index].price = roundToInt(+$scope.stores_items[parent_index].price * (+prop.value) / 100 + +$scope.stores_items[parent_index].price);
            } else if (prop.is_increase == 0 && (prop.value || +prop.value == 0)) {
                $scope.stores_items[parent_index].personal_discounts[index].price = roundToInt(-+$scope.stores_items[parent_index].max_price * (+prop.value) / 100 + +$scope.stores_items[parent_index].max_price);
            } else {
                $scope.stores_items[parent_index].personal_discounts[index].price = '';
                $scope.tempPrice[parent_index] = null;
            }
        });
    }
    function changePersonalPriceFn(parent_index) {
        _.each($scope.stores_items[parent_index].personal_discounts, function(prop, index) {

            if ($scope.stores_items[parent_index].max_price == 0 && prop.is_increase == 0) {
                prop.price = '';
                return;
            } else if ($scope.stores_items[parent_index].price == 0) {
                prop.price = '';
                return;
            }

            if (prop.is_increase == 1 && (prop.price || prop.price == '') && $scope.stores_items[parent_index].price) {
                $scope.stores_items[parent_index].personal_discounts[index].value = roundToInt(+prop.price * 100 / (+$scope.stores_items[parent_index].price) - 100);
            } else if (prop.is_increase == 0 && (prop.price || prop.price=='') && $scope.stores_items[parent_index].max_price) {
                $scope.stores_items[parent_index].personal_discounts[index].value = roundToInt(-(+prop.price) * 100 / (+$scope.stores_items[parent_index].max_price) + 100);
            }
        });
    }

    function changeSalesSaleFn(parent_index) {
        _.each($scope.stores_items[parent_index].sale_discounts, function(prop, index) {
            if ((prop.value || +prop.value == 0)) {
                $scope.stores_items[parent_index].sale_discounts[index].sale_price = roundToInt($scope.stores_items[parent_index].price * (+prop.value) / 100 + +$scope.stores_items[parent_index].price);
            }
        });
    }
    function changeSalesPriceFn(parent_index) {
        _.each($scope.stores_items[parent_index].sale_discounts, function(prop, index) {

            if ($scope.stores_items[parent_index].max_price == 0 && prop.is_increase == 0) {
                prop.sale_price = '';
                return;
            } else if ($scope.stores_items[parent_index].price == 0) {
                prop.sale_price = '';
                return;
            }

            if ((prop.sale_price || +prop.sale_price == 0)) {
                $scope.stores_items[parent_index].sale_discounts[index].value = roundToInt(prop.sale_price * 100 / +$scope.stores_items[parent_index].price - 100);
            }
        });
    }

    function changeTotalSaleFn(parent_index) {
        if ($scope.stores_items[parent_index].sale_discount != '') {
            $scope.stores_items[parent_index].sale_price = roundToInt(-$scope.stores_items[parent_index].max_price * (+$scope.stores_items[parent_index].sale_discount) / 100 + +$scope.stores_items[parent_index].max_price);

            if ($scope.stores_items[parent_index].sale_price < $scope.stores_items[parent_index].price
                || $scope.stores_items[parent_index].sale_price > $scope.stores_items[parent_index].max_price) {

                $scope.stores_items[parent_index].sale_price = roundToInt($scope.stores_items[parent_index].price);
            }

            changeSalesSaleFn(parent_index);
        } else {
            _.each($scope.stores_items[parent_index].sale_discounts, function(prop, index) {
                $scope.stores_items[parent_index].sale_discounts[index].sale_value = '';
            });
            changeSalesSaleFn(parent_index);

            delete $scope.stores_items[parent_index].sale_price;
            delete $scope.stores_items[parent_index].sale_discount;
        }
    }
    function changeTotalPriceFn(parent_index) {

        if ($scope.stores_items[parent_index].sale_price != '') {
            $scope.stores_items[parent_index].sale_discount = roundToInt(+$scope.stores_items[parent_index].sale_price * 100 / -$scope.stores_items[parent_index].max_price + 100);

            changeSalesSaleFn(parent_index);
        } else {
            _.each($scope.stores_items[parent_index].sale_discounts, function(prop, index) {
                $scope.stores_items[parent_index].sale_discounts[index].sale_price = '';
            });

            delete $scope.stores_items[parent_index].sale_price;
            delete $scope.stores_items[parent_index].sale_discount;
        }
    }
    function changeMaxSaleFn(parent_index) {
        if ($scope.stores_items[parent_index].max_sale != '') {
            $scope.stores_items[parent_index].max_sale_price = roundToInt(+$scope.stores_items[parent_index].price * (+$scope.stores_items[parent_index].max_sale) / 100 + +$scope.stores_items[parent_index].price);
        } else {
            delete $scope.stores_items[parent_index].max_sale_price;
        }
    }
    function changeMaxPriceFn(parent_index) {
        if ($scope.stores_items[parent_index].max_sale_price != '') {
            $scope.stores_items[parent_index].max_sale = roundToInt(+$scope.stores_items[parent_index].max_sale_price * 100 / +$scope.stores_items[parent_index].price - 100);
        } else {
            delete $scope.stores_items[parent_index].max_sale;
        }
    }

    // Show Actual Prices
    function showActualPriceFn(parent_index, index) {

        if ($scope.item.type == 'novelty' || $scope.item.type == 'top_sale') {
            if ($scope.stores_items[parent_index].personal_discounts[index].price) {
                if (($scope.stores_items[parent_index].personal_discounts[index].value)
                    && $scope.stores_items[parent_index].personal_discounts[index].price > $scope.stores_items[parent_index].price
                    && $scope.stores_items[parent_index].personal_discounts[index].price < $scope.stores_items[parent_index].max_price) {
                    return $scope.stores_items[parent_index].personal_discounts[index].price;
                } else {
                    return $scope.stores_items[parent_index].max_price;
                }
            } else {
                return $scope.stores_items[parent_index].max_price || 0;
            }
        } else if ($scope.item.type == 'sale') {
            if ($scope.stores_items[parent_index] && $scope.stores_items[parent_index].sale_discounts &&
                $scope.stores_items[parent_index].sale_discounts[index] &&
                $scope.stores_items[parent_index].sale_discounts[index].sale_price) {
                if (($scope.stores_items[parent_index].sale_discounts[index].sale_price >= $scope.stores_items[parent_index].price) &&
                    ($scope.stores_items[parent_index].sale_discounts[index].value > 0) &&
                    ($scope.stores_items[parent_index].personal_discounts[index].price > $scope.stores_items[parent_index].price) &&
                    ($scope.stores_items[parent_index].sale_discounts[index].sale_price <= $scope.stores_items[parent_index].personal_discounts[index].price) &&
                    ($scope.stores_items[parent_index].sale_discounts[index].sale_price < $scope.stores_items[parent_index].max_price)) {
                    if ($scope.stores_items[parent_index].sale_discounts[index].sale_price >= $scope.stores_items[parent_index].sale_price) {
                        return $scope.stores_items[parent_index].sale_price;
                    }
                    return $scope.stores_items[parent_index].sale_discounts[index].sale_price;
                } else if ($scope.stores_items[parent_index].personal_discounts[index].price > $scope.stores_items[parent_index].price &&
                    ($scope.stores_items[parent_index].personal_discounts[index].value > 0) &&
                    ($scope.stores_items[parent_index].sale_discounts[index].sale_price >= $scope.stores_items[parent_index].price) &&
                    ($scope.stores_items[parent_index].personal_discounts[index].price <= $scope.stores_items[parent_index].sale_discounts[index].sale_price) &&
                    ($scope.stores_items[parent_index].personal_discounts[index].price < $scope.stores_items[parent_index].max_price)) {
                    return $scope.stores_items[parent_index].personal_discounts[index].price;
                } else if ($scope.stores_items[parent_index].personal_discounts[index].price < $scope.stores_items[parent_index].price &&
                    ($scope.stores_items[parent_index].sale_discounts[index].sale_price > $scope.stores_items[parent_index].price) &&
                    ($scope.stores_items[parent_index].sale_discounts[index].value > 0) &&
                    ($scope.stores_items[parent_index].sale_discounts[index].sale_price < $scope.stores_items[parent_index].max_price)) {
                    if ($scope.stores_items[parent_index].sale_discounts[index].sale_price >= $scope.stores_items[parent_index].sale_price) {
                        return $scope.stores_items[parent_index].sale_price;
                    }
                    return $scope.stores_items[parent_index].sale_discounts[index].sale_price;
                } else if ($scope.stores_items[parent_index].sale_discounts[index].sale_price < $scope.stores_items[parent_index].price &&
                    ($scope.stores_items[parent_index].personal_discounts[index].price > $scope.stores_items[parent_index].price) &&
                    ($scope.stores_items[parent_index].personal_discounts[index].value > 0) &&
                    ($scope.stores_items[parent_index].personal_discounts[index].price < $scope.stores_items[parent_index].max_price)) {
                    return $scope.stores_items[parent_index].personal_discounts[index].price;
                } else if ($scope.stores_items[parent_index].sale_discounts[index].sale_price >= $scope.stores_items[parent_index].sale_price) {
                    return $scope.stores_items[parent_index].sale_price || 0;
                } else {
                    return $scope.stores_items[parent_index].sale_price || 0;
                }
            } else if ($scope.stores_items[parent_index] && (!$scope.stores_items[parent_index].sale_discounts || !$scope.stores_items[parent_index].sale_discounts[index] || !$scope.stores_items[parent_index].sale_discounts[index].sale_price) &&
                ($scope.stores_items[parent_index].personal_discounts || $scope.stores_items[parent_index].personal_discounts[index] || $scope.stores_items[parent_index].personal_discounts[index].price)) {
                if ($scope.stores_items[parent_index].personal_discounts[index].price > $scope.stores_items[parent_index].price &&
                    ($scope.stores_items[parent_index].personal_discounts[index].value > 0) &&
                    ($scope.stores_items[parent_index].personal_discounts[index].price < $scope.stores_items[parent_index].max_price)) {
                    return $scope.stores_items[parent_index].personal_discounts[index].price;
                } else {
                    return $scope.stores_items[parent_index].price || 0;
                }
            } else {
                if ($scope.stores_items[parent_index].sale_price) {
                    return $scope.stores_items[parent_index].sale_price || 0;
                }

                return $scope.stores_items[parent_index].price;
            }
        } else if ($scope.item.type == 'max_sale') {
            if ($scope.stores_items[parent_index].max_sale_price) {
                if ($scope.stores_items[parent_index].max_sale_price > $scope.stores_items[parent_index].price &&
                    ($scope.stores_items[parent_index].max_sale > 0) &&
                    ($scope.stores_items[parent_index].max_sale_price < $scope.stores_items[parent_index].max_price)) {
                    return $scope.stores_items[parent_index].max_sale_price;
                } else {
                    return $scope.stores_items[parent_index].price || 0;
                }
            } else {
                return $scope.stores_items[parent_index].price || 0;
            }
        }
    }
    // ----------------------------- END Discounts methods ------------------------------ //

    function saveFn() {

        // Get quantities of products sizes
        $scope.$broadcast('sizes:init');
        var sizesData = ProductsService.getSizesData();

        // Copy product discounts for stores
        var stores_items = HelperService.cloneObject($scope.stores_items);

        // Copy all product object to data
        var data = {};
        for (var prop in $scope.item) {
            if ($scope.item.hasOwnProperty(prop)) {
                data[prop] = $scope.item[prop];
            }
        }

        if (data.colors && data.colors.length) {
            var colors = _.map(data.colors, function(elem) {
                return {
                    id: elem.id || elem
                }
            });

            if (colors) {
                data.colors = colors;
            } else {
                delete data.colors;
            }
        }

        if (data.colors == null) {
            delete data.colors;
        }

        if (data.attributes && data.attributes.items) {
            var tempAttributes = [];
            _.each(data.attributes.items, function(elem) {
                if (elem.values) {
                    _.each(elem.values, function(prop) {
                        if (prop.value_id) {
                            elem.value_id = prop.id;
                            tempAttributes.push({
                                id: prop.id
                            });
                        }
                    })
                }
            });

            if (tempAttributes) {
                data.attributes = tempAttributes;
            } else {
                delete data.attributes;
            }
        }

        if (data.attributes == null) {
            delete data.attributes;
        }

        var media = [];

        if (data.additional_images) {
            media = _.map(data.additional_images, function(item){
                if (item && item.id) {
                    return {
                        id: item.id,
                        tag: 'additional_image'
                    };
                }
            });
        }
        if (data.thumbnail) {
            media.push({
                id: data.thumbnail.id,
                tag: 'thumbnail'
            });
        }
        data.media = media;

        // Errors
        $scope.generalErrors = false;
        $scope.pricesErrors = false;
        $scope.sizesErrors = false;
        $scope.discountsErrors = false;

        // Working with prices START
        // Set array of product prices
        var prices = sizesData.cost_price || [];

        if (prices && prices.value && prices.value > 0) {
            prices = {
                currency_id: prices.currency_id || $scope.currencies[0].id,
                value: prices.value || null
            };
        } else {
            prices = false;
        }

        if (prices && prices.value) {
            data.cost_price = prices;
        }
        // Working with prices END
        if (!data.cost_price.value || data.cost_price.value == 0) {
            delete data.cost_price;
        }

        // Set price for new products
        // if ($scope.freshprice && $scope.freshprice.value) {
        //     data.price = $scope.freshprice;
        // }
        if ($scope.freshprice && $scope.freshprice.value) {
            data.price = $scope.freshprice;
        }

        // Working with sizes START
        if (sizesData.store_sizes && sizesData.store_sizes.length) {
            var sizes = [];
            _.each(sizesData.store_sizes, function (sizeItem) {
                _.each(sizeItem.stores, function (elem) {
                    if (elem.original_quantity || elem.original_quantity == 0) {
                        var add_quantity = 0;
                        if (elem.new_quantity || elem.new_quantity == 0) {
                            add_quantity = +elem.new_quantity;
                        }

                        if (((+elem.original_quantity) + (+add_quantity)) || sizeItem.isLocked) {
                            sizes.push({
                                quantity: ((+elem.original_quantity) + (+add_quantity)),
                                store_id: elem.store_id,
                                size_id: elem.size_id
                            });
                        }
                    }
                })
            });
        }
        if (!data.sizes) {
            data.sizes = [];
        }
        data.sizes = sizes;
        // Working with sizes END

        // Working with discounts START

        _.each(stores_items, function(store, parentIndex) {
            store.markup = store.sale;
            store.sale_discount = store.sale_discount;
            store.max_sale_discount = store.max_sale;
            store.markup_price = store.max_price;

            var finalArray = _.map(store.personal_discounts, function(personalDiscount) {
                personalDiscount.personal_value = personalDiscount.value;
                return _.extend(personalDiscount, _.omit(_.findWhere(store.sale_discounts, {id: personalDiscount.id}), 'id'));
            });

            store.user_discounts = finalArray;

            return store;
        });

        var new_stores = [];
        new_stores = _.map(stores_items, function(store,parentIndex) {
            var user_discounts = _.map(store.user_discounts, function(discount,index) {
                return {
                    discount_id: +discount.id,
                    user: +discount.personal_value,
                    sale: +discount.value,
                    price: +showActualPriceFn(parentIndex,index)
                }
            });

            return {
                user_discounts: user_discounts,
                store_id: +store.store_id,
                currency_id: +store.currency_id,
                markup: +store.markup,
                sale_discount: +store.sale_discount,
                max_sale_discount: +store.max_sale_discount,
                markup_price: +store.markup_price,
                sale_price: +store.sale_price,
                max_sale_price: +store.max_sale_price
            }
        });

        data.stores = new_stores;
        // Working with discounts END

        // Remove unnesessary fields
        if (data.additional_categories) {
            delete data.additional_categories;
        }
        if (data.additional_images) {
            delete data.additional_images;
        }
        if (data.additional_media) {
            delete data.additional_media;
        }
        if (data.allAttributes) {
            delete data.allAttributes;
        }
        if (data.media_id) {
            delete data.media_id;
        }
        if (data.brand) {
            delete data.brand;
        }
        if (data.category) {
            delete data.category;
        }
        if (data.transfers) {
            delete data.transfers;
        }

        // New save START
        if ($stateParams.id) {
            var categories = [];
            // Set categories
            if (data.categories) {
                categories = _.map(data.categories, function(item) {
                    if (item) {
                        return {
                            id: item.id || item
                        }
                    }
                });
            }

            if (data.default_category_id) {
                categories.push({
                    id: data.default_category_id,
                    is_default: true
                });

                delete data.default_category_id;
            }

            data.categories = categories;

            HttpService.put('/products/'+ $stateParams.id, data)
                .success(function(response) {
                    $scope.product_id = $scope.item.id;
                    $scope.generalErrors = false;
                    $scope.discountsErrors = false;
                    $scope.pricesErrors = false;

                    // $state.go('products.edit', { id: $scope.item.id }, { reload: true });

                    $state.go('products.list', { }, { reload: true });
                    Notify.success('events.product_saved');
                })
                .error(function(errors) {
                    $scope.discountsErrors = false;
                    $scope.pricesErrors = false;
                    $scope.generalErrors = errors.errors || errors;
                    // Check and show errors for different tabs
                    checkErrors();
                    Notify.warning('events.product_error');
                })
        } else {

            var categories = [];
            // Set categories
            if (data.categories) {
                categories = _.map(data.categories, function(item) {
                    if (item) {
                        return {
                            id: item.id || item
                        }
                    }
                });
            }

            if (data.default_category_id) {
                categories.push({
                    id: data.default_category_id,
                    is_default: true
                });

                delete data.default_category_id;
            }
            data.categories = categories;

            HttpService.post('/products', data)
                .success(function(resolve) {
                    // if (resolve.id) {
                    //     $scope.product_id = resolve.id;
                    //
                    //     $state.go('products.edit', { id: resolve.id }, { reload: true });
                    // } else {
                    //     $state.go('products.list', { }, { reload: true });
                    // }

                    Notify.success('events.product_saved');
                    $state.go('products.list', { }, { reload: true });
                })
                .error(function(errors) {
                    $scope.discountsErrors = false;
                    $scope.pricesErrors = false;
                    $scope.generalErrors = errors.errors || errors;
                    // Check and show errors for different tabs
                    checkErrors();

                    Notify.warning('events.product_error');
                })
        }
        //   New save END
    }


    function checkErrors() {
        if (
            $scope.generalErrors['cost_price.currency_id'] ||
            $scope.generalErrors['cost_price.value'] ||
            $scope.generalErrors['price.value'] ||
            $scope.generalErrors['price.currency_id']) {

            $scope.pricesErrors = true;
        }

        for (var i = 0; i < 5; i++) {
            if (
                $scope.generalErrors['stores.'+ i + '.max_sale_price'] ||
                $scope.generalErrors['stores.'+ i + '.sale_discount'] ||
                $scope.generalErrors['stores.'+ i + '.sale_price']) {
                $scope.discountsErrors = true;
            }
        }
    }
}