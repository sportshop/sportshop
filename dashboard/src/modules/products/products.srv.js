angular
    .module('app.products')
    .constant('paginationConfig', {
        boundaryLinks: false,
        directionLinks: true,
        firstText: '<<',
        previousText: '<',
        nextText: '>',
        lastText: '>>',
        rotate: true
    })
    .service('ProductsService', ProductsService);


ProductsService.$inject = ['HttpService'];
function ProductsService(HttpService) {
    var sizesData;

    /**
     * Builder Types
     * @type {*[]}
     */
    var builderTypes = [
        {id: 1, key: 'equipment',  value: 'products.builder_types.equipment'},
        {id: 2, key: 'uniform', value: 'products.builder_types.uniform'}
    ];

    /**
     * Builder Positions
     * @type {*[]}
     */
    var builderPositions = [
        {id: 1, key: 'top',  value: 'products.positions.top'},
        {id: 2, key: 'center', value: 'products.positions.center'},
        {id: 2, key: 'bottom', value: 'products.positions.bottom'}
    ];

    return {
        getSizesData: getSizesDataFn,
        setSizesData: setSizesDataFn,
        getBuilderTypes: getBuilderTypesFn,
        getBuilderPositions: getBuilderPositionsFn
    };

    /**
     * Get all Sizes Data
     *
     * @returns {*}
     */
    function getSizesDataFn() {
        return sizesData;
    }

    /**
     * Set all Sizes Data
     *
     * @param data
     */
    function setSizesDataFn(data) {
        sizesData = data;
    }

    /**
     * Get all Builder types
     *
     * @returns {*}
     */
    function getBuilderTypesFn() {
        return builderTypes;
    }

    /**
     * Get all Builder positions
     *
     * @returns {*}
     */
    function getBuilderPositionsFn() {
        return builderPositions;
    }
}