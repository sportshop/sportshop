import layoutTemplate       from "../layouts/dashboard.html";
import listTemplate         from "./list.html";
import formTemplate         from "./form.html";
import generalTemplate      from "./tabs/_general.html";
import galleryTemplate      from "./tabs/_gallery.html";
import sizesTemplate        from "./tabs/_sizes.html";
import attributesTemplate   from "./tabs/_attributes.html";
import sidebarTemplate      from "./tabs/_sidebar.html";
import discountsTemplate    from "./tabs/_discounts.html";
import builderTemplate      from "./tabs/_builder.html";
import seoTemplate          from "./tabs/_seo.html";

angular
    .module('app.products', ['app.products.components'])
    .config(configure);


configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('products', {
            abstract: true,
            url: '/products',
            views: {
                '': {
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('products.list', {
            url: '?page?name?sku?status?type?category?store_id?limit',
            views: {
                'content': {
                    controller: 'ProductsController.list',
                    templateUrl: listTemplate
                }
            },
            reloadOnSearch: false,
            resolve: {
                stores: [
                    '$rootScope', 'StoresService',
                    function ($rootScope, StoresService) {
                        if (!$rootScope.globalStores) {
                            return StoresService.getList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalStores));
                    }
                ],
                items: [
                    '$rootScope', '$stateParams', 'HttpService', 'stores',
                    function ($rootScope, $stateParams, HttpService, stores) {
                        var stores;
                        if (stores && stores.data && stores.data.data) {
                            stores = stores.data.data || [];
                            $rootScope.globalStores = JSON.parse(JSON.stringify(stores.data.data));
                        } else {
                            stores = $rootScope.globalStores || [];
                        }

                        var store_id;
                        if (stores && stores.length) {
                            store_id = stores[0].id
                        }
                        return HttpService.getWParams('/products', {
                            params: {
                                paginate: true,
                                limit: 20,
                                page: $stateParams.page,
                                store_id: localStorage.getItem('active_store'), //|| store_id,
                                name: $stateParams.name,
                                sku: $stateParams.sku,
                                type: $stateParams.type,
                                category: $stateParams.category,
                                status: $stateParams.status
                            }
                        });
                    }
                ],
                categories_list: [
                    '$rootScope', 'CategoriesService',
                    function($rootScope, CategoriesService) {
                        var params = {
                            flat: true
                        };

                        if (!$rootScope.globalCategoriesFlatList) {
                            return CategoriesService.getList(params);
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalCategoriesFlatList));
                    }
                ]
            }
        })
        .state('products.create', {
            url: '/create',
            views: {
                'content': {
                    controller: 'ProductsController.edit',
                    templateUrl: formTemplate
                },
                'general@products.create': {
                    templateUrl: generalTemplate
                },
                'gallery@products.create': {
                    templateUrl: galleryTemplate
                },
                'sizes@products.create': {
                    templateUrl: sizesTemplate
                },
                'attributes@products.create': {
                    templateUrl: attributesTemplate
                },
                'discounts@products.create': {
                    templateUrl: discountsTemplate
                },
                'builder@products.create': {
                    templateUrl: builderTemplate
                },
                'seo@products.create': {
                    templateUrl: seoTemplate
                },
                'sidebar@products.create': {
                    templateUrl: sidebarTemplate
                }
            },
            resolve: {
                item: [
                    function () {
                        return {}
                    }
                ],
                settings : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalSettings) {
                            return SettingsService.getSettingsList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalSettings));
                    }
                ],
                product_quantities: [
                    function () {
                        return { data: {} }
                    }
                ],
                product_default_sizes: [
                    function() {
                        return { data: {} }
                    }
                ],
                categories_list: [
                    '$rootScope', 'CategoriesService',
                    function($rootScope, CategoriesService) {
                        var params = {
                            flat: true
                        };

                        if (!$rootScope.globalCategoriesFlatList) {
                            return CategoriesService.getList(params);
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalCategoriesFlatList));
                    }
                ],
                sizes: [
                    '$rootScope', 'HttpService',
                    function ($rootScope, HttpService) {
                        if (!$rootScope.globalSizes) {
                            return HttpService.get('/sizes');
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalSizes));
                    }
                ],
                stores: [
                    '$rootScope', 'StoresService',
                    function ($rootScope, StoresService) {
                        if (!$rootScope.globalStores) {
                            return StoresService.getList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalStores));
                    }
                ],
                currencies : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalCurrencies) {
                            return SettingsService.getCurrenciesList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalCurrencies));
                    }
                ],
                discounts : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalDiscounts) {
                            return SettingsService.getDiscountsList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalDiscounts));
                    }
                ],
                attributes : [
                    '$rootScope', 'AttributesService',
                    function($rootScope, AttributesService) {
                        if (!$rootScope.globalAttributes) {
                            return AttributesService.getList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalAttributes));
                    }
                ],
                colors : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalColors) {
                            return SettingsService.getColorsList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalColors));
                    }
                ],
                brands : [
                    '$rootScope', 'BrandsService',
                    function($rootScope, BrandsService) {
                        if (!$rootScope.globalBrands) {
                            return BrandsService.getList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalBrands));
                    }
                ]
            }
        })
        .state('products.edit', {
            url: '/:id?store_id',
            views: {
                'content': {
                    controller: 'ProductsController.edit as ctrl',
                    templateUrl: formTemplate
                },
                'general@products.edit': {
                    templateUrl: generalTemplate
                },
                'gallery@products.edit': {
                    templateUrl: galleryTemplate
                },
                'sizes@products.edit': {
                    templateUrl: sizesTemplate
                },
                'attributes@products.edit': {
                    templateUrl: attributesTemplate
                },
                'discounts@products.edit': {
                    templateUrl: discountsTemplate
                },
                'builder@products.edit': {
                    templateUrl: builderTemplate
                },
                'seo@products.edit': {
                    templateUrl: seoTemplate
                },
                'sidebar@products.edit': {
                    templateUrl: sidebarTemplate
                }
            },
            resolve: {
                item: [
                    '$stateParams', 'HttpService',
                    function ($stateParams, HttpService) {
                        return HttpService.get('/products/'+ $stateParams.id);
                    }
                ],
                settings : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalSettings) {
                            return SettingsService.getSettingsList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalSettings));
                    }
                ],
                product_quantities: [
                    function () {
                        return { data: {} }
                    }
                ],
                sizes: [
                    '$rootScope', 'HttpService',
                    function ($rootScope, HttpService) {
                        if (!$rootScope.globalSizes) {
                            return HttpService.get('/sizes');
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalSizes));
                    }
                ],
                product_default_sizes: [
                    '$stateParams', 'HttpService', 'item',
                    function ($stateParams, HttpService, item) {
                        if (item && item.data && item.data.sizes && item.data.sizes.group && item.data.sizes.group.id) {
                            return HttpService.get('/sizes/'+ item.data.sizes.group.id);
                        } else {
                            return { data: {} };
                        }
                    }
                ],
                categories_list: [
                    '$rootScope', 'CategoriesService',
                    function($rootScope, CategoriesService) {
                        var params = {
                            flat: true
                        };

                        if (!$rootScope.globalCategoriesFlatList) {
                            return CategoriesService.getList(params);
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalCategoriesFlatList));
                    }
                ],
                stores: [
                    '$rootScope', 'StoresService',
                    function ($rootScope, StoresService) {
                        if (!$rootScope.globalStores) {
                            return StoresService.getList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalStores));
                    }
                ],
                currencies : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalCurrencies) {
                            return SettingsService.getCurrenciesList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalCurrencies));
                    }
                ],
                discounts : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalDiscounts) {
                            return SettingsService.getDiscountsList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalDiscounts));
                    }
                ],
                attributes : [
                    '$rootScope', 'AttributesService',
                    function($rootScope, AttributesService) {
                        if (!$rootScope.globalAttributes) {
                            return AttributesService.getList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalAttributes));
                    }
                ],
                colors : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalColors) {
                            return SettingsService.getColorsList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalColors));
                    }
                ],
                brands : [
                    '$rootScope', 'BrandsService',
                    function($rootScope, BrandsService) {
                        if (!$rootScope.globalBrands) {
                            return BrandsService.getList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalBrands));
                    }
                ]
            }
        });
}