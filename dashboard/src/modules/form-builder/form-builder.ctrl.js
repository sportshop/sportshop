angular
    .module('app.form_builder')

    .controller('FormBuilderController.list', FormBuilderController_List)
    .controller('FormBuilderController.edit', FormBuilderController_Edit);


FormBuilderController_List.$inject = ['$state', 'BreadcrumbsService', 'MainHeaderService', 'FormBuilderService', 'Notify', 'items'];
function FormBuilderController_List($state, BreadcrumbsService, MainHeaderService, FormBuilderService, Notify, items) {
    BreadcrumbsService.push('form_builder.title.list');
    MainHeaderService.set('form_builder.title.list');

    var vm = this;

    // Form Builder Model
    vm.list = items.data.pages || [];

    vm.remove = removeFn;
    vm.changePage = changePageFn;

    // Delete form builder
    function removeFn(item) {
        Notify.confirm(function () {
            FormBuilderService.delete(item.id)
                .success(function(response) {
                    var index = vm.list.data.indexOf(item);
                    vm.list.data.splice(index, 1);
                    Notify.success('general.removed');
                })
                .error(function(errors) {
                    Notify.warning(errors.message);
                });
        }, 'general.are_you_sure')
    }

    // Change page
    function changePageFn() {
        FormBuilderService.getList($stateParams.page)
            .success(function(response) {
                $state.go($state.current, params, { notify: false });
                vm.list = response.pages || {}; //TODO: modified to form-builder
            });
    }
}

FormBuilderController_Edit.$inject = ['$state', '$stateParams', 'BreadcrumbsService', 'MainHeaderService', 'FormBuilderService', 'Notify', 'item'];
function FormBuilderController_Edit($state, $stateParams, BreadcrumbsService, MainHeaderService, FormBuilderService, Notify, item) {
    BreadcrumbsService.push('form_builder.title.list', 'form_builder.list');

    var vm = this;

    // Form builder Model
    vm.item = item.data.page || {}; //TODO: modified to form_builder

    if (_.isEmpty(vm.item)) {
        BreadcrumbsService.push('form_builder.title.create');
        MainHeaderService.set('form_builder.title.create');
    } else {
        BreadcrumbsService.push('form_builder.title.edit');
        MainHeaderService.set('form_builder.title.edit');
    }

    vm.save = saveFn;

    // Update or create form builder
    function saveFn() {
        if ($stateParams.id) {
            FormBuilderService.update(vm.item.id, vm.item)
                .success(function(response) {
                    Notify.success('general.updated');
                    $state.go('form_builder.list');
                })
                .error(function(errors) {
                    vm.errors = errors.errors;
                });
        } else {
            FormBuilderService.save(vm.item)
                .success(function(response) {
                    Notify.success('general.saved');
                    $state.go('form_builder.list');
                })
                .error(function(errors) {
                    vm.errors = errors.errors;
                });
        }
    }
}