angular
    .module('app.form_builder')

    .service('FormBuilderService', FormBuilderService);


FormBuilderService.$inject = ['HttpService'];
function FormBuilderService(HttpService) {
    var baseUrl = '/users';

    return {
        getList:        getListFn,
        get:            getFn,
        save:           saveFn,
        update:         updateFn,
        delete:         deleteFn
    };

    /**
     * Get list
     * @param currentPage
     * @returns {*}
     */
    function getListFn(currentPage) {
        return HttpService.getWParams(baseUrl, {
            params: {
                page: currentPage || 1
            }
        });
    }

    /**
     * Get form-builder
     * @param id
     * @returns {*}
     */
    function getFn(id) {
        return HttpService.get(baseUrl+'/'+ id)
    }

    /**
     * Save form-builder
     * @param data
     * @returns {*}
     */
    function saveFn(data) {
        return HttpService.post(baseUrl, data)
    }

    /**
     * Update form-builder
     * @param id
     * @param data
     * @returns {*}
     */
    function updateFn(id, data) {
        return HttpService.put(baseUrl+'/'+ id, data)
    }

    /**
     * Delete form-builder
     * @param id
     * @returns {*}
     */
    function deleteFn(id) {
        return HttpService.delete(baseUrl+'/'+ id)
    }
}