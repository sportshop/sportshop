import layoutTemplate from "../layouts/dashboard.html";
import listTemplate from "./list.html";
import formTemplate from "./form.html";
import generalTemplate from "./_general.html";
import seoTemplate from "./_seo.html";
import sidebarTemplate from "./_sidebar.html"

angular
    .module('app.form_builder', [])
    .config(configure);

configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('form_builder', {
            abstract: true,
            url: '/form-builder',
            views: {
                '': {
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('form_builder.list', {
            url: '?page',
            views: {
                'content': {
                    controller: 'FormBuilderController.list as ctrl',
                    templateUrl: listTemplate
                }
            },
            resolve: {
                items: [
                    '$stateParams', 'FormBuilderService',
                    function ($stateParams, FormBuilderService) {
                        return FormBuilderService.getList($stateParams.page);
                    }
                ]
            }
        })
        .state('form_builder.create', {
            url: '/create',
            views: {
                'content': {
                    controller: 'FormBuilderController.edit as ctrl',
                    templateUrl: formTemplate
                },
                'general@form_builder.create': {
                    templateUrl: generalTemplate
                },
                'seo@form_builder.create': {
                    templateUrl: seoTemplate
                },
                'sidebar@form_builder.create': {
                    templateUrl: sidebarTemplate
                }
            },
            resolve: {
                item: [
                    function () {
                        return {
                            data: {
                                page: {}
                            }
                        };
                    }
                ]
            }
        })
        .state('form_builder.edit', {
            url: '/:id',
            views: {
                'content': {
                    controller: 'FormBuilderController.edit as ctrl',
                    templateUrl: formTemplate
                },
                'general@form_builder.edit': {
                    templateUrl: generalTemplate
                },
                'seo@form_builder.edit': {
                    templateUrl: seoTemplate
                },
                'sidebar@form_builder.edit': {
                    templateUrl: sidebarTemplate
                }
            },
            resolve: {
                item: [
                    '$stateParams', 'FormBuilderService',
                    function ($stateParams, FormBuilderService) {
                        return FormBuilderService.get($stateParams.id);
                    }
                ]
            }
        });
}