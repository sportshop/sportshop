angular
    .module('app.users')
    .service('UsersService', UsersService);


UsersService.$inject = ['HttpService'];
function UsersService(HttpService) {
    var baseUrl = '/users';

    // Default rate by date
    var rateByDate = [
        {key: 'monthly', value: 'users.attribute.label.month'},
        {key: 'daily',   value: 'users.attribute.label.day'},
        {key: 'hourly',  value: 'users.attribute.label.hour'}
    ];

    // Default Roles
    var roles = [
        {role: 'admin',           title: 'Адміністратор'},
        {role: 'manager',         title: 'Менеджер'},
        {role: 'member',          title: 'Клієнт'},
        {role: 'limited_manager', title: 'Менеджер 2'}
    ];

    // Default Discounts
    var discounts = [
        {id: 1, name: 'A'},
        {id: 2, name: 'B'},
        {id: 3, name: 'C'},
        {id: 4, name: 'D'},
        {id: 5, name: 'E'}
    ];

    return {
        getList:       getListFn,
        get:           getFn,
        save:          saveFn,
        update:        updateFn,
        delete:        deleteFn,
        getRateByDate: getRateByDateFn,
        getRoles:      getRolesFn,
        getDiscounts:  getDiscountsFn
    };

    /**
     * Get users list
     *
     * @param params
     *
     * @returns {*}
     */
    function getListFn(params) {
        return HttpService.getWParams(baseUrl, { params: params });
    }

    /**
     * Get user
     *
     * @param id
     *
     * @returns {*}
     */
    function getFn(id) {
        return HttpService.get(baseUrl +'/'+ id)
    }

    /**
     * Save user
     *
     * @param data
     *
     * @returns {*}
     */
    function saveFn(data) {
        return HttpService.post(baseUrl, data)
    }

    /**
     * Update user
     *
     * @param id
     * @param data
     *
     * @returns {*}
     */
    function updateFn(id, data) {
        return HttpService.put(baseUrl +'/'+ id, data)
    }

    /**
     * Delete user
     *
     * @param id
     *
     * @returns {*}
     */
    function deleteFn(id) {
        return HttpService.delete(baseUrl +'/'+ id)
    }

    /**
     * Get rate by date
     *
     * @returns {*[]}
     */
    function getRateByDateFn() {
        return rateByDate;
    }

    /**
     * Get roles
     *
     * @returns {*[]}
     */
    function getRolesFn() {
        return roles;
    }

    /**
     * Get discounts
     *
     * @returns {*[]}
     */
    function getDiscountsFn() {
        return discounts;
    }
}