import layoutTemplate       from "../layouts/dashboard.html";
import listTemplate         from "./list.html";
import formTemplate         from "./form.html";
import generalTemplate      from "./_general.html";
import userSettingsTemplate from "./_type_settings.html";

angular
    .module('app.users', [])
    .config(configure);


configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('users', {
            abstract: true,
            url: '/users',
            views: {
                '': {
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('users.list', {
            url: '?page?name?purchased_orders_price_min?purchased_orders_price_max?created_at?role?discount_id?purchased_orders_price_currency_id',
            views: {
                'content': {
                    controller: 'UsersController.list as ctrl',
                    templateUrl: listTemplate
                }
            },
            reloadOnSearch: false,
            resolve: {
                items: [
                    '$stateParams', '$rootScope', 'UsersService',
                    function($stateParams, $rootScope, UsersService) {
                        var params = {
                                paginate:  true,
                                limit:     50,
                                page:      $stateParams.page,
                                name:      $stateParams.name,
                                purchased_orders_price_max: $stateParams.purchased_orders_price_max,
                                purchased_orders_price_min: $stateParams.purchased_orders_price_min ,
                                created_at:$stateParams.created_at,
                                role:      $stateParams.role,
                                discount_id:  $stateParams.discount_id,
                                purchased_orders_price_currency_id : $rootScope.defaultCurrencyId
                            };

                        return UsersService.getList(params);
                    }
                ]
            }
        })
        .state('users.create', {
            url: '/create',
            views: {
                'content': {
                    controller: 'UsersController.edit as ctrl',
                    templateUrl: formTemplate
                },
                'general@users.create': {
                    templateUrl: generalTemplate
                },
                'user_settings@users.create': {
                    templateUrl: userSettingsTemplate
                }
            },
            resolve: {
                item: [
                    function() {
                        return {
                            data: {
                                user: {}
                            }
                        }
                    }
                ],
                discounts : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalDiscounts) {
                            return SettingsService.getDiscountsList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalDiscounts));
                    }
                ],
                stores: [
                    '$rootScope', 'StoresService',
                    function ($rootScope, StoresService) {
                        if (!$rootScope.globalStores) {
                            return StoresService.getList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalStores));
                    }
                ],
                currencies : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalCurrencies) {
                            return SettingsService.getCurrenciesList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalCurrencies));
                    }
                ]
            }
        })
        .state('users.edit', {
            url: '/:id',
            views: {
                'content': {
                    controller: 'UsersController.edit as ctrl',
                    templateUrl: formTemplate
                },
                'general@users.edit': {
                    templateUrl: generalTemplate
                },
                'user_settings@users.edit': {
                    templateUrl: userSettingsTemplate
                }
            },
            resolve: {
                item: [
                    '$stateParams', 'UsersService',
                    function($stateParams, UsersService) {
                        return UsersService.get($stateParams.id);
                    }
                ],
                discounts : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalDiscounts) {
                            return SettingsService.getDiscountsList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalDiscounts));
                    }
                ],
                stores: [
                    '$rootScope', 'StoresService',
                    function ($rootScope, StoresService) {
                        if (!$rootScope.globalStores) {
                            return StoresService.getList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalStores));
                    }
                ],
                currencies : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalCurrencies) {
                            return SettingsService.getCurrenciesList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalCurrencies));
                    }
                ]
            }
        });
}