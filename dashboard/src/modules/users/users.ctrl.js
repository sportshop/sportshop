angular
    .module('app.users')
    .controller('UsersController.list', UsersController_List)
    .controller('UsersController.edit', UsersController_Edit);


UsersController_List.$inject = ['$rootScope', '$state', '$stateParams', 'BreadcrumbsService', 'MainHeaderService', 'UsersService', 'Notify', 'items'];
function UsersController_List($rootScope, $state, $stateParams, BreadcrumbsService, MainHeaderService, UsersService, Notify, items) {
    BreadcrumbsService.push('users.title.list');
    MainHeaderService.set('users.title.list');

    var vm = this;

    vm.list = items.data || {}; // Users Model
    transformList(vm.list.data);

    vm.listPerPage = [
        {key: 10, value: 10},
        {key: 20, value: 20},
        {key: 50, value: 50},
        {key: 100, value: 100},
    ];

    function transformList(list) {
        _.each(list, function(item) {
            if (item.purchased_orders_price && item.purchased_orders_price.length != 0) {
                item.purchased_orders_price = _.find(item.purchased_orders_price, function(el) {
                    return el.currency_id == ($rootScope.defaultCurrencyId);
                });
            }

            return item;
        });

        return list;
    }

    // Filter
    vm.searchForm = {
        name:      $stateParams.name || null,
        purchased_orders_price_max: $stateParams.purchased_orders_price_max || null,
        purchased_orders_price_min: $stateParams.purchased_orders_price_min || null,
        created_at:$stateParams.created_at || null,
        role:      $stateParams.role,
        discount_id:  $stateParams.discount || null,
        purchased_orders_price_currency_id : $rootScope.defaultCurrencyId,
        limit: 50
    };

    vm.roles = UsersService.getRoles();
    vm.discounts = UsersService.getDiscounts();

    vm.remove = removeFn;
    vm.changePage = changePageFn;
    vm.processSearch = processSearchFn;

    /**
     * Change Filter
     */
    function processSearchFn() {
        var params = {
            paginate:  true,
            limit:     vm.searchForm.limit,
            name:      vm.searchForm.name,
            purchased_orders_price_max: vm.searchForm.purchased_orders_price_max,
            purchased_orders_price_min: vm.searchForm.purchased_orders_price_min,
            created_at:vm.searchForm.created_at,
            role:      vm.searchForm.role,
            discount_id:  vm.searchForm.discount_id,
            store_id:  localStorage.getItem('active_store'),
            page:      null,
            purchased_orders_price_currency_id : $rootScope.defaultCurrencyId
        };

        UsersService.getList(params)
            .success(function(response){
                vm.list = response || {};
                transformList(vm.list.data);
                $state.go($state.current, params, {notify: false});
            });
    }

    /**
     * Show role
     * @param name
     * @returns {*}
     */
    vm.showRole = function(name) {
        switch(name) {
            case 'member':
                return 'Клієнт';
                break;
            case 'manager':
                return 'Менеджер';
                break;
            case 'limited_manager':
                return 'Менеджер 2';
                break;
            case 'admin':
                return 'Адміністратор';
                break;
            default:
                return '???';
        }
    };

    /**
     * Remove user
     * @param item
     */
    function removeFn(item) {
        Notify.confirm(function() {
            UsersService.delete(item.id)
                .success(function(response) {
                    if (vm.list.current_page > 1 && vm.list.data.length == 1) {
                        $state.go($state.current, {page: +vm.list.current_page - 1}, {reload: true});
                    } else {
                        $state.go($state.current, {}, {reload: true});
                    }

                    Notify.success('general.removed');
                })
                .error(function(errors) {
                    Notify.warning(errors.message);
                });
        }, 'general.are_you_sure')
    }

    /**
     * Change page
     */
    function changePageFn() {
            var params = {
                paginate:  true,
                limit:     vm.searchForm.limit,
                name:      vm.searchForm.name,
                purchased_orders_price_max: vm.searchForm.purchased_orders_price_max,
                purchased_orders_price_min: vm.searchForm.purchased_orders_price_min,
                created_at:vm.searchForm.created_at,
                role:      vm.searchForm.role,
                discount_id:  vm.searchForm.discount_id,
                page:      vm.list.current_page,
                store_id:  localStorage.getItem('active_store'),
                purchased_orders_price_currency_id : $rootScope.defaultCurrencyId
            };

        UsersService.getList(params)
                .success(function(response){
                    vm.list = response || {};
                    transformList(vm.list.data);
                    $state.go($state.current, params, { notify: false });
                });
    }
}

UsersController_Edit.$inject = ['$rootScope', '$state', '$stateParams', 'BreadcrumbsService', 'MainHeaderService', 'UsersService', 'Notify', 'item', 'discounts', 'stores', 'currencies'];
function UsersController_Edit($rootScope, $state, $stateParams, BreadcrumbsService, MainHeaderService, UsersService, Notify, item, discounts, stores, currencies) {
    BreadcrumbsService.push('users.title.list', 'users.list');

    var vm = this;

    vm.options_rate_by_date = UsersService.getRateByDate(); // [options] Rate by date

    // Discounts list
    if (discounts && discounts.data) {
        vm.discounts = discounts.data || [];
        $rootScope.globalDiscounts = JSON.parse(JSON.stringify(discounts.data));
    } else {
        vm.discounts = discounts || [];
    }

   // Stores List
    if (stores && stores.data) {
        vm.stores = stores.data || [];
        $rootScope.globalStores = JSON.parse(JSON.stringify(stores.data));
    } else {
        vm.stores = stores || [];
    }

    // Currencies list
    if (currencies && currencies.data) {
        vm.currencies = currencies.data || [];
        $rootScope.globalCurrencies = JSON.parse(JSON.stringify(currencies.data));
    } else {
        vm.currencies = currencies || [];
    }

    if ($stateParams.id) {
        BreadcrumbsService.push('users.title.edit');
        MainHeaderService.set('users.title.edit');

        vm.item = item.data || {}; // User Model

        if (vm.item.role_name == 'limited_manager') {
            vm.item.role_name = 'manager';
            vm.item.manager_access_type = 'limited_manager';
        } else if (vm.item.role_name == 'manager') {
            vm.item.manager_access_type = 'manager';
        }

        if (!vm.item.sale_percentages) {
            vm.item.sale_percentages = _.map(vm.discounts, function(item) {
                return {
                    title: item.title,
                    discount_id: item.id,
                    value: item.value
                }
            });

            vm.item.sale_percentages.unshift({
                title: '',
                value: ''
            });
        } else {
            var sale_percentages = _.map(vm.discounts, function(item) {
                return {
                    title: item.title,
                    discount_id: item.id,
                    value: item.value
                }
            });

            sale_percentages.unshift({
                title: '',
                value: ''
            });

            _.each(sale_percentages, function(defaultDiscount) {
                _.each(vm.item.sale_percentages, function(customSalePercentage) {
                    if (defaultDiscount.value !== customSalePercentage.value &&
                        defaultDiscount.discount_id == customSalePercentage.discount_id) {
                        defaultDiscount.value = customSalePercentage.value;
                    } else if (defaultDiscount.value !== customSalePercentage.value &&
                        !defaultDiscount.discount_id && !customSalePercentage.discount_id) {
                        defaultDiscount.value = customSalePercentage.value;
                    }

                    return customSalePercentage;
                })
            });

            vm.item.sale_percentages = sale_percentages;
        }

        if (vm.item.data && vm.item.data.rate_type) {
            vm.item.data.rate_type = vm.item.data.rate_type.toString();
        }

        if (vm.item.data && vm.item.data.rate) {
            vm.item.data.rate = _.find(vm.item.data.rate, function (el) {
                return el.is_default;
            });
        }

        if (vm.item.discount && vm.item.discount.id) {
            if (vm.item && !vm.item.data) {
                vm.item.data = {};
            }
            vm.item.data.discount_id = vm.item.discount.id;
        }
    } else {
        BreadcrumbsService.push('users.title.create');
        MainHeaderService.set('users.title.create');

        // User Model
        vm.item = {
            role_name: 'member',
            data: {
                rate_type: 'monthly',
                discount_id: (vm.discounts.length) ? vm.discounts[0].id : null,
                rate: {
                    currency_id: (vm.currencies && vm.currencies.length) ? vm.currencies[0].id : null
                },
                is_show_sizes: true
            }
        };

        vm.item.sale_percentages = _.map(vm.discounts, function(item) {
            return {
                title: item.title,
                discount_id: item.id,
                value: item.value
            }
        });

        vm.item.sale_percentages.unshift({
            title: '',
            value: ''
        });
    }

    vm.save = saveFn;

    /**
     * Save or update user
     */
    function saveFn() {
        var data = {};

        // Copy all user's data
        for (var prop in vm.item) {
            if (vm.item.hasOwnProperty(prop)) {
                data[prop] = vm.item[prop];
            }
        }

        var tempSalePercentages = [];
        _.each(vm.discounts, function(defaultDiscount) {
            _.each(data.sale_percentages, function(customSalePercentage) {
                if (defaultDiscount.id == customSalePercentage.discount_id
                    && defaultDiscount.value != customSalePercentage.value) {
                    customSalePercentage.isChange = true;
                } else if (!customSalePercentage.discount_id && customSalePercentage.value) {
                    customSalePercentage.isChange = true;
                }

            })
        });

        _.each(data.sale_percentages, function(elem) {
                tempSalePercentages.push({
                    value: elem.value,
                    discount_id: elem.discount_id,
                });
        });
        data.sale_percentages = tempSalePercentages;

        if ($stateParams.id) { // Update
            if (data.role_name && data.role_name == 'manager' && data.manager_access_type == 'limited_manager') {
                data.role_name = 'limited_manager';
                data.data.is_show_sizes = false;
            }

            if (data.role_name == 'member') {
                delete data.data.rate;
                delete data.data.rate_type;
                delete data.sale_percentages;

            } else if (data.role_name == 'manager' || data.role_name == 'limited_manager') {
                delete data.data.discount_id;
                data.data.is_show_sizes = false;
            }

            if (data.data && data.data.rate) {
                if (data.data.rate.code) {
                    delete data.data.rate.code;
                }

                if (data.data.rate.coefficient) {
                    delete data.data.rate.coefficient;
                }

                if (data.data.rate.symbol) {
                    delete data.data.rate.symbol;
                }
            }


            UsersService.update(vm.item.id, data)
                .success(function(response) {
                    vm.generalErrors = false;
                    vm.userSettingsErrors = false;

                    if ($rootScope.currentUser.user.id === vm.item.id) {
                        $rootScope.currentUser.user.first_name = vm.item.first_name;
                        $rootScope.currentUser.user.last_name = vm.item.last_name;
                    }

                    Notify.success('general.updated');
                    $state.go('users.list');
                })
                .error(function(errors) {
                    vm.errors = errors.errors;
                    vm.generalErrors = false;
                    vm.userSettingsErrors = false;

                    if (vm.errors['data.company_name'] || vm.errors['first_name'] ||
                        vm.errors['last_name'] || vm.errors['email'] || vm.errors['phone'] ||
                        vm.errors['data.store_number']) {
                        vm.generalErrors = true;
                    } else {
                        vm.userSettingsErrors = true;
                    }
                });
        } else { // Save
            if (data.manager_access_type == 'limited_manager') {
                data.role_name = 'limited_manager';
            }

            if (data.role_name == 'member') {
                delete data.data.rate;
                delete data.data.rate_type;
                delete data.sale_percentages;

                data.data.is_show_sizes = false;
            } else if (data.role_name == 'manager' || data.role_name == 'limited_manager') {
                delete data.data.discount_id;
            }

            UsersService.save(data)
                .success(function (response) {
                    vm.generalErrors = false;
                    vm.userSettingsErrors = false;

                    Notify.success('general.saved');
                    $state.go('users.list');
                })
                .error(function(errors) {
                    vm.errors = errors.errors;
                    vm.generalErrors = false;
                    vm.userSettingsErrors = false;

                    if (vm.errors['data.company_name'] || vm.errors['first_name'] ||
                        vm.errors['last_name'] || vm.errors['email'] || vm.errors['phone'] ||
                        vm.errors['data.store_number']) {
                        vm.generalErrors = true;
                    } else {
                        vm.userSettingsErrors = true;
                    }
                });
        }
    }
}