import layoutTemplate from "../layouts/dashboard.html";
import listTemplate   from "./list.html";
import formTemplate   from "./form.html";

angular
    .module('app.brands', [])
    .config(configure);


configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('brands', {
            abstract: true,
            url: '/brands',
            views: {
                '': {
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('brands.list', {
            url: '',
            views: {
                'content': {
                    controller: 'BrandsController.list as ctrl',
                    templateUrl: listTemplate
                }
            },
            resolve: {
                items: [
                    '$rootScope', 'BrandsService',
                    function($rootScope, BrandsService) {
                        if (!$rootScope.globalBrands) {
                            return BrandsService.getList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalBrands));
                    }
                ]
            }
        })
        .state('brands.create', {
            url: '/create',
            views: {
                'content': {
                    controller: 'BrandsController.edit as ctrl',
                    templateUrl: formTemplate
                }
            },
            resolve: {
                item: [
                    function() {
                        return {
                            data: {
                                brand: {}
                            }
                        };
                    }
                ]
            }
        })
        .state('brands.edit', {
            url: '/:id',
            views: {
                'content': {
                    controller: 'BrandsController.edit as ctrl',
                    templateUrl: formTemplate
                }
            },
            resolve: {
                item: [
                    '$stateParams', 'BrandsService',
                    function($stateParams, BrandsService) {
                        return BrandsService.get($stateParams.id);
                    }
                ]
            }
        });
}