angular
    .module('app.brands')
    .service('BrandsService', BrandsService);


BrandsService.$inject = ['HttpService'];
function BrandsService(HttpService) {
    var baseUrl = '/brands';

    return {
        getList: getListFn,
        get:     getFn,
        delete:  deleteFn,
        save:    saveFn,
        update:  updateFn
    };

    /**
     * Get brand list
     *
     * @returns {*}
     */
    function getListFn() {
        return HttpService.get(baseUrl);
    }

    /**
     * Get brand
     *
     * @param id
     *
     * @returns {*}
     */
    function getFn(id) {
        return HttpService.get(baseUrl +'/'+ id);
    }

    /**
     * Remove brand
     *
     * @param id
     *
     * @returns {*}
     */
    function deleteFn(id) {
        return HttpService.delete(baseUrl +'/'+ id);
    }

    /**
     * Save brand
     *
     * @param data
     *
     * @returns {*}
     */
    function saveFn(data) {
        return HttpService.post(baseUrl, data);
    }

    /**
     * Update brand
     *
     * @param id
     * @param data
     *
     * @returns {*}
     */
    function updateFn(id, data) {
        return HttpService.put(baseUrl +'/'+ id, data);
    }
}