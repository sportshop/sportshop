angular
    .module('app.brands')
    .controller('BrandsController.list', BrandsController_List)
    .controller('BrandsController.edit', BrandsController_Edit);


BrandsController_List.$inject = ['$rootScope', 'BreadcrumbsService', 'MainHeaderService', 'BrandsService', 'Notify', 'items'];
function BrandsController_List($rootScope, BreadcrumbsService, MainHeaderService, BrandsService, Notify, items) {
    BreadcrumbsService.push('brands.title.list');
    MainHeaderService.set('brands.title.list');

    var vm = this;

    // Brand List
    if (items && items.data) {
        vm.list = items.data || [];
        $rootScope.globalBrands = JSON.parse(JSON.stringify(items.data));
    } else {
        vm.list = items || [];
        $rootScope.globalBrands = items || [];
    }

    vm.remove = removeFn;

    /**
     * Remove brand
     *
     * @param item
     */
    function removeFn(item) {
        Notify.confirm(function() {
            BrandsService.delete(item.id)
                .success(function(response) {
                    var index = $rootScope.globalBrands.indexOf(item);
                    $rootScope.globalBrands.splice(index, 1);

                    Notify.success('general.removed');
                })
                .error(function(errors) {
                    Notify.warning(errors.message);
                });
        }, 'general.are_you_sure')
    }
}

BrandsController_Edit.$inject = ['$state', '$stateParams', 'BreadcrumbsService', 'MainHeaderService', 'BrandsService', 'Notify', 'item'];
function BrandsController_Edit($state, $stateParams, BreadcrumbsService, MainHeaderService, BrandsService, Notify, item) {
    BreadcrumbsService.push('brands.title.list', 'brands.list');

    if ($stateParams.id) {
        BreadcrumbsService.push('brands.title.edit');
        MainHeaderService.set('brands.title.edit');
    } else {
        BreadcrumbsService.push('brands.title.create');
        MainHeaderService.set('brands.title.create');
    }

    var vm = this;

    // Brand Model
    vm.item = item.data || {};

    vm.save = saveFn;

    /**
     * Save or update brand
     */
    function saveFn() {
        if (vm.item.id) {
            BrandsService.update(vm.item.id, vm.item)
                .success(function() {
                    $state.go('brands.list');
                    Notify.success('general.updated')
                })
                .error(function(errors) {
                    vm.errors = errors.errors;
                });
        } else {
            BrandsService.save(vm.item)
                .success(function() {
                    $state.go('brands.list');
                    Notify.success('general.saved')
                })
                .error(function(errors) {
                    vm.errors = errors.errors;
                });
        }
    }
}