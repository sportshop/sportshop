import layoutTemplate from "../layouts/dashboard.html";
import formTemplate from "./form.html";
import salesTemplate from "./_sales.html";
import salaryTemplate from "./_salary.html";
import earningsTemplate from "./_earnings.html";
import pendingOrdersTemplate from "./_pending_orders.html";
import movingProductsTemplate from "./_moving_products.html";
import costsTemplate from "./_costs.html";

angular
    .module('app.statistics', ['chart.js'])
    .config(configure);


configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('statistics', {
            abstract   : true,
            url        : '/statistics',
            views : {
                '' : {
                    controller : 'StatisticsController.main as ctrl',
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('statistics.sales', {
            url  : '/sales',
            views: {
                'content': {
                    controller : 'StatisticsController.sales as ctrl',
                    templateUrl: formTemplate
                },
                'sales@statistics.sales': {
                    templateUrl: salesTemplate
                }
            },
            resolve: {
                customers: [
                    'OrdersService',
                    function(OrdersService) {
                        return OrdersService.getCustomerList();
                    }
                ],
                users: [
                    'OrdersService',
                    function(OrdersService) {
                        return OrdersService.getUserList();
                    }
                ],
                statistics: [
                    'StatisticsService', 'HelperService', '$stateParams',
                    function(StatisticsService, HelperService, $stateParams) {
                        // Helpers for init date filter
                        var date = new Date(), y = date.getFullYear(), m = date.getMonth();

                        return StatisticsService.getSalesList({
                            params: {
                                type: $stateParams.type || 'orders',
                                date: $stateParams.date || HelperService.getCurrentMonth(),
                                manager_id: $stateParams.manager_id || null,
                                customer_id: $stateParams.customer_id || null,
                                store_id: localStorage.getItem('active_store')
                            }
                        });
                    }
                ]
            }
        })
        .state('statistics.salary', {
            url  : '/salary',
            views: {
                'content': {
                    controller : 'StatisticsController.salary as ctrl',
                    templateUrl: formTemplate
                },
                'salary@statistics.salary': {
                    templateUrl: salaryTemplate
                }
            },
            resolve: {
                customers: [
                    'OrdersService',
                    function (OrdersService) {
                        return OrdersService.getCustomerList();
                    }
                ],
                users: [
                    'OrdersService',
                    function (OrdersService) {
                        return OrdersService.getUserList();
                    }
                ],
                statistics: [
                    '$stateParams', '$filter', 'StatisticsService', 'HelperService',
                    function($stateParams, $filter, StatisticsService, HelperService) {
                        // Helpers for init date filter
                        var date = new Date(), y = date.getFullYear(), m = date.getMonth();

                        return StatisticsService.getSalaryList({
                            params: {
                                date: $stateParams.date || HelperService.getCurrentMonth(),
                                manager_id: $stateParams.manager_id,
                                store_id: localStorage.getItem('active_store')
                            }
                        });
                    }
                ]
            }
        })
        .state('statistics.earnings', {
            url  : '/earnings',
            views: {
                'content': {
                    controller : 'StatisticsController.earnings as ctrl',
                    templateUrl: formTemplate
                },
                'earnings@statistics.earnings': {
                    templateUrl: earningsTemplate
                }
            },
            resolve: {
                customers: [
                    'OrdersService',
                    function(OrdersService) {
                        return OrdersService.getCustomerList();
                    }
                ],
                users: [
                    'OrdersService',
                    function(OrdersService) {
                        return OrdersService.getUserList();
                    }
                ],
                statistics: [
                    '$stateParams', '$filter', 'StatisticsService', 'HelperService',
                    function($stateParams, $filter, StatisticsService, HelperService) {
                        // Helpers for init date filter
                        var date = new Date(), y = date.getFullYear(), m = date.getMonth();

                        return StatisticsService.getEarningsList({
                            params: {
                                page: $stateParams.page || 1,
                                date: $stateParams.date || HelperService.getCurrentMonth(),
                                store_id: localStorage.getItem('active_store') || '',
                                manager_id: $stateParams.manager_id,
                                customer_id: $stateParams.customer_id
                            }
                        });
                    }
                ]
            }
        })
        .state('statistics.pending_orders', {
            url  : '/pending_orders',
            views: {
                'content': {
                    controller : 'StatisticsController.pending_orders as ctrl',
                    templateUrl: formTemplate
                },
                'pending_orders@statistics.pending_orders': {
                    templateUrl: pendingOrdersTemplate
                }
            },
            resolve: {
                customers: [
                    'OrdersService',
                    function(OrdersService) {
                        return OrdersService.getCustomerList();
                    }
                ],
                users: [
                    'OrdersService',
                    function(OrdersService) {
                        return OrdersService.getUserList();
                    }
                ],
                statistics: [
                    'StatisticsService', '$stateParams', 'HelperService',
                    function(StatisticsService, $stateParams, HelperService) {
                        // Helpers for init date filter
                        var date = new Date(), y = date.getFullYear(), m = date.getMonth();

                        return StatisticsService.getPendingOrdersList({
                            params: {
                                type: 'not_completed',
                                date: $stateParams.date || HelperService.getCurrentMonth(),
                                manager_id: $stateParams.manager_id,
                                customer_id: $stateParams.customer_id
                            }
                        });
                    }
                ]
            }
        })
        .state('statistics.moving_products', {
            url  : '/moving_products',
            views: {
                'content': {
                    controller : 'StatisticsController.moving_products as ctrl',
                    templateUrl: formTemplate
                },
                'moving_products@statistics.moving_products': {
                    templateUrl: movingProductsTemplate
                }
            },
            resolve: {
                users: [
                    'OrdersService',
                    function(OrdersService) {
                        return OrdersService.getUserList();
                    }
                ],
                statistics: [
                    '$stateParams', 'StatisticsService',
                    function($stateParams, StatisticsService) {
                        var data ={
                            params: {
                                page: $stateParams.page,
                                limit: $stateParams.page || 30,
                            }
                        };

                        return StatisticsService.getMovingListProducts(data);
                    }
                ]
            }
        })
        .state('statistics.costs', {
            url  : '/costs',
            views: {
                'content': {
                    controller : 'StatisticsController.costs as ctrl',
                    templateUrl: formTemplate
                },
                'costs@statistics.costs': {
                    templateUrl: costsTemplate
                }
            },
            resolve: {
                statistics: [
                    'StatisticsService', '$stateParams', 'HelperService',
                    function(StatisticsService, $stateParams, HelperService) {
                        // Helpers for init date filter
                        var date = new Date(), y = date.getFullYear(), m = date.getMonth();

                        return StatisticsService.getCostsList({
                            params: {
                                page: $stateParams.page || 1,
                                date: $stateParams.date || HelperService.getCurrentMonth(),
                                store_id: localStorage.getItem('active_store') || '',
                            }
                        });
                    }
                ]
            }
        });
}