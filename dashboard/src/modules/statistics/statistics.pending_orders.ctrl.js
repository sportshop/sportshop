angular
    .module('app.statistics')

    .controller('StatisticsController.pending_orders', StatisticsController_PendingOrders);


StatisticsController_PendingOrders.$inject = ['$state', '$filter', 'HelperService', 'BreadcrumbsService', 'MainHeaderService', 'StatisticsService', 'users', 'customers', 'statistics'];
function StatisticsController_PendingOrders($state, $filter, HelperService, BreadcrumbsService, MainHeaderService, StatisticsService, users, customers, statistics) {
    BreadcrumbsService.push('statistics.title.statistics_pending_orders');
    MainHeaderService.set('statistics.title.statistics_pending_orders');

    var vm = this;

    vm.list = statistics.data || [];

    // Managers List
    vm.users = users.data || [];
    // Customers List
    vm.customers =  customers.data || [];

    // Options for date select
    vm.dateOptions = HelperService.generateDate();
    // Helpers for init date filter
    var date = new Date(), y = date.getFullYear();

    var currentMonth = HelperService.getCurrentMonth();

    // Init data
    vm.searchForm = {
        type: 'orders',
        date: currentMonth //y.toString()
    };

    vm.processSearch = processSearchFn;

    // Change Filter
    function processSearchFn() {
        var params = {
            type:           'not_completed',
            customer_id:    vm.searchForm.customer_id,
            date:           vm.searchForm.date,
            manager_id:     vm.searchForm.manager_id
        };

        var data = {
            params: params
        };

        StatisticsService.getPendingOrdersList(data)
            .success(function(response){
                $state.go($state.current, params, { notify: false, location: true });
                vm.list = response || {};
                prepareData(vm.list);
        });
    }

    // Options for Chart
    vm.options = {
        legend: { display: true },
        scales: {
            yAxes: [{
                ticks: {
                    min: 0
                }
            }]
        }
    };

    // Modified data for chart
    function prepareData(list) {
        vm.labels = [];
        var notPaidData = [];
        var partlyPaidData = [];
        var codData = [];

        _.each(list, function(elem, key) {
            if (key) {
                vm.labels.push( $filter('momentDate')(key, 'DD MMM'));
            }

            if (elem.not_paid) {
                notPaidData.push(elem.not_paid);
            }

            if (elem.partially_paid) {
                partlyPaidData.push(elem.partially_paid);
            }

            if (elem.paid) {
                codData.push(elem.paid);
            }
        });


        vm.totalNotPaidData =_.reduce(list, function(memo, el){
            return memo + (+el.not_paid || 0);
        }, 0);

        vm.totalPartlyPaidData =_.reduce(list, function(memo, el){
            return memo + (+el.partially_paid || 0);
        }, 0);

        vm.totalCodData =_.reduce(list, function(memo, el){
            return memo + (+el.paid || 0);
        }, 0);

        vm.series = ['Неоплачені', 'Частково оплачені', 'Наложки (незавершені)'];

        vm.data = [notPaidData, partlyPaidData, codData];
    }

    prepareData(vm.list);
}