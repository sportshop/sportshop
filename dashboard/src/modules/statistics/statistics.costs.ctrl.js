angular
    .module('app.statistics')
    .controller('StatisticsController.costs', StatisticsController_Costs);


StatisticsController_Costs.$inject = ['$rootScope','$state', '$stateParams', '$filter', 'BreadcrumbsService', 'MainHeaderService', 'StatisticsService', 'HelperService', 'statistics'];
function StatisticsController_Costs($rootScope, $state, $stateParams, $filter, BreadcrumbsService, MainHeaderService, StatisticsService, HelperService, statistics) {
    BreadcrumbsService.push('statistics.title.statistics_costs');
    MainHeaderService.set('statistics.title.statistics_costs');

    var vm = this;

    vm.list = statistics.data || [];

    var list = statistics.data || [];

    // Options for date select
    vm.dateOptions = HelperService.generateDate();
    // Helpers for init date filter
    var date = new Date(), y = date.getFullYear();

    var currentMonth = HelperService.getCurrentMonth();

    // Init data
    vm.searchForm = {
        date: currentMonth //y.toString()
    };

    vm.processSearch = processSearchFn;

    // Change Filter
    function processSearchFn() {
        var params = {
            date: vm.searchForm.date,
            store_id: localStorage.getItem('active_store') || ''
        };

        var data = {
            params: params
        };
        
         StatisticsService.getCostsList(data).success(function(response) {
             $state.go($state.current, params, { notify: false, location: true });

             vm.list = response || {};

             list = response || {};
            prepareData(vm.list, list);
         });
    }

    // Options for Chart
    vm.options = {
        legend: { display: true },
        scales: {
            yAxes: [{
                ticks: {
                    min: 0
                }
            }]
        }
    };

    // Modified data for chart
    function prepareData(list, calcList) {
        var list = _.map(list, function(values, key) {
            return {
                date: key,
                items: values
            }
        });

        list = _.sortBy(list, 'date');

        vm.labels = [];
        var hostingData = [];
        var taxData = [];
        var utilitiesData = [];
        var addData = [];
        var deliveryData = [];
        var developmentData = [];
        var otherData = [];

        _.each(list, function(elem) {
            vm.labels.push($filter('momentDate')(elem.date, 'DD MMM'));

                if (elem.items.hosting) {
                    elem.items.hosting = _.find(elem.items.hosting, function(item) {
                        return item.currency_id == ($rootScope.defaultCurrencyId);
                    });

                    if (elem.items.hosting && elem.items.hosting.value) {
                        elem.items.hosting = +elem.items.hosting.value;
                        hostingData.push(elem.items.hosting);
                    }
                } else {
                    hostingData.push(0);
                }

                if (elem.items.tax) {
                    elem.items.tax = _.find(elem.items.tax, function(item) {
                        return item.currency_id == ($rootScope.defaultCurrencyId);
                    });

                    if (elem.items.tax && elem.items.tax.value) {
                        elem.items.tax = +elem.items.tax.value;
                        taxData.push(elem.items.tax);
                    }
                } else {
                    taxData.push(0);
                }

                if (elem.items.utilities) {
                    elem.items.utilities = _.find(elem.items.utilities, function(item) {
                        return item.currency_id == ($rootScope.defaultCurrencyId);
                    });

                    if (elem.items.utilities && elem.items.utilities.value) {
                        elem.items.utilities = +elem.items.utilities.value;
                        utilitiesData.push(elem.items.utilities);
                    }
                } else {
                    utilitiesData.push(0);
                }

                if (elem.items.add) {
                    elem.items.add = _.find(elem.items.add, function(item) {
                        return item.currency_id == ($rootScope.defaultCurrencyId);
                    });

                    if (elem.items.add && elem.items.add.value) {
                        elem.items.add = +elem.items.add.value;
                        addData.push(elem.items.add);
                    }
                } else {
                    addData.push(0);
                }

                if (elem.items.delivery) {
                    elem.items.delivery = _.find(elem.items.delivery, function(item) {
                        return item.currency_id == ($rootScope.defaultCurrencyId);
                    });

                    if (elem.items.delivery && elem.items.delivery.value) {
                        elem.items.delivery = +elem.items.delivery.value;
                        deliveryData.push(elem.items.delivery.value);
                    }
                } else {
                    deliveryData.push(0);
                }

                if (elem.items.development) {
                    elem.items.development = _.find(elem.items.development, function(item) {
                        return item.currency_id == ($rootScope.defaultCurrencyId);
                    });

                    if (elem.items.development && elem.items.development.value) {
                        elem.items.development = +elem.items.development.value;
                        developmentData.push(elem.items.development.value);
                    }
                } else{
                    developmentData.push(0);
                }

                if (elem.items.other) {
                    elem.items.other = _.find(elem.items.other, function(item) {
                        return item.currency_id == ($rootScope.defaultCurrencyId);
                    });

                    if (elem.items.other && elem.items.other.value) {
                        elem.items.other = +elem.items.other.value;
                        otherData.push(elem.items.other.value);
                    }
                } else {
                    otherData.push(0);
                }
        });

        vm.totalHostingData =_.reduce(calcList, function(memo, el){
            return memo + (+el.hosting || 0);
        }, 0);

        vm.totalTaxData =_.reduce(calcList, function(memo, el){
            return memo + (+el.tax || 0);
        }, 0);

        vm.totalUtilitiesData =_.reduce(calcList, function(memo, el){
            return memo + (+el.utilities || 0);
        }, 0);

        vm.totalAddData =_.reduce(calcList, function(memo, el){
            return memo + (+el.add || 0);
        }, 0);

        vm.totalDeliveryData =_.reduce(calcList, function(memo, el){
            return memo + (+el.delivery || 0);
        }, 0);

        vm.totalDevelopmentData =_.reduce(calcList, function(memo, el){
            return memo + (+el.development || 0);
        }, 0);

        vm.totalOtherData =_.reduce(calcList, function(memo, el){
            return memo + (+el.other || 0);
        }, 0);

        vm.totalData = vm.totalHostingData + vm.totalTaxData + vm.totalUtilitiesData + vm.totalAddData + vm.totalDeliveryData + vm.totalDevelopmentData + vm.totalOtherData;

        vm.series = ['Оренда, Хостинг, Домен', 'Податок', 'Комунальні послуги', 'Реклама, SEO, Adwords', 'Доставка', 'Розробка, падтримка сайту', 'Інші витрати'];

        vm.data = [hostingData, taxData, utilitiesData, addData, deliveryData, developmentData, otherData];
    }

    prepareData(vm.list, list);
}