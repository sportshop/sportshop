angular
    .module('app.statistics')
    .controller('StatisticsController.earnings', StatisticsController_Earnings);


StatisticsController_Earnings.$inject = ['$rootScope', '$state', '$stateParams', '$filter', 'HelperService', 'BreadcrumbsService', 'MainHeaderService', 'StatisticsService', 'users', 'customers', 'statistics'];
function StatisticsController_Earnings($rootScope, $state, $stateParams, $filter, HelperService, BreadcrumbsService, MainHeaderService, StatisticsService, users, customers, statistics) {
    BreadcrumbsService.push('statistics.title.statistics_earnings');
    MainHeaderService.set('statistics.title.statistics_earnings');

    var vm = this;
    var list = [];

    // Statistic Model
    vm.list = statistics.data.data || [];
    list = statistics.data.data || [];

    vm.users = users.data || [];
    vm.customers =  customers.data || [];

    // Options for date select
    vm.dateOptions = HelperService.generateDate();
    // Helpers for init date filter
    var date = new Date(), y = date.getFullYear();

    var currentMonth = HelperService.getCurrentMonth();

    // Init data
    vm.searchForm = {
        date:  currentMonth, //y.toString(),
        customer_id: $stateParams.customer_id ? +$stateParams.customer_id : null,
        manager_id: $stateParams.manager_id ? +$stateParams.manager_id : null,
        store_id: localStorage.getItem('active_store')
    };

    vm.processSearch = processSearchFn;

    // Change Filter
    function processSearchFn() {
        var params = {
            customer_id: vm.searchForm.customer_id,
            date: vm.searchForm.date,
            store_id: localStorage.getItem('active_store') || '',
            manager_id: vm.searchForm.manager_id
        };

        var data = {
            params: params
        };

        StatisticsService.getEarningsList(data)
            .success(function(response) {
                $state.go($state.current, params, { notify: false, location: true });

                vm.list = response.data || [];
                list = response.data || {};

                prepareData(vm.list, list);
        });
    }

    // Options for Chart
    vm.options = {
        legend: { display: true },
        // scales: {
        //     yAxes: [{
        //         ticks: {
        //             min: 0
        //         }
        //     }]
        // }
    };

    // Modified data for chart
    function prepareData(list, calcList) {
        var list = _.map(list, function(values, key) {
            return {
                date: key,
                items: values
            }
        });

        list = _.sortBy(list, 'date');

        vm.labels = [];
        // Receipts
        var salesData = [];
        // Profit
        var earningsData = [];
        // Costs
        var costsData = [];
        // Net profit
        var netSalesData = [];

        _.each(list, function(elem) {
            vm.labels.push($filter('momentDate')(elem.date, 'DD MMM'));

                if (elem.items.receipts) {
                    elem.items.receipts = _.find(elem.items.receipts, function(item) {
                        return item.currency_id == ($rootScope.defaultCurrencyId);
                    });

                    if (elem.items.receipts && elem.items.receipts.value) {
                        elem.items.receipts = elem.items.receipts.value;
                        salesData.push(elem.items.receipts);
                    }
                } else {
                    salesData.push(0);
                }

                if (elem.items.profit) {
                    elem.items.profit = _.find(elem.items.profit, function(item) {
                        return item.currency_id == ($rootScope.defaultCurrencyId);
                    });

                    if (elem.items.profit && elem.items.profit.value) {
                        elem.items.profit = elem.items.profit.value;
                        earningsData.push(elem.items.profit);
                    }
                } else {
                    earningsData.push(0);
                }

                if (elem.items.costs) {
                    elem.items.costs = _.find(elem.items.costs, function(item) {
                        return item.currency_id == ($rootScope.defaultCurrencyId);
                    });

                    if (elem.items.costs && elem.items.costs.value) {
                        elem.items.costs = elem.items.costs.value;
                        costsData.push(elem.items.costs);
                    }
                } else {
                    costsData.push(0);
                }

                if (elem.items.net_profit) {
                    elem.items.net_profit = _.find(elem.items.net_profit, function(item) {
                        return item.currency_id == ($rootScope.defaultCurrencyId);
                    });

                    if (elem.items.net_profit && elem.items.net_profit.value) {
                        elem.items.net_profit = elem.items.net_profit.value;
                        netSalesData.push(elem.items.net_profit);
                    }
                } else {
                    netSalesData.push(0);
                }
        });

        vm.totalSalesData =_.reduce(calcList, function(memo, el){
            return memo + (+el.receipts || 0);
        }, 0);

        vm.totalEarningsData =_.reduce(calcList, function(memo, el){
            return memo + (+el.profit || 0);
        }, 0);

        vm.totalCostsData =_.reduce(calcList, function(memo, el){
            return memo + (+el.costs || 0);
        }, 0);

        vm.totalNetSalesData =_.reduce(calcList, function(memo, el){
            return memo + (+el.net_profit || 0);
        }, 0);

        vm.series = ['Виручка', 'Заробіток', 'Витрати', 'Чистий заробіток'];

        vm.data = [salesData, earningsData, costsData, netSalesData];
    }

    vm.percentLabels = ['Відсоток накрутки', 'Собівартість'];
    vm.percentData = [25, 75];

    prepareData(vm.list, list);
}