angular
    .module('app.statistics')
    .controller('StatisticsController.sales', StatisticsController_Sales);


StatisticsController_Sales.$inject = ['$state', '$stateParams', '$filter', 'HelperService', 'BreadcrumbsService', 'MainHeaderService', 'StatisticsService', 'users', 'customers', 'statistics'];
function StatisticsController_Sales($state, $stateParams, $filter, HelperService, BreadcrumbsService, MainHeaderService, StatisticsService, users, customers, statistics) {
    BreadcrumbsService.push('statistics.title.statistics_sales');
    MainHeaderService.set('statistics.title.statistics_sales');

    var vm = this;

    vm.list = statistics.data || [];
    var calcList = statistics.data || [];

    // Managers
    vm.users = users.data || [];
    // Customers
    vm.customers =  customers.data || [];

    // Options for date select
    vm.dateOptions = HelperService.generateDate();
    // Helpers for init date filter
    var date = new Date(), y = date.getFullYear();

    var currentMonth = HelperService.getCurrentMonth();

    // Init data
    vm.searchForm = {
        type: 'orders',
        date: currentMonth, //y.toString(),
        customer_id: $stateParams.customer_id ? +$stateParams.customer_id : null,
        manager_id:  $stateParams.manager_id ? +$stateParams.manager_id : null,
        store_id: localStorage.getItem('active_store')
    };

    vm.processSearch = processSearchFn;

    // Change Filter
    function processSearchFn() {
        var params = {
            type:        vm.searchForm.type,
            customer_id: vm.searchForm.customer_id,
            date:        vm.searchForm.date,
            store_id:    localStorage.getItem('active_store') || '',
            manager_id:  vm.searchForm.manager_id,
        };

        var data = {
            params: params
        };

        StatisticsService.getSalesList(data)
            .success(function(response) {
                $state.go($state.current, params, { notify: false, location: true });
                vm.list = response || {};

                var list = response || {};
                prepareData(vm.list, list);
            });
    }

    // Options for Chart
    vm.options = {
        legend: { display: true },
        scales: {
            yAxes: [{
                ticks: {
                    min: 0
                }
            }]
        }
    };

    // Modified data for chart
    function prepareData(list, calcList) {
        var list = _.map(list, function(values, key) {
            return {
                date: key,
                items: values
            }
        });

        list = _.sortBy(list, 'date');

        vm.labels = [];
        var storeData = [];
        var internetStoreData = [];

        _.each(list, function(item) {

            if (item.date) {
                vm.labels.push($filter('momentDate')(item.date, 'DD MMM'));
            }

            if (vm.searchForm.type == 'orders') {
                item.items.store_data = item.items.orders.local_store || 0;
                item.items.internet_store_data = item.items.orders.web_store || 0;
                storeData.push(item.items.store_data);
                internetStoreData.push(item.items.internet_store_data);
            }

            if (vm.searchForm.type == 'products') {
                item.items.store_data = item.items.products.local_store|| 0;
                item.items.internet_store_data = item.items.products.web_store || 0;
                storeData.push(item.items.store_data);
                internetStoreData.push(item.items.internet_store_data);
            }
        });

        vm.totalStoreData =_.reduce(calcList, function(memo, el){
            return memo + (+el.store_data || 0);
        }, 0);


        vm.totalInternetStoreData =_.reduce(calcList, function(memo, el) {
            return memo + (+el.internet_store_data || 0);
        }, 0);

        vm.totalData = vm.totalStoreData + vm.totalInternetStoreData;

        vm.series = ['Магазин', 'Інтернет-магазин'];

        vm.data = [storeData, internetStoreData];
    }

    prepareData(vm.list, calcList);
}