angular
    .module('app.statistics')
    .controller('StatisticsController.moving_products', StatisticsController_MovingProducts);


StatisticsController_MovingProducts.$inject = ['$state', '$filter', 'HelperService', 'BreadcrumbsService', 'MainHeaderService', 'HttpService', 'StatisticsService', 'users', 'statistics'];
function StatisticsController_MovingProducts($state, $filter, HelperService, BreadcrumbsService, MainHeaderService, HttpService, StatisticsService, users, statistics) {
    BreadcrumbsService.push('statistics.title.statistics_moving_products');
    MainHeaderService.set('statistics.title.statistics_moving_products');

    var vm = this;

    // Transfers data
    vm.list = statistics.data || {};
    // Types
    vm.actionTypes = StatisticsService.getActionTypes();
    // Managers
    vm.users = users.data || [];

    vm.listPerPage = [
        {key: 15, value: 15},
        {key: 30, value: 30},
        {key: 50, value: 50}
    ];

    vm.current_date = new Date();

    // Options for date select
    vm.dateOptions = HelperService.generateDate();
    // Helpers for init date filter
    var date = new Date(), y = date.getFullYear();

    var currentMonth = HelperService.getCurrentMonth();

    vm.searchForm = {
        date: {},
        action:        null,
        manager_id:    null,
        from_store_id: null,
        to_store_id:   null,
        name:          null,
        sku:           null,
        size:          null,
        price:         null,
        limit:         30,
        quantity:      null
    };

    vm.dateRangeOptions = {
        opens: 'left',
        singleDatePicker: true,
        locale : {
            format : 'DD/MM/YYYY',
            applyLabel: "Пошук",
            cancelLabel: "Очистити",
        },
        eventHandlers : {
            'apply.daterangepicker' : function() {
                processSearchFn();
            },
            'cancel.daterangepicker' : function() {
                vm.searchForm.date = {
                    startDate: null
                };

                processSearchFn();
            },
            'hide.daterangepicker': function() {
                if (vm.searchForm.date && vm.searchForm.date.startDate == null) {
                    processSearchFn();
                }
            }
        }
    };

    $('#single-date-picker').val('');

    vm.print = printFn;
    vm.showActionType = StatisticsService.showMovingProductsType;
    vm.processSearch = processSearchFn;
    vm.changePage = changePageFn;
    vm.print = printFn;

    function printFn() {
        var params = {
            date:          vm.searchForm.date ? $filter('momentDate')(vm.searchForm.date._d, 'YYYY-MM-DD') : null,
            action:        vm.searchForm.action,
            manager_id:    vm.searchForm.manager_id,
            from_store_id: vm.searchForm.store_id_from,
            to_store_id:   vm.searchForm.store_id_to,
            name:          vm.searchForm.name,
            sku:           vm.searchForm.sku,
            size:          vm.searchForm.size,
            price:         vm.searchForm.price,
            quantity:      vm.searchForm.quantity,
            limit:         vm.searchForm.limit,
            page:          vm.list.items.current_page
        };

        var url = '/products/transfers/download';

        if (params.action || params.manager_id || params.from_store_id || params.to_store_id || params.name || params.sku || params.size || params.price || params.quantity || params.date_end || params.date_start || params.limit) {
            url = url + '?';
        }
        if (params.date) {
            url = url + '&date='+ params.date;
        }

        if (params.action) {
            url = url + '&action='+ params.action;
        }
        if (params.manager_id) {
            url = url + '&manager_id='+ params.manager_id;
        }
        if (params.from_store_id) {
            url = url + '&from_store_id='+ params.from_store_id;
        }
        if (params.limit) {
            url = url + '&limit='+ params.limit;
        }
        if (params.to_store_id) {
            url = url + '&to_store_id='+ params.to_store_id;
        }
        if (params.name) {
            url = url + '&name='+ params.name;
        }
        if (params.sku) {
            url = url + '&sku='+ params.sku;
        }
        if (params.size) {
            url = url + '&size='+ params.size;
        }
        if (params.price) {
            url = url + '&price='+ params.price;
        }
        if (params.quantity) {
            url = url + '&quantity='+ params.quantity;
        }
        if (params.page) {
            url = url + '&page='+ params.page;
        }

        HttpService.post(url, null, null, null, { responseType: 'blob'})
            .then(function(response) {
                var blob = new Blob([response.data], { type: 'application/pdf;charset=utf-8' });
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = `products-${moment().format('DD-MM-YYYY')}.pdf`;
                link.click();
            });
    }

    // Change Filter
    function processSearchFn() {
        var params = {
            date:          vm.searchForm.date ? $filter('momentDate')(vm.searchForm.date._d, 'YYYY-MM-DD') : null,
            action:        vm.searchForm.action,
            manager_id:    vm.searchForm.manager_id,
            from_store_id: vm.searchForm.store_id_from,
            to_store_id:   vm.searchForm.store_id_to,
            product_name:  vm.searchForm.name,
            product_sku:   vm.searchForm.sku,
            product_size:  vm.searchForm.size,
            product_price: vm.searchForm.price,
            limit:         vm.searchForm.limit,
            product_quantity: vm.searchForm.quantity
        };

        var data = {
            params: params
        };

        StatisticsService.getMovingListProducts(data)
            .success(function(response) {
                vm.list = response || {};
            });
    }

    function changePageFn() {
        var params = {
            date:          vm.searchForm.date ? $filter('momentDate')(vm.searchForm.date._d, 'YYYY-MM-DD') : null,
            action:        vm.searchForm.action,
            manager_id:    vm.searchForm.manager_id,
            from_store_id: vm.searchForm.store_id_from,
            to_store_id:   vm.searchForm.store_id_to,
            product_name:  vm.searchForm.name,
            product_sku:   vm.searchForm.sku,
            product_size:  vm.searchForm.size,
            product_quantity: vm.searchForm.quantity,
            product_price: vm.searchForm.price,
            limit:         vm.searchForm.limit,
            page:          vm.list.items.current_page
        };

        var data = {
            params: params
        };

        StatisticsService.getMovingListProducts(data)
            .success(function(response) {
                vm.list = response || {};
            });
    }
}