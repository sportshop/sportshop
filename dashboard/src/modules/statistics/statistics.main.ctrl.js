angular
    .module('app.statistics')
    .controller('StatisticsController.main', StatisticsController_Main);


StatisticsController_Main.$inject = ['$scope', '$state'];
function StatisticsController_Main($scope, $state) {
    $scope.goTo = goToFn;

    function goToFn(url) {
        $state.go(url);
    }
}