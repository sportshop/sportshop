angular
    .module('app.statistics')
    .service('StatisticsService', StatisticsService);


StatisticsService.$inject = ['HttpService'];
function StatisticsService(HttpService) {
    var baseSalesUrl = '/orders/statistic';
    var baseSalaryUrl = '/users/salary';
    var baseEarningsUrl = '/statistic';
    var basePendingOrdersUrl = '/orders/not-completed';
    var baseMovingProductsUrl = '/products/transfers';
    var baseCostsUrl = '/costs/statistic';

    // Action types for moving products
    var types = [
        {key: 'move', value: 'statistics.attribute.label.moving'},
        {key: 'delete', value: 'statistics.attribute.label.delete'},
        {key: 'add',    value: 'statistics.attribute.label.add'}
    ];

    return {
        getSalesList:           getSalesListFn,
        getSalaryList:          getSalaryListFn,
        getEarningsList:        getEarningsListFn,
        getPendingOrdersList:   getPendingOrdersListFn,
        getMovingListProducts:  getMovingProductsListFn,
        getCostsList:           getCostsListFn,
        getActionTypes:         getActionTypesFn,
        showMovingProductsType: showMovingProductsTypeFn
    };

    /**
     * Get sales statistics
     *
     * @param params
     *
     * @returns {*}
     */
    function getSalesListFn(params) {
        return HttpService.getWParams(baseSalesUrl, params);
    }

    /**
     * Get salary statistics
     *
     * @param data
     *
     * @returns {*}
     */
    function getSalaryListFn(data) {
        return HttpService.getWParams(baseSalaryUrl, data);
    }

    /**
     * Get earnings statistics
     *
     * @param data
     *
     * @returns {*}
     */
    function getEarningsListFn(data) {
        return HttpService.getWParams(baseEarningsUrl, data);
    }

    /**
     * Get pending orders statistics
     *
     * @param data
     *
     * @returns {*}
     */
    function getPendingOrdersListFn(data) {
        return HttpService.getWParams(basePendingOrdersUrl, data);
    }

    /**
     * Get moving products statistics
     *
     * @param data
     *
     * @returns {*}
     */
    function getMovingProductsListFn(data) {
        return HttpService.getWParams(baseMovingProductsUrl, data);
    }

    /**
     * Get costs statistics
     *
     * @param data
     *
     * @returns {*}
     */
    function getCostsListFn(data) {
        return HttpService.getWParams(baseCostsUrl, data);
    }

    /**
     * Get action types
     *
     * @returns {*[]}
     */
    function getActionTypesFn() {
        return types;
    }

    /**
     * Show action types
     *
     * @param type
     *
     * @returns {*}
     */
    function showMovingProductsTypeFn(type) {
        if (type == 'move') {
            return 'statistics.attribute.label.moving_product';
        } else if (type == 'add') {
            return 'statistics.attribute.label.adding_product';
        } else {
            return 'statistics.attribute.label.deleting_product';
        }
    }
}