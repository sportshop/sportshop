angular
    .module('app.statistics')
    .controller('StatisticsController.salary', StatisticsController_Salary);


StatisticsController_Salary.$inject = ['$rootScope', '$state', '$filter', 'HelperService', 'BreadcrumbsService', 'MainHeaderService', 'StatisticsService', 'users', 'customers', 'statistics'];
function StatisticsController_Salary($rootScope, $state, $filter, HelperService, BreadcrumbsService, MainHeaderService, StatisticsService, users, customers, statistics) {
    BreadcrumbsService.push('statistics.title.statistics_salaries');
    MainHeaderService.set('statistics.title.statistics_salaries');

    var vm = this;

    vm.list = statistics.data.data || [];

    vm.total = (statistics.data && statistics.data.total) ? statistics.data.total : {};

    vm.users = users.data || []; // Managers
    vm.customers = customers.data || []; // Customers

    // Options for date select
    vm.dateOptions = HelperService.generateDate();
    // Helpers for init date filter
    var date = new Date(), y = date.getFullYear();

    var currentMonth = HelperService.getCurrentMonth();

    // Init data
    vm.searchForm = {
        date: currentMonth, //y.toString(),
        store_id: localStorage.getItem('active_store')
    };

    vm.processSearch = processSearchFn;

    // Change Filter
    function processSearchFn() {
        var params = {
            customer:   vm.searchForm.customer,
            date:       vm.searchForm.date,
            store_id:   localStorage.getItem('active_store'),
            manager_id: vm.searchForm.manager_id
            // store_id:   vm.searchForm.store_id,
        };

        var data = {
            params: params
        };

        StatisticsService.getSalaryList(data)
            .success(function(response){
        $state.go($state.current, params, { notify: false, location: true });
        vm.list = response.data || {};
        vm.total = response.total || {};

        prepareData(vm.list);
        });
    }

    // Options for Chart
    vm.options = {
        legend: { display: true },
        scales: {
            yAxes: [{
                ticks: {
                    min: 0
                }
            }]
        }
    };

    // Modified data for chart
    function prepareData(list) {
        var list = _.map(list, function(values, key) {
            return {
                date: key,
                items: values
            }
        });

        list = _.sortBy(list, 'date');

        vm.labels = [];
        var salaryData = [];
        var rateData = [];
        var percentData = [];

        _.each(list, function(elem) {
            vm.labels.push($filter('momentDate')(elem.date, 'DD MMM'));

            if (elem.items.salary) {
                elem.items.salary = _.find(elem.items.salary, function(item) {
                    return item.currency_id == ($rootScope.defaultCurrencyId);
                });

                if (elem.items.salary && elem.items.salary.value) {
                    elem.items.salary = elem.items.salary.value;
                    salaryData.push(elem.items.salary);
                }
            } else {
                salaryData.push(0);
            }

            if (elem.items.salary_rate) {
                elem.items.salary_rate = _.find(elem.items.salary_rate, function(item) {
                    return item.currency_id == ($rootScope.defaultCurrencyId);
                });

                if (elem.items.salary_rate && elem.items.salary_rate.value) {
                    elem.items.salary_rate = elem.items.salary_rate.value;
                    rateData.push(elem.items.salary_rate);
                }
            } else {
                rateData.push(0);
            }

            if (elem.items.sales) {
                elem.items.sales = _.find(elem.items.sales, function(item) {
                    return item.currency_id == ($rootScope.defaultCurrencyId);
                });

                if (elem.items.sales && elem.items.sales.value) {
                    elem.items.sales = elem.items.sales.value;
                    percentData.push(elem.items.sales);
                }
            } else {
                percentData.push(0);
            }
        });

        if (vm.total && vm.total.salary) {
            vm.total.salary = _.find(vm.total.salary, function(item) {
                return item.currency_id == ($rootScope.defaultCurrencyId);
            });

            if (vm.total.salary && vm.total.salary.value) {
                vm.total.salary = vm.total.salary.value;
            }
        }

        if (vm.total && vm.total.salary_rate) {
            vm.total.salary_rate = _.find(vm.total.salary_rate, function(item) {
                return item.currency_id == ($rootScope.defaultCurrencyId);
            });

            if (vm.total.salary_rate && vm.total.salary_rate.value) {
                vm.total.salary_rate = vm.total.salary_rate.value;
            }
        } else {
            rateData.push(0);
        }

        if (vm.total && vm.total.sales) {
            vm.total.sales = _.find(vm.total.sales, function(item) {
                return item.currency_id == ($rootScope.defaultCurrencyId);
            });

            if (vm.total.sales && vm.total.sales.value) {
                vm.total.sales = vm.total.sales.value;
                percentData.push(vm.total.sales);
            }
        } else {
            percentData.push(0);
        }

        vm.series = ['Зарплата', 'Ставка', 'Відсоток'];

        vm.data = [salaryData, rateData, percentData];
    }

    prepareData(vm.list);
}