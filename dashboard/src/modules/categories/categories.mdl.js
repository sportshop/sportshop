import layoutTemplate from "../layouts/dashboard.html";
import listTemplate from "./list.html";
import formTemplate from "./form.html";
import generalTemplate from "./_general.html";
import seoTemplate from "./_seo.html";
import sidebarTemplate from "./_sidebar.html";

angular
    .module('app.categories', [])
    .config(configure);


configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('categories', {
            abstract: true,
            url: '/categories',
            views: {
                '': {
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('categories.list', {
            url: '',
            views: {
                'content': {
                    controller: 'CategoriesController.list as ctrl',
                    templateUrl: listTemplate
                }
            },
            resolve: {
                items: [
                    '$rootScope', 'CategoriesService',
                    function($rootScope, CategoriesService) {
                        var params = {
                            flat: true
                        };

                        if (!$rootScope.globalCategoriesFlatList) {
                            return CategoriesService.getList(params);
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalCategoriesFlatList));
                    }
                ]
            }
        })
        .state('categories.create', {
            url: '/create',
            views: {
                'content': {
                    controller: 'CategoriesController.edit as ctrl',
                    templateUrl: formTemplate
                },
                'general@categories.create': {
                    templateUrl: generalTemplate
                },
                'seo@categories.create': {
                    templateUrl: seoTemplate
                },
                'sidebar@categories.create': {
                    templateUrl: sidebarTemplate
                }
            },
            resolve: {
                item: [
                    function() {
                        return {
                            data: {
                                category: []
                            }
                        }
                    }
                ],
                categories_list: [
                    '$rootScope', 'CategoriesService',
                    function($rootScope, CategoriesService) {
                        var params = {
                            flat: true
                        };

                        if (!$rootScope.globalCategoriesFlatList) {
                            return CategoriesService.getList(params);
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalCategoriesFlatList));
                    }
                ]
            }
        })
        .state('categories.edit', {
            url: '/:id',
            views: {
                'content': {
                    controller: 'CategoriesController.edit as ctrl',
                    templateUrl: formTemplate
                },
                'general@categories.edit': {
                    templateUrl: generalTemplate
                },
                'seo@categories.edit': {
                    templateUrl: seoTemplate
                },
                'sidebar@categories.edit': {
                    templateUrl: sidebarTemplate
                }
            },
            resolve: {
                item: [
                    '$stateParams', 'CategoriesService',
                    function($stateParams, CategoriesService) {
                        return CategoriesService.get($stateParams.id);
                    }
                ],
                categories_list: [
                    '$rootScope', 'CategoriesService',
                    function($rootScope, CategoriesService) {
                        var params = {
                            flat: true
                        };

                        if (!$rootScope.globalCategoriesFlatList) {
                            return CategoriesService.getList(params);
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalCategoriesFlatList));
                    }
                ]
            }
        });
}