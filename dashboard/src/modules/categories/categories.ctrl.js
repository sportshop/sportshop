angular
    .module('app.categories')
    .controller('CategoriesController.list', CategoriesController_List)
    .controller('CategoriesController.edit', CategoriesController_Edit);


CategoriesController_List.$inject = ['$rootScope', 'BreadcrumbsService', 'MainHeaderService', 'Notify', 'CategoriesService', 'items'];
function CategoriesController_List($rootScope, BreadcrumbsService, MainHeaderService, Notify, CategoriesService, items) {
    BreadcrumbsService.push('categories.title.list');
    MainHeaderService.set('categories.title.list');

    var vm = this;

    // Categories Model
    if (items && items.data) {
        vm.list = items.data || [];
        $rootScope.globalCategoriesFlatList = JSON.parse(JSON.stringify(items.data));
    } else {
        vm.list = items || [];
        $rootScope.globalCategoriesFlatList = items || [];
    }

    vm.removeItem = removeItemFn;

    /**
     * Remove category
     *
     * @param item
     */
    function removeItemFn(item) {
        Notify.confirm(function() {
            CategoriesService.delete(item.id)
                .success(function() {
                    var index = $rootScope.globalCategoriesFlatList.indexOf(item);
                    $rootScope.globalCategoriesFlatList.splice(index, 1);

                    Notify.success('general.removed');
                })
                .error(function(errors) {
                    Notify.warning(errors.message);
                });
        }, 'general.are_you_sure')
    }
}

CategoriesController_Edit.$inject = ['$rootScope', '$state', '$stateParams', '$timeout', 'BreadcrumbsService', 'MainHeaderService', 'HttpService', 'Notify', 'MyFileUploader', 'CategoriesService', 'item', 'categories_list'];
function CategoriesController_Edit($rootScope, $state, $stateParams, $timeout, BreadcrumbsService, MainHeaderService, HttpService, Notify, MyFileUploader, CategoriesService, item, categories_list) {
    BreadcrumbsService.push('categories.title.list', 'categories.list');

    if ($stateParams.id) {
        BreadcrumbsService.push('categories.title.edit');
        MainHeaderService.set('categories.title.edit');
    } else {
        BreadcrumbsService.push('categories.title.create');
        MainHeaderService.set('categories.title.create');
    }

    var vm = this;

    // Categories list
    if (categories_list && categories_list.data) {
        vm.options_categories = categories_list.data || [];
        $rootScope.globalCategoriesFlatList = JSON.parse(JSON.stringify(categories_list.data));
    } else {
        vm.options_categories = categories_list || [];
    }

    // Statuses list
    vm.statuses = CategoriesService.getStatuses();
    // Icons list
    vm.icons = CategoriesService.getIcons();

    // Check is edit or create category
    if ($stateParams.id) {
        // Category Model
        vm.item = item.data || {};
        // Modify media
        vm.item.media_id = vm.item.thumbnail ? vm.item.thumbnail.id : null;

        // Remove category name from select of categories
        vm.options_categories = _.filter(vm.options_categories, function(item) {
            if (vm.item.name == item.name) {
                return;
            }

            return item;
        });
    } else {
        // Category Model
        vm.item = {
            is_active: true
        };
    }

    vm.save = saveFn;

    /**
     * Save category
     */
    function saveFn() {
        if ($stateParams.id) {
            CategoriesService.update(vm.item.id, vm.item)
                .success(function() {
                    Notify.success('general.updated');
                    $state.go('categories.list');
                })
                .error(function(errors) {
                    vm.errors = errors.errors;
                });
        } else {
            CategoriesService.save(vm.item)
                .success(function() {
                    Notify.success('general.saved');
                    $state.go('categories.list');
                })
                .error(function(errors) {
                    vm.errors = errors.errors;
                });
        }
    }
}