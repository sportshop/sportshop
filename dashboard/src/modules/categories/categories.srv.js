angular
    .module('app.categories')
    .service('CategoriesService', CategoriesService);


CategoriesService.$inject = ['HttpService'];
function CategoriesService(HttpService) {
    var baseUrl = '/categories';

    // Default statuses
    var statuses = [
        {key: true,  value: 'categories.general.status_active'},
        {key: false, value: 'categories.general.status_inactive'}
    ];

    // Default icons
    var icons = [
        {key: 'icons-04', value: 'icons-04'},
        {key: 'icons-09', value: 'icons-09'},
        {key: 'icons-15', value: 'icons-15'},
        {key: 'icons-21', value: 'icons-21'},
        {key: 'icons-61', value: 'icons-61'},
        {key: 'icons-65', value: 'icons-65'},
        {key: 'icons-70', value: 'icons-70'},
        {key: 'icons-74', value: 'icons-74'},
        {key: 'icons-78', value: 'icons-78'},
        {key: 'icons-83', value: 'icons-83'},
        {key: 'icons-88', value: 'icons-88'},
        {key: 'icons-05', value: 'icons-05'},
        {key: 'icons-10', value: 'icons-10'},
        {key: 'icons-17', value: 'icons-17'},
        {key: 'icons-49', value: 'icons-49'},
        {key: 'icons-62', value: 'icons-62'},
        {key: 'icons-67', value: 'icons-67'},
        {key: 'icons-71', value: 'icons-71'},
        {key: 'icons-75', value: 'icons-75'},
        {key: 'icons-79', value: 'icons-79'},
        {key: 'icons-84', value: 'icons-84'},
        {key: 'icons-07', value: 'icons-07'},
        {key: 'icons-13', value: 'icons-13'},
        {key: 'icons-18', value: 'icons-18'},
        {key: 'icons-59', value: 'icons-59'},
        {key: 'icons-63', value: 'icons-63'},
        {key: 'icons-68', value: 'icons-68'},
        {key: 'icons-72', value: 'icons-72'},
        {key: 'icons-76', value: 'icons-76'},
        {key: 'icons-80', value: 'icons-80'},
        {key: 'icons-86', value: 'icons-86'},
        {key: 'icons-08', value: 'icons-08'},
        {key: 'icons-14', value: 'icons-14'},
        {key: 'icons-20', value: 'icons-20'},
        {key: 'icons-60', value: 'icons-60'},
        {key: 'icons-64', value: 'icons-64'},
        {key: 'icons-69', value: 'icons-69'},
        {key: 'icons-73', value: 'icons-73'},
        {key: 'icons-77', value: 'icons-77'},
        {key: 'icons-82', value: 'icons-82'},
        {key: 'icons-87', value: 'icons-87'}
    ];

    return {
        getList:     getListFn,
        get:         getFn,
        save:        saveFn,
        update:      updateFn,
        delete:      deleteFn,
        getStatuses: getStatusesFn,
        getIcons:    getIconsFn
    };

    /**
     * Get categories list
     *
     * @param params
     *
     * @returns {*}
     */
    function getListFn(params) {
        return HttpService.getWParams(baseUrl, { params: params });
    }

    /**
     * Get category
     *
     * @param id
     *
     * @returns {*}
     */
    function getFn(id) {
        return HttpService.get(baseUrl +'/'+ id)
    }

    /**
     * Save category
     *
     * @param data
     *
     * @returns {*}
     */
    function saveFn(data) {
        return HttpService.post(baseUrl, data)
    }

    /**
     * Update category
     *
     * @param id
     * @param data
     *
     * @returns {*}
     */
    function updateFn(id, data) {
        return HttpService.put(baseUrl +'/'+ id, data)
    }

    /**
     * Delete category
     *
     * @param id
     *
     * @returns {*}
     */
    function deleteFn(id) {
        return HttpService.delete(baseUrl +'/'+ id)
    }

    /**
     * Get default statuses
     *
     * @returns {*[]}
     */
    function getStatusesFn() {
        return statuses;
    }

    /**
     * Get default icons
     *
     * @returns {*[]}
     */
    function getIconsFn() {
        return icons;
    }
}