angular
    .module('app.orders')
    .service('OrdersService', OrdersService);


OrdersService.$inject = ['$q', 'HttpService'];
function OrdersService($q, HttpService) {
    var baseUrl = '/orders';

    /**
     * Payment Methods
     * @type {*[]}
     */
    var payment_methods = [
        {id: 1, key: 'cod',  value: 'orders.payment_methods.post_pay'},
        {id: 2, key: 'card', value: 'orders.payment_methods.card_pay'},
        {id: 3, key: 'cash', value: 'orders.payment_methods.cash'}
    ];

    /**
     * Delivery Methods
     * @type {*[]}
     */
    var delivery_methods = [
        {id: 1, key: 'courier',  value: 'orders.delivery_methods.courier'},
        {id: 2, key: 'pickup', value: 'orders.delivery_methods.pickup'},
        {id: 3, key: 'new_post', value: 'orders.delivery_methods.new_post'}
    ];

    /**
     * Delivery Methods for list
     * @type {*[]}
     */
    var delivery_methods_for_list = [
        {id: 1, key: 'courier',  value: 'orders.delivery_methods.courier'},
        {id: 2, key: 'pickup', value: 'orders.delivery_methods.pickup'},
        {id: 3, key: 'new_post', value: 'orders.delivery_methods.new_post'},
        {id: 4, key: 'local_store', value: 'orders.delivery_methods.local_store'},
        {id: 4, key: 'web_store', value: 'orders.delivery_methods.web_store'}
    ];

    /**
     * Payment Statuses
     * @type {*[]}
     */
    var payment_statuses = [
        {id: 1, key: 'paid',            value: 'orders.payment_statuses.paid'},
        {id: 2, key: 'not_paid',        value: 'orders.payment_statuses.not_paid'},
        {id: 3, key: 'partially_paid',  value: 'orders.payment_statuses.partly_paid'}
    ];

    /**
     * Order Statuses
     * @type {*[]}
     */
    var order_statuses = [
        {id: 1, key: 'new',         value: 'orders.order_statuses.new'},
        {id: 2, key: 'accepted',    value: 'orders.order_statuses.accepted'},
        {id: 3, key: 'sent',        value: 'orders.order_statuses.sent'},
        {id: 4, key: 'completed',   value: 'orders.order_statuses.completed'},
        {id: 5, key: 'canceled',    value: 'orders.order_statuses.cancelled'},
        {id: 6, key: 'refunded',    value: 'orders.order_statuses.return'}
    ];

    /**
     * Order Statuses Plural
     * @type {*[]}
     */
    var order_statuses_plural = [
        {id: 1, key: 'new',         value: 'orders.order_statuses.new_plural'},
        {id: 2, key: 'accepted',    value: 'orders.order_statuses.accepted_plural'},
        {id: 3, key: 'sent',        value: 'orders.order_statuses.sent_plural'},
        {id: 4, key: 'completed',   value: 'orders.order_statuses.completed_plural'},
        {id: 5, key: 'canceled',    value: 'orders.order_statuses.cancelled_plural'},
        {id: 6, key: 'refunded',      value: 'orders.order_statuses.return_plural'}
    ];

    var storeTypes = [
        {value: false, name: 'orders.attribute.label.local_store'},
        {value: true, name: 'orders.attribute.label.web_store'}
    ];

    return {
        getOrderList:           getOrderListFn,
        getUserList:            getUserListFn,
        getCustomerList:        getCustomerListFn,
        getOrder:               getOrderFn,
        cancelOrder:            cancelOrderFn,
        getPaymentMethods:      getPaymentMethodsFn,
        getDeliveryMethods:     getDeliveryMethodsFn,
        getDeliveryMethodsForList:     getDeliveryMethodsForListFn,
        getPaymentStatuses:     getPaymentStatusesFn,
        getOrderStatuses:       getOrderStatusesFn,
        getOrderStatusesPlural: getOrderStatusesPluralFn,
        showPayment:            showPaymentFn,
        showPaymentStatus:      showPaymentStatusFn,
        showDeliveryMethod:     showDeliveryMethodFn,
        showOrderStatus:        showOrderStatusFn,
        updateOrderStatus:      updateOrderStatusFn,
        saveOrder:              saveOrderFn,
        updateOrder:            updateOrderFn,
        addOrderPayment:        addOrderPaymentFn,
        withdrawOrderPayment:   withdrawOrderPaymentFn,
        withdrawAllOrders:      withdrawAllOrdersFn,
        returnOrder:            returnOrderFn,
        removeProduct:          removeProductFn,
        returnAllOrder:         returnAllOrderFn,
        getStoreTypes:          getStoreTypesFn,
    };

    /**
     * Get Order List
     * @param params
     * @returns {*}
     */
    function getOrderListFn(params) {
        return HttpService.getWParams(baseUrl, params);
    }

    /**
     * Get User List
     * @returns {*}
     */
    function getUserListFn() {
        return HttpService.getWParams('/users?role=manager,limited_manager,admin');
    }

    /**
     * Get Customer List
     * @returns {*}
     */
    function getCustomerListFn() {
        return HttpService.getWParams('/users?role=member');
    }

    /**
     * Get Order
     * @param id
     * @returns {*}
     */
    function getOrderFn(id) {
        return HttpService.get(baseUrl+'/'+ id)
    }

    /**
     * Cancel Order
     * @param id
     * @returns {*}
     */
    function cancelOrderFn(id) {
        return HttpService.post(baseUrl+'/'+ id + '/cancel')
    }

    /**
     * Get Payment Methods
     * @returns {*[]}
     */
    function getPaymentMethodsFn() {
        return payment_methods;
    }

    /**
     * Get Delivery Methods
     * @returns {*[]}
     */
    function getDeliveryMethodsFn() {
        return delivery_methods;
    }

    /**
     * Get Delivery Methods for list
     * @returns {*[]}
     */
    function getDeliveryMethodsForListFn() {
        return delivery_methods_for_list;
    }

    /**
     * Get Order Statuses
     * @returns {*[]}
     */
    function getOrderStatusesFn() {
        return order_statuses;
    }

    /**
     * Get Order Statuses Plural
     * @returns {*[]}
     */
    function getOrderStatusesPluralFn() {
        return order_statuses_plural;
    }

    /**
     * Get Payment Statuses
     * @returns {*[]}
     */
    function getPaymentStatusesFn() {
        return payment_statuses;
    }

    /**
     * Show Payment
     * @param value
     * @returns {*}
     */
    function showPaymentFn(value) {
        switch(value) {
            case 'cod':
                return 'orders.payment_methods.post_pay';
                break;
            case 'card':
                return 'orders.payment_methods.card_pay';
                break;
            case 'cash':
                return 'orders.payment_methods.cash';
                break;
            default:
                return '-';
        }
    }

    /**
     * Show Payment Status
     * @param value
     * @returns {*}
     */
    function showPaymentStatusFn(value) {
        switch(value) {
            case 'paid':
                return 'orders.payment_statuses.paid';
                break;
            case 'not_paid':
                return 'orders.payment_statuses.not_paid';
                break;
            case 'partially_paid':
                return 'orders.payment_statuses.partly_paid';
                break;
            default:
                return '-';
        }
    }

    /**
     * Show Delivery Method
     *
     * @param value
     * @returns {*}
     */
    function showDeliveryMethodFn(value) {
        switch(value) {
            case 'courier':
                return 'orders.delivery_methods.courier';
                break;
            case 'pickup':
                return 'orders.delivery_methods.pickup';
                break;
            case 'new_post':
                return 'orders.delivery_methods.new_post';
                break;
            case 'shop':
                return 'orders.delivery_methods.local_store';
                break;
            default:
                return 'orders.delivery_methods.local_store';
        }
    }

    /**
     * Show Order Status
     * @param value
     * @returns {*}
     */
    function showOrderStatusFn(value) {
        switch(value) {
            case 'new':
                return 'orders.order_statuses.new';
                break;
            case 'accepted':
                return 'orders.order_statuses.accepted';
                break;
            case 'sent':
                return 'orders.order_statuses.sent';
                break;
            case 'completed':
                return 'orders.order_statuses.completed';
                break;
            case 'canceled':
                return 'orders.order_statuses.cancelled';
                break;
            case 'return':
                return 'orders.order_statuses.return';
                break;
            case 'refunded':
                return 'orders.order_statuses.return';
                break;
            case 'partially_refunded':
                return 'orders.order_statuses.partly_returned';
                break;
            default:
                return '-';
        }
    }

    /**
     * Update Order Status
     * @param item
     * @returns {*}
     */
    function updateOrderStatusFn(item) {
        if (item.status == 'new') {
            return HttpService.post(baseUrl+'/'+ item.id +'/accept');
        } else if (item.status == 'accepted') {
            var data = {
                declaration: item.delivery && item.delivery.declaration ? item.delivery.declaration : ''
            };

            if (!item.delivery || !item.delivery.method || item.delivery.method != 'new_post') {
                return HttpService.post(baseUrl+'/'+ item.id +'/complete');
            }
            return HttpService.post(baseUrl+'/'+ item.id +'/send', data);
        } else if (item.status == 'sent') {
            return HttpService.post(baseUrl+'/'+ item.id +'/complete');
        } else if ((!item.delivery || !item.delivery.method) && item.status == 'accepted') {
            return HttpService.post(baseUrl+'/'+ item.id +'/complete');
        } else if ((item.delivery && item.delivery.method == 'pickup') && item.status == 'accepted') {
            return HttpService.post(baseUrl+'/'+ item.id +'/complete');
        }
    }

    /**
     * Save Order
     * @param data
     * @returns {*}
     */
    function saveOrderFn(data) {
        return HttpService.post(baseUrl, data);
    }

    /**
     * Update Order
     * @param id
     * @param data
     * @returns {*}
     */
    function updateOrderFn(id, data) {
        return HttpService.put(baseUrl+'/'+ id, data);
    }

    /**
     * Add Order Payment
     * @param id
     * @param data
     * @returns {*}
     */
    function addOrderPaymentFn(id, data) {
        return HttpService.post(baseUrl+'/'+ id +'/payments', data);
    }

    /**
     * Withdraw Order Payment
     * @param order_id
     * @param paymet_id
     * @param data
     * @returns {*}
     */
    function withdrawOrderPaymentFn(order_id) {
        var data = {
            id: order_id
        };
        return HttpService.post(baseUrl+'/withdrawn-payments', data);
    }

    /**
     * Return Order
     * @param order_id
     * @param products
     * @returns {*}
     */
    function returnOrderFn(order_id, products) {
        return HttpService.post(baseUrl+'/'+ order_id +'/refund', products);
    }

    /**
     * Remove product from order
     * @param size_id
     * @returns {*}
     */
    function removeProductFn(elem, order_id) {
        HttpService.delete(baseUrl+'/'+ order_id +'/products/'+ elem.id);

        return 'deleted';
    }

    /**
     * Return All Order
     * @param order_id
     * @param products
     * @returns {*}
     */
    function returnAllOrderFn(order_id, products) {
        return HttpService.post(baseUrl+'/'+ order_id +'/refund');
    }

    /**
     * Refund all Orders
     * @returns {*}
     */
    function withdrawAllOrdersFn(url) {
        return HttpService.post(baseUrl + url);
    }

    /**
     * Get store types
     * @returns {*}
     */
    function getStoreTypesFn() {
        return storeTypes;
    }
}