angular
    .module('app.orders')

    .controller('OrdersController.addProduct', OrdersController_Add_Product);


OrdersController_Add_Product.$inject = ['$rootScope', '$scope', 'ngDialog', 'HttpService', '$http', 'CONFIG'];
function OrdersController_Add_Product($rootScope, $scope, ngDialog, HttpService, $http, CONFIG) {
    var vm = this;

    vm.products = [];
    $scope.remoteUrlRequest = remoteUrlRequestFn;
    $scope.modifyResponse = modifyResponseFn;

    function remoteUrlRequestFn(userInputString, timeoutPromise) {
        var baseUrl = CONFIG.host + '/products?';

        if ($scope.urlParams) {
            baseUrl = baseUrl + $scope.urlParams;
        }

        if (userInputString) {
            baseUrl = baseUrl + '&search=' + userInputString;
        }

        baseUrl = baseUrl + '&available_for_order=true';

        return $http.get(baseUrl, {timeout: timeoutPromise});
    }

    function modifyResponseFn(resp) {
        var resp = resp.products;
        _.each(resp || [], function(elem) {
            if (elem && elem.sizes && elem.sizes.items && !_.isEmpty(elem.sizes.items)) {
                if (!elem.media || elem.media.length == 0) {
                    elem.thumbnail= {
                        url :'/assets/images/dashboard/no-image.png'
                    };
                } else {
                    elem.thumbnail = _.find(elem.media, function(el) {
                        return el.tag == 'thumbnail';
                    });
                }
                vm.products.push(elem);
            }
        });

        return {data: resp};
    }

    var productsId = [];

    _.each($scope.element.products, function(elem) {
        if (elem.product_id) {
            productsId.push(elem.product_id);
        }
    });

    $scope.focusInProduct = focusInProductFn;

    function focusInProductFn() {
        $scope.$broadcast('angucomplete-alt:clearInput', 'product_search');
    }

    $scope.maxValue = maxValueFn;

    // Not allow enter quantity > total quantity
    function maxValueFn(elem) {
        if (elem.quantity > elem.total_quantity || elem.quantity == 0 || elem.quantity < 0 || (typeof elem.quantity === 'undefined')) {
            elem.quantity = '';
            elem.available_quantity = elem.total_quantity;

            return elem.quantity;
        }

        elem.available_quantity = elem.total_quantity - elem.quantity;
    }

    // Delete products from list if they are in order list
    $scope.products = vm.products;

    $scope.selectProductChanged = function (item) {
        if (item && item.originalObject) {
            $scope.item = item.originalObject || {};

            if ($scope.item.media && $scope.item.media.length) {
                $scope.item.thumbnail = _.find($scope.item.media, function(el) {
                    return el.tag == 'thumbnail';
                });
            }

            if ($scope.item.cost_price) {
                $scope.item.cost_price = _.find($scope.item.cost_price, function(el) {
                    // return el.is_default;
                    return el.currency_id == ($rootScope.defaultCurrencyId);;
                });
            }

            if ($scope.item.sale_price) {
                $scope.item.sale_price = _.find($scope.item.sale_price, function(el) {
                    // return el.is_default;
                    return el.currency_id == ($rootScope.defaultCurrencyId);;
                });
            }

            $scope.item.product_id = $scope.item.id;

            $scope.item.size = []; // Temp data for product sizes

            if ($scope.item.sizes && $scope.item.sizes.items) {
                _.each($scope.item.sizes.items || {}, function(elem) {
                    if (elem.quantity > 0 && elem.store_id == $scope.element.store_id) {
                        $scope.isSizes = true;

                        elem.total_quantity = elem.quantity;
                        elem.available_quantity = elem.quantity;
                        elem.quantity = '';
                        elem.size_name = '';

                        elem.is_refunded = 0;

                        elem.size_id = elem.id;

                        $scope.item.size.push(elem);
                    }
                });
                $scope.item.sizes.items = $scope.item.size;
            }

            _.each($scope.element.products, function(orderProduct) {
                if (orderProduct.id == $scope.item.id) {
                        _.each(orderProduct.sizes.items, function(productSize) {
                            _.each($scope.item.sizes.items, function(newProductSize, newProductSizeIndex) {

                            if (newProductSize && productSize && newProductSize.id == productSize.size_id) {
                                $scope.item.sizes.items.splice(newProductSizeIndex, 1);
                            }
                        });
                    })
                }
            });
        }
    };

    $scope.addProduct = function() {
        var temp = JSON.parse(JSON.stringify($scope.item));
        temp.sizes = {}; // Temp data of product sizes where quantity > 0
        temp.sizes.items = []; // Temp data of product sizes where quantity > 0
        _.each($scope.item.sizes.items, function(elem) {
            if (elem.quantity > 0) {
                temp.sizes.items.push(elem);
            }
        });

        if ($scope.item.sale_price) {
            temp.price = $scope.item.sale_price;
        }

        if (temp.sizes && temp.sizes.items && temp.sizes.items.length == 0) {
            $scope.error = 'orders.general.error_no_quantity';
            return;
        }

        $scope.item = temp;
        localStorage.removeItem('order_product');
        localStorage.setItem('order_product', JSON.stringify($scope.item));

        $rootScope.$emit('products:add');
        ngDialog.closeAll();
    }
}