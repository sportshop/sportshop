angular
    .module('app.orders')

    .controller('OrdersController.list', OrdersController_List);


OrdersController_List.$inject = ['$rootScope', '$state', '$stateParams', '$filter', 'HttpService', 'BreadcrumbsService', 'MainHeaderService', 'Notify', 'OrdersService', 'items', 'users'];
function OrdersController_List($rootScope, $state, $stateParams, $filter, HttpService, BreadcrumbsService, MainHeaderService, Notify, OrdersService, items, users) {
    BreadcrumbsService.push('orders.title.list');
    MainHeaderService.set('orders.title.list');

    var vm = this;

    // Orders Model
    vm.list = items.data.orders || {};

    vm.historyPaymentList = [
        {id: 1, key: true,  value: 'orders.history_payments.taken_money'},
        {id: 2, key: false, value: 'orders.history_payments.no_taken_money'},
    ];

    // Transform list
    function transformList(list) {
        _.each(list, function (item) {

            if (item.products && item.products.length) {
                _.each(item.products, function(product) {
                    if (product.media && product.media.length) {
                        product.thumbnail = _.find(product.media, function(el) {
                            return el.tag == 'thumbnail';
                        });
                    }
                });
            }

            if (item.price) {
                item.total_price = _.find(item.price, function(el) {
                    return el.currency_id == ($rootScope.defaultCurrencyId);
                });
            }

            if (item.residual_price) {
                item.residual_price = _.find(item.residual_price, function(el) {
                    return el.currency_id == ($rootScope.defaultCurrencyId);
                });
            }

            if (item.residual_payments_rate) {
                item.residual_payments_rate = _.find(item.residual_payments_rate, function(el) {
                    return el.currency_id == ($rootScope.defaultCurrencyId);
                });
            }

            if (item.payments && item.payments.length) {
                _.each(item.payments, function(payment) {
                    payment.rate = _.find(payment.rate, function(el) {
                        return el.is_default;
                    });

                    if (payment.rate && payment.rate.value) {
                        payment.value = payment.rate.value;
                        payment.currency_id = payment.rate.currency_id;
                    }
                });
            }

            return item;
        });

        return list;
    }

    transformList(vm.list.data);

    vm.listPerPage = [
        {key: 10, value: 10},
        {key: 20, value: 20},
        {key: 50, value: 50},
        {key: 100, value: 100},
    ];

    vm.totalWithDrownSum = (items.data && items.data.non_withdrawal_payments_sum) ? items.data.non_withdrawal_payments_sum : '';
    vm.totalWithDrownSum = _.find(vm.totalWithDrownSum, function(el) {
        return el.currency_id == ($rootScope.defaultCurrencyId);
    });

    vm.current_date = new Date();

    vm.payment_methods = OrdersService.getPaymentMethods() || []; // Default payment methods
    vm.payment_status = OrdersService.getPaymentStatuses() || []; // Default payment statuses
    vm.order_status = OrdersService.getOrderStatuses() || []; // Default order statuses
    vm.order_status_plural = OrdersService.getOrderStatusesPlural() || []; // Default order statuses (plural)
    vm.delivery_methods = OrdersService.getDeliveryMethodsForList(); // Default delivery methods
    vm.users = users.data || []; // Users

    // Filters
    vm.searchForm = {
        id: $stateParams.id || null,
        product: $stateParams.product || null,
        customer: $stateParams.customer || null,
        payment: $stateParams.payment || null,
        is_withdrawn: $stateParams.is_withdrawn || null,
        payment_status: $stateParams.payment_status || null,
        order_status: $stateParams.order_status || null,
        delivery_method: $stateParams.delivery_method || null,
        date: {
            startDate: $stateParams.date_start || null,
            endDate: $stateParams.date_end || null
        },
        store_id: localStorage.getItem('active_store') || null,
        manager_id: $stateParams.manager_id || null,
        money_status: $stateParams.money_status || null,
        limit: $stateParams.limit || 20
    };

    vm.dateRangeOptions = {
        locale : {
            format : 'DD/MM/YYYY',
            applyLabel: "Пошук",
            cancelLabel: "Очистити",
        },
        eventHandlers : {
            'apply.daterangepicker' : function() {
                processSearchFn();
            },
            'cancel.daterangepicker' : function() {
                vm.searchForm.date = {
                    startDate: null,
                    endDate: null
                };

                processSearchFn();
            }
        }
    };

    vm.processSearch = processSearchFn;
    vm.filterByStatus = filterByStatusFn;
    vm.showPayment = OrdersService.showPayment;
    vm.showDeliveryMethod = OrdersService.showDeliveryMethod;
    vm.showPaymentStatus = OrdersService.showPaymentStatus;
    vm.showOrderStatus = OrdersService.showOrderStatus;
    vm.updateOrderStatus = updateOrderStatusFn;
    vm.cancelDeclaration = cancelDeclarationFn;
    vm.withDrawPayment = withDrawPaymentFn;
    vm.withdrawAllOrders = withdrawAllOrdersFn;
    vm.changePage = changePageFn;
    vm.printOrders = printOrdersFn;
    vm.printOrder = printOrderFn;

    function printOrderFn(order) {
        HttpService.post('/orders/'+ order.id +'/download', null, null, null, { responseType: 'blob'})
            .then(function(response) {
                var blob = new Blob([response.data], { type: 'application/pdf;charset=utf-8' });
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = `order.pdf`;
                link.click();
            });
    }

    function printOrdersFn() {
        var params = {
            id:              vm.searchForm.id,
            product:         vm.searchForm.product,
            customer:        vm.searchForm.customer,
            payment_method:  vm.searchForm.payment_method,
            payment_status:  vm.searchForm.payment_status,
            is_withdrawn:    vm.searchForm.is_withdrawn,
            status:          vm.searchForm.status,
            delivery_method: vm.searchForm.delivery_method,
            date_end:        vm.searchForm.date.endDate ? $filter('momentDate')(vm.searchForm.date.endDate._d, 'YYYY-MM-DD') : null,
            date_start:      vm.searchForm.date.startDate ? $filter('momentDate')(vm.searchForm.date.startDate._d, 'YYYY-MM-DD') : null,
            store_id:        localStorage.getItem('active_store'),
            manager_id:      vm.searchForm.manager_id,
            money_status:    vm.searchForm.money_status,
            page:            vm.list.current_page || null,
            limit:           vm.searchForm.limit || 20
        };

        var url = '/orders/download';
        if (params.id || params.product || params.customer || params.payment_method || params.payment_status || params.status || params.declaration || params.date_start || params.store_id || params.manager_id || params.money_status || params.page || params.limit || params.is_withdrawn) {
            url = url + '?';
        }
        if (params.id) {
            url = url + 'id='+ params.id;
        }
        if (params.product) {
            url = url + '&product='+ params.product;
        }
        if (params.customer) {
            url = url + '&customer='+ params.customer;
        }
        if (params.payment_method) {
            url = url + '&payment_method='+ params.payment_method;
        }
        if (params.payment_status) {
            url = url + '&payment_status='+ params.payment_status;
        }
        if (params.status) {
            url = url + '&status='+ params.status;
        }
        if (params.delivery_method) {
            url = url + '&delivery_method='+ params.delivery_method;
        }
        if (params.date_end) {
            url = url + '&date_end='+ params.date_end;
        }
        if (params.date_start) {
            url = url + '&date_start='+ params.date_start;
        }
        if (params.store_id) {
            url = url + '&store_id='+ params.store_id;
        }
        if (params.manager_id) {
            url = url + '&manager_id='+ params.manager_id;
        }
        if (params.money_status) {
            url = url + '&money_status='+ params.money_status;
        }
        if (params.page) {
            url = url + '&page='+ params.page;
        }
        if (params.limit) {
            url = url + '&limit='+ params.limit;
        }
        if (params.is_withdrawn) {
            url = url + '&limit='+ params.is_withdrawn;
        }

        HttpService.post(url, null, null, null, { responseType: 'blob'})
            .then(function(response) {
                var blob = new Blob([response.data], { type: 'application/pdf;charset=utf-8' });
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = `orders.pdf`;
                link.click();
            });
    }

    // Change Filter
    function processSearchFn() {
        var params = {
            id:              vm.searchForm.id,
            product:         vm.searchForm.product,
            customer:        vm.searchForm.customer,
            payment_method:  vm.searchForm.payment_method,
            payment_status:  vm.searchForm.payment_status,
            is_withdrawn:    vm.searchForm.is_withdrawn,
            status:          vm.searchForm.status,
            delivery_method: vm.searchForm.delivery_method,
            date_end:        vm.searchForm.date.endDate ? $filter('momentDate')(vm.searchForm.date.endDate._d, 'YYYY-MM-DD') : null,
            date_start:      vm.searchForm.date.startDate ? $filter('momentDate')(vm.searchForm.date.startDate._d, 'YYYY-MM-DD') : null,
            store_id:        localStorage.getItem('active_store'),
            manager_id:      vm.searchForm.manager_id,
            money_status:    vm.searchForm.money_status,
            limit:           vm.searchForm.limit
        };

        var data = {
            params: params
        };
        OrdersService.getOrderList(data)
            .success(function(response){
                $state.go($state.current, params, { notify: false });
                vm.list = response.orders || {};
                transformList(vm.list.data);
                vm.totalWithDrownSum = response.non_withdrawal_payments_sum || '';
                vm.totalWithDrownSum = _.find(vm.totalWithDrownSum, function(el) {
                    return el.currency_id == ($rootScope.defaultCurrencyId);
                });
            });
    }

    function filterByStatusFn(status) {
        vm.searchForm.status = status;
        var params = {
            id:                 vm.searchForm.id,
            product:            vm.searchForm.product,
            customer:           vm.searchForm.customer,
            payment_method:     vm.searchForm.payment_method,
            payment_status:     vm.searchForm.payment_status,
            is_withdrawn:       vm.searchForm.is_withdrawn,
            status:             vm.searchForm.status || status,
            delivery_method:    vm.searchForm.delivery_method,
            date_end:           vm.searchForm.date.endDate ? $filter('momentDate')(vm.searchForm.date.endDate._d, 'YYYY-MM-DD') : null,
            date_start:         vm.searchForm.date.startDate ? $filter('momentDate')(vm.searchForm.date.startDate._d, 'YYYY-MM-DD') : null,
            store_id:           localStorage.getItem('active_store') || '',
            manager_id:         vm.searchForm.manager_id,
            money_status:       vm.searchForm.money_status,
            limit:              vm.searchForm.limit
        };

        var data = {
            params: params
        };
        OrdersService.getOrderList(data)
            .success(function(response){
                $state.go($state.current, params, { notify: false, location: false });
                vm.list = response.orders || {};
                transformList(vm.list.data);
                vm.totalWithDrownSum = response.non_withdrawal_payments_sum || '';
                vm.totalWithDrownSum = _.find(vm.totalWithDrownSum, function(el) {
                    return el.currency_id == ($rootScope.defaultCurrencyId);
                });
            });
    }

    function updateOrderStatusFn(item) {
        if (item.payment_status == 'paid' && item.status == 'accepted') {
            if (item.delivery && item.delivery.method == 'pickup') {
            } else if (!item.delivery || !item.delivery.method) {

            } else {
                item.status = 'sent';
            }
        } else if (item.payment && item.payment.status == 'paid' && (item.status == 'sent')) {
            if (item.payment.method == 'cod') {
            } else {
                item.status = 'completed';
            }
        } else if (item.payment && item.payment.status == 'paid' && (item.status == 'accepted') && (item.delivery && item.delivery.method == 'courier')) {
            item.status = 'sent';
        }

        else {
            if (item.payment && item.payment.status != 'paid' && item.payment.method == 'cod' && item.status != 'sent' && item.delivery.method != 'pickup') {

            } else if (item.payment && item.payment.status != 'paid' && item.status != 'new') {
                Notify.warning('orders.general.need_before_paid_order');
                return
            }
        }

        OrdersService.updateOrderStatus(item)
            .success(function(response) {
                if (item.status == 'new') {
                    item.status = 'accepted';

                    if (!item.manager) {
                        item.manager = {};
                    }
                    item.manager.id = $rootScope.currentUser.user.id;
                } else if (item.status == 'accepted') {
                    if (!item.delivery || !item.delivery.method || (item.payment && item.payment.status == 'paid')) {
                        item.showButton = false;
                        item.status = 'completed';
                    } else if (item.delivery && item.delivery.method == 'pickup') {
                        item.showButton = false;
                        item.status = 'completed';
                    } else {
                        item.status = 'sent';
                    }
                } else if (item.status == 'sent') {
                    item.showButton = false;
                    item.status = 'completed';
                }
            })
            .catch(function(errors) {
                var data = errors. data || errors;

                Notify.warning(data.message)

            })
    }

    function cancelDeclarationFn(item) {
        item.showButton = true;
        item.no_declaration = !item.no_declaration;

        if (item.residual_price && item.residual_price.value <= 0 && item.no_declaration == true) {
            updateOrderStatusFn(item);
            item.status = 'completed';
        } else if (item.no_declaration == true) {

        } else {
            item.status = 'accepted';
        }
    }

    function withDrawPaymentFn(item) {
        OrdersService.withdrawOrderPayment(item.id)
            .success(function() {
                item.currentDate = new Date();
                $state.reload();
            });
    }

    function withdrawAllOrdersFn() {
        var params = {
            id:              vm.searchForm.id,
            product:         vm.searchForm.product,
            customer:        vm.searchForm.customer,
            payment_method:  vm.searchForm.payment_method,
            payment_status:  vm.searchForm.payment_status,
            is_withdrawn:    vm.searchForm.is_withdrawn,
            status:          vm.searchForm.status,
            delivery_method: vm.searchForm.delivery_method,
            date_end:        vm.searchForm.date.endDate ? $filter('momentDate')(vm.searchForm.date.endDate._d, 'YYYY-MM-DD') : null,
            date_start:      vm.searchForm.date.startDate ? $filter('momentDate')(vm.searchForm.date.startDate._d, 'YYYY-MM-DD') : null,
            store_id:        localStorage.getItem('active_store'),
            manager_id:      vm.searchForm.manager_id,
            money_status:    vm.searchForm.money_status,
            page:            vm.list.current_page || null,
            limit:           vm.searchForm.limit || 20
        };

        var url = '/withdrawn-payments';
        if (params.id || params.product || params.customer || params.payment_method || params.payment_status || params.status || params.declaration || params.date_start || params.store_id || params.manager_id || params.money_status || params.page || params.limit || params.is_withdrawn) {
            url = url + '?';
        }
        if (params.id) {
            url = url + 'id='+ params.id;
        }
        if (params.product) {
            url = url + '&product='+ params.product;
        }
        if (params.customer) {
            url = url + '&customer='+ params.customer;
        }
        if (params.payment_method) {
            url = url + '&payment_method='+ params.payment_method;
        }
        if (params.payment_status) {
            url = url + '&payment_status='+ params.payment_status;
        }
        if (params.status) {
            url = url + '&status='+ params.status;
        }
        if (params.delivery_method) {
            url = url + '&delivery_method='+ params.delivery_method;
        }
        if (params.date_end) {
            url = url + '&date_end='+ params.date_end;
        }
        if (params.date_start) {
            url = url + '&date_start='+ params.date_start;
        }
        if (params.store_id) {
            url = url + '&store_id='+ params.store_id;
        }
        if (params.manager_id) {
            url = url + '&manager_id='+ params.manager_id;
        }
        if (params.money_status) {
            url = url + '&money_status='+ params.money_status;
        }
        if (params.page) {
            url = url + '&page='+ params.page;
        }
        if (params.limit) {
            url = url + '&limit='+ params.limit;
        }
        if (params.is_withdrawn) {
            url = url + '&limit='+ params.is_withdrawn;
        }

        OrdersService.withdrawAllOrders(url)
            .success(function() {
                $state.reload();
            });
    }

    function changePageFn() {
        var params = {
            id:              vm.searchForm.id,
            product:         vm.searchForm.product,
            customer:        vm.searchForm.customer,
            payment_method:  vm.searchForm.payment_method,
            payment_status:  vm.searchForm.payment_status,
            is_withdrawn:    vm.searchForm.is_withdrawn,
            status:          vm.searchForm.status,
            delivery_method: vm.searchForm.delivery_method,
            date_end:        vm.searchForm.date_end,
            date_start:      vm.searchForm.date_start,
            limit:           vm.searchForm.limit,
            page:            vm.list.current_page,
            store_id:        localStorage.getItem('active_store') || '',
            manager_id:      vm.searchForm.manager_id,
            money_status:    vm.searchForm.money_status
        };

        var data = {
            params: params
        };

        OrdersService.getOrderList(data)
            .success(function(response){
                $state.go($state.current, params, { notify: false });
                vm.list = response.orders || {};
                transformList(vm.list.data);
                vm.totalWithDrownSum = response.non_withdrawal_payments_sum || '';
                vm.totalWithDrownSum = _.find(vm.totalWithDrownSum, function(el) {
                    return el.currency_id == ($rootScope.defaultCurrencyId);
                });
            });
    }
}