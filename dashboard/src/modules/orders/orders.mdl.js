import layoutTemplate from "../layouts/dashboard.html";
import listTemplate from "./list.html";
import formTemplate from "./form.html";
import generalTemplate from "./_general.html";
import customerInfoTemplate from "./_customer-info.html";
import statusTemplate from "./_status.html";
import orderProductsTemplate from "./_orders_products.html";

angular
    .module('app.orders', [])
    .config(configure);


configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('orders', {
            abstract: true,
            url: '/orders',
            views: {
                '': {
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('orders.list', {
            url: '?page?product?customer?payment_method?payment_status?status?declaration?date_start?date_end?store_id?user_id?money_status?is_withdrawn',
            views: {
                'content': {
                    controller: 'OrdersController.list as ctrl',
                    templateUrl: listTemplate
                }
            },
            reloadOnSearch: false,
            resolve: {
                items: [
                    '$stateParams', 'OrdersService',
                    function ($stateParams, OrdersService) {
                        return OrdersService.getOrderList({
                            params: {
                                page: $stateParams.page || 1,
                                limit: $stateParams.limit || 20,
                                product: $stateParams.product,
                                customer: $stateParams.customer,
                                payment_method: $stateParams.payment_method,
                                payment_status: $stateParams.payment_status,
                                is_withdrawn: $stateParams.is_withdrawn,
                                status: $stateParams.status,
                                delivery_method: $stateParams.delivery_method,
                                date_end: $stateParams.date_end,
                                date_start: $stateParams.date_start,
                                store_id: localStorage.getItem('active_store') || '',
                                user_id: $stateParams.user_id,
                                money_status: $stateParams.money_status
                            }
                        });
                    }
                ],
                users: [
                    'OrdersService',
                    function (OrdersService) {
                        return OrdersService.getUserList();
                    }
                ],
            }
        })
        .state('orders.create', {
            url: '/create',
            views: {
                'content': {
                    controller: 'OrdersController.edit as ctrl',
                    templateUrl: formTemplate
                },
                'general@orders.create': {
                    templateUrl: generalTemplate
                },
                'customer@orders.create': {
                    templateUrl: customerInfoTemplate
                },
                'status@orders.create': {
                    templateUrl: statusTemplate
                },
                'order_products@orders.create': {
                    templateUrl: orderProductsTemplate
                }
            },
            resolve: {
                item: [
                    function () {
                        return {}
                    }
                ],
                discounts : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalDiscounts) {
                            return SettingsService.getDiscountsList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalDiscounts));
                    }
                ],
                currencies : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalCurrencies) {
                            return SettingsService.getCurrenciesList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalCurrencies));
                    }
                ],
                users: [
                    'OrdersService',
                    function (OrdersService) {
                        return OrdersService.getUserList();
                    }
                ]
            }
        })
        .state('orders.edit', {
            url: '/:id',
            views: {
                'content': {
                    controller: 'OrdersController.edit as ctrl',
                    templateUrl: formTemplate
                },
                'general@orders.edit': {
                    templateUrl: generalTemplate
                },
                'customer@orders.edit': {
                    templateUrl: customerInfoTemplate
                },
                'status@orders.edit': {
                    templateUrl: statusTemplate
                },
                'order_products@orders.edit': {
                    templateUrl: orderProductsTemplate
                }
            },
            resolve: {
                item: [
                    '$stateParams', 'OrdersService',
                    function ($stateParams, OrdersService) {
                        return OrdersService.getOrder($stateParams.id);
                    }
                ],
                discounts : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalDiscounts) {
                            return SettingsService.getDiscountsList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalDiscounts));
                    }
                ],
                currencies : [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalCurrencies) {
                            return SettingsService.getCurrenciesList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalCurrencies));
                    }
                ],
                users: [
                    'OrdersService',
                    function (OrdersService) {
                        return OrdersService.getUserList();
                    }
                ]
            }
        });
}