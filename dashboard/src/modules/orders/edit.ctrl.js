import addProductTemplate from "./product/add/add.html";

angular
    .module('app.orders')
    .controller('OrdersController.edit', OrdersController_Edit);


OrdersController_Edit.$inject = ['$rootScope', '$scope', '$state', '$stateParams', '$http', '$filter', 'BreadcrumbsService', 'MainHeaderService', 'HttpService', 'Notify', 'ngDialog', 'OrdersService', 'SettingsService', 'item', 'discounts', 'currencies', 'users', 'CONFIG'];
function OrdersController_Edit($rootScope, $scope, $state, $stateParams, $http, $filter, BreadcrumbsService, MainHeaderService, HttpService, Notify, ngDialog, OrdersService, SettingsService, item, discounts, currencies, users, CONFIG) {
    BreadcrumbsService.push('orders.title.list', 'orders.list');

    var vm = this;
    vm.cities = [];

    npApi.init({
        citiesApiUrl    :   'https://api.novaposhta.ua/v2.0/json/Address/getCities',
        warehousesApiUrl:   'https://api.novaposhta.ua/v2.0/json/AddressGeneral/getWarehouses',
        apiKey          :   '92e0bc62e5e8076e83768c6caebea1b2',
        language        :   'UA'
    });

    vm.disableGeneralDiscounts = false; // not disable general discount

    vm.citiesCatalog = JSON.parse(localStorage.getItem('citiesCatalog'));
    vm.departmentsCatalog = JSON.parse(localStorage.getItem('warehouses'));

    vm.payment_methods = OrdersService.getPaymentMethods(); // Default payment methods

    vm.delivery_methods = OrdersService.getDeliveryMethods(); // Default delivery methods
    vm.payment_status = OrdersService.getPaymentStatuses(); // Default payment statuses
    vm.order_status = OrdersService.getOrderStatuses();     // Default order statuses

    // Default currencies
    if (currencies && currencies.data) {
        vm.currencies = currencies.data || [];
        $rootScope.globalCurrencies = JSON.parse(JSON.stringify(currencies.data));
    } else {
        vm.currencies = currencies || [];
    }

    // Discounts List
    if (discounts && discounts.data) {
        vm.discounts = discounts.data || [];
        $rootScope.globalDiscounts = JSON.parse(JSON.stringify(discounts.data));
    } else {
        vm.discounts = discounts || [];
    }

    vm.users = users.data  || [];                      // Users

    // Store types
    vm.storeTypes = OrdersService.getStoreTypes();

    vm.is_no_refund = 0; //

    vm.remoteCustomerUrlRequest = remoteCustomerUrlRequestFn;
    vm.modifyCustomersResponse = modifyCustomersResponseFn;

    vm.cancelOrder = function (id) {
        HttpService.post('/orders/'+ id + '/cancel')
            .then(function() {
                $state.go('orders.list')
            });
    };

    function remoteCustomerUrlRequestFn(userInputString, timeoutPromise) {
        var baseUrl = CONFIG.host + '/users?role=member';

        if (userInputString) {
            baseUrl = baseUrl + '&name=' + userInputString;
        }

        return $http.get(baseUrl, {timeout: timeoutPromise});
    }

    function modifyCustomersResponseFn(resp) {
        return {data: resp};
    }

    if ($stateParams.id) { // Edit orders
        BreadcrumbsService.push('orders.title.edit');
        MainHeaderService.set('orders.title.edit');
        vm.orderId = $stateParams.id;

        // Order Model
        vm.item = item.data || {};
        vm.item.store_id = vm.item.store.id;

        vm.is_no_refunded = 0;

        if (vm.item && vm.item.manager && vm.item.manager.id) {
            vm.item.manager_id = vm.item.manager.id;
        } else {
            vm.item.manager_id = ($rootScope.currentUser && $rootScope.currentUser.user && $rootScope.currentUser.user.id) ? $rootScope.currentUser.user.id : null;
        }
        if (!vm.item.payments) {
            vm.item.payments = [];
        }
        if (vm.item.payments && vm.item.payments.length) {
            _.each(vm.item.payments, function(payment) {
                payment.default_rate = _.find(payment.rate, function(el) {
                    return el.is_default;
                });

                if (payment.default_rate && payment.default_rate.value) {
                    payment.value = payment.default_rate.value;
                    payment.currency_id = payment.default_rate.currency_id;
                }
            });
        }

        if (vm.item.customer && vm.item.customer.phone) {
            vm.item.customer.phone = vm.item.customer.phone.replace(/^(\+38|38|8)/, '');
        }

        if (vm.item.customer && vm.item.customer['id']) {
            vm.item.customer_type = 'isset';

            if (vm.item.customer.id) {
                vm.item.customer_id = vm.item.customer.id;
            }

            vm.customer = JSON.parse(JSON.stringify(vm.item.customer));

            if (vm.item.customer.discount && vm.item.customer.discount.id) {
                $scope.discount_id = vm.item.customer.discount.id;
            }
            vm.item.customer.first_name = null;
            vm.item.customer.last_name = null;
            vm.item.customer.phone = null;
            vm.item.customer.address = null;
            vm.item.customer.email = null;

        } else {
            vm.item.customer_type = 'new';
        }

        if (vm.item.products) {
            var generalDiscount, generalDiscountCount = 0; var sizesQuantity = 0;
            var temp_products = [];
            _.each(vm.item.products, function(product, productIndex) {
                if (product.media && product.media.length) {
                    product.thumbnail = _.find(product.media, function(el) {
                        return el.tag == 'thumbnail';
                    });
                }

                if (product.cost_price) {
                    product.cost_price = _.find(product.cost_price, function(el) {
                        return el.currency_id == ($rootScope.defaultCurrencyId);
                    });
                }

                if (product.sale_price) {
                    product.price = _.find(product.sale_price, function(el) {
                        return el.currency_id == ($rootScope.defaultCurrencyId);
                    });
                }

                if (product.sizes && product.sizes.items && product.sizes.items.length) {
                    sizesQuantity += product.sizes.items.length;
                }

                _.each(product.sizes.items, function(size, index) {
                    if (size.size_id) {
                        size.id = size.size_id;
                    }

                    if (size.discount) {
                        if (index == 0 && productIndex == 0) {
                            generalDiscount = size.discount;
                        }

                        if (generalDiscount == size.discount) {
                            generalDiscountCount++;
                        }
                    }

                    if (size.is_refunded == 1) {
                        vm.is_refunded = 1;
                    }

                    if (size.is_refunded == 0) {
                        vm.is_no_refunded = 1;
                    }

                    size.available_quantity = size.total_quantity;
                    size.total_quantity = size.total_quantity + size.quantity;

                    return size;
                });

                return product;
            });


            // General discount or product discount
            if (sizesQuantity && (generalDiscountCount == sizesQuantity)) {
                vm.item.discount = generalDiscount;
                vm.disableGeneralDiscounts = false;
            } else if (sizesQuantity && (generalDiscountCount != sizesQuantity)) {
                _.each(vm.item.products, function(product) {
                    _.each(product.sizes.items, function(size, index) {
                        if (size.discount) {
                            if (index == 0) {
                                vm.disableGeneralDiscounts = true;
                                product.custom_price_discount = size.discount;
                                customPriceDiscountFn(product);
                            }
                        }
                    });
                });
            }
        }

        // Hide save button if get status canceled
        if (vm.item.status === 'canceled') {
            vm.hideSave = true;
        }

        if (vm.item.is_from_web_store) {
            vm.payment_methods = [
                {id: 1, key: 'cod',  value: 'orders.payment_methods.post_pay'},
                {id: 2, key: 'card', value: 'orders.payment_methods.card_pay'},
                {id: 3, key: 'cash', value: 'orders.payment_methods.cash'}
            ];
        } else {
            vm.payment_methods = [
                {id: 3, key: 'cash', value: 'orders.payment_methods.cash'},
                {id: 2, key: 'card', value: 'orders.payment_methods.card_pay'}
            ];
        }
    } else { // Create orders
        BreadcrumbsService.push('orders.title.create');
        MainHeaderService.set('orders.title.create');

        // Order Model
        vm.item = {
            customer_type: 'new',
            customer: {},
            delivery:{
                method: 'pickup'
            },
            payment: {
                method: 'cash',
                status: 'not_paid',
            },
            status: 'new',
            payments: [],
            manager_id: ($rootScope.currentUser && $rootScope.currentUser.user && $rootScope.currentUser.user.id) ? $rootScope.currentUser.user.id : null,
            is_from_web_store: false,
            currency_id: $rootScope.defaultCurrencyId
        };

        vm.payment_methods = [
            {id: 2, key: 'card', value: 'orders.payment_methods.card_pay'},
            {id: 3, key: 'cash', value: 'orders.payment_methods.cash'}
        ];

        var store_id = localStorage.getItem('active_store');
        if (store_id) {
            vm.item.store_id = +store_id;
        } else {
        }
    }

    vm.disableFieldsForOtherManager = disableFieldsForOtherManagerFn;

    function disableFieldsForOtherManagerFn() {
     if (vm.item && vm.item.manager && vm.item.manager.id && $rootScope.currentUser && $rootScope.currentUser.user
         && $rootScope.currentUser.user.id && $rootScope.currentUser.user.role_name != 'admin'
         && vm.item.manager.id != $rootScope.currentUser.user.id) {
         return true;
     }

     return false;
    }

    vm.updateCustomerData = updateCustomerDataFn;
    vm.updateStore = updateStoreFn;

    function updateStoreFn(discount) {
        if (discount) {
            $scope.discount_id = discount;
        } else {
            $scope.discount_id = null;
        }
        if  (vm.item.products && vm.item.products.length) {
            var params = '?';
            var ids = [];

            if ($scope.discount_id) {
                params = params + 'discount_id=' + $scope.discount_id;
            }

            if (vm.item.store_id) {
                params = params + '&store_id=' + vm.item.store_id;
            }

            _.each(vm.item.products, function(elem) {

                if (elem.id) {
                    ids.push(elem.id);
                }
            });

            params = params + '&available_for_order=true&id='+ ids.join(',');

            HttpService.get('/products'+ params)
                .success(function(resp) {
                    var resp = resp.products || [];
                    _.each(vm.item.products, function(elem) {
                        if (elem.price) {
                            var newElem = _.find(resp, function(item) {
                                return item.id == elem.id;
                            });

                            if (newElem && newElem.sale_price) {
                                elem.price = _.find(newElem.sale_price, function(el) {
                                    return el.currency_id == ($rootScope.defaultCurrencyId);
                                });
                            }
                        }
                    });
                });
        }
    }

    function updateCustomerDataFn() {
        if (vm.item.customer_type == 'new') {

            if  (vm.item.products && vm.item.products.length) {
                var params = '?';
                var ids = [];

                if (vm.item.store_id) {
                    params = params + '&store_id=' + vm.item.store_id;
                }

                _.each(vm.item.products, function(elem) {

                    if (elem.id) {
                        ids.push(elem.id);
                    }
                });

                params = params + '&available_for_order=true&id='+ ids.join(',');

                HttpService.get('/products'+ params)
                    .success(function(resp) {
                        var resp = resp.products || [];
                        _.each(vm.item.products, function(elem) {
                            if (elem.price) {
                                var newElem = _.find(resp, function(item) {
                                    return item.id == elem.id;
                                });

                                if (newElem && newElem.sale_price) {
                                    elem.price = _.find(newElem.sale_price, function(el) {
                                        return el.currency_id == ($rootScope.defaultCurrencyId);
                                    });
                                }
                            }
                        });
                    });
            }
        } else if (vm.item.customer_type == 'isset' && vm.item.customer_id) {
            if  (vm.item.products && vm.item.products.length) {
                var params = '?';
                var ids = [];

                if ($scope.discount_id) {
                    params = params + 'discount_id=' + $scope.discount_id;
                }

                if (vm.item.store_id) {
                    params = params + '&store_id=' + vm.item.store_id;
                }

                _.each(vm.item.products, function(elem) {

                    if (elem.id) {
                        ids.push(elem.id);
                    }
                });

                params = params + '&available_for_order=true&id='+ ids.join(',');

                HttpService.get('/products'+ params)
                    .success(function(resp) {
                        var resp = resp.products || [];
                        _.each(vm.item.products, function(elem) {
                            if (elem.price) {
                                var newElem = _.find(resp, function(item) {
                                    return item.id == elem.id;
                                });

                                if (newElem && newElem.sale_price) {
                                    elem.price = _.find(newElem.sale_price, function(el) {
                                        return el.currency_id == ($rootScope.defaultCurrencyId);
                                    });
                                }
                            }
                        });
                    });
            }
        }
    }

    vm.focusInCustomer = focusInCustomerFn;

    function focusInCustomerFn() {
        $scope.$broadcast('angucomplete-alt:clearInput', 'customer_search');
    }

    vm.current_date = new Date();

    vm.maxValue = maxValueFn;
    vm.validatePrice = validatePriceFn;
    vm.validatePercent = validatePercentFn;

    vm.totalQuantity = totalQuantityFn;
    vm.totalProductsCostPrice = totalProductsCostPriceFn;

    function totalQuantityFn() {
        var totalQuantity = 0;

        if (vm.item.products && vm.item.products.length) {
            _.each(vm.item.products, function(product) {
                if (product.sizes && product.sizes.items && product.sizes.items.length) {
                    _.each(product.sizes.items, function(elem) {
                        var quantity = 0;

                        if (elem.refunded_quantity) {
                            quantity = elem.quantity - elem.refunded_quantity;
                        } else {
                            quantity = elem.quantity;
                        }

                        totalQuantity += quantity;
                    });
                }
            });
        }

        return totalQuantity;
    }

    function totalProductsCostPriceFn() {
        var totalCostPrice = 0;

        if (vm.item.products && vm.item.products.length) {
            _.each(vm.item.products, function(product) {
                if (product.sizes && product.sizes.items && product.sizes.items.length) {
                    _.each(product.sizes.items, function(elem) {
                        var quantity = 0;

                        if (elem.refunded_quantity) {
                            quantity = elem.quantity - elem.refunded_quantity;
                        } else {
                            quantity = elem.quantity;
                        }

                        totalCostPrice += quantity * $filter('rounded')(product.cost_price.value);
                    });
                }
            });
        }

        return totalCostPrice;
    }

    function validatePercentFn(elem) {
        if (elem.discount <= 0 || elem.discount > 100) {
            elem.discount = '';
            return elem.discount;
        }
    }

    function validatePriceFn(elem) {
        var maxValue = elem.price.value * elem.price.coefficient;
        if (elem.custom_price.value <= 0) {

            elem.custom_price.value = '';
            return $filter('rounded')(elem.custom_price.value);
        }
        if (elem.custom_price.value > maxValue) {
            elem.custom_price.value = $filter('rounded')(maxValue);
            return $filter('rounded')(elem.custom_price.value);
        }
    }

    vm.validateMaxPayment = validateMaxPaymentFn;
    vm.checkEmptyProduct = checkEmptyProductFn;

    function checkEmptyProductFn(product) {
        var empty;

        empty = _.some(product, function(elem) {
            if (elem.quantity > 0 || (elem.quantity == 0 && elem.refunded_quantity <= 0) || elem.quantity == null) {
                return true;
            }
        });

        return empty;
    }

    function validateMaxPaymentFn(payment) {
        // var balance = vm.showBalance();
        //
        // if (balance < 0) {
        //     payment.value = '';
        // }
    }

    function maxValueFn(elem) {
        if (elem.return_piece > elem.quantity || (typeof elem.return_piece === 'undefined')) {
            elem.return_piece = elem.quantity;
        }

        if (elem.quantity > elem.total_quantity || elem.quantity < 0 || (typeof elem.quantity === 'undefined')) {
            elem.quantity = '';
            elem.available_quantity = elem.total_quantity;
            return elem.quantity;
        }

        elem.available_quantity = elem.total_quantity - elem.quantity;
    }

    vm.checkChangeFields = vm.item.status == 'completed' || vm.item.status == 'refunded' || vm.item.status == 'partially_refunded' || vm.item.status == 'canceled';
    vm.checkChangeSomeFields = vm.item.status == 'completed' || vm.item.status == 'refunded' || vm.item.status == 'partially_refunded' || vm.item.status == 'canceled' || vm.item.status == 'sent';

    vm.checkChangeManagerOrCustomer = $rootScope.currentUser.user.role_name == 'manager' && vm.item.status != 'new' && vm.item.status != 'accepted';
    vm.checkModifyProducts = $rootScope.currentUser.user.role_name == 'manager' &&
        ((vm.item.is_from_web_store && (vm.item.status == 'completed' || vm.item.status == 'refunded' || vm.item.status == 'partially_refunded' || vm.item.status == 'sent'))
            || (!vm.item.is_from_web_store && vm.item.status == 'completed' || vm.item.status == 'refunded' || vm.item.status == 'partially_refunded'));

    vm.isManager = $rootScope.currentUser.user.role_name == 'manager' || $rootScope.currentUser.user.role_name == 'limited_manager';

    vm.selectCustomerChanged = selectCustomerChangedFn;
    vm.showPartiallyPaidTable = showPartiallyPaidTableFn;
    vm.addPayment = addPaymentFn;
    vm.deletePayment = deletePaymentFn;
    vm.withdrawPayment = withdrawPaymentFn;
    vm.showBalance = showBalanceFn;
    vm.updateOrderStatus = updateOrderStatusFn;
    vm.cancelDeclaration = cancelDeclarationFn;
    vm.checkWithdrawAt = checkWithdrawAtFn;

    vm.addProduct = addProductFn;
    vm.deleteProduct = deleteProductFn;

    vm.returnAllOrder = returnAllOrderFn;
    vm.returnPartOrder = returnPartOrderFn;

    vm.showOrderStatus = OrdersService.showOrderStatus;
    vm.showPaymentStatus = OrdersService.showPaymentStatus;

    vm.returnOrderProducts = returnOrderProductsFn;

    vm.printOrder = printOrderFn;
    vm.payAllMoney = payAllMoneyFn;
    vm.showTotalPaymentWithDrawSum = showTotalPaymentWithDrawSumFn;
    vm.changeStoreType = changeStoreTypeFn;

    function payAllMoneyFn() {
        if (vm.item && vm.item.payments && vm.item.payments.length) {
            var lastItem = vm.item.payments.length - 1;
            vm.item.payments[lastItem].value = 0;
            var sum = showBalanceFn();
            vm.item.payments[lastItem].value = sum;

            if (vm.currencies && vm.currencies.length && vm.currencies[0]) {
                vm.item.payments[lastItem].currency_id = vm.currencies[0].value;
            } else {
                vm.item.payments[lastItem].currency_id = $rootScope.defaultCurrencyId;
            }
        }
    }

    function showTotalPaymentWithDrawSumFn() {
        var sum = 0;
        if (vm.item && vm.item.payments) {

        _.each(vm.item.payments, function(payment) {
            payment.grn_rate = _.find(payment.rate, function(el) {
                return el.currency_id == $rootScope.defaultCurrencyId; // set to default
            });
        });


         _.each(vm.item.payments, function(elem) {
             if (elem && elem.grn_rate && elem.grn_rate.value && elem.withdrawn_at) {
                 sum = sum + +elem.grn_rate.value;
             }
         });

         return sum;

     }
    }

    function changeStoreTypeFn() {
        if (vm.item.is_from_web_store == true) {
            vm.payment_methods = [
                {id: 1, key: 'cod',  value: 'orders.payment_methods.post_pay'},
                {id: 2, key: 'card', value: 'orders.payment_methods.card_pay'},
                {id: 3, key: 'cash', value: 'orders.payment_methods.cash'}
            ];

            if (!vm.item.payment) {
                vm.item.payment = {};
            }
            vm.item.payment.method = 'cod';

            if (!vm.item.delivery) {
                vm.item.delivery = {};
            }
            vm.item.delivery.method = 'new_post';
        } else {
            if (!vm.item.payment) {
                vm.item.payment = {};
            }
            vm.item.payment.method = 'cash';

            vm.payment_methods = [
                {id: 2, key: 'card', value: 'orders.payment_methods.card_pay'},
                {id: 3, key: 'cash', value: 'orders.payment_methods.cash'}
            ];
        }
    }
    function checkWithdrawAtFn() {
        if (vm.item.payments) {
            return _.find(vm.item.payments, function(el) { return el.withdrawn_at == null; });
        }
    }

    function printOrderFn() {
        HttpService.post('/orders/'+ $stateParams.id +'/download', null, null, null, { responseType: 'blob'})
            .then(function(response) {
                var blob = new Blob([response.data], { type: 'application/pdf;charset=utf-8' });
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = `order.pdf`;
                link.click();
            });
    }

    function returnOrderProductsFn() {
        vm.returnProducts = true;
    }

    vm.save = saveFn;

    function getWarehousesForCurrentCity(warehouses, cityRef) {
        let result = [];
        let l = warehouses.length;
        for (let i = 0; i < l; i++) {
            if (warehouses[i].CityRef == cityRef) {
                result.push(warehouses[i]);
            }
        }
        return result;
    }

    vm.focusInCity = focusInCityFn;
    function focusInCityFn() {
        vm.item.delivery.city = null;
        vm.item.delivery.ref = null;
        $scope.$broadcast('angucomplete-alt:clearInput', 'city');
    }
    vm.focusInDepartment = focusInDepartmentFn;
    function focusInDepartmentFn() {
        vm.item.delivery.department = null;
        $scope.$broadcast('angucomplete-alt:clearInput', 'department');
    }

    vm.selectCityChanged = selectCityChangedFn;
    function selectCityChangedFn(item) {
        if (item && item.originalObject) {
            if (!vm.item.delivery) {
                vm.item.delivery = {};
            }
            if (!vm.item.delivery.city) {
                vm.item.delivery.city = null;
            }

            vm.item.delivery.city = item.originalObject.value;
            vm.item.delivery.ref = item.originalObject.Ref;

            vm.departmentsCatalog = getWarehousesForCurrentCity(JSON.parse(localStorage.getItem('warehouses')), item.originalObject.Ref);
        }
    }

    if (vm.item && vm.item.delivery && vm.item.delivery.ref) {
        vm.departmentsCatalog = getWarehousesForCurrentCity(JSON.parse(localStorage.getItem('warehouses')), vm.item.delivery.ref);
    }

    vm.selectDepartmentChanged = selectDepartmentChangedFn;
    function selectDepartmentChangedFn(item) {
        if (item && item.originalObject) {
            if (!vm.item.delivery) {
                vm.item.delivery = {};
            }
            if (!vm.item.delivery.department) {
                vm.item.delivery.department = null;
            }

            vm.item.delivery.department = item.originalObject.value;
        }
    }

    function updateOrderStatusFn(item) {
        if (item.payment && item.payment.status == 'paid' && (item.status == 'accepted')) {
            if (item.delivery && item.delivery.method == 'pickup') {
            } else if (!item.delivery || !item.delivery.method) {
            } else {
                item.status = 'sent';
            }
        } else if (item.payment && item.payment.status == 'paid' && (item.status == 'sent')) {
            if (item.payment && item.payment.method == 'cod') {
            } else {
                item.status = 'completed';
            }
        } else if (item.payment && item.payment.status == 'paid' && (item.status == 'accepted') && (item.delivery && item.delivery.method == 'courier')) {
            item.status = 'sent';
        }
        else {
            if (item.payment && item.payment.status != 'paid' && vm.item.payment.method == 'cod' && item.status != 'sent' && item.delivery.method != 'pickup') {
            } else if (item.payment && item.payment.status != 'paid' && item.status != 'new') {
                Notify.warning('orders.general.need_before_paid_order');
                return
            }
        }

        OrdersService.updateOrderStatus(item)
            .success(function() {
                if (vm.item.status == 'new') {
                    vm.item.status = 'accepted';
                } else if (vm.item.status == 'accepted') {
                    if (vm.item.payment && vm.item.payment.status == 'paid') {
                        vm.item.status = 'completed';
                    } else {
                        if (!item.delivery || !item.delivery.method) {
                            item.status = 'completed';
                        } else if (item.delivery && item.delivery.method == 'pickup') {
                            item.status = 'completed';
                        } else {
                            item.status = 'sent';
                        }
                    }
                } else if (vm.item.status == 'sent') {
                    vm.item.status = 'completed';
                }
            })
            .catch(function(errors) {
                var data = errors.data || errors;
                // Notify.warning(data.message);
                Notify.warning('events.product_error');
            });
    }

    function cancelDeclarationFn(item) {
        if (vm.showBalance() <= 0 && vm.item.no_declaration == true) {
            updateOrderStatusFn(item);
            vm.item.status = 'completed';
        } else if (vm.item.no_declaration == true) {
            vm.item.status = 'sent';
        } else {
            vm.item.status = 'accepted';
        }
    }

    // Return all order
    function returnAllOrderFn() {
        Notify.confirm(function () {
            var products = [];

            if (vm.item && vm.item.products) {
                _.each(vm.item.products, function(product) {
                    if (product.sizes && product.sizes.items) {
                        _.each(product.sizes.items, function(size) {

                            if (size.quantity) {
                                var data = {
                                    size_id:   size.id,
                                    quantity:  size.quantity
                                };
                                products.push(data);
                            }
                        })
                    }
                })
            }

            if (products && products.length) {
                OrdersService.returnOrder(vm.item.id, {products: products})
                    .success(function(resp) {
                        Notify.success('Замовлення повернуте');
                        $state.go('orders.edit', {id: vm.item.id}, {reload: true, notify: true });
                    });
            }
        }, 'users.general.are_you_sure');
    }

    // Return a part of order
    function returnPartOrderFn() {
        Notify.confirm(function () {
            var products = [];

            if (vm.item && vm.item.products) {
                _.each(vm.item.products, function(product) {
                    if (product.sizes && product.sizes.items) {
                        _.each(product.sizes.items, function(size) {
                            if (size.return_piece) {

                                var data = {
                                    size_id:  size.id,
                                    quantity: size.return_piece
                                };
                                products.push(data);
                            }
                        })
                    }
                })
            }

            if (products && products.length) {
                OrdersService.returnOrder(vm.item.id, {products: products})
                    .success(function(resp) {
                        Notify.success('Замовлення частково повернуте');
                        $state.go('orders.edit', { id: vm.item.id }, { reload: true, notify: true });
                    });
            }
        }, 'users.general.are_you_sure');
    }

    function selectCustomerChangedFn(item) {
        if (item && item.originalObject) {
            if (item.originalObject && item.originalObject.discount && item.originalObject.discount.id) {
                $scope.discount_id = item.originalObject.discount.id;
            }

            if  (vm.item.products && vm.item.products.length) {
                var params = '?';
                var ids = [];

                if ($scope.discount_id) {
                    params = params + 'discount_id=' + $scope.discount_id;
                }

                if (vm.item.store_id) {
                    params = params + '&store_id=' + vm.item.store_id;
                }

                _.each(vm.item.products, function(elem) {

                    if (elem.id) {
                        ids.push(elem.id);
                    }
                });

                params = params + '&available_for_order=true&id='+ ids.join(',');

                HttpService.get('/products'+ params)
                    .success(function(resp) {
                        var resp = resp.products || [];
                        _.each(vm.item.products, function(elem) {
                            if (elem.price) {
                                var newElem = _.find(resp, function(item) {
                                    return item.id == elem.id;
                                });

                                if (newElem && newElem.sale_price) {
                                    elem.price = _.find(newElem.sale_price, function(el) {
                                        return el.currency_id == ($rootScope.defaultCurrencyId);
                                    });
                                }
                            }
                        });
                    });
            }

            vm.item.customer_type = 'isset';

            vm.item.customer = [];

            vm.item.customer_id = item.originalObject.id;
            vm.customer = item.originalObject;
        }
    }

    // Show partially paid table
    function showPartiallyPaidTableFn() {
        var currency = null;

        if (vm.item.payment && vm.item.payment.status == 'partially_paid') {
            var payments_length = vm.item.payments.length;
            if (payments_length == 0) {
                vm.item.payments.push({
                    rate: {
                        value: '',
                        currency_id: currency || $rootScope.defaultCurrencyId
                    },
                    edit: true
                });
            } else if (payments_length > 0 && vm.item.payments[payments_length-1] && vm.item.payments[payments_length-1].value != '') {
                vm.item.payments.push({
                    rate: {
                        value: '',
                        currency_id: currency || $rootScope.defaultCurrencyId
                    },
                    edit: true
                });
            }
        }
    }

    // Add payment in partially paid table
    function addPaymentFn() {
        var balance = showBalanceFn();
        if (!(vm.total_price_with_user_discount > 0 || vm.total_price_with_discount > 0)) {
            Notify.warning('Спочатку потрібно додати товар');
            return;
        }

        var currency = null;
        var data = {};
        var paymentsLength = vm.item.payments.length;
        if (paymentsLength) {
            var temp = vm.item.payments[paymentsLength-1];
            var index = vm.item.payments.indexOf(temp);
            currency = (temp.currency_id) ? temp.currency_id : ($rootScope.defaultCurrencyId);
            data = {
                payments: [ temp ]
            };
        } else {
            var temp = [];
            var index = null;
            currency = (temp.currency_id) ? temp.currency_id : ($rootScope.defaultCurrencyId);
            data = {
                payments: [ temp ]
            };
        }

        function makePayment() {
            vm.paymentErrors = null;
            vm.errors = null;

            // Add new payment
            vm.item.payments.push({
                    value: '',
                    currency_id: currency || ($rootScope.defaultCurrencyId),
                    edit: true
            });
        }

        var orderData = {};
        for (var prop in vm.item) {
            if (vm.item.hasOwnProperty(prop)) {
                orderData[prop] = vm.item[prop];
            }
        }

        makePayment();
    }

    // Remove payment from partially paid table
    function deletePaymentFn(index) {
        vm.item.payments.splice(index, 1);
    }

    // Withdraw payment from partially paid table
    function withdrawPaymentFn(order_id, index) {
        if (order_id && payment_id) {
            OrdersService.withdrawOrderPayment(order_id)
                .success(function(response) {
                    Notify.success(response.message);
                    vm.item.payments[index].withdrawn_at = vm.current_date;
                    vm.paymentErrors = false;
                })
                .error(function(errors) {
                    vm.paymentErrors = errors.errors;
                })
        }
    }

    // Show balance in partially paid table
    function showBalanceFn() {
        var defaultCurrencyId = $rootScope.defaultCurrencyId;
        var defaultCurrencyValue = _.findWhere(vm.currencies, {id: defaultCurrencyId});
        defaultCurrencyValue = +defaultCurrencyValue.value;

        var remains =_.reduce(vm.item.payments, function(memo, elem){

            var paymentPrice = (elem.value) ? +elem.value : 0;

            var paymentCurrencyValue = _.findWhere(vm.currencies, {
                id: (elem.currency_id) ? elem.currency_id : ($rootScope.defaultCurrencyId)
            });

            paymentCurrencyValue = (paymentCurrencyValue && paymentCurrencyValue.value) ? +paymentCurrencyValue.value : 0;

            if (defaultCurrencyValue < paymentCurrencyValue) {
                paymentPrice = (paymentPrice * paymentCurrencyValue);
            } else if (defaultCurrencyValue > paymentCurrencyValue) {
                paymentPrice = (paymentPrice / paymentCurrencyValue);
            }
            return $filter('rounded')(memo + paymentPrice);
        }, 0);

        var totalPrice = $filter('rounded')(vm.total_price_with_user_discount || vm.total_price_with_discount);

        return (totalPrice || 0) - remains;
    }

    function addProductFn() {
        if (vm.item && !vm.item.store_id) {
            Notify.warning('Виберіть спочатку магазин');
            return;
        }

        $scope.element = vm.item;

        $scope.urlParams = '';
        if ($scope.discount_id && vm.item.customer_type == 'isset') {
            $scope.urlParams =$scope.urlParams + '&discount_id=' + $scope.discount_id;
        }
        if (vm.item && vm.item.store_id) {
            $scope.urlParams = $scope.urlParams + '&store_id=' + vm.item.store_id;
        }

        ngDialog.open({
            templateUrl: addProductTemplate,
            showClose: false,
            className: 'dialog-theme-default',
            controller: 'OrdersController.addProduct',
            scope: $scope
        });
    }

    // Add new product to order's products table
    $rootScope.$on('products:add', function() {
        var newProduct = JSON.parse(localStorage.getItem('order_product'));
        var counter = 0;

        vm.is_no_refunded = 1;

        if (!vm.item.products) {
            vm.item.products = [];
        }

        _.each(vm.item.products, function(productItem, index) {
            if (productItem.id == newProduct.id && productItem.sizes  && productItem.sizes.items) {
                _.each(newProduct.sizes.items, function(newProductSize) {
                    counter++;

                    vm.item.products[index].sizes.items.push(newProductSize);
                });
            }
        });

        if (counter == 0) {
            vm.item.products.push(newProduct);
        }

        if (!vm.item.products.length) {
            vm.item.products.push(newProduct);
        }
    });

    // BEGIN working with products
    vm.totalProductPrice = totalProductPriceFn;
    vm.totalProductsPrice = totalProductsPriceFn;
    vm.totalProductsPriceWithDiscount = totalProductsPriceWithDiscountFn;
    vm.totalProductsPriceWithUserDiscount = totalProductsPriceWithUserDiscountFn;

    vm.customPriceValue = customPriceValueFn;
    vm.customPriceDiscount = customPriceDiscountFn;

    function disableProductDiscounts() {
        var checkDiscounts = false;

        _.each(vm.item.products, function(product) {
            _.each(product.sizes.items, function(size, index) {
                if (product.custom_price_discount) {
                    checkDiscounts = true;
                }
            });
        });

        if (checkDiscounts) {
            vm.disableGeneralDiscounts = true;
        } else {
            vm.disableGeneralDiscounts = false;
        }
    }

    function disableProductPrice() {
        var checkDiscounts = false;

        _.each(vm.item.products, function(product) {
            _.each(product.sizes.items, function(size, index) {
                if (product.custom_price_value) {
                    checkDiscounts = true;
                }
            });
        });

        if (checkDiscounts) {
            vm.disableGeneralDiscounts = true;
        } else {
            vm.disableGeneralDiscounts = false;
        }
    }

    function customPriceDiscountFn(elem) {
        var price_with_discount = 0;
        if (elem.price && elem.price.value) {
            price_with_discount = JSON.parse(JSON.stringify(elem.price.value));
        }

        if (elem.custom_price_discount) {
            price_with_discount = price_with_discount - price_with_discount * elem.custom_price_discount / 100;
            elem.custom_price_value = $filter('rounded')(price_with_discount);

            // vm.disableGeneralDiscounts = true;
        } else {
            // vm.disableGeneralDiscounts = false;
            if ($filter('rounded')(elem.custom_price_value) != $filter('rounded')(price_with_discount)) {
                elem.custom_price_value = '';
            }
        }

        disableProductDiscounts();

        return $filter('rounded')(price_with_discount);
    }

    function customPriceValueFn(elem) {
        var price_with_discount = 0;

        if (elem.price && elem.price.value) {
            price_with_discount = JSON.parse(JSON.stringify(elem.price.value));
        }

        if (elem.custom_price_value) {
            price_with_discount = elem.custom_price_value;
            elem.custom_price_discount = Math.round(100000*(100-(elem.custom_price_value/price_with_discount)*100))/100000;
            // vm.disableGeneralDiscounts = true;
        } else {
            elem.custom_price_discount = '';
            elem.custom_price_value = '';
            // vm.disableGeneralDiscounts = false;
        }

        // disableProductDiscounts();
        disableProductPrice();

        return $filter('rounded')(price_with_discount);
    }

    function totalProductPriceFn(elem) {
        elem.total_product_price = 0;

        var totalElementQuantity =_.reduce(elem.sizes.items, function(memo, el) {
            var elementQuantity = 0;

            if (el.quantity > 0) {
                elementQuantity = +el.quantity || 0;
            }

            return memo + elementQuantity;
        }, 0);

        if (elem.custom_price_value) {
            elem.total_product_price = $filter('rounded')(+totalElementQuantity * (+elem.custom_price_value));
            elem.custom_price_discount = Math.round(100000*(100-(elem.custom_price_value/elem.price.value)*100))/100000;
        } else
            if (elem.price && elem.price.value) {

            elem.total_product_price = $filter('rounded')(+totalElementQuantity * (elem.price.value));
        }

        return $filter('rounded')(elem.total_product_price);
    }

    function totalProductsPriceFn() {
        vm.total_price = 0;
        vm.total_price =_.reduce(vm.item.products, function(memo, elem){
            var elementPrice = elem && elem.total_product_price ? +elem.total_product_price : 0;

            return memo + elementPrice;
        }, 0);

        return $filter('rounded')(vm.total_price);
    }
    function totalProductsPriceWithDiscountFn() {
        vm.total_price_with_discount = JSON.parse(JSON.stringify(vm.total_price));
        if (vm.item.discount) {
            vm.total_price_with_discount = vm.total_price_with_discount - vm.total_price_with_discount * vm.item.discount / 100;
            vm.item.user_discount = $filter('rounded')(vm.total_price_with_discount);
        } else {
            if ($filter('rounded')(vm.item.user_discount) != $filter('rounded')(vm.total_price_with_discount)) {
                vm.item.user_discount = '';
            }
        }

        return $filter('rounded')(vm.total_price_with_discount);
    }

    function totalProductsPriceWithUserDiscountFn() {
        vm.total_price_with_discount = vm.total_price_with_discount || vm.total_price;
        if (vm.item.user_discount) {
            vm.total_price_with_discount = vm.item.user_discount;
            vm.item.discount = Math.round(100000*(100-(vm.item.user_discount/vm.total_price)*100))/100000;
        } else {
            vm.item.discount = '';
            vm.item.user_discount = '';
        }
    }

    vm.totalReturnProductPrice = totalReturnProductPriceFn;
    vm.totalReturnProductsPrice = totalReturnProductsPriceFn;

    function totalReturnProductPriceFn(elem) {
        elem.total_return_product_price = 0;

        var totalElementQuantity =_.reduce(elem.sizes.items, function(memo, el) {
            var elementQuantity = 0;

            if (el.refunded_quantity && el.refunded_quantity > 0) {
                elementQuantity = +el.refunded_quantity || 0;
            }

            return memo + elementQuantity;
        }, 0);


        if (elem.custom_price && elem.custom_price.value) {
            elem.total_return_product_price = $filter('rounded')(+totalElementQuantity * (elem.custom_price.value));
        } else if (elem.price && elem.price.value) {
            elem.total_return_product_price = $filter('rounded')(+totalElementQuantity * (elem.price.value));
        }

        return $filter('rounded')(elem.total_return_product_price);
    }
    function totalReturnProductsPriceFn() {
        vm.total_return_price = 0;
        vm.total_return_price =_.reduce(vm.item.products, function(memo, elem){
            var elementPrice = elem && elem.total_return_product_price ? +elem.total_return_product_price : 0;

            return memo + elementPrice;
        }, 0);

        return $filter('rounded')(vm.total_return_price);
    }

    // Remove product from order's products table
    function deleteProductFn(index, elem) {
        if (vm.item.id) {
            Notify.confirm(function () {
                var action = OrdersService.removeProduct(elem, vm.item.id);

                if (action == 'deleted') {
                    vm.item.products.splice(index, 1);
                }
            }, 'general.are_you_sure');
        } else {
            vm.item.products.splice(index, 1);
        }
    }
    // END working with products

    /**
     * Save or Update Order
     */
    function saveFn() {
        var data = JSON.parse(JSON.stringify(vm.item));
        // for (var prop in vm.item) {
        //     if (vm.item.hasOwnProperty(prop)) {
        //         data[prop] = vm.item[prop];
        //     }
        // }

        var discountId;

        if ($scope.discount_id) {
            discountId = JSON.parse(JSON.stringify($scope.discount_id));
        }

        if (data.customer && data.customer.id && data.customer_type == 'isset') {
            data.customer = {
                id: data.customer.id || data.customer_id
            };

            if (discountId) {
                data.customer.discount_id = discountId;
            }
        } else if (data.customer_type == 'new') {
            delete data.customer_id;
            if (data.customer && data.customer.id) {
                delete data.customer.id;
            }
            if (data.customer && data.customer.full_name) {
                delete data.customer.full_name;
            }
            if (data.customer && data.customer.nickname) {
                delete data.customer.nickname;
            }
            if (data.customer && data.customer.address) {
                delete data.customer.address;
            }
            if (data.customer && data.customer.created_at) {
                delete data.customer.created_at;
            }
            if (data.customer && data.customer.data) {
                delete data.customer.data;
            }
            if (data.customer && data.customer.discount) {
                delete data.customer.discount;
            }
            if (data.customer && data.customer.has_password) {
                delete data.customer.has_password;
            }
            if (data.customer && data.customer.role_name) {
                delete data.customer.role_name;
            }

            if (discountId) {
                data.customer.discount_id = discountId;
            }
        }

        if (data.customer_id) {
            data.customer = {
                id: data.customer_id
            };

            if (discountId) {
                data.customer.discount_id = discountId;
            }
        }

        if (discountId) {
            data.discount_id = discountId;
        }

        // If order not from internet shop
        if (!data.is_from_web_store) {
            delete data.delivery;
        }

        // payments START
        if (data.payments) {
            var temp_payments = [];
            _.each(data.payments, function(el) {
                if (el.value > 0) {
                    var data = {
                        rate: {
                            value: (el.value),
                            currency_id: el.currency_id
                        }
                    };

                    if (el.id) {
                        data.id = el.id;
                    }
                    temp_payments.push(data);
                }
            });

            data.payments = temp_payments;
        }
        // payments END

        if (data.products) {

            var products = [];
            _.each(data.products, function(product) {
                if (product.sizes && product.sizes.items && product.sizes.items.length) {
                    _.each(product.sizes.items, function(size) {

                        if (size.quantity > 0) {
                            var elem = {
                                size_id: size.id,
                                quantity: size.quantity,
                            };

                            if (data.discount) {
                                elem.discount = data.discount;
                            }

                            if (!product.custom_price_discount && !data.discount) {
                                elem.discount = 0;
                            }

                            if (product.custom_price_discount) {
                                elem.discount = product.custom_price_discount;
                            }

                            products.push(elem);
                        }
                    })
                }
            });

            data.products = products;
        }

        if (!data.currency_id) {
            data.currency_id = $rootScope.defaultCurrencyId;
        }

        if (data.delivery && data.delivery.method && data.delivery.method == 'pickup') {
            delete data.delivery.city;
            delete data.delivery.ref;
            delete data.delivery.department;
        }

        if ($stateParams.id) {
            OrdersService.updateOrder(vm.item.id, data)
                .success(function(response) {
                    Notify.success('general.updated');
                    $state.go('orders.list');
                })
                .catch(function(errors) {
                    if (errors.status == 422) {
                        vm.errors = errors.data.errors;
                        Notify.warning('events.product_error');
                    } else {

                        if (errors.data && errors.data.message && errors.data.code && errors.data.code == 'permission_denied' && errors.data.message == 'The order is completed') {
                            Notify.warning('Замовлення завершене. Зміни неможливі.');
                        } else {
                            Notify.warning(errors.data.message);
                        }
                    }
                });
        } else {
            OrdersService.saveOrder(data)
                .success(function (response) {
                    Notify.success('general.saved');
                    $state.go('orders.list');
                })
                .catch(function(errors) {
                    if (errors.status == 422) {
                        vm.errors = errors.data.errors;
                        Notify.warning('events.product_error');
                    } else {
                        if (errors.data && errors.data.message) {
                            Notify.warning(errors.data.message);
                        }
                    }
                });
        }
    }
}