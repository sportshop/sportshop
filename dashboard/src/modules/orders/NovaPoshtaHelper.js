(function (window) {
    'use strict';

    function AutoCompleteUnit(options) {

        for (let p in options) {
            if (options.hasOwnProperty(p)) {
                this[p] = options[p];
            }
        }

        if (options.language == 'UA') {
            this.language = 'Description';
        } else if (options.language == 'RU') {
            this.language = 'DescriptionRu';
        }
    }

    let npApi = {
        init(option){
            let autoCompleteUnit = new AutoCompleteUnit(option);
            prepareLocalStorage(autoCompleteUnit);
            // autocompleteInit(autoCompleteUnit);
        }
    };

    /*                                  Local storage prepare
     ****************************************************************************************************
     */

    let citiesCatalog = {};
    let warehouses = {};


    function prepareLocalStorage(autoCompleteUnit) {
        if (isLastUpdateToday() && localStorage.getItem('citiesCatalog') !== null && localStorage.getItem('warehouses') !== null) {
            citiesCatalog = JSON.parse(localStorage.getItem('citiesCatalog'));
            warehouses = JSON.parse(localStorage.getItem('warehouses'));
        } else {
            localStorage.setItem('catalogLastUpdate', new Date());
            putCitiesCatalogToLocalStorage(autoCompleteUnit);
            putWarehousesToLocalStorage(autoCompleteUnit);
        }
    }


    /**
     * Check if localStorage was last time updated today or not.
     */
    function isLastUpdateToday() {
        if (localStorage.getItem('catalogLastUpdate') === null) {
            return false;
        } else if (new Date().getDate() !== new Date(localStorage.getItem('catalogLastUpdate')).getDate()) {
            return false;
        }
        return true;
    }

    function putCitiesCatalogToLocalStorage(autoCompleteUnit) {
        citiesCatalog = getCitiesCatalog(autoCompleteUnit);
        let l = citiesCatalog.length;
        // delete unnecessary properties form citiesCatalog to decrease its size
        for (let i = 0; i < l; i++) {
            for (let property in citiesCatalog[i]) {
                if (citiesCatalog[i].hasOwnProperty(property)) {
                    if (property != autoCompleteUnit.language && property != 'Ref') {
                        delete citiesCatalog[i][property];
                    }
                }
                citiesCatalog[i].value = citiesCatalog[i][autoCompleteUnit.language];
                citiesCatalog[i].label = citiesCatalog[i][autoCompleteUnit.language];
            }
        }
        localStorage.setItem('citiesCatalog', JSON.stringify(citiesCatalog));
    }

    function putWarehousesToLocalStorage(autoCompleteUnit) {
        warehouses = getWarehousesCatalog(autoCompleteUnit);
        let l = warehouses.length;
        // delete unnecessary properties form warehouses to decrease its size
        for (let i = 0; i < l; i++) {
            for (let property in warehouses[i]) {
                if (warehouses[i].hasOwnProperty(property)) {
                    if (property != autoCompleteUnit.language && property != 'Ref' && property != 'CityRef') {
                        delete warehouses[i][property];
                    }
                }
                warehouses[i].value = warehouses[i][autoCompleteUnit.language];
                warehouses[i].label = warehouses[i][autoCompleteUnit.language];
            }
        }
        localStorage.setItem('warehouses', JSON.stringify(warehouses));
    }

    function getCitiesCatalog(autoCompleteUnit) {
        let result = {};

        if ($.ajaxSettings && $.ajaxSettings.headers) {
            delete $.ajaxSettings.headers['X-Socket-Id'];
        }

        $.ajax({
            type: "POST",
            async: false,
            contentType: "application/json; charset=utf-8",
            headers: {
                'Access-Control-Request-Headers': 'content-type'
            },
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Access-Control-Request-Headers", "content-type")
            },
            url: autoCompleteUnit.citiesApiUrl,
            data: JSON.stringify({
                "modelName": "Address",
                "calledMethod": "getCities",
                "apiKey": autoCompleteUnit.apiKey
            }),
            success: function (response) {
                let l = response.data.length;
                for (let i = 0; i < l; i++) {
                    response.data[i].value = response.data[i][autoCompleteUnit.language];
                    response.data[i].label = response.data[i][autoCompleteUnit.language];
                }
                result = response.data;
            }
        });
        return result;
    }


    function getWarehousesCatalog(autoCompleteUnit) {
        let result = {};

        if ($.ajaxSettings && $.ajaxSettings.headers) {
            delete $.ajaxSettings.headers['X-Socket-Id'];
        }
        $.ajax({
            type: "POST",
            async: false,
            headers: {
                'Access-Control-Request-Headers': 'content-type'
            },
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Access-Control-Request-Headers", "content-type")
            },
            url: autoCompleteUnit.warehousesApiUrl,
            data: JSON.stringify({
                "modelName": "AddressGeneral",
                "calledMethod": "getWarehouses",
                "apiKey": autoCompleteUnit.apiKey
            }),
            success: function (response) {
                let l = response.data.length;
                for (let i = 0; i < l; i++) {
                    response.data[i].value = response.data[i][autoCompleteUnit.language];
                    response.data[i].label = response.data[i][autoCompleteUnit.language];
                }
                result = response.data;
            }
        });
        return result;
    }

    window.npApi = npApi;
})(window);