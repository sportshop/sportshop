angular
    .module('app.costs')
    .controller('CostsController.list', CostsController_List)
    .controller('CostsController.edit', CostsController_Edit);


CostsController_List.$inject = ['$rootScope', '$state', '$stateParams', '$filter', 'HelperService', 'BreadcrumbsService', 'MainHeaderService', 'CostsService', 'Notify', 'items', 'stores'];
function CostsController_List($rootScope, $state, $stateParams, $filter, HelperService, BreadcrumbsService, MainHeaderService, CostsService, Notify, items, stores) {
    BreadcrumbsService.push('costs.title.list');
    MainHeaderService.set('costs.title.list');

    var vm = this;

    // Cost list
    vm.list = items.data || {};
    transformList(vm.list.data);

    if (vm.list.total_amount) {
        vm.list.total_amount = _.find(vm.list.total_amount, function(el) {
            return el.is_default;
        });
    }

    // Options for date select
    vm.dateOptions = HelperService.generateDate();
    // Helpers for init date filter
    var date = new Date(), y = date.getFullYear(), m = date.getMonth();

    var defaultDate;
    if ((+m+1) < 10) {
        defaultDate = y +'_0'+ (+m+1);
    } else {
        defaultDate = y +'_'+ (+m+1);
    }

    // Store list
    if (stores && stores.data) {
        vm.stores = stores.data || [];
        $rootScope.globalStores = JSON.parse(JSON.stringify(stores.data));
    } else {
        vm.stores = stores || [];
    }

    // Costs type list
    vm.costsTypes = CostsService.getCostsTypes();

    // Filter
    vm.searchForm = {
        type: $stateParams.type || null,
        store_id: $stateParams.store_id || null,
        date : defaultDate
    };

    vm.showType = showTypeFn;
    vm.remove = removeCostFn;
    vm.processSearch = processSearchFn;
    vm.changePage = changePageFn;

    // Transform list
    function transformList(list) {
        _.each(list, function (item) {
            if (item.rate) {
                item.rate = _.find(item.rate, function(el) {
                    return el.is_default;
                });
            }

            return item;
        });

        return list;
    }

    /**
     * Change Filter
     */
    function processSearchFn() {
        var params = {
            paginate:   true,
            limit:      10,
            page:       null,
            store_id:   vm.searchForm.store_id || null,
            type:       vm.searchForm.type,
            date:       vm.searchForm.date
        };

        CostsService.getList(params)
            .success(function(response) {
                vm.list = response || {};
                transformList(vm.list.data);

                if (vm.list.total_amount) {
                    vm.list.total_amount = _.find(vm.list.total_amount, function(el) {
                        return el.is_default;
                    });
                }

                $state.go($state.current, params, { notify: false });
            });
    }

    /**
     * Show cost's type
     *
     * @param id
     *
     * @returns {*}
     */
    function showTypeFn(id) {
        switch(id) {
            case 'hosting':
                return 'Оренда, хостинг, домен';
                break;
            case 'tax':
                return 'Податок';
                break;
            case 'utilities':
                return 'Комунальні послуги';
                break;
            case 'add':
                return 'Реклама, SEO, Adwords';
                break;
            case 'delivery':
                return 'Доставка';
                break;
            case 'development':
                return 'Розробка, підтримка сайту';
                break;
            case 'salary':
                return 'Зарплата';
                break;
            case 'other':
                return 'Інші витрати';
                break;
            default:
                return '-';
        }
    }

    /**
     * Remove cost
     *
     * @param item
     */
    function removeCostFn(item) {
        Notify.confirm(function() {
            CostsService.delete(item.id)
                .success(function() {
                    if (vm.list.current_page > 1 && vm.list.data.length == 1) {
                        $state.go($state.current, { page: +vm.list.current_page - 1 }, { reload: true });
                    } else {
                        $state.go($state.current, {}, { reload: true });
                    }

                    Notify.success('general.removed');
                })
                .error(function(errors) {
                    Notify.warning(errors.message);
                });
        }, 'general.are_you_sure');
    }

    /**
     * Change page
     */
    function changePageFn() {
        var params = {
            paginate:  true,
            limit:     vm.list.per_page || 10,
            page:      vm.list.current_page,
            store_id:  vm.searchForm.store_id || null,
            type:      vm.searchForm.type,
            date:      vm.searchForm.date,
        };

        CostsService.getList(params)
            .success(function(response) {
                vm.list = response || {};
                transformList(vm.list.data);

                if (vm.list.total_amount) {
                    vm.list.total_amount = _.find(vm.list.total_amount, function(el) {
                        return el.is_default;
                    });
                }

                $state.go($state.current, params, { notify: false });
            });
    }
}

CostsController_Edit.$inject = ['$rootScope', '$state', '$stateParams', 'BreadcrumbsService', 'MainHeaderService', 'CostsService', 'Notify', 'item', 'stores', 'currencies'];
function CostsController_Edit($rootScope, $state, $stateParams, BreadcrumbsService, MainHeaderService, CostsService, Notify, item, stores, currencies) {
    BreadcrumbsService.push('costs.title.list', 'costs.list');

    var vm = this;

    // Store List
    if (stores && stores.data) {
        vm.stores = stores.data || [];
        $rootScope.globalStores = JSON.parse(JSON.stringify(stores.data));
    } else {
        vm.stores = stores || [];
    }

    // Currency List
    if (currencies && currencies.data) {
        vm.currencies = currencies.data || [];
        $rootScope.globalCurrencies = JSON.parse(JSON.stringify(currencies.data));
    } else {
        vm.currencies = currencies || [];
    }
    // Cost types List
    vm.costsTypes = CostsService.getCostsTypes();

    if ($stateParams.id) {
        BreadcrumbsService.push('costs.title.edit');
        MainHeaderService.set('costs.title.edit');

        // Cost Model
        vm.item = item.data || {};
        vm.item.store_id = vm.item.store ? vm.item.store.id : '';

        if (vm.item.rate) {
            var rate = _.find(vm.item.rate, function(elem) {
                return elem.is_default
            });

            vm.item.rate = rate;
        }
    } else {
        BreadcrumbsService.push('costs.title.create');
        MainHeaderService.set('costs.title.create');

        // Cost Model
        vm.item = {
            store_id: (vm.stores && localStorage.getItem('active_store')) ? +localStorage.getItem('active_store') : '',
            type:     'hosting',
            rate: {
                currency_id: (vm.currencies && vm.currencies.length) ? vm.currencies[0].id : null
            }
        };
    }

    vm.save = saveCostFn;

    /**
     * Save or update cost
     */
    function saveCostFn() {
        if ($stateParams.id) {
            CostsService.update(vm.item.id, vm.item)
                .success(function() {
                    Notify.success('general.saved');
                    $state.go('costs.list');
                })
                .error(function(errors) {
                    vm.errors = errors.errors;
                });
        } else {
            CostsService.save(vm.item)
                .success(function() {
                    Notify.success('general.saved');
                    $state.go('costs.list');
                })
                .error(function(errors) {
                    vm.errors = errors.errors;
                });
        }
    }
}