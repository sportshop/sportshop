import layoutTemplate from "../layouts/dashboard.html";
import listTemplate from "./list.html";
import formTemplate from "./form.html";

angular
    .module('app.costs', [])
    .config(configure);


configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('costs', {
            abstract: true,
            url: '/costs',
            views: {
                '': {
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('costs.list', {
            url: '?page?type?date_start?date_end?limit?store_id',
            views: {
                'content': {
                    controller: 'CostsController.list as ctrl',
                    templateUrl: listTemplate
                }
            },
            reloadOnSearch: false,
            resolve: {
                items: [
                    '$stateParams', 'CostsService',
                    function ($stateParams, CostsService) {
                        // Helpers for init date filter
                        var date = new Date(), y = date.getFullYear(), m = date.getMonth();
                        var defaultDate;
                        if ((+m+1) < 10) {
                            defaultDate = y +'_0'+ (+m+1);
                        } else {
                            defaultDate = y +'_'+ (+m+1);
                        }

                        var params = {
                            paginate:   true,
                            limit:      $stateParams.limit || 10,
                            page:       $stateParams.page || 1,
                            store_id:   $stateParams.store_id || '',
                            type:       $stateParams.type,
                            date:       $stateParams.date || defaultDate,
                        };

                        return CostsService.getList(params);
                    }
                ],
                stores: [
                    '$rootScope', 'StoresService',
                    function ($rootScope, StoresService) {
                        if (!$rootScope.globalStores) {
                            return StoresService.getList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalStores));
                    }
                ]
            }
        })
        .state('costs.create', {
            url: '/create',
            views: {
                'content': {
                    controller: 'CostsController.edit as ctrl',
                    templateUrl: formTemplate
                }
            },
            resolve: {
                item: [
                    function() {
                        return {}
                    }
                ],
                stores: [
                    '$rootScope', 'StoresService',
                    function ($rootScope, StoresService) {
                        if (!$rootScope.globalStores) {
                            return StoresService.getList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalStores));
                    }
                ],
                currencies: [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalCurrencies) {
                            return SettingsService.getCurrenciesList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalCurrencies));
                    }
                ]
            }
        })
        .state('costs.edit', {
            url: '/:id',
            views: {
                'content': {
                    controller: 'CostsController.edit as ctrl',
                    templateUrl: formTemplate
                }
            },
            resolve: {
                item: [
                    '$stateParams', 'CostsService',
                    function ($stateParams, CostsService) {
                        return CostsService.get($stateParams.id);
                    }
                ],
                stores: [
                    '$rootScope', 'StoresService',
                    function ($rootScope, StoresService) {
                        if (!$rootScope.globalStores) {
                            return StoresService.getList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalStores));
                    }
                ],
                currencies: [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalCurrencies) {
                            return SettingsService.getCurrenciesList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalCurrencies));
                    }
                ]
            }
        });
}