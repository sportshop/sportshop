angular
    .module('app.costs')
    .service('CostsService', CostsService);


CostsService.$inject = ['HttpService'];
function CostsService(HttpService) {
    var baseUrl = '/costs';

    var costsTypes = [
        {id: 1, name: 'Оренда, хостинг, домен',    type: 'hosting'},
        {id: 2, name: 'Податок',                   type: 'tax'},
        {id: 3, name: 'Комунальні послуги',        type: 'utilities'},
        {id: 4, name: 'Реклама, SEO, Adwords',     type: 'add'},
        {id: 5, name: 'Доставка',                  type: 'delivery'},
        {id: 6, name: 'Розробка, підтримка сайту', type: 'development'},
        {id: 7, name: 'Зарплата',                  type: 'salary'},
        {id: 8, name: 'Інші витрати',              type: 'other'}
    ];

    return {
        getList:       getListFn,
        get:           getFn,
        delete:        deleteFn,
        save:          saveFn,
        update:        updateFn,
        getCostsTypes: getCostsTypesFn
    };

    /**
     * Get cost list
     *
     * @param params
     *
     * @returns {*}
     */
    function getListFn(params) {
        return HttpService.getWParams(baseUrl, { params: params });
    }

    /**
     * Get cost
     *
     * @param id
     *
     * @returns {*}
     */
    function getFn(id) {
        return HttpService.get(baseUrl +'/'+ id);
    }

    /**
     * Remove cost
     *
     * @param id
     *
     * @returns {*}
     */
    function deleteFn(id) {
        return HttpService.delete(baseUrl +'/'+ id);
    }

    /**
     * Save cost
     *
     * @param data
     *
     * @returns {*}
     */
    function saveFn(data) {
        return HttpService.post(baseUrl, data);
    }

    /**
     * Update cost
     *
     * @param id
     * @param data
     *
     * @returns {*}
     */
    function updateFn(id, data) {
        return HttpService.put(baseUrl +'/'+ id, data);
    }

    /**
     * Get costs types
     *
     * @returns {*[]}
     */
    function getCostsTypesFn() {
        return costsTypes;
    }
}