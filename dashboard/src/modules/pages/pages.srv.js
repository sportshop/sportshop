angular
    .module('app.pages')
    .service('PagesService', PagesService);


PagesService.$inject = ['HttpService'];
function PagesService(HttpService) {
    var baseUrl = '/pages';

    // Default page's statuses
    var pagesStatuses = [
        {key: true,  value: 'pages.general.status_active'},
        {key: false, value: 'pages.general.status_inactive'}
    ];

    return {
        getList:         getListFn,
        get:             getFn,
        save:            saveFn,
        update:          updateFn,
        delete:          deleteFn,
        getPageStatuses: getPageStatusesFn
    };

    /**
     * Get pages list
     * @param params
     * @returns {*}
     */
    function getListFn(params) {
        return HttpService.getWParams(baseUrl, { params: params });
    }

    /**
     * Get page
     *
     * @param id
     * @returns {*}
     */
    function getFn(id) {
        return HttpService.get(baseUrl +'/'+ id)
    }

    /**
     * Save page
     *
     * @param data
     * @returns {*}
     */
    function saveFn(data) {
        return HttpService.post(baseUrl, data)
    }

    /**
     * Update Page
     *
     * @param id
     * @param data
     *
     * @returns {*}
     */
    function updateFn(id, data) {
        return HttpService.put(baseUrl +'/'+ id, data)
    }

    /**
     * Remove page
     *
     * @param id
     *
     * @returns {*}
     */
    function deleteFn(id) {
        return HttpService.delete(baseUrl +'/'+ id)
    }

    /**
     * Get page's statuses
     *
     * @returns {*[]}
     */
    function getPageStatusesFn() {
        return pagesStatuses;
    }
}