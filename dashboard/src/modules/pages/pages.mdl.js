import layoutTemplate from "../layouts/dashboard.html";
import listTemplate from "./list.html";
import formTemplate from "./form.html";
import generalTemplate from "./_general.html";
import seoTemplate from "./_seo.html";
import sidebarTemplate from "./_sidebar.html"

angular
    .module('app.pages', [])
    .config(configure);


configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('pages', {
            abstract: true,
            url: '/pages',
            views: {
                '': {
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('pages.list', {
            url: '?page',
            views: {
                'content': {
                    controller: 'PagesController.list as ctrl',
                    templateUrl: listTemplate
                }
            },
            resolve: {
                items: [
                    '$stateParams', 'PagesService',
                    function ($stateParams, PagesService) {
                        var params = {
                            page: $stateParams.page
                        };

                        return PagesService.getList(params);
                    }
                ]
            }
        })
        .state('pages.create', {
            url: '/create',
            views: {
                'content': {
                    controller: 'PagesController.edit as ctrl',
                    templateUrl: formTemplate
                },
                'general@pages.create': {
                    templateUrl: generalTemplate
                },
                'seo@pages.create': {
                    templateUrl: seoTemplate
                },
                'sidebar@pages.create': {
                    templateUrl: sidebarTemplate
                }
            },
            resolve: {
                item: [
                    function () {
                        return {
                            data: {
                                page: {}
                            }
                        };
                    }
                ]
            }
        })
        .state('pages.edit', {
            url: '/:id',
            views: {
                'content': {
                    controller: 'PagesController.edit as ctrl',
                    templateUrl: formTemplate
                },
                'general@pages.edit': {
                    templateUrl: generalTemplate
                },
                'seo@pages.edit': {
                    templateUrl: seoTemplate
                },
                'sidebar@pages.edit': {
                    templateUrl: sidebarTemplate
                }
            },
            resolve: {
                item: [
                    '$stateParams', 'PagesService',
                    function ($stateParams, PagesService) {
                        return PagesService.get($stateParams.id);
                    }
                ]
            }
        });
}