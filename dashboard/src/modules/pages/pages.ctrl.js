angular
    .module('app.pages')
    .controller('PagesController.list', PagesController_List)
    .controller('PagesController.edit', PagesController_Edit);


PagesController_List.$inject = ['$state', 'BreadcrumbsService', 'MainHeaderService', 'PagesService', 'Notify', 'items'];
function PagesController_List($state, BreadcrumbsService, MainHeaderService, PagesService, Notify, items) {
    BreadcrumbsService.push('pages.title.list');
    MainHeaderService.set('pages.title.list');

    var vm = this;

    // Pages Model
    vm.list = items.data || [];

    vm.remove = removeFn;
    vm.changePage = changePageFn;

    /**
     * Delete page
     * @param item
     */
    function removeFn(item) {
        Notify.confirm(function() {
            PagesService.delete(item.id)
                .success(function() {
                    var index = vm.list.data.indexOf(item);
                    vm.list.data.splice(index, 1);

                    Notify.success('general.removed');
                })
                .error(function(errors) {
                    Notify.warning(errors.message);
                });
        }, 'general.are_you_sure')
    }

    /**
     * Change page
     */
    function changePageFn() {
        var params = {
            page: vm.list.current_page
        };

        PagesService.getList(params)
            .success(function(response){
                vm.list = response || {};
                $state.go($state.current, params, { notify: false });
            });
    }
}

PagesController_Edit.$inject = ['$state', '$stateParams', 'BreadcrumbsService', 'MainHeaderService', 'PagesService', 'Notify', 'item'];
function PagesController_Edit($state, $stateParams, BreadcrumbsService, MainHeaderService, PagesService, Notify, item) {
    BreadcrumbsService.push('pages.title.list', 'pages.list');

    var vm = this;

    // Page's statuses
    vm.statusList = PagesService.getPageStatuses();
    // Page Model
    vm.item = item.data || {};

    if ($stateParams.id) {
        BreadcrumbsService.push('pages.title.edit');
        MainHeaderService.set('pages.title.edit');
    } else {
        BreadcrumbsService.push('pages.title.create');
        MainHeaderService.set('pages.title.create');

        vm.item = {
            is_active: true
        };
    }

    vm.save = saveFn;

    /**
     * Update or create page
     */
    function saveFn() {
        if ($stateParams.id) {
            PagesService.update(vm.item.id, vm.item)
                .success(function() {
                    Notify.success('general.updated');
                    $state.go('pages.list');
                })
                .error(function(errors) {
                    vm.errors = errors.errors;
                });
        } else {
            PagesService.save(vm.item)
                .success(function() {
                    Notify.success('general.saved');
                    $state.go('pages.list');
                })
                .error(function(errors) {
                    vm.errors = errors.errors;
                });
        }
    }
}