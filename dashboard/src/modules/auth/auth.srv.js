angular
    .module('app.auth')
    .service('AuthService', AuthService);


AuthService.$inject = ['HttpService'];
function AuthService(HttpService) {
    return {
        register:            registerFn,
        confirmRegistration: confirmRegistrationFn,
        resetPassword:       resetPasswordFn,
        confirmNewPassword:  confirmNewPasswordFn
    };

    /**
     * Register user
     * @param data
     * @returns {*}
     */
    function registerFn(data) {
        return HttpService.post('/auth/registration', data);
    }

    /**
     * Confirm user registration
     * @param params
     * @returns {*}
     */
    function confirmRegistrationFn(params) {
        return HttpService.getWParams('/auth/registration/confirm', {
            params: params
        });
    }

    /**
     * Reset user's password
     * @param data
     * @returns {*}
     */
    function resetPasswordFn(data) {
        return HttpService.post('/auth/password/send', data);
    }

    /**
     * Confirm new user's password
     * @param data
     * @returns {*}
     */
    function confirmNewPasswordFn(data) {
        return HttpService.post('/auth/password/reset', data);
    }
}