import layoutTemplate from "../layouts/auth.html";
import loginTemplate from "./login.html";
import registrationTemplate from "./registration.html"
import registrationSuccessTemplate from "./registration-success.html"
import resetPasswordTemlate from "./reset_password.html"
import resetPasswordConfirmTemplate from "./reset_password_confirm.html"

angular
    .module('app.auth', [])
    .config(configure);


configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('auth', {
            abstract: true,
            url: '/auth',
            views: {
                '': {
                    controller: 'AuthController.main as authMainCtrl',
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('auth.login', {
            url: '/login',
            views: {
                'content': {
                    controller: 'AuthController.login as loginCtrl',
                    templateUrl: loginTemplate
                }
            }
        })
        .state('auth.logout', {
            url: '/logout',
            views: {
                'content': {
                    controller: 'AuthController.logout'
                }
            }
        })
        .state('auth.registration', {
            url: '/registration',
            views: {
                'content': {
                    controller: 'AuthController.registration as registrationCtrl',
                    templateUrl: registrationTemplate
                }
            }
        })
        .state('auth.registration_success', {
            url: '/registration/success',
            views: {
                'content': {
                    controller: 'AuthController.registrationSuccess',
                    templateUrl: registrationSuccessTemplate
                }
            }
        })
        .state('auth.registration_confirm', {
            url: '/registration/confirm',
            views: {
                'content': {
                    controller: 'AuthController.registrationConfirm'
                }
            }
        })
        .state('auth.reset', {
            url: '/password/email',
            views: {
                'content': {
                    controller: 'AuthController.reset as resetPassCtrl',
                    templateUrl: resetPasswordTemlate
                }
            }
        })
        .state('auth.reset_confirm', {
            url: '/password/reset?token&email',
            views: {
                'content': {
                    controller: 'AuthController.reset_confirm as resetConfirmCtrl',
                    templateUrl: resetPasswordConfirmTemplate
                }
            }
        });
}