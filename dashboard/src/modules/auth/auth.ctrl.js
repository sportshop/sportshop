angular
    .module('app.auth')
    .controller('AuthController.main', AuthController_Main)
    .controller('AuthController.login', AuthController_Login)
    .controller('AuthController.logout', AuthController_Logout)
    .controller('AuthController.registration', AuthController_Registration)
    .controller('AuthController.registrationSuccess', AuthController_Registration_Success)
    .controller('AuthController.registrationConfirm', AuthController_Registration_Confirm)
    .controller('AuthController.reset', AuthController_Reset)
    .controller('AuthController.reset_confirm', AuthController_Confirm);


AuthController_Main.$inject = [];
function AuthController_Main() {}

AuthController_Login.$inject = ['$rootScope', '$scope', '$auth', '$state', 'BreadcrumbsService', 'AuthDataService', 'EchoService'];
function AuthController_Login($rootScope, $scope, $auth, $state, BreadcrumbsService, AuthDataService, EchoService) {
    BreadcrumbsService.push('auth.titles.log_in');

    var vm = this;

    vm.errorText = false;

    if (localStorage.getItem('satellizer_token')) {
        $state.go('orders.list');
    }

    vm.login = loginFn;

    /**
     * Authorize user
     */
    function loginFn() {
        var credentials = {
            email:    vm.email,
            password: vm.password,
            from_dashboard: true
        };

        $auth.login(credentials)
            .then(
                function(response) {
                    // Return an $http request for the now authenticated
                    // user so that we can flatten the promise chain

                    // return $http.get(verifyUrl);
                    if (response && response.data) {
                        // Stringify the returned data to prepare it
                        // to go into local storage
                        var user = JSON.stringify(response.data);

                        // Set the stringified user data into local storage
                        localStorage.setItem('user', user);
                        localStorage.setItem('satellizer_token', response.data.access_token);

                        // The user's authenticated state gets flipped to
                        // true so we can now show parts of the UI that rely
                        // on the user being logged in
                        $rootScope.authenticated = true;

                        // Putting the user's data on $rootScope allows
                        // us to access it anywhere across the app
                        $rootScope.currentUser = response.data || {};

                        if ($rootScope.currentUser.user && $rootScope.currentUser.user.permissions) {
                            $rootScope.perms = {};
                            $rootScope.currentUser.user.permissions.forEach(function (elem) {
                                $rootScope.perms[elem] = true;
                            });
                        }

                        // Socket.io configuration
                        EchoService.socketConfigure();

                        // Set unread messages and counter
                        EchoService.setOrders();

                        // Set data in rootScope
                        EchoService.setDataInRootScope();

                        // Connect and listen channel
                        EchoService.connectToChannel();

                        // Connect and listen channel
                        EchoService.entityUpdateChannel();

                        // Init menu for user
                        $rootScope.$emit('nav:init');

                        // Everything worked out so we can now redirect to
                        // the users state to view the data
                        $state.go('orders.list');
                    }
                }, // Handle errors
                function(error) {
                    vm.errors = error.data || '';
                    vm.errorText = (error.data && error.data.message) ? error.data.message : '';

                    if (vm.errorText && vm.errorText == 'Invalid credentials') {
                        vm.errorText = 'Неправильний email або пароль';
                    }

                    // Because we returned the $http.get request in the $auth.login
                    // promise, we can chain the next promise to the end here
                    $scope.$emit('form:error', vm.errors);
                    AuthDataService.clearAuthData();
                });
    }
}

AuthController_Logout.$inject = ['$rootScope', '$state', '$auth', 'AuthDataService'];
function AuthController_Logout($rootScope, $state, $auth, AuthDataService) {
    // We would normally put the logout method in the same
    // spot as the login method, ideally extracted out into
    // a service. For this simpler example we'll leave it here
    $auth.logout().then(function() {
        AuthDataService.clearAuthData();

        // Flip authenticated to false so that we no longer
        // show UI elements dependant on the user being logged in
        $rootScope.authenticated = false;

        // Remove the current user info from rootScope
        $rootScope.currentUser = null;

        $state.go('auth.login');
    });
}

AuthController_Registration.$inject = ['$state', 'BreadcrumbsService', 'AuthService'];
function AuthController_Registration($state, BreadcrumbsService, AuthService) {
    BreadcrumbsService.push('titles.auth.sign_up');

    var vm = this;

    vm.register = registerFn;

    /**
     *  Register user
     */
    function registerFn() {
        var data = {
            first_name:             vm.first_name,
            last_name:              vm.last_name,
            middle_name:            vm.middle_name,
            password:               vm.password,
            password_confirmation:  vm.password_confirmation,
            email:                  vm.email,
            phone:                  vm.phone,
            agree:                  vm.agree,
        };

        AuthService.register(data)
            .success(function() {
                $state.go('auth.registration_success');
            })
            .error(function(errors) {
                vm.errors = errors.errors;
            });
    }
}

AuthController_Registration_Success.$inject = ['BreadcrumbsService'];
function AuthController_Registration_Success(BreadcrumbsService) {
    BreadcrumbsService.push('titles.auth.registration_success');
}

AuthController_Registration_Confirm.$inject = ['$state', '$stateParams', 'AuthService', 'Notify'];
function AuthController_Registration_Confirm($state, $stateParams, AuthService, Notify) {
    if ($stateParams.token) {
        var params = {
            token: $stateParams.token
        };

        // Confirm user registration
        AuthService.confirmRegistration(params)
            .success(function(response) {
                Notify.success('general.confirmed');

                $state.go('auth.login')
            })
            .error(function(errors) {
                if (errors && errors.message) {
                    Notify.warning(errors.message);
                }

                $state.go('auth.login');
            });
    }
}

AuthController_Reset.$inject = ['$state', 'AuthService', 'BreadcrumbsService', 'Notify'];
function AuthController_Reset($state, AuthService, BreadcrumbsService, Notify) {
    BreadcrumbsService.push('titles.auth.reset');

    var vm = this;

    vm.success_message = false;

    vm.reset = resetFn;

    /**
     * Reset user's password
     */
    function resetFn() {
        var data = {
            email: vm.email
        };
        AuthService.resetPassword(data)
            .success(function(response) {
                Notify.success('general.password_was_reseted');

                $state.go('auth.login');
            })
            .error(function(errors) {
                vm.errors = errors.errors;
                vm.success_message = false;
            });
    }
}

AuthController_Confirm.$inject = ['$state', '$stateParams', 'BreadcrumbsService', 'AuthService', 'Notify'];
function AuthController_Confirm($state, $stateParams, BreadcrumbsService, AuthService, Notify) {
    BreadcrumbsService.push('titles.auth.confirm');

    var vm = this;

    vm.server_message = false;

    if (!$stateParams.token) {
        $state.go('auth.login');
    }

    vm.confirm = confirmFn;

    /**
     * Confirm new user's password
     */
    function confirmFn() {
        var data = {
            token:                  $stateParams.token,
            email:                  $stateParams.email,
            password:               vm.password,
            password_confirmation:  vm.password_confirmation
        };

        AuthService.confirmNewPassword(data)
            .success(function() {
                vm.success_message = '';
                vm.error_message = false;

                Notify.success('events.password_reset_success');
                $state.go('auth.login');
            })
            .error(function(errors) {
                vm.errors = errors.errors;
                vm.success_message = false;
                vm.error_message = errors.errors;
            });
    }
}