angular
    .module('app.attributes')
    .service('AttributesService', AttributesService);


AttributesService.$inject = ['HttpService'];
function AttributesService(HttpService) {
    var baseUrl = '/attributes';

    return {
        getList: getListFn,
        get:     getFn,
        delete:  deleteFn,
        save:    saveFn,
        update:  updateFn
    };

    /**
     * Get attribute list
     *
     * @returns {*}
     */
    function getListFn() {
        return HttpService.get(baseUrl);
    }

    /**
     * Get attribute
     *
     * @param id
     *
     * @returns {*}
     */
    function getFn(id) {
        return HttpService.get(baseUrl +'/'+ id);
    }

    /**
     * Remove attribute
     *
     * @param id
     *
     * @returns {*}
     */
    function deleteFn(id) {
        return HttpService.delete(baseUrl +'/'+ id);
    }

    /**
     * Save attribute
     *
     * @param data
     *
     * @returns {*}
     */
    function saveFn(data) {
        return HttpService.post(baseUrl, data);
    }

    /**
     * Update attribute
     *
     * @param id
     * @param data
     *
     * @returns {*}
     */
    function updateFn(id, data) {
        return HttpService.put(baseUrl +'/'+ id, data);
    }
}