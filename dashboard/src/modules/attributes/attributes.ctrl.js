angular
    .module('app.attributes')
    .controller('AttributesController.list', AttributesController_List)
    .controller('AttributesController.edit', AttributesController_Edit);


AttributesController_List.$inject = ['$rootScope', 'BreadcrumbsService', 'MainHeaderService', 'AttributesService', 'Notify', 'items'];
function AttributesController_List($rootScope, BreadcrumbsService, MainHeaderService, AttributesService, Notify, items) {
    BreadcrumbsService.push('attributes.title.list');
    MainHeaderService.set('attributes.title.list');

    var vm = this;

    // List of attributes
    if (items && items.data) {
        vm.list = items.data || {};
        $rootScope.globalAttributes = JSON.parse(JSON.stringify(items.data));
    } else {
        vm.list = items || [];
        $rootScope.globalAttributes = items || [];
    }

    vm.remove = removeFn;

    /**
     * Remove attribute
     *
     * @param item
     */
    function removeFn(item) {
        Notify.confirm(function () {
            AttributesService.delete(item.id)
                .success(function() {
                    var index = $rootScope.globalAttributes.indexOf(item);
                    $rootScope.globalAttributes.splice(index, 1);

                    Notify.success('general.removed');
                })
                .error(function(errors) {
                    Notify.warning(errors.message);
                });
        }, 'general.are_you_sure')
    }
}


AttributesController_Edit.$inject = ['$state', '$stateParams', 'BreadcrumbsService', 'MainHeaderService', 'AttributesService', 'Notify', 'item'];
function AttributesController_Edit($state, $stateParams, BreadcrumbsService, MainHeaderService, AttributesService, Notify, item) {
    BreadcrumbsService.push('attributes.title.list', 'attributes.list');

    if ($stateParams.id) {
        BreadcrumbsService.push('attributes.title.edit');
        MainHeaderService.set('attributes.title.edit');
    } else {
        BreadcrumbsService.push('attributes.title.create');
        MainHeaderService.set('attributes.title.create');
    }

    var vm = this;

    // Attribute Model
    vm.item = item.data || {};

    vm.addAttribute = addAttributeFn;
    vm.addProperty = addPropertyFn;
    vm.removeAttribute = removeAttributeFn;
    vm.removeProperty = removePropertyFn;
    vm.save = saveFn;

    /**
     * Add attribute
     */
    function addAttributeFn() {
        var data = {
            name: ''
        };

        if (!vm.item.attributes) {
            vm.item.attributes = [];
        }

        vm.item.attributes.push(data);
    }

    /**
     * Add property
     *
     * @param index
     */
    function addPropertyFn(index) {
        var data = {
            value: ''
        };

        if (vm.item.attributes && (!vm.item.attributes[index] || !vm.item.attributes[index].values)) {
            vm.item.attributes[index].values = [];
        }

        vm.item.attributes[index].values.push(data);
    }

    /**
     * Remove attribute
     *
     * @param index
     */
    function removeAttributeFn(index) {
        Notify.confirm(function() {
            vm.item.attributes.splice(index, 1);
        }, 'general.are_you_sure');
    }

    /**
     * Remove property
     *
     * @param attributeIndex
     * @param index
     */
    function removePropertyFn(attributeIndex, index) {
        Notify.confirm(function() {
            vm.item.attributes[attributeIndex].values.splice(index, 1);
        }, 'general.are_you_sure');
    }

    /**
     * Save or update attribute
     */
    function saveFn() {
        if (vm.item.id) {
            AttributesService.update(vm.item.id, vm.item)
                .success(function() {
                    $state.go('attributes.list');
                    Notify.success('general.updated')
                })
                .error(function(errors) {
                    vm.errors = errors.errors;
                });
        } else { // Save attribute
            AttributesService.save(vm.item)
                .success(function() {
                    $state.go('attributes.list');
                    Notify.success('general.saved')
                })
                .error(function(errors) {
                    vm.errors = errors.errors;
                });
        }
    }
}