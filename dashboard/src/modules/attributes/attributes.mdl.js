import layoutTemplate from "../layouts/dashboard.html";
import listTemplate   from "./list.html";
import formTemplate   from "./form.html";

angular
    .module('app.attributes', [])
    .config(configure);


configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('attributes', {
            abstract: true,
            url: '/attributes',
            views: {
                '': {
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('attributes.list', {
            url: '',
            views: {
                'content': {
                    controller: 'AttributesController.list as ctrl',
                    templateUrl: listTemplate
                }
            },
            resolve: {
                items: [
                    '$rootScope', 'AttributesService',
                    function($rootScope, AttributesService) {
                        if (!$rootScope.globalAttributes) {
                            return AttributesService.getList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalAttributes));
                    }
                ]
            }
        })
        .state('attributes.create', {
            url: '/create',
            views: {
                'content': {
                    controller: 'AttributesController.edit as ctrl',
                    templateUrl: formTemplate
                }
            },
            resolve: {
                item: [
                    function () {
                        return {
                            data: {
                                group: {}
                            }
                        };
                    }
                ]
            }
        })
        .state('attributes.edit', {
            url: '/:id',
            views: {
                'content': {
                    controller: 'AttributesController.edit as ctrl',
                    templateUrl: formTemplate
                }
            },
            resolve: {
                item: [
                    '$stateParams', 'AttributesService',
                    function ($stateParams, AttributesService) {
                        return AttributesService.get($stateParams.id);
                    }
                ]
            }
        });
}