import layoutTemplate from "../layouts/dashboard.html";
import listTemplate from "./list.html";
import formTemplate from "./form.html";
import sidebarTemplate from "./_sidebar.html";

angular
    .module('app.stores', [])
    .config(configure);


configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('stores', {
            abstract: true,
            url: '/stores',
            views: {
                '': {
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('stores.list', {
            url: '?page',
            views: {
                'content': {
                    controller: 'StoresController.list as ctrl',
                    templateUrl: listTemplate
                }
            },
            resolve: {
                items: [
                    '$rootScope', 'StoresService',
                    function ($rootScope, StoresService) {
                        if (!$rootScope.globalStores) {
                            return StoresService.getList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalStores));
                    }
                ]
            }
        })
        .state('stores.create', {
            url: '/create',
            views: {
                'content': {
                    controller: 'StoresController.edit as ctrl',
                    templateUrl: formTemplate
                },
                'sidebar@stores.create': {
                    templateUrl: sidebarTemplate
                }
            },
            resolve: {
                item: [
                    function() {
                        return {
                            data: {}
                        }
                    }
                ]
            }
        })
        .state('stores.edit', {
            url: '/:id',
            views: {
                'content': {
                    controller: 'StoresController.edit as ctrl',
                    templateUrl: formTemplate
                },
                'sidebar@stores.edit': {
                    templateUrl: sidebarTemplate
                }
            },
            resolve: {
                item: [
                    '$stateParams', 'StoresService',
                    function($stateParams, StoresService) {
                        return StoresService.get($stateParams.id);
                    }
                ]
            }
        });
}