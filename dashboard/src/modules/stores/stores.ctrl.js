angular
    .module('app.stores')
    .controller('StoresController.list', StoresController_List)
    .controller('StoresController.edit', StoresController_Edit);


StoresController_List.$inject = ['$rootScope', '$state', 'BreadcrumbsService', 'MainHeaderService', 'StoresService', 'Notify', 'items'];
function StoresController_List($rootScope, $state, BreadcrumbsService, MainHeaderService, StoresService, Notify, items) {
    BreadcrumbsService.push('stores.title.list');
    MainHeaderService.set('stores.title.list');

    var vm = this;

    // Store List
    if (items && items.data) {
        vm.list = items.data || [];
        $rootScope.globalStores = JSON.parse(JSON.stringify(items.data));

        $rootScope.storesList =JSON.parse(JSON.stringify(items.data)) || [];

    } else {
        vm.list = items || [];
    }

    vm.remove = removeStoreFn;
    vm.changePage = changePageFn;

    /**
     * Remove store
     *
     * @param item
     */
    function removeStoreFn(item) {
        Notify.confirm(function () {
            StoresService.delete(item.id)
                .success(function() {
                    if (vm.list.current_page > 1 && vm.list.data.length == 1) {
                        $state.go($state.current, { page: +vm.list.current_page - 1 }, { reload: true });
                    } else {
                        $state.go($state.current, {}, { reload: true });
                    }

                    Notify.success('general.removed');
                })
                .error(function(errors) {
                    if (errors.message && errors.message == 'The store has products') {
                        Notify.warning('У магазині є товари');
                    } else {
                        Notify.warning(errors.message);
                    }
                });
        }, 'general.are_you_sure')
    }

    /**
     * Change page
     */
    function changePageFn() {
        var params = {
            paginate: true,
            page: vm.list.current_page,
            store_id: localStorage.getItem('active_store')
        };

        StoresService.getList(params)
            .success(function(response) {
                vm.list = response || {};
                $state.go($state.current, params, { notify: false });
            });
    }
}

StoresController_Edit.$inject = ['$rootScope', '$state', '$stateParams', '$timeout', 'BreadcrumbsService', 'MainHeaderService', 'StoresService', 'Notify', 'MyFileUploader', 'HttpService', 'item'];
function StoresController_Edit($rootScope, $state, $stateParams, $timeout, BreadcrumbsService, MainHeaderService, StoresService, Notify, MyFileUploader, HttpService, item) {
    BreadcrumbsService.push('stores.title.list', 'stores.list');

    var vm = this;

    vm.uploadUrl = '/stores/upload-favicon';

    vm.themes = [
        {key: 'blue', value: 'Синя'},
        {key: 'red',  value: 'Червона'},
        {key: 'green', value: 'Зелена'}
    ];

    if ($stateParams.id) {
        BreadcrumbsService.push('stores.title.edit');
        MainHeaderService.set('stores.title.edit');
    } else {
        BreadcrumbsService.push('stores.title.create');
        MainHeaderService.set('stores.title.create');
    }

    // Store Model
    if ($stateParams.id) {
        vm.item = item.data || {};
    } else {
        vm.item = {
            theme: 'blue'
        }
    }

    // Set additional gallery images
    var onCompleteItem = function(fileItem, response, status, headers) {
        if (status == 200) {
            if (!vm.item.additional_media) {
                vm.item.additional_media = {};
            }
            var data = response;
            // vm.item.additional_media.push(data);
            vm.item.additional_media = data;
        }
    };
    // Init MyFileUploader for gallery images
    var uploadAdditionalImages =  MyFileUploader.init(true, 1, false, { onCompleteItem: onCompleteItem });
    // Upload additional gallery images
    vm.uploadAdditionalImages = uploadAdditionalImages;

    vm.uploadGalleryImage = uploadGalleryImageFn;
    vm.deleteGalleryImage = deleteGalleryImageFn;
    vm.save = saveStoreFn;

    // Upload image for product's gallery
    function uploadGalleryImageFn() {
        $timeout(function() {
            var element = angular.element('#files-upload');
            element.triggerHandler('click');
            element.trigger('click');
        });
    }

    // Delete favicon image
    function deleteGalleryImageFn() {
        HttpService.delete('/stores/upload-favicon/'+ vm.item.id)
            .success(function() {
                delete vm.item.favicon_id;
                delete vm.item.additional_media;
            });
    }

    /**
     * Save or update store
     */
    function saveStoreFn() {
        if (vm.item.logo && vm.item.logo.id) {
            vm.item.logo_id = vm.item.logo.id;
        }
        if (vm.item.favicon && vm.item.favicon.id) {
            vm.item.favicon_id = vm.item.favicon.id;
        }

        if ($stateParams.id) {
            StoresService.update(vm.item.id, vm.item)
                .success(function() {
                    Notify.success('general.updated');
                    // $rootScope.globalStores = null;
                    $state.go('stores.list');
                })
                .error(function(errors) {
                    vm.errors = errors.errors;
                });
        } else {
            StoresService.save(vm.item)
                .success(function() {
                    Notify.success('general.saved');
                    $state.go('stores.list');
                })
                .error(function(errors) {
                    vm.errors = errors.errors;
                });
        }
    }
}