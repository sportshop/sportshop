angular
    .module('app.stores')
    .service('StoresService', StoresService);


StoresService.$inject = ['HttpService'];
function StoresService(HttpService) {
    var baseUrl = '/stores';

    return {
        getList: getListFn,
        get:     getFn,
        delete:  deleteFn,
        save:    saveFn,
        update:  updateFn
    };

    /**
     * Get store list
     *
     * @param params
     *
     * @returns {*}
     */
    function getListFn(params) {
        return HttpService.getWParams(baseUrl, { params: params });
    }

    /**
     * Get store
     *
     * @param id
     *
     * @returns {*}
     */
    function getFn(id) {
        return HttpService.get(baseUrl +'/'+ id);
    }

    /**
     * Remove store
     *
     * @param id
     *
     * @returns {*}
     */
    function deleteFn(id) {
        return HttpService.delete(baseUrl +'/'+ id);
    }

    /**
     * Save store
     *
     * @param data
     *
     * @returns {*}
     */
    function saveFn(data) {
        return HttpService.post(baseUrl, data);
    }

    /**
     * Update store
     *
     * @param id
     * @param data
     *
     * @returns {*}
     */
    function updateFn(id, data) {
        return HttpService.put(baseUrl +'/'+ id, data);
    }
}