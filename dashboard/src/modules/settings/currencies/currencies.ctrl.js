angular
    .module('app.settings')
    .controller('CurrenciesController.edit', CurrenciesController_Edit);


CurrenciesController_Edit.$inject = ['$rootScope', 'SettingsService', 'Notify', 'currencies'];
function CurrenciesController_Edit($rootScope, SettingsService, Notify, currencies) {
    var vm = this;

    // Model Currencies
    if (currencies && currencies.data) {
        vm.currencies = currencies.data || [];
        $rootScope.globalCurrencies = JSON.parse(JSON.stringify(currencies.data));
    } else {
        vm.currencies = currencies || [];
    }

    // Currencies errors
    vm.currenciesErrors = null;

    vm.addCurrency = addCurrencyFn;
    vm.deleteCurrency = deleteCurrencyFn;
    vm.saveCurrencies = saveCurrenciesFn;

    /**
     * Add Currency
     */
    function addCurrencyFn() {
        vm.currencies.push({
            id: '',
            code: '',
            name: '',
            value: '',
            is_default: false
        });
    }

    /**
     * Delete Currency
     *
     * @param index
     */
    function deleteCurrencyFn(index) {
        Notify.confirm(function() {
            vm.currencies.splice(index, 1);
        }, 'general.are_you_sure');
    }

    /**
     * Save currencies
     */
    function saveCurrenciesFn() {
        SettingsService.saveCurrencies(vm.currencies)
            .success(function() {
                vm.currenciesErrors = $rootScope.currenciesErrors = null;
                Notify.success('events.updated');
            })
            .error(function(errors) {
                vm.currenciesErrors = $rootScope.currenciesErrors = errors.errors;
            });
    }
}