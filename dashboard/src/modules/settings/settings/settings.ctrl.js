angular
    .module('app.settings')
    .controller('SettingsController.edit', SettingsController_Edit);


SettingsController_Edit.$inject = ['$rootScope', 'BreadcrumbsService', 'MainHeaderService', 'SettingsService', 'Notify', 'settings', 'categories'];
function SettingsController_Edit($rootScope, BreadcrumbsService, MainHeaderService, SettingsService, Notify, settings, categories) {
    BreadcrumbsService.push('settings.title.list');
    MainHeaderService.set('settings.title.list');

    var vm = this;

    // Model Settings
    if (settings && settings.data) {
        vm.settings = settings.data || [];
        $rootScope.globalSettings = JSON.parse(JSON.stringify(settings.data));
    } else {
        vm.settings = settings || [];
    }

    // Categories list
    vm.categories = categories.data || categories;
    if (categories && categories.data) {
        vm.categories = categories.data || [];
        $rootScope.globalCategoriesFlatList = JSON.parse(JSON.stringify(categories.data));
    } else {
        vm.categories = categories || [];
    }

    // Settings errors
    vm.settinsErrors = null;

    if (vm.settings.feature_categories) {
        // Sort categories by 'sort_order'
        var categories = _.sortBy(vm.settings.feature_categories, 'sort_order');
        vm.item = {categories: []};

        _.each(categories, function(el) {
            if (el && el.id) {
                vm.item.categories.push(el.id);
            }
        });
    }

    vm.saveSettings = saveSettingsFn;
    vm.saveHomeNavigation = saveHomeNavigationFn;

    /**
     * Save Settings
     */
    function saveSettingsFn() {
        var data = {};
        var settings = _.clone(vm.settings);

        SettingsService.saveSettings(settings)
            .success(function() {
                vm.settinsErrors = $rootScope.settingsErrors = null;
                Notify.success('events.updated');
            })
            .error(function(errors) {
                vm.settinsErrors = $rootScope.settingsErrors = errors.errors;
            });
    }

    /**
     * Save home navigation
     */
    function saveHomeNavigationFn() {
        var data = {};
        var settings = _.clone(vm.settings);

        if (vm.item.categories) {
            var navCategories = [];
            _.each(vm.item.categories, function(el, index) {
                navCategories.push({
                    id: el,
                    sort_order: index + 1
                });
            });

            if (navCategories.length > 4) {
                Notify.warning('Максимально можна вибрати 4 категорії');

                navCategories = arr.splice(4, navCategories.length);
            }
        }

        settings.feature_categories = navCategories;

        SettingsService.saveSettings(settings)
            .success(function() {
                vm.settinsErrors = $rootScope.settingsErrors = null;
                Notify.success('events.updated');
            })
            .error(function(errors) {
                vm.settinsErrors = $rootScope.settingsErrors = errors.errors;
            });
    }
}