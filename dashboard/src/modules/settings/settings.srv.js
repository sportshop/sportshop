angular
    .module('app.settings')
    .service('SettingsService', SettingsService);


SettingsService.$inject = ['HttpService'];
function SettingsService(HttpService) {
    var baseCurrenciesUrl = '/currencies';
    var baseColorsUrl = '/colors';
    var baseDiscountsUrl  = '/discounts';
    var baseSettingsUrl   = '/settings';

    // Discounts type
    var discountsType = [
        {key: true, value: '+'},
        {key: false, value: '-'}
    ];

    return {
        getDiscountsType:  getDiscountsTypeFn,
        getCurrenciesList: getCurrenciesListFn,
        getColorsList:     getColorsListFn,
        getDiscountsList:  getDiscountsListFn,
        saveCurrencies:    saveCurrenciesListFn,
        saveColors:        saveColorsListFn,
        saveDiscounts:     saveDiscountsListFn,
        getSettingsList:   getSettingsListFn,
        saveSettings:      saveSettingsFn
    };

    /**
     * Get discount types
     *
     * @returns {*}
     */
    function getDiscountsTypeFn() {
        return discountsType;
    }

    /**
     * Get currencies list
     *
     * @returns {*}
     */
    function getCurrenciesListFn() {
        return HttpService.get(baseCurrenciesUrl);
    }

    /**
     * Get colors list
     *
     * @returns {*}
     */
    function getColorsListFn() {
        return HttpService.get(baseColorsUrl);
    }

    /**
     * Get discounts list
     *
     * @returns {*}
     */
    function getDiscountsListFn() {
        return HttpService.get(baseDiscountsUrl);
    }

    /**
     * Save currencies list
     *
     * @param data
     *
     * @returns {*}
     */
    function saveCurrenciesListFn(data) {
        return HttpService.post(baseCurrenciesUrl, { currencies: data });
    }

    /**
     * Save colors list
     *
     * @param data
     *
     * @returns {*}
     */
    function saveColorsListFn(data) {
        return HttpService.post(baseColorsUrl, { colors: data });
    }

    /**
     * Save discounts list
     *
     * @param data
     *
     * @returns {*}
     */
    function saveDiscountsListFn(data) {
        return HttpService.post(baseDiscountsUrl, { discounts: data });
    }

    /**
     * Get Settings List
     *
     * @returns {*}
     */
    function getSettingsListFn() {
        return HttpService.get(baseSettingsUrl);
    }

    /**
     * Save Settings
     *
     * @param data
     *
     * @returns {*}
     */
    function saveSettingsFn(data) {
        return HttpService.put(baseSettingsUrl, data);
    }
}