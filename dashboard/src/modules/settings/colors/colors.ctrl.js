angular
    .module('app.settings')
    .controller('ColorsController.edit', ColorsController_Edit);


ColorsController_Edit.$inject = ['$rootScope', 'SettingsService', 'Notify', 'colors'];
function ColorsController_Edit($rootScope, SettingsService, Notify, colors) {
    var vm = this;

    // Model Colors
    if (colors && colors.data) {
        vm.colors = colors.data || [];
        $rootScope.globalColors = JSON.parse(JSON.stringify(colors.data));
    } else {
        vm.colors = colors || [];
    }

    vm.addColor = addColorFn;
    vm.deleteColor = deleteColorFn;
    vm.saveColors = saveColorsFn;

    /**
     * Add Color
     */
    function addColorFn() {
        vm.colors.push({
            id: '',
            hex: '',
            name: ''
        });
    }

    /**
     * Delete Color
     *
     * @param index
     */
    function deleteColorFn(index) {
        Notify.confirm(function() {
            vm.colors.splice(index, 1);
        }, 'general.are_you_sure');
    }

    /**
     * Save colors
     */
    function saveColorsFn() {
        SettingsService.saveColors(vm.colors)
            .success(function() {
                vm.colorsErrors = $rootScope.colorsErrors = null;
                Notify.success('events.updated');
            })
            .error(function(errors) {
                vm.colorsErrors = $rootScope.colorsErrors = errors.errors;
            });
    }
}