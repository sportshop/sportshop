angular
    .module('app.settings')
    .controller('DiscountsController.edit', DiscountsController_Edit);


DiscountsController_Edit.$inject = ['$rootScope', 'SettingsService', 'Notify', 'discounts'];
function DiscountsController_Edit($rootScope, SettingsService, Notify, discounts) {
    var vm = this;

    // Model Discounts
    if (discounts && discounts.data) {
        vm.discounts = discounts.data || [];
        $rootScope.globalDiscounts = JSON.parse(JSON.stringify(discounts.data));
    } else {
        vm.discounts = discounts || [];
    }

    // Discount types
    vm.DiscountsType = SettingsService.getDiscountsType() || [];
    // Discounts errors
    vm.discountsErrors = null;

    // Set if isset default discount for registration
    _.each(vm.discounts, function(item, index) {
        if (item.is_default) {
            vm.discount_is_default = index || 0;
        }
    });

    vm.addDiscount = addDiscountFn;
    vm.deleteDiscount = deleteDiscountFn;
    vm.saveDiscounts = saveDiscountsFn;

    /**
     * Add Discount
     */
    function addDiscountFn() {
        vm.discounts.push({
            id: '',
            title: '',
            value: '',
            sale_value: '',
            is_increase: true,
            is_default: false
        });
    }

    /**
     * Delete discount
     *
     * @param index
     */
    function deleteDiscountFn(index) {
        Notify.confirm(function () {
            vm.discounts.splice(index, 1);
        }, 'general.are_you_sure');
    }

    /**
     * Save discounts
     */
    function saveDiscountsFn() {
        // Set if isset default discount for registration
        _.each(vm.discounts, function(item, index) {
            item.is_default = (index == vm.discount_is_default) ? true : false;
        });


        SettingsService.saveDiscounts(vm.discounts)
            .success(function() {
                vm.discountsErrors = $rootScope.discountsErrors = null;
                Notify.success('events.updated');
            })
            .error(function(errors) {
                vm.discountsErrors = $rootScope.discountsErrors = errors.errors;
            });
    }
}