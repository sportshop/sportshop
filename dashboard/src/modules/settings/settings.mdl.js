import layoutTemplate       from "../layouts/dashboard.html";
import formTemplate         from "./form.html";
import currenciesTemplate   from "./currencies/_currencies.html";
import colorsTemplate       from "./colors/_colors.html";
import discountsTemplate    from "./discounts/_discounts.html";
import settingsTemplate     from "./settings/_settings.html";
import mainPageTemplate     from "./main-page/_main-page.html";

angular
    .module('app.settings', ['colorpicker.module'])
    .config(configure);


configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('settings', {
            abstract: true,
            url: '/settings',
            views: {
                '': {
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('settings.edit', {
            url: '',
            views: {
                'content': {
                    templateUrl: formTemplate,
                    controller: 'SettingsController.edit as ctrl'
                },
                'currencies@settings.edit': {
                    templateUrl: currenciesTemplate,
                    controller: 'CurrenciesController.edit as ctrl',
                },
                'colors@settings.edit': {
                    templateUrl: colorsTemplate,
                    controller: 'ColorsController.edit as ctrl',
                },
                'discounts@settings.edit': {
                    templateUrl: discountsTemplate,
                    controller: 'DiscountsController.edit as ctrl',
                },
                'settings@settings.edit': {
                    templateUrl: settingsTemplate
                },
                'main-page@settings.edit': {
                    templateUrl: mainPageTemplate
                }
            },
            resolve: {
                currencies: [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalCurrencies) {
                            return SettingsService.getCurrenciesList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalCurrencies));
                    }
                ],
                colors: [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalColors) {
                            return SettingsService.getColorsList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalColors));
                    }
                ],
                discounts: [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalDiscounts) {
                            return SettingsService.getDiscountsList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalDiscounts));
                    }
                ],
                settings: [
                    '$rootScope', 'SettingsService',
                    function($rootScope, SettingsService) {
                        if (!$rootScope.globalSettings) {
                            return SettingsService.getSettingsList();
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalSettings));
                    }
                ],
                categories: [
                    '$rootScope', 'CategoriesService',
                    function($rootScope, CategoriesService) {
                        var params = {
                            flat: true
                        };

                        if (!$rootScope.globalCategoriesFlatList) {
                            return CategoriesService.getList(params);
                        }

                        return JSON.parse(JSON.stringify($rootScope.globalCategoriesFlatList));
                    }
                ]
            }
        });
}