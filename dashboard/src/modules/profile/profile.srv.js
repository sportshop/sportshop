angular
    .module('app.profile')
    .service('ProfileService', ProfileService);


ProfileService.$inject = ['HttpService'];
function ProfileService(HttpService) {
    var baseUrl = '/profile';

    return {
        get:    getFn,
        update: updateFn
    };

    /**
     * Get profile data
     * @param id
     * @returns {*}
     */
    function getFn() {
        return HttpService.get(baseUrl)
    }

    /**
     * Update profile data
     * @param data
     * @returns {*}
     */
    function updateFn(data) {
        return HttpService.put(baseUrl, data)
    }
}