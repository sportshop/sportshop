angular
    .module('app.profile')
    .controller('ProfileController.edit', ProfileController_Edit);


ProfileController_Edit.$inject = ['$rootScope', 'BreadcrumbsService', 'MainHeaderService', 'ProfileService', 'Notify', 'profile'];
function ProfileController_Edit($rootScope, BreadcrumbsService, MainHeaderService, ProfileService, Notify, profile) {
    BreadcrumbsService.push('profile.title.list');
    MainHeaderService.set('profile.title.list');

    var vm = this;

    vm.item = profile.data || {}; // Profile Model

    vm.save = saveFn;

    /**
     * Update Profile Settings
     */
    function saveFn() {
        ProfileService.update(vm.item)
            .success(function() {
                $rootScope.currentUser.user.first_name = vm.item.first_name;
                $rootScope.currentUser.user.last_name = vm.item.last_name;

                // Update user in localStorage
                localStorage.removeItem('user');
                localStorage.setItem('user', JSON.stringify($rootScope.currentUser));

                vm.item.current_password = '';
                vm.item.password = '';
                vm.item.password_confirmation = '';

                vm.errors = false;

                Notify.success('events.updated');
            })
            .error(function(errors) {
                vm.item.current_password = '';
                vm.item.password = '';
                vm.item.password_confirmation = '';

                vm.errors = errors.errors;

                if (errors && errors.message) {
                    Notify.warning(errors.message);
                }
            });
    }
}