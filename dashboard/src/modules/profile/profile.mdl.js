import layoutTemplate from "../layouts/dashboard.html";
import editTemplate from "./edit.html";

angular
    .module('app.profile', [])
    .config(configure);


configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('profile', {
            abstract: true,
            url: '/profile',
            views: {
                '': {
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('profile.edit', {
            url: '',
            views: {
                'content': {
                    controller: 'ProfileController.edit as ctrl',
                    templateUrl: editTemplate
                }
            },
            resolve: {
                profile: [
                    'ProfileService',
                    function(ProfileService) {
                        return ProfileService.get();
                    }
                ]
            }
        });
}