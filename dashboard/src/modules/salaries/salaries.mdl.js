import layoutTemplate from '../layouts/dashboard.html';
import listTemplate from './list.html';
import statisticTemplate from './statistic.html';
import filtersTemplate from './_filters.html';
import advanceTemplate from './_advance.html';
import additionallyTemplate from './_additionally.html';

angular
    .module('app.salaries', [])
    .config(configure);

configure.$inject = ['$stateProvider'];

function configure($stateProvider) {
    $stateProvider
        .state('salaries', {
            abstract: true,
            url: '/salaries',
            views: {
                '': {
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('salaries.list', {
            url: '?page?name?year?id?store_id',
            reloadOnSearch: false,
            views: {
                'content': {
                    controller: 'SalariesController.list as ctrl',
                    templateUrl: listTemplate
                },
                'filters@salaries.list': {
                    templateUrl: filtersTemplate
                },
                'advance@salaries.list': {
                    templateUrl: advanceTemplate
                },
                'additionally@salaries.list': {
                    templateUrl: additionallyTemplate
                }
            },
            resolve: {
                users: [
                    '$stateParams', 'HttpService',
                    function ($stateParams, HttpService) {
                        return HttpService.get('/users?role=manager,limited_manager');
                    }
                ]
            }
        })
        .state('salaries.view', {
            url: '/monthly?user_id?year?month?store_id',
            views: {
                'content': {
                    controller: 'SalariesController.view as ctrl',
                    templateUrl: statisticTemplate
                }
            },
            resolve: {
                salary: [
                    '$stateParams', 'SalariesService',
                    function ($stateParams, SalariesService) {
                        var params = {
                            year: $stateParams.year,
                            name: $stateParams.user_id,
                            id: $stateParams.user_id,
                            month: moment(new Date($stateParams.month)).format('M'),
                            store_id: localStorage.getItem('active_store')
                        };

                        return SalariesService.getMonthSalary(params);
                    }
                ]
            }
        });
}