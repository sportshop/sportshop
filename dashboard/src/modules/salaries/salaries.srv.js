angular
    .module('app.salaries')
    .service('SalariesService', SalariesService);

SalariesService.$inject = ['HttpService'];

function SalariesService(HttpService) {
    var baseUrl = '/users/';

    var years = [
        { value: 2018 },
        { value: 2019 },
        { value: 2020 }
    ];

    return {
        getSalariesList: getSalariesListFn,
        getActivities: getActivitiesFn,
        getTransactions: getTransactionsFn,
        getUserInfo: getUserInfoFn,
        getUserSalesInfo: getUserSalesInfoFn,
        getUserSalaryInfo: getUserSalaryInfoFn,
        getMonthSalary: getMonthSalaryFn,
        getSalary: getSalaryFn,
        makePayment: makePaymentFn,
        saveTransaction: saveAdditionalFn,
        removeAdditional: removeAdditionalFn,
        saveAdvance: saveAdvanceFn,
        removeAdvance: removeAdvanceFn,
        getYears: getYearsFn
    };

    // Get Activities list
    function getSalariesListFn(params) {
        return HttpService.getWParams(baseUrl + params.name + '/salaries', {
            params: params.year || null
        });
    }

    // Get Transactions list
    function getTransactionsFn(params) {
        var param = {
            year: params.year || 2018
        };

        return HttpService.getWParams(baseUrl + params.name + '/transactions', {
            params: param
        });
    }

    // Get User Info
    function getUserInfoFn(params) {
        return HttpService.getWParams(baseUrl + params.id);
    }

    // Get User Info
    function getUserSalesInfoFn(params) {
        return HttpService.getWParams(baseUrl + params.name + '/sales');
    }

    // Get User Salary per year for list
    function getUserSalaryInfoFn(params) {
        var fullUrl = baseUrl + params.id + '/salary?year=' + params.year;
        if(params.store_id) {
            fullUrl = fullUrl + '&store_id=' + params.store_id;
        }
        return HttpService.getWParams(fullUrl);
    }

    // Get User Salary per month for view
    function getMonthSalaryFn(params) {
        var fullUrl = baseUrl + params.id + '/salary?year=' + params.year + '&month=' + params.month;
        if(params.store_id) {
            fullUrl = fullUrl + '&store_id=' + params.store_id;
        }
        return HttpService.getWParams(fullUrl);
    }

    // Get Activities info
    function getActivitiesFn(params) {
        var param = {
            year: params.year || 2017,
            group_by: params.group_by
        };
        return HttpService.getWParams(baseUrl + params.name + '/activity', {
            params: param
        });
    }

    // Get salary per month
    function getSalaryFn(user_id, salary_id) {
        return HttpService.get(baseUrl + user_id + '/salaries/' + salary_id);
    }

    // Save additional bonus
    function saveAdditionalFn(item, data, user) {
        return HttpService.post(baseUrl + user.id + '/transactions', data);
    }

    // Remove additional bonus
    function removeAdditionalFn(item, element_id, user) {
        return HttpService.delete(baseUrl + user.id + '/transactions/' + element_id);
    }

    // Save Advance
    function saveAdvanceFn(item, data) {
        return HttpService.post(baseUrl + item.user.id + '/salaries/' + item.id + '/additional_costs', data);
    }

    // Remove advance
    function removeAdvanceFn(item, element_id, user) {
        return HttpService.delete(baseUrl + user.id + '/salaries/' + item.id + '/additional_costs/' + element_id);
    }

    // Make payment
    function makePaymentFn(item) {
        return HttpService.post(baseUrl + '/' + item.id + '/pay', item);
    }

    // Get years
    function getYearsFn() {
        return years;
    }
}