angular
    .module('app.salaries')

    .controller('SalariesController.list', SalariesController_List)
    .controller('SalariesController.view', SalariesController_View);

SalariesController_List.$inject = [
    '$rootScope',
    '$state',
    '$stateParams',
    'BreadcrumbsService',
    'MainHeaderService',
    'HttpService',
    'Notify',
    'SalariesService',
    'users'
];

function SalariesController_List(
    $rootScope,
    $state,
    $stateParams,
    BreadcrumbsService,
    MainHeaderService,
    HttpService,
    Notify,
    SalariesService,
    users
) {
    BreadcrumbsService.push('salaries.title.list');
    MainHeaderService.set('salaries.title.list');

    var vm = this;

    // Users
    vm.users = users.data || [];

    // Years
    vm.years = SalariesService.getYears();

    vm.currentDate = new Date();

    // Filter
    vm.searchForm = {
        name: $stateParams.name || null,
        id: $stateParams.id || null,
        year: $stateParams.year || 2018,
        store_id: localStorage.getItem('active_store')
    };

    if ($stateParams.id && $stateParams.year) {
        vm.searchForm.id = +vm.searchForm.id;
        vm.searchForm.year = +vm.searchForm.year;
        processSearchFn();
    }

    // Transform list
    function transformList(list) {
        _.each(list, function (item) {
            if (item.transactions && item.transactions.length) {
                _.each(item.transactions, function (transaction) {
                    if (transaction.rate) {
                        transaction.rate = _.find(transaction.rate, function (el) {
                            return el.currency_id == ($rootScope.defaultCurrencyId);
                        });
                    }
                });
            }

            item.salary = _.find(item.salary, function (el) {
                return el.currency_id == ($rootScope.defaultCurrencyId);
            });

            item.sales = _.find(item.sales, function (el) {
                return el.currency_id == ($rootScope.defaultCurrencyId);
            });

            return item;
        });

        return list;
    }

    vm.startDateBeforeRender = startDateBeforeRenderFn;

    vm.processSearch = processSearchFn;
    vm.makePay = makePayFn;
    vm.addAdditionalyItem = addAdditionalyItemFn;
    vm.removeAdditionalyItem = removeAdditionalyItemFn;
    vm.addAdvanceItem = addAdvanceItemFn;
    vm.removeAdvanceItem = removeAdvanceItemFn;
    vm.showComment = showCommentFn;
    vm.hideComment = hideCommentFn;
    vm.changePage = changePageFn;

    vm.totalAmount = totalAmountFn;
    vm.totalBalance = totalBalanceFn;
    vm.isEmptyObject = isEmptyObjectFn;

    function startDateBeforeRenderFn($dates, key) {
        const todaySinceMidnight = new Date();

        var date = new Date(key),
            y = date.getFullYear(),
            m = date.getMonth();
        var firstDay = new Date(y, m, 1);
        var lastDay = new Date(y, m + 1, 1);

        todaySinceMidnight.setUTCHours(0, 0, 0, 0);
        firstDay.setUTCHours(0, 0, 0, 0);
        lastDay.setUTCHours(0, 0, 0, 0);

        $dates.filter(function (date) {
            return (date.utcDateValue <= firstDay.getTime());
        }).forEach(function (date) {
            date.selectable = false;
        });

        $dates.filter(function (date) {
            return (date.utcDateValue > lastDay.getTime());
        }).forEach(function (date) {
            date.selectable = false;
        });
    }

    function isEmptyObjectFn(obj) {
        for (var prop in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                return false;
            }
        }
        return true;
    }

    // Total salary amount
    function totalAmountFn(elem) {
        elem.total_amount = 0;
        var rate = 0;
        var salesPercentage = 0;

        var totalAmount = _.reduce(elem.transactions, function (memo, el) {
            var elementAmount = 0;

            if (el && el.type && el.type == 'additional') {
                elementAmount = (el.rate && el.rate.value) ? (+el.rate.value) : 0;
            }

            return memo + +elementAmount;
        }, 0);

        if (elem.salary && elem.salary.value) {
            rate = +elem.salary.value;
        }

        if (elem.sales && elem.sales.value) {
            salesPercentage = +elem.sales.value;
        }

        elem.total_amount = +rate + +salesPercentage + +totalAmount;

        return elem.total_amount;
    }

    // Total user balance
    function totalBalanceFn(elem) {
        elem.total_balance = 0;

        var finalAmount = _.reduce(elem.transactions, function (memo, el) {
            var elementAmount = 0;
            if (el && el.type && el.type == 'final') {
                elementAmount = (el.rate && el.rate.value) ? (+el.rate.value) : 0;
            }
            return memo + +elementAmount;
        }, 0);

        var totalAmount = _.reduce(elem.transactions, function (memo, el) {
            var elementAmount = 0;
            if (el && el.type && el.type == 'prepayment') {
                elementAmount = (el.rate && el.rate.value) ? (+el.rate.value) : 0;
            }
            return memo + +elementAmount;
        }, 0);

        if (finalAmount) {
            elem.total_balance = +elem.total_amount - +finalAmount;
        } else {
            elem.total_balance = +elem.total_amount - +totalAmount;
        }

        return elem.total_balance;
    }

    // Change Filter
    function processSearchFn() {
        var searchParams = {
            id: vm.searchForm.id || null,
            name: vm.searchForm.name || null,
            year: vm.searchForm.year,
            group_by: 'month',
            store_id: localStorage.getItem('active_store')
        };

        vm.list = [];

        if (searchParams.id && searchParams.year) {
            SalariesService.getUserSalaryInfo(searchParams)
                .success(function (resp) {
                    vm.list = resp || [];
                    // Transform list
                    transformList(vm.list);

                    _.each(vm.list, function (value, key) {
                        var is_paid = _.find(value.transactions,
                            function (elem) {
                                if (elem && elem.type) {
                                    return elem.type == 'final';
                                }
                                return null;
                            });

                        value.is_paid = !!(is_paid);
                        value.payment = is_paid;

                        if (!!is_paid) {
                            value.payed_at = is_paid.payed_at;
                        }
                    });

                    $state.go($state.current, searchParams, { notify: false });
                });
        } else {
            return;
        }

    }

    // Make payment for user
    function makePayFn(item, date) {
        var temp = angular.copy(item) || {};

        var data = {
            rate: {
                value: +totalAmountFn(item),
                currency_id: $rootScope.defaultCurrencyId
            },
            payed_at: moment(new Date(date)).format('YYYY-MM-DD HH:mm:ss'),
            type: 'final',
            comment: 'final payment'
        };

        SalariesService.saveTransaction(item, data, vm.searchForm)
            .success(function (response) {
                item.is_paid = true;
                item.total_balance = 0;

                // Notify.success('general.saved');
            })
            .error(function (errors) {
                Notify.warning(errors.message);
            });
    }

    // Add additional bonus for user salary
    function addAdditionalyItemFn(key, item, elem) {
        var temp = angular.copy(elem) || {};
        var data = {
            rate: {
                value: +temp.value || null,
                currency_id: $rootScope.defaultCurrencyId
            },
            payed_at: temp.payed_at ? moment(new Date(temp.payed_at)).format('YYYY-MM-DD HH:mm:ss') : null,
            comment: temp.comment || '',
            type: 'additional'
        };

        SalariesService.saveTransaction(item, data, vm.searchForm)
            .success(function (response) {
                var response = response;
                var rate = {};

                if (response.rate) {
                    rate = _.find(response.rate, function (el) {
                        return el.currency_id == ($rootScope.defaultCurrencyId);
                    });

                    response.rate = rate;
                }

                vm.errors = false;

                if (item && !item.transactions) {
                    item.transactions = [];
                }

                item.transactions.push(response);

                elem.payed_at = null;
                elem.value = null;
                elem.currency_id = elem.currency_id;
                elem.comment = '';
                elem.type = null;
            })
            .error(function (errors) {
                vm.errors = {};
                vm.errors[key] = errors.errors;
            });
    }

    // Remove transaction from user's salary
    function removeAdditionalyItemFn(item, index, elem) {
        Notify.confirm(function () {
            SalariesService.removeAdditional(item, elem.id, vm.searchForm)
                .success(function (response) {
                    var itemIndex = item.transactions.indexOf(elem);
                    item.transactions.splice(itemIndex, 1);

                    // Notify.success('general.removed');
                })
                .error(function (errors) {
                    Notify.warning(errors.message);
                });
        }, 'general.are_you_sure');
    }

    // Make advance for user
    function addAdvanceItemFn(key, item, elem) {
        var temp = angular.copy(elem) || {};
        var data = {
            rate: {
                value: +temp.value || null,
                currency_id: $rootScope.defaultCurrencyId
            },
            payed_at: temp.payed_at ? moment(new Date(temp.payed_at)).format('YYYY-MM-DD HH:mm:ss') : null,
            type: 'prepayment'
        };

        SalariesService.saveTransaction(item, data, vm.searchForm)
            .success(function (response) {
                var response = response;
                var rate = {};
                if (response.rate) {
                    rate = _.find(response.rate, function (el) {
                        return el.is_default;
                    });

                    response.rate = rate;
                }

                vm.advanceErrors = false;

                if (item && !item.transactions) {
                    item.transactions = [];
                }

                item.transactions.push(response);

                elem.payed_at = null;
                elem.value = null;
                elem.currency_id = elem.currency_id || $rootScope.defaultCurrencyId;
                elem.comment = '';
                elem.type = null;
            })
            .error(function (errors) {
                vm.advanceErrors = {};
                vm.advanceErrors[key] = errors.errors;
            });
    }

    // Remove user's advance
    function removeAdvanceItemFn(item, advanceIndex, elem) {
        Notify.confirm(function () {
            SalariesService.removeAdvance(item, elem.id, vm.searchForm)
                .success(function (response) {
                    var itemIndex = item.transactions.indexOf(elem);
                    item.transactions.splice(itemIndex, 1);

                    // Notify.success('general.removed');
                })
                .error(function (errors) {
                    Notify.warning(errors.message);
                });
        }, 'general.are_you_sure');
    }

    // Show comment
    function showCommentFn(parentIndex, index) {
        vm.commentIsVisible = parentIndex + '-' + index;
    }

    // Hide comment
    function hideCommentFn() {
        vm.commentIsVisible = null;
    }

    // Change page
    function changePageFn() {
        var params = {
            name: vm.searchForm.name,
            id: vm.searchForm.id,
            year: vm.searchForm.year,
            page: vm.list.current_page,
            store_id: localStorage.getItem('active_store')
        };

        SalariesService.getSalariesList(params)
            .success(function (response) {
                $state.go($state.current, params, { notify: false });
                vm.list = response || [];
                // Transform list
                transformList(vm.list);
            });
    }
}

SalariesController_View.$inject = [
    '$rootScope',
    '$stateParams',
    '$filter',
    'BreadcrumbsService',
    'MainHeaderService',
    'salary'
];

function SalariesController_View($rootScope, $stateParams, $filter, BreadcrumbsService, MainHeaderService, salary) {
    BreadcrumbsService.push('salaries.title.list', 'salaries.list');
    BreadcrumbsService.push('salaries.title.view');

    var vm = this;

    vm.salary = salary.data || [];

    _.each(vm.salary.data, function (item) {
        if (item.additional) {
            item.additional = _.find(item.additional, function (el) {
                return el.currency_id == ($rootScope.defaultCurrencyId);
            });
        }

        if (item.salary) {
            item.salary = _.find(item.salary, function (el) {
                return el.currency_id == ($rootScope.defaultCurrencyId);
            });
        }

        if (item.sales) {
            item.sales = _.find(item.sales, function (el) {
                return el.currency_id == ($rootScope.defaultCurrencyId);
            });
        }

        if (item.total) {
            item.total = _.find(item.total, function (el) {
                return el.currency_id == ($rootScope.defaultCurrencyId);
            });
        }

        return item;
    });

    if (vm.salary.total.additional) {
        vm.salary.total.additional = _.find(vm.salary.total.additional, function (el) {
            return el.currency_id == ($rootScope.defaultCurrencyId);
        });
    }

    if (vm.salary.total.salary) {
        vm.salary.total.salary = _.find(vm.salary.total.salary, function (el) {
            return el.currency_id == ($rootScope.defaultCurrencyId);
        });
    }

    if (vm.salary.total.sales) {
        vm.salary.total.sales = _.find(vm.salary.total.sales, function (el) {
            return el.currency_id == ($rootScope.defaultCurrencyId);
        });
    }

    if (vm.salary.total.total) {
        vm.salary.total.total = _.find(vm.salary.total.total, function (el) {
            return el.currency_id == ($rootScope.defaultCurrencyId);
        });
    }

    var month = $stateParams.month;

    vm.sumByDay = sumByDayFn;

    function sumByDayFn(elem) {
        if (elem.salary && elem.salary.type) {
            if (elem.salary.type == 'monthly') {
                elem.salary_day = '';
            } else if (elem.salary.type == 'hourly') {
                if (elem.salary.hours && elem.salary.value && elem.salary.value.value) {
                    elem.salary_day = +elem.salary.value.value * +elem.salary.value.coefficient;
                } else {
                    elem.salary_day = 0;
                }
            } else if (elem.salary.type == 'daily') {
                if (elem.salary.value && elem.salary.value.value) {
                    elem.salary_day = +elem.salary.hours * +elem.salary.value.value * +elem.salary.value.coefficient;
                } else {
                    elem.salary_day = 0;
                }
            }
        } else {
            elem.salary_day = '';
        }

        var sum = +elem.salary_day + +elem.sales + +elem.transactions;

        return sum;
    }

    MainHeaderService.set('salaries.title.view', { date: $filter('momentDate')(month, 'MM.YYYY') });
}