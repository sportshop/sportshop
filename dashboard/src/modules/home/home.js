import layoutTemplate from "../layouts/dashboard.html";
import listTemplate from "./main.html";

angular.module('app.home', [])
    .config(configure)
    .controller('HomeController.main', HomeController_Main);


configure.$inject = ['$stateProvider'];
function configure($stateProvider) {
    $stateProvider
        .state('home', {
            abstract: true,
            url: '/home',
            views: {
                '': {
                    templateUrl: layoutTemplate
                }
            }
        })
        .state('home.main', {
            url: '',
            views: {
                'content': {
                    controller: 'HomeController.main',
                    templateUrl: listTemplate
                }
            }
        });
}

HomeController_Main.$inject = ['MainHeaderService'];
function HomeController_Main(MainHeaderService) {
    MainHeaderService.set('home.title.main');
}