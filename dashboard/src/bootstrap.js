/**
 * Libs
 */
import 'tether'
import 'bootstrap-sass'
import 'admin-lte'
import 'admin-lte/plugins/slimScroll/jquery.slimscroll'
import 'admin-lte/plugins/iCheck/icheck'
import 'bower-redactor'


/**
 * Angular's components
 */
import 'angular'
import 'angular-ui-router'
import 'angular-ui-notification'
import 'angular-sanitize'
import 'angular-file-upload'
import 'angular-local-storage'
import 'angular-animate'
import 'angular-perfect-scrollbar'
import 'angular-bootstrap/ui-bootstrap'
import 'angular-bootstrap/ui-bootstrap-tpls'
import 'angular-bootstrap-datetimepicker'
import 'angular-translate'
import 'angular-translate-loader-static-files'
import 'angular-loading-bar'
import 'ng-dialog'
import 'angucomplete-alt/dist/angucomplete-alt.min'
import 'angular-chart.js'
import 'sm-daterangepicker/lib/index.js'
import 'angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.js'
import 'ng-mask/dist/ngMask'


/**
 * Fix ordering conflict
 */
import 'satellizer'
import 'ui-select'
import 'ui-cropper'
import 'angular-redactor'
import 'ng-dialog/css/ngDialog.min.css'
import 'ng-dialog/css/ngDialog-theme-default.min.css'
import 'ui-cropper/compile/minified/ui-cropper.css'
import 'angucomplete-alt/angucomplete-alt.css'
import 'bootstrap-daterangepicker/daterangepicker.js'
import 'bootstrap-daterangepicker/daterangepicker.css'
import 'angular-daterangepicker/js/angular-daterangepicker.min.js'
import 'angular-bootstrap-colorpicker/css/colorpicker.css'

// Fix Libs
import "font-awesome/css/font-awesome.css";
import "admin-lte/bootstrap/css/bootstrap.css";
import "admin-lte/dist/css/AdminLTE.css";
import "admin-lte/dist/css/skins/skin-blue.css";
import "admin-lte/dist/css/skins/skin-green.css";
import "angular-bootstrap-datetimepicker/src/css/datetimepicker.css";
import "ui-select/dist/select.css";
import "bower-redactor/redactor/redactor.css";
import "admin-lte/plugins/iCheck/square/_all.css";

/**
 * Components
 */
import './components/factories'
import './components/filters'
import './components/notify'
import './components/services'
import './components/utils'


/**
 * Directives
 */
import './directives/main/main'
import './directives/dashboard/layout_partials'


/**
 * Modules
 */
import './modules/auth/auth.mdl'
import './modules/auth/auth.srv.js'
import './modules/auth/auth.ctrl.js'
import './modules/home/home'
import './modules/pages/pages.mdl.js'
import './modules/pages/pages.srv.js'
import './modules/pages/pages.ctrl.js'
import './modules/banners/banners.mdl.js'
import './modules/banners/banners.srv.js'
import './modules/banners/banners.ctrl.js'
import './modules/form-builder/form-builder.mdl.js'
import './modules/form-builder/form-builder.srv.js'
import './modules/form-builder/form-builder.ctrl.js'
import './modules/salaries/salaries.mdl.js'
import './modules/salaries/salaries.srv.js'
import './modules/salaries/salaries.сtrl.js'
import './modules/settings/settings.mdl'
import './modules/settings/settings.srv'
import './modules/settings/settings/settings.ctrl'
import './modules/settings/currencies/currencies.ctrl'
import './modules/settings/colors/colors.ctrl'
import './modules/settings/discounts/discounts.ctrl'
import './modules/attributes/attributes.mdl'
import './modules/attributes/attributes.srv'
import './modules/attributes/attributes.ctrl'
import './modules/brands/brands.mdl'
import './modules/brands/brands.srv'
import './modules/brands/brands.ctrl'
import './modules/users/users.mdl'
import './modules/users/users.srv'
import './modules/users/users.ctrl'
import './modules/stores/stores.mdl'
import './modules/stores/stores.srv'
import './modules/stores/stores.ctrl'
import './modules/categories/categories.mdl'
import './modules/categories/categories.srv'
import './modules/categories/categories.ctrl'
import './modules/tables/tables.mdl'
import './modules/tables/tables.srv'
import './modules/tables/tables.ctrl'
import './modules/products/products.mdl'
import './modules/products/products.srv'
import './modules/products/products.list.ctrl'
import './modules/products/products.edit.ctrl'
import './modules/profile/profile.mdl'
import './modules/profile/profile.srv'
import './modules/profile/profile.ctrl'
import './modules/costs/costs.mdl'
import './modules/costs/costs.srv'
import './modules/costs/costs.ctrl'
import './modules/orders/NovaPoshtaHelper'
import './modules/orders/orders.mdl.js'
import './modules/orders/orders.srv.js'
import './modules/orders/list.ctrl.js'
import './modules/orders/edit.ctrl.js'
import './modules/orders/product/add/add.ctrl.js'
import './modules/errors/errors'
import './modules/products/components/sizesCmp'
import './modules/statistics/statistics.mdl'
import './modules/statistics/statistics.srv'
import './modules/statistics/statistics.main.ctrl'
import './modules/statistics/statistics.sales.ctrl'
import './modules/statistics/statistics.salary.ctrl'
import './modules/statistics/statistics.earnings.ctrl'
import './modules/statistics/statistics.moving_products.ctrl'
import './modules/statistics/statistics.pending_orders.ctrl'
import './modules/statistics/statistics.costs.ctrl'


/**
 * Dashboard
 */
import './dashboard'