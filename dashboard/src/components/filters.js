(function ()
{
    'use strict';

    angular
        .module('app.filters', [])

        .filter('dateFormat', dateFormat)
        .filter('dateTimeFormat', dateTimeFormat)
        .filter('dateToISO', dateToISO)
        .filter('currencyFormat', currencyFormat)
        .filter('amount', amount)
        .filter('defaultCurrency', defaultCurrency)
        .filter('sortByDefaultSize', sortByDefaultSize)
        .filter('rounded', rounded)
        .filter('formatNumbers', formatNumbers)
        .filter('amountWithoutZero', amountWithoutZero)
        .filter('words', words)
        .filter('characters', characters)
        .filter('cut', cut)
        .filter('bindHtml', bindHtml)
        .filter('strip_tags', strip_tags)
        .filter('momentDate', momentDate)
        .filter('currentdate', currentdate)
        .filter('isDefaultFirst', isDefaultFirst)
        .filter('getIsDefaultFirst', getIsDefaultFirst);

    /* Fn
     ============================================================================================================= */

    currentdate.$inject = ['$filter'];
    function currentdate($filter)
    {
        return function ()
        {
            return $filter('date')(new Date(), 'dd MMM yyyy');
        };
    }

    isDefaultFirst.$inject = ['$filter'];
    function isDefaultFirst($filter)
    {
        return function (input)
        {
            return $filter('orderBy')(input, 'is_default', true);
        };
    }

    getIsDefaultFirst.$inject = ['$filter'];
    function getIsDefaultFirst($filter)
    {
        return function (input)
        {
            var filterData = $filter('orderBy')(input, 'is_default', true);
            if (filterData && filterData.length) {
                return filterData[0];
            }
        };
    }

    dateFormat.$inject = ['$filter'];
    function dateFormat($filter)
    {
        return function (input, format)
        {
            format = format || 'dd MMM yyyy';

            if (angular.isNumber(input))
            {
                return $filter('date')(input, format);
            }

            input = $filter('dateToISO')(input);
            if (input)
            {
                return $filter('date')(input, format);
            } else
            {
                return input;
            }
        };
    }

    dateTimeFormat.$inject = ['$filter'];
    function dateTimeFormat($filter)
    {
        return function (input, format)
        {
            input = $filter('dateToISO')(input);
            format = format || 'dd MMM yyyy h:mm a';
            if (input)
            {
                return $filter('date')(input, format);
            } else
            {
                return input;
            }
        };
    }

    function dateToISO()
    {
        return function (input)
        {
            if (input instanceof Date) return input.toISOString(); // if input instanceof Date
            if (angular.isDefined(moment) && moment.isMoment(input)) return input.toISOString(); // if input instanceof moment
            if (input && input !== '0000-00-00 00:00:00' && input !== '0000-00-00 00:00:00' && input !== '0000/00/00 00:00:00')
            {
                input = input.toString().split('-').join('/').split('.').join('/');
                return new Date(input).toISOString();
            } else
            {
                return null;
            }
        };
    }

    /**
     * Format amount according to selected currency
     */
    currencyFormat.$inject = ['$filter', '$locale', '$rootScope'];
    function currencyFormat($filter, $locale, $rootScope)
    {
        var currencyFilter = $filter('currency');
        var formats = $locale.NUMBER_FORMATS;
        return function (amount)
        {
            amount = amount ? (amount * 1).toFixed(2) : 0.00;
            var value = currencyFilter(amount, '$');
            // split into parts
            var parts = value.split(formats.DECIMAL_SEP);
            var dollar = parts[0];
            var cents = parts[1] || '00';
            cents = cents.substring(0, 2) == '00' ? cents.substring(2) : '.' + cents; // remove "00" cent amount
            return dollar + cents;
        };
    }

    function amount()
    {
        return function (input, symbol)
        {
            input = parseFloat(input);

            if (isNaN(input))
            {
                input = 0;
            }

            if (symbol)
            {
                return symbol + ' ' + input.toFixed(2);
            } else
            {
                return input.toFixed(2);
            }

        };
    }


    defaultCurrency.$inject = ['$rootScope'];
    function defaultCurrency($rootScope)
    {
        return function (input)
        {
            var currency = _.find(input, function(el) {
                return el.currency_id == $rootScope.defaultCurrencyId;
            });

            if (currency && currency.value) {
                return currency.value;
            }

            return;
        };
    }

    sortByDefaultSize.$inject = ['$rootScope'];
    function sortByDefaultSize($rootScope)
    {
        return function (input)
        {
            var currency = _.find(input, function(el) {
                return el.currency_id == $rootScope.defaultCurrencyId;
            });

            if (currency && currency.value) {
                return currency.value;
            }

            return;
        };
    }

    function rounded()
    {
        return function (input, symbol)
        {
            input = parseFloat(input);

            if (isNaN(input))
            {
                input = 0;
            }

            return Math.round(input || 0);
        };
    }

    function formatNumbers()
    {

        return function (input)
        {
            if (isNaN(input))
            {
                return 0;
            }

            var parts = input.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");

            return parts.join(".");
        };
    }

    function amountWithoutZero()
    {
        return function (input)
        {
            input = parseFloat(input);

            if (isNaN(input))
            {
                input = 0;
            }

            var amount = input.toFixed(0);
            return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        };
    }

    function words()
    {
        return function (input, words)
        {
            if (isNaN(words)) return input;
            if (words <= 0) return '';
            if (input)
            {
                var inputWords = input.split(/\s+/);
                if (inputWords.length > words)
                {
                    input = inputWords.slice(0, words).join(' ') + '...';

                }
            }

            return input;
        };
    }

function cut() {
    return function (value, wordwise, max, tail) {
        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace !== -1) {
                //Also remove . and , so its gives a cleaner result.
                if (value.charAt(lastspace-1) === '.' || value.charAt(lastspace-1) === ',') {
                    lastspace = lastspace - 1;
                }
                value = value.substr(0, lastspace);
            }
        }

        return value + (tail || ' …');
    };
}

    function characters()
    {
        return function (input, chars, breakOnWord)
        {

            if (isNaN(chars)) return input;
            if (chars <= 0) return '';
            if (input && input.length > chars)
            {
                input = input.substring(0, chars);

                if (!breakOnWord)
                {
                    var lastspace = input.lastIndexOf(' ');
                    //get last space
                    if (lastspace !== -1)
                    {
                        input = input.substr(0, lastspace);
                    }
                } else
                {
                    while (input.charAt(input.length - 1) === ' ')
                    {
                        input = input.substr(0, input.length - 1);
                    }
                }

                return input + '...';
            }
            return input;
        };
    }

    bindHtml.$inject = ['$sce'];
    function bindHtml($sce)
    {
        return function (val)
        {
            return $sce.trustAsHtml(val);
        };
    }

    function strip_tags()
    {
        return function strip_tags(input, allowed)
        {
            allowed = (((allowed || '') + '')
                .toLowerCase()
                .match(/<[a-z][a-z0-9]*>/g) || [])
                .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
            var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
                commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
            return input.replace(commentsAndPhpTags, '')
                .replace(tags, function ($0, $1)
                {
                    return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
                });
        }
    }

    /**
     * Format date with moment.js
     */
    function momentDate()
    {
        return function(val, format)
        {
            moment.locale('uk');
            if (val) {
                return moment(val).format(format || 'MMMM Do YYYY')
            }
        }
    }

})();