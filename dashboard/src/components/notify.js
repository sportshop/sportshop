import notifyTemplate from './views/angular-ui-notification.html'
import confirmView from '../directives/main/confirm.html';

(function ()
{
    'use strict';

    angular
        .module('app.notify', [
            'ui-notification' // https://github.com/alexcrack/angular-ui-notification
        ])

        .config(function (NotificationProvider)
        {
            NotificationProvider.setOptions({
                delay: 3000,
                startTop: 20,
                startRight: 10,
                verticalSpacing: 20,
                horizontalSpacing: 20,
                positionX: 'left',
                positionY: 'bottom',
                templateUrl: notifyTemplate
            });
        })

        .factory('Notify', ['$timeout', 'ngDialog', 'Notification', function ($timeout, ngDialog, Notification)
        {

            var defaultDelay = window.location.href.indexOf('seleniumtc.l') !== -1 ? 100 : 3000;

            return {
                confirm: function (callback, title, template, data)
                {
                    ngDialog.openConfirm({
                        className: 'modal notify-width fade in',
                        templateUrl: template ? template.toString() : confirmView,
                        showClose: false,
                        data: {
                            title: title,
                            data: data
                        },
                        scope: true
                    }).then(function (value)
                    {
                        callback(value);
                    });
                },
                alert: function (message, title)
                {
                    title = title ? title : 'Attention!';

                    ngDialog.open({
                        plain: true,
                        template: '<div class="custom-alert"><img `images/logo.png" /><h4>' + title + '</h4><span>' + message + '</span></div>'
                    });
                },

                info: function (text)
                {
                    return Notification.info({message: text});
                },
                success: function (text, delay, isHard)
                {
                    var notify = Notification.success({message: text, delay: delay || defaultDelay});
                    
                    if (isHard) {
                        $timeout(function () {
                            notify.then((notification) => notification.kill(true))
                        }, (delay || defaultDelay) * 3);
                    }
                },
                warning: function (text, delay, isHard)
                {
                    var notify = Notification.warning({message: text, delay: delay || defaultDelay});
    
                    if (isHard) {
                        $timeout(function () {
                            notify.then((notification) => notification.kill(true))
                        }, (delay || defaultDelay) * 3);
                    }
                },
                error: function (text, delay, isHard)
                {
                    var notify = Notification.error({message: text, delay: delay || defaultDelay});
    
                    if (isHard) {
                        $timeout(function () {
                            notify.then((notification) => notification.kill(true))
                        }, (delay || defaultDelay) * 3);
                    }
                },
                clear: function ()
                {
                    Notification.clearAll();
                },
                close: function ()
                {
                    ngDialog.closeAll();
                }
            };
        }])

    ;

})();
