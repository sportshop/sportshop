(function ()
{
    'use strict';
    angular
        .module('app.services', [])
        .service('FileService', FileService)
        .service('HttpService', HttpService)
        .service('ConfigService', ConfigService)
        .service('BreadcrumbsService', BreadcrumbsService)
        .service('SeoService', SeoService)
        .service('MyFileUploader', MyFileUploader)
        .service('MainHeaderService', MainHeaderService)
        .service('HelperService', HelperService);


    HelperService.$inject = ['CONFIG'];
    function HelperService(CONFIG) {
        // Clone all object in depth
        this.cloneObject = function (obj) {
            if (obj === null || typeof obj !== 'object') {
                return obj;
            }

            var temp = obj.constructor(); // give temp the original object's constructor
            for (var key in obj) {
                temp[key] = this.cloneObject(obj[key]);
            }

            return temp;
        }

        /**
         * Get current month
         *
         * @returns {string}
         */
        this.getCurrentMonth = function() {
            var date = new Date(),
                y = date.getFullYear(),
                m = date.getMonth(),
                currentMonth = y;

            if ((m+1) < 10) {
                currentMonth = currentMonth +'_0'+ (m+1);
            } else {
                currentMonth = currentMonth +'_'+ (m+1);
            }

            return currentMonth;
        }

        // Generate select with data
        this.generateDate = function () {
            var dateOptions = [];
            var currentDate = new Date(),
                currentYear = currentDate.getFullYear(),
                currentMonth = currentDate.getMonth() + 1;

            if (currentMonth <= 9) {
                currentMonth = '0'+ currentMonth;
            }

            var startDate = CONFIG.start_date || '2018_01';
            var startYear = startDate.slice(0, 4);
            var startMonth = +(startDate.slice(5));
            var endDate = currentYear +'_'+ currentMonth;

            function generateGroupForYear(date, status, startMonth, currentMonth) {
                var year = date.slice(0, 4);
                var month = +(date.slice(5));
                var groupYear = {};
                var groupMonths = [
                    {key: year+'_01', value: 'Січень'},
                    {key: year+'_02', value: 'Лютий'},
                    {key: year+'_03', value: 'Березень'},
                    {key: year+'_04', value: 'Квітень'},
                    {key: year+'_05', value: 'Травень'},
                    {key: year+'_06', value: 'Червень'},
                    {key: year+'_07', value: 'Липень'},
                    {key: year+'_08', value: 'Серпень'},
                    {key: year+'_09', value: 'Вересень'},
                    {key: year+'_10', value: 'Жовтень'},
                    {key: year+'_11', value: 'Листопад'},
                    {key: year+'_12', value: 'Грудень'}
                ];

                // only for last year
                if (status == 'zero') {
                    groupMonths = groupMonths.slice(startMonth-1, currentMonth-1);
                }
                // only one year
                else if (status == 'one') {
                    groupMonths = groupMonths.slice(month-1);
                }
                // only one year
                else if (status == 'first') {
                    groupMonths = groupMonths.slice(month-1,13);
                }
                // only one year
                else if (status == 'last') {
                    groupMonths = groupMonths.slice(0, month);
                }
                // More then one year
                else {
                    groupMonths = groupMonths.slice(0);
                }

                groupMonths.unshift({key: year, value: 'Весь рік'});
                // groupMonths.unshift({key: year, value: 'Весь '+ year +' рік'});

                groupYear = {
                    name: year,
                    groups: groupMonths
                };

                return groupYear;
            }

            if (currentYear - startYear == 0) {
                dateOptions.push(generateGroupForYear(startDate, 'zero', +startMonth, +currentMonth+1));
            } else if (currentYear - startYear == 1) {
                dateOptions.push(generateGroupForYear(startDate, 'one'));
                dateOptions.push(generateGroupForYear(endDate, 'last'));
            } else if (currentYear - startYear > 1) {
                dateOptions.push(generateGroupForYear(startDate, 'first'));
                while (currentYear - startYear > 1) {
                    startYear++;
                    var tempDate = startYear+ '_00';

                    var elem = generateGroupForYear(tempDate, false);
                    dateOptions.push(elem);
                }

                dateOptions.push(generateGroupForYear(endDate, 'last'));
            }


            return dateOptions;

        }
    }

    MainHeaderService.$inject = [];
    function MainHeaderService()
    {
        this.set = function (value, object)
        {
            item.value = value;
            item.object = object || {};

        };

        this.getLabel = function ()
        {
            return item;
        };

        this.clear = function ()
        {
            item = this.init();
        };

        this.init = function ()
        {
            return {
                value: '',
                object: {}
            };
        };

        var item = this.init();
    }

    /**
     * File Uploader
     * @type {string[]}
     */
    MyFileUploader.$inject = ['$state', 'FileUploader', 'Notify', 'cfpLoadingBar'];
    function MyFileUploader($state, FileUploader, Notify, cfpLoadingBar)
    {
        this.init = function (autoUploadValue, queueLimit, reload, events, formData)
        {
            var uploader = new FileUploader({
                headers: {
                    'Authorization': 'Bearer ' + (localStorage.getItem('satellizer_token') || 'undefined'),
                    'Accept': 'application/json',
                    // 'X-CSRF-TOKEN': CSRF_TOKEN
                },
                autoUpload: autoUploadValue,
                queueLimit: queueLimit || 10,
                formData: formData
            });

            uploader.onSuccessItem = function ()
            {
                if (reload) {
                    $state.go($state.current, {}, {reload: true, location:false})
                }
            };
            uploader.onErrorItem = function (error, response, status)
            {
                if (response && response.errors && response.errors.file && response.errors.file[0])
                {
                    Notify.warning(response.errors.file[0]);
                } else if (response && response.message)
                {
                    Notify.warning(response.message);
                }
            };
            uploader.onProgressItem = function (fileItem, progress) {
                cfpLoadingBar.start();

                if (events.hasOwnProperty('onProgressItem')) {
                    events['onProgressItem'](fileItem, progress);
                }
            };
            uploader.onCompleteItem = function (fileItem, response, status, headers) {
                cfpLoadingBar.complete();

                if (events.hasOwnProperty('onCompleteItem')) {
                    events['onCompleteItem'](fileItem, response, status, headers);
                }
            };

            for (var event in events) {
                if (event == 'onProgressItem' || event == 'onCompleteItem') {
                    break;
                }

                uploader[event] = events[event];
            }

            return uploader;
        };
    }

    OverlayService.$inject = ['$compile', '$rootScope'];
    function OverlayService($compile, $rootScope)
    {

        var TEMPLATE_STRING = '<div class="overlay overlay-simplegenie" ng-class="show ? \'open\' : \'hide\'"><button type="button" class="overlay-close" ng-click="hide()"></button><div ng-include src="url"></div></div>',
            element = angular.element(TEMPLATE_STRING),
            scope = $rootScope.$new();

        scope.show = false;

        // create layer if dom is ready
        angular.element(document).ready(function ()
        {
            element = $compile(element)(scope);
            angular.element('.page-content-wrapper').append(element);
        });

        scope.hide = function ()
        {
            angular.element('.l-body').removeClass('l-body-overlay');
            scope.show = false;
        };

        // show function --> allows to overwrite defaults per show
        this.show = function (url, obj)
        {
            scope.url = url;

            scope.data = obj;

            angular.element('.l-body').addClass('l-body-overlay');
            angular.element('.l-body').removeClass('menu-pin sidebar-visible');
            scope.show = true;
        };

        this.hideAll = function ()
        {
            // reactivate body scrolling
            angular.element('.l-body').removeClass('l-body-overlay');
            scope.show = false;
        };
    }

    /* Fn
     ============================================================================================================= */
    FileService.$inject = ['CONFIG'];
    function FileService(CONFIG)
    {
        var cfg = CONFIG.files;
        this.src = function (type, token, size, ext)
        {
            ext = ext || false;
            type = parseInt(type);
            if (typeof token !== 'undefined' && token)
            {
                return CONFIG.host + ['', cfg.assets_dir, cfg.dir[type], token, size + '.' + (ext ? ext : (cfg.sizes[type][size].format || 'jpg') )].join('/');
            } else
            {
                if (cfg.rules[type].type == '1')
                {
                    return CONFIG.host + '/images/stubs/' + cfg.dir[type] + '/' + size + '.jpg';
                }
                else
                {
                    return CONFIG.host + '/images/stubs/' + cfg.dir[type] + '/' + size + '.jpg';
                }
            }
        }
    }

    HttpService.$inject = ['$http', '$rootScope', 'CONFIG', 'AuthDataService'];
    function HttpService($http, $rootScope, CONFIG, AuthDataService)
    {
        var baseUrl = CONFIG.host;

        var currentFormName = 'form';

        return {
            get: getFn,
            getWithChange: getFnWithChange,
            getWParams: getFnWParams,
            post: postFn,
            put: putFn,
            delete: deleteFn,
            setCurrentFormName: setCurrentFormName,
            getCurrentFormName: getCurrentFormName
        };

        function getCurrentFormName()
        {
            return currentFormName;
        }

        function setCurrentFormName(formName)
        {
            currentFormName = formName;
        }

        /* --- Functions --- */
        function config(options)
        {
            var config = {
                withCredentials: true,
                headers: {
                    'Authorization': AuthDataService.getToken(),
                    'Content-type': 'application/json'
                }
            };

            if (options && options.headers) {
                _.extend(config.headers, options.headers);
            }

            if (options && options.responseType) {
                config.responseType = options.responseType;
            }

            return config;
        }

        function successCallback(resp, callback)
        {
            if (callback)
            {
                return callback(resp);
            } else
            {
                return resp;
            }
        }

        function errorCallback(resp, callback)
        {
            if (callback)
            {
                return callback(resp);
            } else
            {
                return resp;
            }
        }

        function getFn(url, callback, errorCb)
        {
            url = baseUrl + url;
            return $http.get(url, config())
                .success(function (resp)
                {
                    if (callback) callback(resp);
                })
                .error(function (resp)
                {
                    if (errorCb) errorCallback(resp, errorCb)
                });
        }

        function getFnWithChange(url, callback, errorCb)
        {

        }

        function getFnWParams(url, params, callback, errorCb)
        {
            params = params || {};
            _.extend(params, config());
            url = baseUrl + url;
            return $http.get(url, params)
                .success(function (resp)
                {
                    successCallback(resp, callback);
                })
                .error(function (resp)
                {
                    errorCallback(resp, errorCb);
                });
        }

        function postFn(url, data, callback, errorCb, options)
        {
            $rootScope.$emit('form:submitted');

            url = baseUrl + url;
            return $http.post(url, data, config(options))
                .success(function (resp)
                {
                    $rootScope.$emit('form:success');
                    if (callback) successCallback(resp, callback);
                })
                .error(function (resp)
                {
                    $rootScope.$emit('form:error', resp);
                    if (errorCb) errorCallback(resp, errorCb);
                });
        }

        function putFn(url, data, callback, errorCb)
        {
            $rootScope.$emit('form:submitted');

            url = baseUrl + url;
            return $http.put(url, data, config())
                .success(function (resp)
                {
                    $rootScope.$emit('form:success');
                    successCallback(resp, callback);
                })
                .error(function (resp)
                {
                    $rootScope.$emit('form:error', resp);
                    if (errorCb) errorCallback(resp, errorCb);
                });
        }

        function deleteFn(url, callback, errorCb)
        {
            $rootScope.$emit('form:submitted');

            url = baseUrl + url;
            return $http.delete(url, config())
                .success(function (resp)
                {
                    $rootScope.$emit('form:success');
                    successCallback(resp, callback);
                })
                .error(function (resp)
                {
                    $rootScope.$emit('form:error', resp);
                    if (errorCb) errorCallback(resp, errorCb);
                });
        }
    }

    ConfigService.$inject = ['CONFIG'];
    function ConfigService(CONFIG)
    {
        this.get = function ()
        {
            return CONFIG;
        }
    }
    
    BreadcrumbsService.$inject = [];
    function BreadcrumbsService()
    {
        this.push = function (label, state)
        {
            state = state || false;

            crumbs.push({
                state: state,
                label: label
            });
        };

        this.getItems = function ()
        {
            return crumbs;
        };

        this.clear = function ()
        {
            crumbs = this.init();
        };

        this.init = function ()
        {
            return [{
                state: 'home.main',
                label: 'home.title.main'
            }];
        };

        var crumbs = this.init();
    }

    /**
     * Service for meta title and meta description etc.
     * @type {string[]}
     */
    SeoService.$inject = ['$rootScope', 'BreadcrumbsService'];
    function SeoService($rootScope, BreadcrumbsService)
    {
        /**
         * Get title for html
         * @returns {string}
         */
        this.getTitle = function ()
        {
            return _.last(BreadcrumbsService.getItems()).label + ' - ' + 'sportshop';
        };
    }

})();
