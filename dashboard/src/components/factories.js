import Echo from 'laravel-echo';

module.exports = angular
    .module('app.factories', [])
    .factory('redirectWhenLoggedOut', redirectWhenLoggedOut)
    .factory('authInterceptor', authInterceptor)
    .factory('AuthDataService', AuthDataService)
    .factory('EchoService', EchoService);


/**
 * Authorization jwt interceptor
 * @type {string[]}
 */
redirectWhenLoggedOut.$inject = ['$rootScope', '$q', '$timeout', '$location'];
function redirectWhenLoggedOut($rootScope, $q, $timeout, $location) {
    return {
        // Intercept 401s and redirect you to login
        responseError: function (response) {
            if (response.status === 401) {
                // Remove any stale tokens
                $rootScope.currentUser = null;
                localStorage.removeItem('satellizer_token');
                $timeout(function () {
                    $location.path('/login');
                });
                return $q.reject(response);
            } else {
                return $q.reject(response);
            }

            return $q.reject(rejection);
        }
    }
}

authInterceptor.$inject = ['$rootScope', '$q', '$timeout', '$location'];
function authInterceptor($rootScope, $q, $timeout, $location) {
    return {
        response: function (response) {

            var freshJwt = response.headers()['authorization'];
            if (freshJwt) {
                // Refresh local token
                localStorage.setItem('satellizer_token', freshJwt);
            }

            if (typeof response.data == 'object') {
                // Redirect to page, if user was not authorized.
                if (response.data.authorization === false)
                {
                    $rootScope.currentUser = null;
                    localStorage.removeItem('satellizer_token');
                    $timeout(function () {
                        $location.path('/login');
                    });
                }
            }

            return response;
        },

        // Intercept 401s and redirect you to login
        responseError: function (response) {
            if (response.status === 401) {
                // Remove any stale tokens
                $rootScope.currentUser = null;
                localStorage.removeItem('satellizer_token');
                $timeout(function () {
                    $location.path('/login');
                });
                return $q.reject(response);
            } else {
                return $q.reject(response);
            }
        }
    };
}

/**
 * AuthData Service
 * @type {string[]}
 */
AuthDataService.$inject = ['$rootScope'];
function AuthDataService($rootScope) {
    return {
        setAuthData: function (user) {
            $rootScope.currentUser = user;
        },
        clearAuthData: function () {
            $rootScope.currentUser = null;
            localStorage.removeItem('satellizer_token');
            localStorage.removeItem('user');
            localStorage.removeItem('active_store');
            localStorage.removeItem('all_stores');
        },
        getToken: function () {
            return 'Bearer '+ localStorage.getItem('satellizer_token');
        }
    };
}

/**
 * Laravel Echo Service for Chat Messenger
 */
EchoService.$inject = ['$rootScope', '$stateParams', '$window', '$state', '$timeout', 'AuthDataService', 'HttpService', 'BrandsService', 'SettingsService', 'Notify', 'CONFIG'];
function EchoService($rootScope, $stateParams, $window, $state, $timeout, AuthDataService, HttpService, BrandsService, SettingsService, Notify, CONFIG) {

    return {
        /**
         * Configure socket
         */
        socketConfigure: function () {
            var baseUrl = CONFIG.web_socket_host;

            $window.echo = new Echo({
                broadcaster: "socket.io",
                host: baseUrl,
                auth: {
                    headers: {
                        'Authorization': AuthDataService.getToken(),
                        'Origin': $window.location.origin
                    }
                }
            });
        },

        /**
         * Set new orders
         */
        setOrders: function() {
            HttpService.get('/orders/count?status=new')
                .success(function(response) {
                    $rootScope.orderCounter = response ? response : null;
                });
        },

        /**
         * Set data in rootScope
         */
        setDataInRootScope: function() {
            // Set brands
            BrandsService.getList()
                .success(function(response) {
                    $rootScope.globalBrands = response || [];
                });

            // Set categories flat list
            HttpService.get('/categories?flat=true')
                .success(function(response) {
                    $rootScope.globalCategoriesFlatList = response || [];
                });

            // Set colors
            SettingsService.getColorsList()
                .success(function(response) {
                    $rootScope.globalColors = response || [];
                });

            // Set currencies from dashboard.js
            HttpService.get('/currencies')
                .success(function(response) {
                    $rootScope.globalCurrencies = response || [];
                });

            // Set discounts
            HttpService.get('/discounts')
                .success(function(response) {
                    $rootScope.globalDiscounts = response || [];
                });

            // Set settings
            HttpService.get('/settings')
                .success(function(response) {
                    $rootScope.globalSettings = response || [];
                });

            // Set sizes
            HttpService.get('/sizes')
                .success(function(response) {
                    $rootScope.globalSizes = response || [];
                });

            // Set attributes
            HttpService.get('/attributes')
                .success(function(response) {
                    $rootScope.globalAttributes = response || [];
                });
        },

        /**
         * Connect to channel
         */
        connectToChannel: function() {
            $timeout(function () {
                $window.echo
                    .private('orders')
                    .listen('.updated', function (data) {
                        $timeout(function () {
                            $rootScope.orderCounter = data && data['orders-number'] ? data['orders-number'] : null;
                        }, 0);

                        if ($rootScope.state.current.name == 'orders.list') {
                            $state.go('orders.list', {}, { notify: true, reload: true });
                        }

                        if (data.order_id && data.manager_id && $rootScope.state.current.name == 'orders.edit'
                            && $stateParams.id == data.order_id && $rootScope.currentUser.user.id != data.manager_id) {

                            Notify.warning('Замовлення №'+ data.order_id +' оновилося!');

                            $timeout(function () {
                                $state.go('orders.list', {}, { notify: true, reload: true });
                            }, 2000);

                        }
                    });
            });
        },

        /**
         * Entity Update Channel
         */
        entityUpdateChannel: function() {
            /**
             * Update data in RootScope
             *
             * @param type
             */
            function updateDataInRootScope(type) {
                switch (type) {
                    case 'brand':
                        BrandsService.getList()
                            .success(function(response) {
                                $rootScope.globalBrands = response || [];
                            });
                        break;
                    case 'category':
                        HttpService.get('/categories?flat=true')
                            .success(function(response) {
                                $rootScope.globalCategoriesFlatList = response || [];
                            });
                        break;
                    case 'colors':
                        SettingsService.getColorsList()
                            .success(function(response) {
                                $rootScope.globalColors = response || [];
                            });
                        break;
                    case 'currencies':
                        HttpService.get('/currencies')
                            .success(function(response) {
                                $rootScope.globalCurrencies = response || [];
                            });
                        break;
                    case 'discounts':
                        HttpService.get('/discounts')
                            .success(function(response) {
                                $rootScope.globalDiscounts = response || [];
                            });
                        break;
                    case 'settings':
                        HttpService.get('/settings')
                            .success(function(response) {
                                $rootScope.globalSettings = response || [];
                            });
                        break;
                    case 'store':
                        HttpService.get('/stores')
                            .success(function(response) {
                                $rootScope.globalStores = response || [];

                                $rootScope.storesList =JSON.parse(JSON.stringify(response)) || [];
                            });
                        break;
                    case 'sizes':
                        HttpService.get('/sizes')
                            .success(function(response) {
                                $rootScope.globalSizes = response || [];
                            });
                        break;
                    case 'attributes':
                        HttpService.get('/attributes')
                            .success(function(response) {
                                $rootScope.globalAttributes = response || [];
                            });
                        break;
                }
            }

            $window.echo
                .private('hot-reload')
                .listen('.entity.updated', function (data) {
                    if (data.entity_type) {
                        updateDataInRootScope(data.entity_type)
                    }
                });
        }
    };
}