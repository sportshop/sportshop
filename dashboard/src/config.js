export default {
    "host": process.env.API_URL,
    "web_socket_host": process.env.WEB_SOCKET_URL,
    "shop_name": process.env.SHOP_NAME,
    "theme_name": process.env.THEME_NAME,
    "start_date": process.env.START_DATE
}