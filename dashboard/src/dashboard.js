/**
 * Config
 */
import config from './config';


/**
 * Initializing
 */
angular.module('app.templates', []);
angular.module('app', [
    'ui.router',
    'ui.bootstrap',
    'ui.bootstrap.datetimepicker',
    'satellizer',
    'ngSanitize',
    'ngDialog',
    'ngAnimate',
    'angularFileUpload',
    'LocalStorageModule',
    'angular-loading-bar',
    'pascalprecht.translate',
    'ui.select',
    'uiCropper',
    'angular-redactor',
    'angucomplete-alt',
    'sm-daterangepicker',
    'daterangepicker',
    'ngMask',

    // tpl
    'app.templates',

    // Components
    'app.factories',
    'app.filters',
    'app.services',
    'app.utils',
    'app.notify',

    // Directives
    'app.directives_main',
    'app.layout_partials',

    // Applications
    'app.auth',
    'app.home',
    'app.pages',
    'app.banners',
    'app.form_builder',
    'app.settings',
    'app.attributes',
    'app.brands',
    'app.users',
    'app.stores',
    'app.categories',
    'app.products',
    'app.tables',
    'app.profile',
    'app.costs',
    'app.orders',
    'app.salaries',
    'app.statistics',
    'app.errors'
])
    .config(configure)
    .constant("CSRF_TOKEN", csrftoken())
    .constant("CONFIG", config)
    .run(runBlock);


function configure($locationProvider, $urlRouterProvider, $authProvider, $httpProvider, $translateProvider, $animateProvider, cfpLoadingBarProvider, redactorOptions, CSRF_TOKEN, CONFIG) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: true
    });

    // Push the new factory onto the $http interceptor array
    $httpProvider.interceptors.push('redirectWhenLoggedOut');

    // Satellizer configuration that specifies which API
    // route the JWT should be retrieved from

    $authProvider.loginUrl = CONFIG.host + '/auth/login';

    // Loading bar
    cfpLoadingBarProvider.includeSpinner = false;
    cfpLoadingBarProvider.latencyThreshold = 0;
    cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
    cfpLoadingBarProvider.loadingBarTemplate = '<div class="loading">Loading&#8230;</div>';

    $animateProvider.classNameFilter(/nganimation/);

    $urlRouterProvider.otherwise("/auth/login");

    $translateProvider.useStaticFilesLoader({
        prefix: '/languages/',
        suffix: '.json',
    });

    $translateProvider.preferredLanguage('ua_UA');
    $translateProvider.useSanitizeValueStrategy('sanitizeParameters');

    // If has slash in the end redirect to url without slash
    $urlRouterProvider.rule(function($injector, $location) {
        var path = $location.path();
        var hasTrailingSlash = path[path.length-1] === '/';

        if(hasTrailingSlash) {
            // If last character is a slash, return the same url without the slash
            var newPath = path.substr(0, path.length - 1);
            return newPath;
        }
    });

    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.headers.common['X-CSRF-TOKEN'] = CSRF_TOKEN;

    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    // Imperavi redactor options
    redactorOptions.minHeight = 250;
    redactorOptions.maxHeight = 400;

    if (CONFIG.theme_name == 'green') {
        var body = document.body;
        body.classList.add("skin-green");
    }
}

function runBlock($rootScope, $location, $http, $state, $timeout, BreadcrumbsService, MainHeaderService, SeoService, ngDialog, AuthDataService, EchoService, HttpService, ProfileService, CONFIG) {
    // Grab the user from local storage and parse it to an object
    var user = JSON.parse(localStorage.getItem('user'));

    $rootScope.shopName = CONFIG.shop_name;
    $rootScope.themeName = CONFIG.theme_name || 'blue';

    if (!user) {
        AuthDataService.clearAuthData();
    }

    HttpService.get('/currencies')
        .success(function(resp) {
            $rootScope.staticCurrencies = resp || [];
            $rootScope.globalCurrencies = resp || [];

            // Set default currency
            var defaultCurrency;
            if ($rootScope.staticCurrencies && $rootScope.staticCurrencies.length) {
                defaultCurrency = _.find($rootScope.staticCurrencies, function(el) {
                    return el.is_default;
                });

                $rootScope.defaultCurrency = defaultCurrency;
                $rootScope.defaultCurrencyId = defaultCurrency.id;
            }
        });

    // If there is any user data in local storage then the user is quite
    // likely authenticated. If their token is expired, or if they are
    // otherwise not actually authenticated, they will be redirected to
    // the auth state because of the rejected request anyway
    if (user) {
        // The user's authenticated state gets flipped to
        // true so we can now show parts of the UI that rely
        // on the user being logged in
        $rootScope.authenticated = true;

        ProfileService.get()
            .success(function(resp) {
                if (resp && resp.user && resp.user.permissions) {
                    // Putting the user's data on $rootScope allows
                    // us to access it anywhere across the app. Here
                    // we are grabbing what is in local storage
                    $rootScope.currentUser = user;
                    if ($rootScope.currentUser.user && $rootScope.currentUser.user.permissions) {
                        $rootScope.currentUser.user.permissions = resp.user.permissions;
                        $rootScope.perms = {};
                        $rootScope.currentUser.user.permissions.forEach(function (elem) {
                            $rootScope.perms[elem] = true;
                        });
                    }

                    localStorage.setItem('user', JSON.stringify($rootScope.currentUser));

                    // Socket.io configuration
                    EchoService.socketConfigure();

                    // Set unread messages and counter
                    EchoService.setOrders();

                    // Set data in rootScope
                    EchoService.setDataInRootScope();

                    // Connect and listen channel
                    EchoService.connectToChannel();

                    // Connect and listen channel
                    EchoService.entityUpdateChannel();
                }
            });
    } else {
        AuthDataService.clearAuthData();
        // Redirect to login
        var reg = '^/auth.*';
        if (!$location.path().match(reg)) {
            $location.path('/auth/login');
        }
    }

    // State events
    $rootScope.$on('$stateChangeStart', function (event, state) {
        BreadcrumbsService.clear();
        MainHeaderService.clear();
        
        $rootScope.isCollapseSidebar = false;
        $rootScope.state = $state;
        ngDialog.closeAll();
        
        // Sidebar
        angular.element('body').removeClass('sidebar-open');
    });
    
    $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {});

    $rootScope.$on('$stateChangeSuccess', function (event, to, toParams, from, fromParams) {
        $rootScope.breadcrumbs = BreadcrumbsService.getItems();
        $timeout(function() {
            $rootScope.mainHeader = MainHeaderService.getLabel();
        }, 1);
        
        $(window).scrollTop(0);
    });
    
    // For SEO
    $rootScope.getSeoTitle = SeoService.getTitle;
    $rootScope.getSeoDescription = SeoService.getDescription;
    
    $rootScope.state = $state;
    $rootScope.location = $location;
    
    // Init Underscore in template
    $rootScope._ = _;
    
    // Back button
    $rootScope.$back = function () {
        if (window.history.length > 2) {
            window.history.back();
        } else {
            $state.transitionTo('home.main');
        }
    };
    
    $rootScope.isEmptyObject = function (object) {
        return !object || _.isEmpty(object);
    };
    
    // Check if dashboard
    $rootScope.isDashboard = function () {
        return $location.path().indexOf('dashboard') !== -1;
    };
}

function csrftoken() {
    var metas = window.document.getElementsByTagName('meta');
    
    // Finding one has csrf token
    for(var i=0 ; i < metas.length ; i++) {
        if ( metas[i].name === "csrf-token") {
            return  metas[i].content;
        }
    }
}