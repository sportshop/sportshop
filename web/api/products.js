export default {
    findAll(ctx, params) {
        return ctx.$axios.get('/products', { params });
    },
    findOne(ctx, { category_slug, slug, ...params }) {
        return ctx.$axios.get(`/products/${category_slug}/${slug}`, { params });
    },
    requestFeedback(ctx, productId, data) {
        return ctx.$axios.post(`/products/${productId}/feedback`, data);
    },
    requestCall(ctx, productId, data) {
        return ctx.$axios.post(`/products/${productId}/call`, data);
    },
    getFilterValues(ctx, params) {
        return ctx.$axios.get('/products/filter', { params });
    }
};
