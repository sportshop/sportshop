export default {
    findAll(ctx, params) {
        return ctx.$axios.get('/sizes', { params });
    },
    findById(ctx, id) {
        return ctx.$axios.get(`/sizes/${id}`);
    }
};
