export default {
    findAll(ctx, params) {
        return ctx.$axios.get('/users', { params });
    }
};
