export default {
    getPageBySlug(ctx, slug) {
        return ctx.$axios.get(`/pages/${slug}`);
    }
};
