export default {
    loginByProvider(ctx, provider, data) {
        return ctx.$axios.post(`/auth/login/${provider}`, data);
    },
    register(ctx, data) {
        return ctx.$axios.post('/auth/register', data);
    },
    passwordSend(ctx, data) {
        return ctx.$axios.post('/auth/password/send', data);
    },
    passwordReset(ctx, data) {
        return ctx.$axios.post('/auth/password/reset', data);
    }
};
