export default {
    update(ctx, data) {
        return ctx.$axios.put('/profile', data);
    },
    updatePassword(ctx, data) {
        return ctx.$axios.put('/profile/update-password', data);
    }
};
