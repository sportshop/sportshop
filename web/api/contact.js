export default {
    send(ctx, data) {
        return ctx.$axios.post('/feedback', data);
    }
};
