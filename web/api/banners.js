export default {
    findAll(ctx) {
        const params = { active: true };

        return ctx.$axios.get('/banners', { params });
    }
};
