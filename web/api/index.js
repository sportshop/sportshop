import auth from './auth';
import banners from './banners';
import categories from './categories';
import config from './config';
import contact from './contact';
import currencies from './currencies';
import discounts from './discounts';
import orders from './orders';
import pages from './pages';
import products from './products';
import profile from './profile';
import sizes from './sizes';
import users from './users';

export default {
    auth,
    banners,
    categories,
    config,
    contact,
    currencies,
    discounts,
    orders,
    pages,
    products,
    profile,
    sizes,
    users
};
