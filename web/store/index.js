import Vuex from 'vuex';
import modules from './modules';

const createStore = () => {
    return new Vuex.Store({
        actions: {
            nuxtClientInit({ dispatch }) {
                return Promise.all([

                    // Retrieve products for cart.
                    dispatch('cart/fetchProducts')
                ]);
            },

            // Fetch data from api before rendering.
            nuxtServerInit({ dispatch }) {
                return Promise.all([
                    dispatch('categories/fetchAll'),
                    dispatch('config/fetchAll'),
                    dispatch('currencies/fetchAll'),
                    dispatch('discounts/fetchAll')
                ]);
            }
        },
        modules
    })
};

export default createStore;
