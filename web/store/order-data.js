export const orderStatuses = [
    { key: 'new', value: 'Новий', class: 'primary' },
    { key: 'accepted', value: 'Прийнятий', class: 'primary' },
    { key: 'sent', value: 'Відправлено', class: 'primary' },
    { key: 'completed', value: 'Завершено', class: 'success' },
    { key: 'canceled', value: 'Відмінено', class: 'danger' },
    { key: 'refunded', value: 'Повернений', class: 'danger' },
    { key: 'partially_refunded', value: 'Частково повернений', class: 'warning' }
];

export const paymentStatuses = [
    { key: 'paid', value: 'Оплачено', icon: 'icon-sport_shop_icons-11' },
    { key: 'not_paid', value: 'Неоплачено', icon: 'icon-sport_shop_icons-28' },
    { key: 'partially_paid', value: 'Частково оплачено', icon: 'icon-sport_shop_icons-42' }
];

export const paymentMethods = [
    { key: 'cod', value: 'Наложений платіж' },
    { key: 'card', value: 'Оплата карткою' },
    { key: 'cash', value: 'Оплата готівкою' }
];

export const deliveryMethods = [
    { key: 'new_post', value: 'Нова пошта', iconClass: 'icon-nova_poshta_logo', description: 'Нова пошта' },
    { key: 'courier', value: 'Курєром', iconClass: 'icon-sport_shop_icons-50', description: 'Доставка кур\'єром (по Івано-Франківську)' },
    { key: 'pickup', value: 'Самовивіз', iconClass: 'icon-sport_shop_icons-52', description: 'Самовивіз (м. Івано-Франківськ, вул. Миколи Грінченка, 58, прим. 212)' }
];

export default {
    orderStatuses,
    paymentStatuses,
    paymentMethods,
    deliveryMethods
};
