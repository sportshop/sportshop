import api from '@/api';
import * as types from '../mutation-types';

const orderStatuses = [
    { key: null, value: 'Всі' },
    { key: 'new', value: 'Новий', class: 'primary' },
    { key: 'accepted', value: 'Прийнятий', class: 'primary' },
    { key: 'sent', value: 'Відправлено', class: 'primary' },
    { key: 'completed', value: 'Завершено', class: 'success' },
    { key: 'canceled', value: 'Відмінено', class: 'danger' },
    { key: 'refunded', value: 'Повернений', class: 'danger' },
    { key: 'partially_refunded', value: 'Частково повернений', class: 'warning' }
];
const paymentStatuses = [
    { key: null, value: 'Всі' },
    { key: 'paid', value: 'Оплачено', class: 'success' },
    { key: 'not_paid', value: 'Неоплачено', class: 'warning' },
    { key: 'partially_paid', value: 'Частково оплачено', class: 'danger' }
];
const paymentMethods = [
    { key: 'cod', value: 'Наложений платіж' },
    { key: 'card', value: 'Оплата карткою' },
    { key: 'cash', value: 'Оплата готівкою' }
];
const deliveryMethods = [
    { key: null, value: 'Всі' },
    { key: 'courier', value: 'Курєром' },
    { key: 'pickup', value: 'Самовивіз' },
    { key: 'new_post', value: 'Нова пошта' }
];
const perPageValues = [10, 25, 50];

const defaultQuery = {
    product: null,
    orderStatus: orderStatuses[0],
    paymentStatus: paymentStatuses[0],
    deliveryMethod: deliveryMethods[0],
    date: null,
    perPage: perPageValues[0],
    page: 1
};

const state = {
    orders: [],
    orderStatuses,
    paymentStatuses,
    paymentMethods,
    deliveryMethods,
    perPageValues,
    query: defaultQuery
};

const getters = {
    transformedQuery(state) {
        return {
            product: state.query.product,
            status: state.query.orderStatus.key,
            payment_status: state.query.paymentStatus.key,
            delivery_method: state.query.deliveryMethod.key,
            created_at: state.query.date,
            limit: state.query.perPage,
            page: state.query.page
        };
    }
};

const actions = {

    /**
     * Find all orders.
     *
     * @param commit
     * @param getters
     * @returns {Promise<void>}
     */
    async fetchAll({ commit, getters }) {

        // Start loading component.
        if (process.client) {
            this._vm.$nuxt.$loading.start();
        }

        const { data } = await api.orders.findAll(this, getters.transformedQuery);

        commit(types.SET_ORDERS, data.orders);

        // Finish loading component.
        if (process.client) {
            this._vm.$nuxt.$loading.finish();
        }
    },

    /**
     * Set query to store.
     *
     * @param commit
     * @param dispatch
     * @param values
     * @returns {Promise<void>}
     */
    async setQuery({ commit, dispatch }, values) {
        commit(types.SET_QUERY, values);

        return await dispatch('fetchAll');
    },

    /**
     * Clear query.
     *
     * @param commit
     */
    clearQuery({ commit }) {
        commit(types.CLEAR_QUERY);
    }
};

const mutations = {
    [types.SET_ORDERS](state, orders) {
        state.orders = orders;
    },
    [types.SET_QUERY](state, values) {
        state.query = { ...state.query, ...values };
    },
    [types.CLEAR_QUERY](state) {
        state.query = defaultQuery;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
