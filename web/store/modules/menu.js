import * as types from '../mutation-types';

const state = {
    isOpened: false
};

const actions = {
    toggle({ commit }) {
        commit(types.MENU_TOGGLE);
    },
    close({ commit }) {
        commit(types.MENU_CLOSE);
    }
};

const mutations = {
    [types.MENU_TOGGLE](state) {
        state.isOpened = !state.isOpened;
    },
    [types.MENU_CLOSE](state) {
        state.isOpened = false;
    }
};

export default {
    namespaced: true,
    state,
    actions,
    mutations
};
