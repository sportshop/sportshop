import api from '@/api';
import * as types from '../mutation-types';

const state = {
    all: []
};

const actions = {

    /**
     * Fetch all categories from api.
     *
     * @param commit
     * @returns {Promise.<void>}
     */
    async fetchAll({ commit }) {
        const params = {
            products_exists: true
        };

        const { data } = await api.categories.findAll(this, params);

        commit(types.SET_CATEGORIES, data);
    }
};

const mutations = {
    [types.SET_CATEGORIES](state, categories) {
        state.all = categories;
    }
};

export default {
    namespaced: true,
    state,
    actions,
    mutations
};
