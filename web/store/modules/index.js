import breadcrumbs from './breadcrumbs';
import cart from './cart';
import categories from './categories';
import checkout from './checkout';
import config from './config';
import contactForm from './contact-form';
import discounts from './discounts';
import currencies from './currencies';
import featureProducts from './feature-products';
import menu from './menu';
import modals from './modals';
import orders from './orders';
import products from './products';
import product from './product';
import uniforms from './uniforms';

export default {
    breadcrumbs,
    cart,
    categories,
    checkout,
    config,
    contactForm,
    currencies,
    discounts,
    featureProducts,
    menu,
    modals,
    orders,
    products,
    product,
    uniforms
}