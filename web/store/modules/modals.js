import * as types from '../mutation-types';

const state = {
    all: {}
};

const actions = {

    /**
     * Show modal.
     *
     * @param commit
     * @param type
     */
    show({ commit }, type) {
        commit(types.SHOW, type);
    },

    /**
     * Hide modal.
     *
     * @param commit
     * @param type
     */
    hide({ commit }, type) {
        commit(types.HIDE, type);
    }
};

const mutations = {
    [types.SHOW](state, type) {
        state.all = { ...state.all, [type]: { show: true } };
    },
    [types.HIDE](state, type) {
        state.all = { ...state.all, [type]: { show: false } };
    }
};

export default {
    namespaced: true,
    state,
    actions,
    mutations
};
