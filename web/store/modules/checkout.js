import types from '../mutation-types';
import {
    orderStatuses,
    paymentStatuses,
    paymentMethods,
    deliveryMethods
} from '../order-data';

// Defined default data structure.
const customerModel = {
    first_name: null,
    last_name: null,
    phone: null,
    email: null
};
const paymentModel = {
    status: null,
    method: 'cod',
    amount: null
};
const deliveryModel = {
    method: null,
    city: null,
    department: null,
    address: null
};

const state = {
    orderStatuses,
    paymentStatuses,
    paymentMethods,
    deliveryMethods,
    isWebStore: false,
    customer: customerModel,
    payment: paymentModel,
    delivery: deliveryModel,
    comment: null,
    errors: {}
};

const getters = {

    isWebStore: state => state.isWebStore,

    /**
     * Collect request data
     *
     * @param state
     * @param getters
     * @param rootState
     * @param rootGetters
     */
    formData: (state, getters, rootState, rootGetters) => {
        const currencyId = rootGetters['currencies/current'].id;
        const totalPrice = rootGetters['cart/priceWithDiscount'];
        const products = rootGetters['cart/products'];
        const discount = rootGetters['cart/discount'];
        const discountId = rootGetters['discounts/current'].id;

        const formData = {
            customer: { ...state.customer, discount_id: discountId },
            payment: {
                method: state.payment.method
            },
            is_from_web_store: state.isWebStore,
            currency_id: currencyId,
            products: []
        };

        // Collect delivery
        if (state.isWebStore) {
            formData.delivery = state.delivery;
        }

        // Collect full payment
        if (state.payment.status == 'paid') {
            formData.payments = [{
                rate: {
                    currency_id: currencyId,
                    value: totalPrice.value
                }
            }];
        }
        // Collect partially payment
        else if (state.payment.status == 'partially_paid' && state.payment.amount) {
            formData.payments = [{
                rate: {
                    currency_id: currencyId,
                    value: state.payment.amount
                }
            }];
        }

        // Collect products
        products.forEach(product => {
            product.sizes.items.forEach(size => {
                if (size.selected_quantity) {
                    formData.products.push({
                        size_id: size.id,
                        quantity: size.selected_quantity,
                        discount: discount
                    });
                }
            });
        });

        return formData;
    },
    errors: state => state.errors
};

const actions = {

    /**
     * Set "webStore" attribute value.
     *
     * @param commit
     * @param value
     */
    setWebStore({ commit }, value) {
        commit(types.SET_WEB_STORE_VALUE, value);
        commit(types.SET_ERRORS, {});
    },

    /**
     * Set customer data.
     *
     * @param commit
     * @param data
     */
    setCustomer({ commit }, data) {
        commit(types.SET_CUSTOMER_DATA, data);
    },

    /**
     * Set payment method.
     *
     * @param commit
     * @param value
     */
    setPaymentMethod({ commit }, value) {
        commit(types.SET_PAYMENT_DATA, { method: value });
    },

    /**
     * Set payment status.
     *
     * @param commit
     * @param value
     */
    setPaymentStatus({ commit }, value) {
        commit(types.SET_PAYMENT_DATA, { status: value });
    },

    /**
     * Set payment amount.
     *
     * @param commit
     * @param value
     */
    setPaymentAmount({ commit }, value) {
        commit(types.SET_PAYMENT_DATA, { amount: value });
    },

    /**
     * Set delivery method.
     *
     * @param commit
     * @param value
     */
    setDeliveryMethod({ commit }, value) {
        commit(types.SET_DELIVERY_DATA, { method: value });
    },

    /**
     * Set delivery city.
     *
     * @param commit
     * @param value
     */
    setDeliveryCity({ commit }, value) {
        commit(types.SET_DELIVERY_DATA, { city: value });
    },

    /**
     * Set delivery department.
     *
     * @param commit
     * @param value
     */
    setDeliveryDepartment({ commit }, value) {
        commit(types.SET_DELIVERY_DATA, { department: value });
    },

    /**
     * Set delivery address.
     *
     * @param commit
     * @param value
     */
    setDeliveryAddress({ commit }, value) {
        commit(types.SET_DELIVERY_DATA, { address: value });
    },

    /**
     * Set comment.
     *
     * @param commit
     * @param value
     */
    setComment({ commit }, value) {
        commit(types.SET_COMMENT, value);
    },

    /**
     * Set errors.
     *
     * @param commit
     * @param data
     */
    setErrors({ commit }, data) {
        commit(types.SET_ERRORS, data);
    },

    /**
     * Clear customer from state.
     *
     * @param commit
     */
    clearCustomer({ commit }) {
        commit(types.CLEAR_CUSTOMER);
    },

    /**
     * Clear all the collected data.
     *
     * @param commit
     */
    clearData({ commit }) {
        commit(types.CLEAR_CHECKOUT);
    }
};

const mutations = {
    [types.SET_WEB_STORE_VALUE](state, value) {
        state.isWebStore = value;

        if (value === true) {
            state.payment.method = 'cod';
            state.payment.status = 'not_paid';
        } else if (value === false) {
            state.payment.method = 'cash';
            state.payment.status = 'paid';
        }
    },
    [types.SET_CUSTOMER_DATA](state, data) {
        state.customer = { ...state.customer, ...data };
    },
    [types.SET_PAYMENT_DATA](state, value) {
        state.payment = { ...state.payment, ...value };
    },
    [types.SET_DELIVERY_DATA](state, value) {
        state.delivery = { ...state.delivery, ...value };
    },
    [types.SET_COMMENT](state, value) {
        state.comment = value;
    },
    [types.SET_ERRORS](state, data) {
        state.errors = data;
    },
    [types.CLEAR_CUSTOMER](state) {
        state.customer = {};
    },
    [types.CLEAR_CHECKOUT](state) {
        state.isWebStore = true;
        state.customer = customerModel;
        state.payment = paymentModel;
        state.delivery = deliveryModel;
        state.comment = null;
        state.errors = {};
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
