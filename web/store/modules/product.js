import api from '@/api';
import types from '../mutation-types';

const state = {
    product: {},
    relatedProducts: []
};

const actions = {

    /**
     * Find product and related products.
     *
     * @param commit
     * @param state
     * @param rootGetters
     * @param params
     * @returns {Promise<void>}
     */
    async fetch({ commit, state, rootGetters }, params) {

        // Start loading component.
        if (process.client) {
            this._vm.$nuxt.$loading.start();
        }

        params = {
            ...params,
            discount_id: rootGetters['discounts/current'].id
        };

        const responses = await Promise.all([
            api.products.findOne(this, params),
            api.products.findAll(this, { limit: 15, ...params })
        ]);

        const productResponse = responses[0].data;
        const productsResponse = responses[1].data.products.data;

        commit(types.SET_DATA, {
            product: productResponse,
            relatedProducts: productsResponse
        });

        // Finish loading component.
        if (process.client) {
            this._vm.$nuxt.$loading.finish();
        }
    }
};

const mutations = {
    [types.SET_DATA](state, { product, relatedProducts }) {
        state.product = product;
        state.relatedProducts = relatedProducts;
    }
};

export default {
    namespaced: true,
    state,
    actions,
    mutations
};
