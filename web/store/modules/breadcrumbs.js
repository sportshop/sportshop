import * as types from '../mutation-types';

const state = {
    items: []
};

const actions = {

    /**
     * Set links.
     *
     * @param commit
     * @param items
     * @returns {Promise.<void>}
     */
    async setItems({ commit }, items) {
        commit(types.SET_ITEMS, items);
    },

    /**
     * Clear breadcrumbs.
     *
     * @param commit
     * @returns {Promise.<void>}
     */
    async clear({ commit }) {
        commit(types.SET_ITEMS, []);
    }
};

const mutations = {
    [types.SET_ITEMS](state, items) {
        state.items = items;
    }
};

export default {
    namespaced: true,
    state,
    actions,
    mutations
};
