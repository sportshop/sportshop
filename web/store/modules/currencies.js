import _ from 'lodash';
import api from '@/api';
import types from '../mutation-types';

const state = {
    all: []
};

const getters = {

    /**
     * Get all currencies.
     *
     * @param state
     * @return Array
     */
    all: state => state.all,

    /**
     * Get default currency.
     *
     * @param state
     * @return Object
     */
    default: state => _.find(state.all, { is_default: true }),

    /**
     * Get current currency.
     * TODO: Currently used default currency. After implementing currency selector, should change logic.
     *
     * @param state
     * @return Object
     */
    current: state => _.find(state.all, { is_default: true })
};

const actions = {

    /**
     * Fetch all currencies from API.
     *
     * @param commit
     * @return Promise
     */
    async fetchAll({ commit }) {
        const { data } = await api.currencies.findAll(this);

        return commit(types.SET_CURRENCIES, data);
    }
};

const mutations = {
    [types.SET_CURRENCIES](state, currencies) {
        state.all = currencies;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
