import _ from 'lodash';
import api from '@/api';
import types from '../mutation-types';

const productTypes = [
    { key: 'novelty', value: 'Новинки' },
    { key: 'sale', value: 'Sale' },
    { key: 'max_sale', value: 'MaxSale' },
    { key: 'top_sale', value: 'Лідери продаж' }
];

const sortOrderValues = [
    { key: 'lowest_price', value: 'від дешевих до дорогих' },
    { key: 'highest_price', value: 'від дорогих до дешевих' },
    { key: 'top_sale', value: 'по популярності' },
    { key: 'novelty', value: 'новинки' }
];

const limitValues = [24, 16, 8];

const state = {
    products: {},
    types: productTypes,
    sortOrderValues,
    limitValues,
    query: {
        attributes: [],
        brands: [],
        categories: [],
        category_slug: null,
        colors: [],
        sizes: [],
        limit: null,
        page: 1,
        prices: {
            min: null,
            max: null
        },
        type: null,
        sortOrder: null,
        search: null
    },
    filter: {
        attributes: [],
        brands: [],
        categories: [],
        colors: [],
        sizes: [],
        prices: {
            min: null,
            max: null
        }
    },
    priceChanged: false
};

const getters = {
    products: state => state.products,
    sortOrderValues: state => state.sortOrderValues,
    sortOrder: state => state.query.sortOrder || state.sortOrderValues[0],
    limitValues: state => state.limitValues,
    limit: state => state.query.limit || state.limitValues[0],
    page: state => state.query.page,
    transformedQuery(state, getters, rootState, rootGetters) {

        // Transform attributes.
        let attributes = _.pickBy(state.query.attributes, (value, key) => (key ? value : null));
        attributes = _.keys(attributes).join();

        // Transform brands.
        let brands = _.pickBy(state.query.brands, (value, key) => (key ? value : null));
        brands = _.keys(brands).join();

        // Transform categories.
        let categories = _.pickBy(state.query.categories, (value, key) => (key ? value : null));
        categories = _.keys(categories).join();

        // Transform colors.
        let colors = _.pickBy(state.query.colors, (value, key) => (key ? value : null));
        colors = _.keys(colors).join();

        // Transform sizes.
        let sizes = _.pickBy(state.query.sizes, (value, key) => (key ? value : null));
        sizes = _.keys(sizes).join();

        return {
            attributes,
            brands,
            categories,
            category_slug: state.filter.category_slug,
            colors,
            sizes,
            currency_id: rootGetters['currencies/default'].id,
            discount_id: rootGetters['discounts/current'].id,
            limit: getters.limit,
            order_by: getters.sortOrder.key,
            page: getters.page,
            price_min: state.query.prices.min,
            price_max: state.query.prices.max,
            type: state.query.type,
            search: state.query.search
        };
    }
};

const actions = {

    /**
     * Find all products.
     *
     * @param commit
     * @param state
     * @param getters
     * @param dispatch
     * @param params
     * @returns {Promise<void>}
     */
    async fetchAll({ commit, state, getters, dispatch }, params) {
        if (params && params.category_slug != state.query.category_slug) {
            await dispatch('clearQuery');
        }

        params = { ...getters.transformedQuery, ...params };

        // Start loading component.
        if (process.client) {
            this._vm.$nuxt.$loading.start();
        }

        // Fetch data from API.
        const responses = await Promise.all([
            api.products.getFilterValues(this, params),
            api.products.findAll(this, params)
        ]);
        const filterResponse = responses[0].data;
        const productResponse = responses[1].data.products;

        // Build filter data.
        const filterData = {
            ...state.filter,
            categories: filterResponse.categories,
            category_slug: params.category_slug
        };

        if (!state.priceChanged) {
            filterData.prices = filterResponse.prices;
        }

        // Determine if current categories different between categories from response.
        const differenceCategories = _.differenceBy(filterResponse.categories, state.filter.categories, 'id');

        if (!filterResponse.categories.length || differenceCategories.length || params.search) {
            filterData.attributes = filterResponse.attributes;
            filterData.brands = filterResponse.brands;
            filterData.colors = filterResponse.colors;
            filterData.sizes = _.values(filterResponse.sizes);
            filterData.prices = filterResponse.prices;
        }

        commit(types.SET_PRODUCTS, {
            products: productResponse,
            filter: filterData,
            query: {
                page: params.page,
                type: params.type
            }
        });

        // Finish loading component.
        if (process.client) {
            this._vm.$nuxt.$loading.finish();
        }
    },

    /**
     * Set query to store.
     *
     * @param commit
     * @param dispatch
     * @param values
     * @returns {Promise<void>}
     */
    async setQuery({ commit, dispatch }, values) {
        commit(types.SET_QUERY, values);

        return dispatch('fetchAll');
    },

    /**
     * Clear query from store.
     *
     * @param commit
     * @returns {Promise<void>}
     */
    async clearQuery({ commit }) {
        await commit(types.CLEAR_QUERY);
    }
};

const mutations = {
    [types.SET_PRODUCTS](state, { products, filter, query }) {
        state.products = products;
        state.filter = filter;
        state.query = { ...state.query, ...query };
        state.priceChanged = false;
    },
    [types.SET_QUERY](state, values) {
        if (values.prices && (values.prices.min || values.prices.max)) {
            state.priceChanged = true;
        }

        if (values.resetPrice) {
            values.prices = {
                min: null,
                max: null
            }
        }

        state.query = { ...state.query, ...values };
    },
    [types.CLEAR_QUERY](state) {
        state.query = {
            attributes: [],
            brands: [],
            categories: [],
            category_slug: null,
            colors: [],
            sizes: [],
            limit: null,
            page: 1,
            prices: {
                min: null,
                max: null
            },
            type: null,
            sortOrder: null,
            search: null
        };
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
