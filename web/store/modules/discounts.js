import isEmpty from 'lodash/isEmpty';
import api from '@/api';
import * as types from '../mutation-types';

const state = {
    all: [],
    current: {}
};

const getters = {
    all: state => {
        return [{ id: null, title: 'Без знижки' }, ...state.all];
    },
    current: (state, getters) => {
        let { current } = state;

        if (isEmpty(current)) {
            current = getters.all[0];
        }

        return current;
    }
};

const actions = {

    /**
     * Fetch all discounts from api.
     *
     * @param commit
     * @returns {Promise.<void>}
     */
    async fetchAll({ commit }) {
        const { data } = await api.discounts.findAll(this);

        commit(types.SET_DISCOUNTS, data);
    },

    /**
     * Set current discount.
     *
     * @param commit
     * @param discount
     */
    setCurrent({ commit }, discount) {
        commit(types.SET_CURRENT_DISCOUNT, discount);
    },

    /**
     * Set current discount by given id.
     *
     * @param commit
     * @param discountId
     */
    setCurrentById({ commit }, discountId) {
        commit(types.SET_CURRENT_DISCOUNT_BY_ID, discountId);
    },

    /**
     * Unset current discount.
     *
     * @param commit
     */
    unsetCurrent({ commit }) {
        commit(types.SET_CURRENT_DISCOUNT, {});
    }
};

const mutations = {
    [types.SET_DISCOUNTS](state, discounts) {
        state.all = discounts;
    },
    [types.SET_CURRENT_DISCOUNT](state, discount) {
        state.current = discount;
    },
    [types.SET_CURRENT_DISCOUNT_BY_ID](state, discountId) {
        state.current = state.all.filter(item => item.id == discountId)[0];
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
