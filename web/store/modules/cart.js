import _ from 'lodash';
import api from '@/api';
import types from '../mutation-types';

const state = {
    products: [],
    discount: null,
    errors: {}
};

const getters = {
    products: state => state.products,
    quantity: state => state.products.length,
    discount: state => state.discount,
    price: (state, getters, rootState, rootGetters) => {
        let sum = 0;
        const currencySymbol = rootGetters['currencies/current'].symbol;

        state.products.forEach(product => {
            product.sizes.items.forEach(size => {
                if (size.selected_quantity > 0) {
                    sum += (product.sale_price.value * size.selected_quantity);
                }
            });
        });

        return {
            value: sum,
            symbol: currencySymbol
        };
    },
    priceWithDiscount: (state, getters) => {
        const price = { ...getters.price };
        price.value -= price.value * getters.discount / 100;
        price.value = _.round(price.value);

        return price;
    },
    isEmpty: state => !state.products.length
};

const actions = {

    /**
     * Add multiple products to cart.
     *
     * @param commit
     * @param products
     */
    addProducts({ commit }, products) {
        products.forEach(item => {
            commit(types.ADD_PRODUCT, item);
        });

        this.$toast.success('Товар доданий в корзину');
    },

    /**
     * Add product to cart.
     *
     * @param commit
     * @param product
     */
    addProduct({ commit }, product) {
        commit(types.ADD_PRODUCT, product);

        this.$toast.success('Товар доданий в корзину');
    },

    /**
     * Remove product from cart by given ID.
     *
     * @param commit
     * @param id
     */
    removeProduct({ commit }, id) {
        commit(types.REMOVE_PRODUCT, id);
    },

    /**
     * Update product size quantity.
     *
     * @param commit
     * @param params
     */
    updateSizeQuantity({ commit }, params) {
        commit(types.UPDATE_PRODUCT_SIZE_QUANTITY, params);
    },

    /**
     * Remove all products from cart.
     *
     * @param commit
     */
    clearCart({ commit }) {
        commit(types.CLEAR_CART);
    },

    /**
     * Set discount.
     *
     * @param commit
     * @param value
     */
    setDiscount({ commit }, value) {
        commit(types.SET_DISCOUNT, value);
    },

    /**
     * Fetch products from api.
     *
     * @param commit
     * @param state
     * @param localState
     * @param rootGetters
     * @returns {Promise<void>}
     */
    async fetchProducts({ commit, state, localState, rootGetters }) {
        let products = state.products;

        if (!products.length) {
            const cart = JSON.parse(localStorage.getItem('cart'));

            if (cart && cart.products) {
                products = cart.products;
            }
        }

        if (products.length) {
            commit(types.SET_PRODUCTS, products);

            const id = _.map(products, 'id').join(',');

            const params = {
                id,
                currency_id: rootGetters['currencies/current'].id,
                discount_id: rootGetters['discounts/current'].id
            };

            const { data } = await api.products.findAll(this, params);

            commit(types.UPDATE_PRODUCTS_PRICE, data.products);
        }
    },

    /**
     * Set errors.
     *
     * @param commit
     * @param data
     */
    setErrors({ commit }, data) {
        commit(types.SET_ERRORS, data);
    },

    /**
     * Clear errors.
     *
     * @param commit
     */
    clearErrors({ commit }) {
        commit(types.SET_ERRORS, {});
    }
};

const mutations = {

    /**
     * Add product to cart.
     *
     * @param state
     * @param product
     */
    [types.ADD_PRODUCT](state, product) {

        // Transform product.
        product = {
            ...product,
            sale_price: this._vm.$nuxt.$fetchDefaultCurrency(product.sale_price)
        };

        // Find product in state "products".
        let productIndex = _.findIndex(state.products, { id: product.id });
        if (productIndex === -1) {
            productIndex = state.products.length;
        }

        // Push/Replace product to state "products",
        state.products.splice(productIndex, 1, product);

        // Update localStorage.
        const cart = JSON.parse(localStorage.getItem('cart')) || { products: [] };
        cart.products = state.products;
        localStorage.setItem('cart', JSON.stringify(cart));
    },

    /**
     * Remove product from store and localStorage.
     *
     * @param state
     * @param id
     */
    [types.REMOVE_PRODUCT](state, id) {
        const productIndex = _.findIndex(state.products, { id });

        // Remove product from store.
        state.products.splice(productIndex, 1);

        // Update localStorage.
        const cart = JSON.parse(localStorage.getItem('cart')) || { products: [] };
        cart.products = state.products;
        localStorage.setItem('cart', JSON.stringify(cart));
    },

    /**
     * Update product size in store and localStorage.
     *
     * @param state
     * @param productId
     * @param sizeId
     * @param quantity
     */
    [types.UPDATE_PRODUCT_SIZE_QUANTITY](state, { productId, sizeId, quantity }) {

        // Find product.
        const productIndex = _.findIndex(state.products, { id: productId });
        const product = state.products[productIndex];

        // Get sizes.
        const sizes = { ...product.sizes };
        const sizeIndex = _.findIndex(sizes.items, { id: sizeId });

        // Update size quantity.
        sizes.items[sizeIndex].selected_quantity = quantity;

        // Update product sizes.
        product.sizes = sizes;

        // Update localStorage.
        const cart = JSON.parse(localStorage.getItem('cart')) || { products: [] };
        cart.products[productIndex].sizes.items[sizeIndex].selected_quantity = quantity;
        localStorage.setItem('cart', JSON.stringify(cart));
    },

    /**
     * Clear "products" state and localStorage.
     *
     * @param state
     */
    [types.CLEAR_CART](state) {
        state.products = [];
        state.discount = null;
        localStorage.removeItem('cart');
    },
    [types.SET_PRODUCTS](state, products) {
        state.products = products;
    },
    [types.SET_DISCOUNT](state, value) {
        state.discount = value;
    },
    [types.UPDATE_PRODUCTS_PRICE](state, products) {
        state.products = state.products.map((item) => {
            const product = _.find(products, { id: item.id });
            const price = _.find(product.sale_price, { is_default: true });

            item.sale_price = price;

            return item;
        });

        localStorage.setItem('cart', JSON.stringify({
            products: state.products
        }));
    },
    [types.SET_ERRORS](state, data) {
        state.errors = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
