import _ from 'lodash';
import api from '@/api';
import types from '../mutation-types';

const LIMIT = 100;

const state = {
    products: [],
    types: [
        { key: 'novelty', value: 'Новинки' },
        { key: 'sale', value: 'Sale' },
        { key: 'max_sale', value: 'MaxSale' },
        { key: 'top_sale', value: 'Лідери продаж' }
    ],
    currentType: null,
    categories: [],
    currentCategory: null
};

const getters = {
    types(state) {
        return state.types;
    },
    currentType(state) {
        return state.currentType || state.types[0];
    },
    categories(state) {
        return state.categories;
    },
    currentCategory(state) {
        return state.currentCategory;
    },
    query(state, getters, rootState, rootGetters) {
        return {
            type: getters.currentType.key,
            currency_id: rootGetters['currencies/current'].id,
            discount_id: rootGetters['discounts/current'].id,
            limit: LIMIT
        };
    },
    products(state, getters) {
        const products = state.products.filter(item => {
            return _.find(item.categories, { id: getters.currentCategory.id });
        });

        return products;
    }
};

const actions = {

    /**
     * Find all products.
     *
     * @param commit
     * @param getters
     * @returns {Promise<void>}
     */
    async fetchAll({ commit, getters }) {
        const response = await api.products.findAll(this, getters.query);

        let result = response.data.products;

        if (result.data) {
            result = result.data
        }

        commit(types.SET_FEATURE_PRODUCTS, result);
    },

    /**
     * Set current type.
     * Re-fetch products from API.
     *
     * @param commit
     * @param dispatch
     * @param item
     * @returns {Promise<void>}
     */
    async setType({ commit, dispatch }, item) {
        commit(types.SET_TYPE, item);

        return dispatch('fetchAll');
    },

    /**
     * Set current category.
     *
     * @param commit
     * @param item
     */
    setCategory({ commit }, item) {
        commit(types.SET_CATEGORY, item);
    }
};

const mutations = {
    [types.SET_FEATURE_PRODUCTS](state, products) {
        state.products = products;

        let categories = [];
        products.forEach(item => {
            const filteredCategories = item.categories.filter(filteredItem => filteredItem.is_default);
            categories = [ ...categories, ...filteredCategories ];
        });

        state.categories = _.uniqBy(categories, item => item.id);
        state.currentCategory = state.categories[0];
    },
    [types.SET_TYPE](state, item) {
        state.currentType = item;
    },
    [types.SET_CATEGORY](state, item) {
        state.currentCategory = item;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
