import _ from 'lodash';
import api from '@/api';
import * as types from '../mutation-types';

const state = {
    types: [
        { key: 'uniform', value: 'Футбольна форма' },
        { key: 'equipment', value: 'Футбольна екіпіровка' }
    ],
    products: {},
    selectedProducts: {},
    productDiscount: 10
};

const getters = {
    types: state => state.types,
    products: state => state.products,
    selectedProducts: state => Object.values(state.selectedProducts),
    productDiscount: state => state.productDiscount
};

const actions = {

    /**
     * Fetch uniforms from api.
     *
     * @param commit
     * @param getters
     * @param type
     * @returns {Promise.<void>}
     */
    async fetchAll({ commit, getters: localGetters, rootGetters }, type) {

        // Collect params
        const params = {
            discount_id: rootGetters['discounts/current'].id,
            uniform_type: type
        };

        let { data } = await api.products.findAll(this, params);

        commit(types.SET_PRODUCTS, data.products);
    },

    /**
     * Set selected product.
     *
     * @param commit
     * @param data
     */
    setSelectedProduct({ commit }, data) {
        commit(types.SET_SELECTED_PRODUCT, data);
    }
};

const mutations = {

    /**
     * Transform products. Group by "position", "category", "model".
     *
     * @param state
     * @param data
     */
    [types.SET_PRODUCTS](state, data) {
        let result = [];

        data.forEach((item) => {
            const position = item.uniform.position;
            const category = item.uniform.category.name;
            const model = item.uniform.model;

            // Group by position
            let resultItem = _.find(result, { position });
            if (!resultItem) {
                let order;
                switch (position) {
                    case 'top':
                        order = 1;
                        break;

                    case 'center':
                        order = 2;
                        break;

                    case 'bottom':
                        order = 3;
                        break
                }
                result.push({
                    position,
                    items: [],
                    order
                });
                resultItem = _.find(result, { position });
            }

            result = _.sortBy(result, 'order');

            // Group by category
            let resultCategoryItem = _.find(resultItem.items, { category });
            if (!resultCategoryItem) {
                resultItem.items.push({
                    category,
                    items: []
                });
                resultCategoryItem = _.find(resultItem.items, { category })
            }

            // Group by model
            let resultModelItem = _.find(resultCategoryItem.items, { model });
            if (!resultModelItem) {
                resultCategoryItem.items.push({
                    model,
                    items: []
                });
                resultModelItem = _.find(resultCategoryItem.items, { model })
            }

            resultModelItem.items.push(item);
        });

        state.products = result;
    },

    /**
     * Set selected product.
     *
     * @param state
     * @param key
     * @param value
     */
    [types.SET_SELECTED_PRODUCT](state, { key, value }) {
        state.selectedProducts = { ...state.selectedProducts, [key]: value };
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
