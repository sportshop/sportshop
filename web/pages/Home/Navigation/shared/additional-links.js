import saleImg from './sale.png';
import maxSaleImg from './max-sale.png';
import uniformBuilderImg from './uniform-builder.jpg';

export default [
    {
        route: { name: 'products.index', query: { type: 'sale' } },
        image: saleImg,
        icon: 'icon-sport_shop_icons-86',
        content: `
            <span class="sale-type">Sale</span>
            <div class="sale-percent">50%</div>
            <span class="sale-title">Знижки</span>
        `,
        sort_order: 1
    },
    {
        route: { name: 'products.index', query: { type: 'max_sale' } },
        image: maxSaleImg,
        icon: 'icon-sport_shop_icons-84',
        content: `
            <span class="sale-type">MaxSale</span>
            <div class="sale-percent">70%</div>
            <span class="sale-title">Знижки</span>
        `,
        sort_order: 3
    },
    {
        route: { name: 'uniforms.index' },
        image: uniformBuilderImg,
        text: 'Конструктор форм',
        sort_order: 5
    }
];