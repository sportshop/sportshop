export default ({ $axios, req }) => {
    const host = process.server ? req.headers.host : window.location.host;

    $axios.setHeader('store-url', host);
};