import Vue from 'vue';
import _ from 'lodash';

const Currency = {
    install(Vue) {
        Vue.prototype.$fetchDefaultCurrency = (items) => {
            return { ..._.find(items, { is_default: true }) };
        }
    }
};

Vue.use(Currency);
