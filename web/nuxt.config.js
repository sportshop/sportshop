require('dotenv').config();

module.exports = {

    /*
     ** Headers of the page
     */
    head: {
        titleTemplate: (titleChunk) => {
            return titleChunk ? titleChunk : 'Sportshop';
        },
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'Sportshop' }
        ]
    },

    /*
     ** Custom css
     */
    css: [
        '@/assets/scss/style.scss',
        'swiper/dist/css/swiper.css'
    ],

    /*
     ** Modules
     */
    modules: [
        '@nuxtjs/auth',
        '@nuxtjs/axios',
        '@nuxtjs/font-awesome',
        '@nuxtjs/router',
        '@nuxtjs/toast',
        ['bootstrap-vue/nuxt', { css: false }],
        'nuxt-material-design-icons',
        'nuxt-client-init-module'
    ],

    /*
     ** Plugins
     */
    plugins: [
        '@/plugins/currency',
        '@/plugins/axios',
        { src: '@plugins/modernizr-plugin.js', ssr: false },
        { src: '@/plugins/swiper.js', ssr: false }
    ],

    /*
     ** Toast component
     */
    toast: {
        position: 'bottom-left',
        duration : 3000,
        icon: 'check'
    },

    /*
     ** Custom preloader.
     */
    loading: '@/components/Preloader/Preloader.vue',

    /*
     ** Set up transition
     */
    transition: {
        name: 'page',
        mode: 'out-in'
    },

    /*
     ** Build configuration
     */
    build: {
        /*
         ** Run ESLint on save
         */
        extend(config, { isDev, isClient }) {
            if (isDev && isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                });
            }
        }
    },

    /*
     ** Router
     */
    router: {
        middleware: []
    },

    /*
     ** Auth module
     */
    auth: {
        redirect: {
            login: '/',
            logout: '/',
            home: false,
            callback: '/'
        },
        strategies: {
            local: {
                endpoints: {
                    login: { url: '/auth/login', method: 'post', propertyName: 'access_token' },
                    logout: { url: '/auth/logout', method: 'post' },
                    user: { url: '/profile', method: 'get', propertyName: 'user' }
                }
            },
            google: {
                client_id: process.env.GOOGLE_CLIENT_ID,
                response_type: 'code',
                redirect_uri: process.env.GOOGLE_REDIRECT_URI
            },
            facebook: {
                client_id: process.env.FACEBOOK_CLIENT_ID,
                response_type: 'code',
                redirect_uri: process.env.FACEBOOK_REDIRECT_URI
            }
        },
        resetOnError: true
    },

    /*
     ** Server middleware
     */
    serverMiddleware: []
};

