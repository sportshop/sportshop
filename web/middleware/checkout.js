export default ({ store, route, redirect }) => {
    if (process.server) {
        return;
    }

    if (store.getters['cart/isEmpty'] && !route.query.success) {
        redirect('/');
    } else if (store.state.auth.loggedIn && store.state.auth.user.role_name != 'member') {
        redirect('/');
    }
};
