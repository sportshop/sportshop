import Vue from 'vue';
import Router from 'vue-router';

// General
import Home from '@/pages/Home/Home';
import Page from '@/pages/Page/Page';
import Reviews from '@/pages/Reviews/Reviews';
import Sizes from '@/pages/Sizes/Sizes';

// Profile
import Profile from '@/pages/Profile/Profile';
import ProfileData from '@/pages/Profile/ProfileData/ProfileData';
import ProfileForm from '@/pages/Profile/ProfileData/ProfileForm/ProfileForm';
import ProfilePasswordForm from '@/pages/Profile/ProfileData/PasswordForm/PasswordForm';
import ProfileOrders from '@/pages/Profile/Orders/Orders';

// Products
import ProductList from '@/pages/ProductList/ProductList';
import ProductDetail from '@/pages/ProductDetail/ProductDetail';

// Cart
import Cart from '@/pages/Cart/Cart';

// Checkout
import Checkout from '@/pages/Checkout/Checkout';

// Uniform builder
import Uniforms from '@/pages/Uniforms/Uniforms';

Vue.use(Router);

const createRouter = () => new Router({
    mode: 'history',
    routes: [

        // General
        { path: '/', component: Home },
        { path: '/sizes', name: 'sizes.index', component: Sizes },
        { path: '/reviews', name: 'reviews.index', component: Reviews },

        // Profile
        { path: '/profile', component: Profile, children: [
            { path: '', name: 'profile.index', component: ProfileData },
            { path: 'edit', name: 'profile.edit', component: ProfileForm },
            { path: 'password/edit', name: 'profile.password.edit', component: ProfilePasswordForm },
            { path: 'orders', name: 'profile.orders', component: ProfileOrders }
        ] },

        // Products
        { path: '/products/:category_slug?', name: 'products.index', component: ProductList },
        { path: '/products/:category_slug/:slug', name: 'products.view', component: ProductDetail },

        // Cart
        { path: '/cart', name: 'cart.index', component: Cart },

        // Uniform builder
        { path: '/uniforms', name: 'uniforms.index', component: Uniforms },

        // Checkout
        { path: '/checkout', name: 'checkout.index', component: Checkout },

        // Pages
        { path: '/:slug', name: 'pages.view', component: Page }
    ],
    scrollBehavior(to, from, savedPosition) {
        return { x: 0, y: 0 };
    }
});

export { createRouter };
